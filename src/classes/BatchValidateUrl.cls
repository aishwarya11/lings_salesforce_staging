Global class BatchValidateUrl implements Database.Batchable<Product2>, Database.AllowsCallouts {
    
    //Start batchable
    global Iterable<Product2> start(Database.BatchableContext BC){ 
        System.debug('BatchValidateUrl START');
        List<Product2> query = [SELECT Id, Name, DisplayUrl, IsDeleted__c,DB_Fotichaeschtli__c, Validate_URL__c FROM Product2 
                                WHERE DisplayUrl LIKE 'https://www.fotichaestli.ch%' and DB_Fotichaeschtli__c != NULL 
                                and IsDeleted__c = FALSE AND Validate_URL__c = False];
        return query;
    }
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Product2> scope) { 
        System.debug('BatchValidateUrl EXECUTE');
        
        List<Product2> brokenLinkId = new List<Product2>();
        Integer count = 0;
        String responseMessage = '';
        for(Product2 pro: scope) {    
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(pro.DisplayUrl);
            request.setMethod('POST');
            request.setTimeout(20000);
            HttpResponse response = http.send(request);
            if (response.getStatusCode() == 200) {
                continue;
            }else {
                count = count + 1;
                pro.Validate_URL__c = TRUE;
                responseMessage = String.valueOf(response);
                if(responseMessage.length() > 255) {
                   pro.Comment__c = responseMessage.substring(0, 255); 
                } else {
                    pro.Comment__c = responseMessage;
                }
                
                brokenLinkId.add(pro);
            }        
        }
        
        if(brokenLinkId.size() > 0) {
            try{
                update brokenLinkId;
            } catch(Exception bl) {
                Utility.sendException(bl,'UPDATE FAILED IN THE URL VALIDATION');
            }
        }
        System.debug('Total Broken image List: '+count);
        System.debug(brokenLinkId);        
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC){ 
    	System.debug('BatchValidateUrl FINISH');
    }    
}