global class LightningLoginFormController {
	public static String userNameSuffix = System.Label.UserNameSuffix;
    public LightningLoginFormController() {
        
    }

    @AuraEnabled
    public static String login(String username, String password, String startUrl) {
        try{
            //Added the Username suffix by default while logging in --18OCT2019.
            username = username + userNameSuffix;
            ApexPages.PageReference lgn = Site.login(username, password, startUrl);
//            Utility.sendInfo('Password reset', password, username);
            aura.redirect(lgn);
            return null;
        }
        catch (Exception ex) {
			utility.sendException(ex, 'Error in Mobile login page '+ex.getStackTraceString());
            return 'Fehlerhafte Zugangsdaten.';           
        }
    }
    
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'0DB1t000000Go0IGAS'); //added network id --20OCT2019
        return authConfig;
    }

    @AuraEnabled
    global static String setExperienceId(String expId) {
        
        // Return null if there is no error, else it will return the error message 
        try {
            if (expId != null) {
                Site.setExperienceId(expId);
            }
            return null; 
        } catch (Exception ex) {
            return ex.getMessage();            
        }
    }   
}