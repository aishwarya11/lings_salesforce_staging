public class UpdateFlag {
    //Update Approve and Publish flag values in Lings_Object__c--This method will be Used in the ApprovedPublish Lightning Component which will only update the Flags.
    @AuraEnabled 
    public static void approved(Object__c inpobj, String objId){
        List <Object__c> updateObjectList = New List<Object__c>();
        List <Product2> updateProductList = New List<Product2>();
        for(Object__c obj:[SELECT id, Approved__c, Product__c, Customer__c, Product__r.Source__c, 
                           Product__r.Insured_Value__c, Product__r.Flatrate_Premium__c, 
                           Product__r.FlatRate_Savings__c, Product__r.Ondemand_Premium__c, 
                           Insured_Value__c, Premium_OnDemand__c, Premium_FlatRate__c FROM Object__c 
                           WHERE id = :objId LIMIT 1]){
                               obj.Approved__c = True;
                               
                               Product2 objProd = new Product2(id=obj.Product__c);
                               objProd.Searchable__c = true;
                               objProd.Status__c = 'Closed';
                               if(obj.Product__r.Source__c == 'DB Bike'){
                                   objProd.Flatrate_Premium__c = NULL;
                                   objProd.OnDemand_Premium__c = NULL;
                                   objProd.Insured_Value__c = NULL;
                                   objProd.Price__c = NULL;
                                   objProd.Old_Price__c = NULL;
                                   objProd.Price_Customer__c = NULL;
                               } else if (obj.Insured_Value__c != objProd.Insured_Value__c) {
                                   //LM-1171--Updating the Object Price with the Product --03MAR2020
                                   obj.Insured_Value__c = obj.Product__r.Insured_Value__c;
                                   obj.Premium_OnDemand__c = obj.Product__r.OnDemand_Premium__c;
                                   obj.Premium_FlatRate__c = obj.Product__r.Flatrate_Premium__c;
                                   obj.FlatRate_Savings__c = obj.Product__r.FlatRate_Savings__c;
                               }
                               updateProductList.add(objProd);                                                              
                               updateObjectList.add(obj); 
                           }
        Update updateObjectList;
        update updateProductList;
    }
    
    //LM-459 
    //24June2019--This SendEmail method is used in the ApproveAndSendEmail lightning component which will update the flag and will send email to the Customer.
    
    @AuraEnabled 
    public static void sendEmail(Object__c inpobj, String objId){
        try{
            UpdateFlag.approved(inpobj, objId);
            // Get Contact Id.
            String getId = 'SELECT id, Name, Customer__c, Approved__c, Is_Deleted__c, Product__r.Searchable__c, Customer__r.Email FROM Object__c WHERE ID = \'' + objId + '\'';
            Object__c ObjValue = Database.query(getId);
            
            if(ObjValue.Approved__c == true && objValue.Product__r.Searchable__c == true && objValue.Is_Deleted__c == false){
                String contactId = ObjValue.Customer__c;
                String Email = objValue.Customer__r.Email;
                
                // Send Email
                List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
                EmailTemplate template = [Select id from EmailTemplate where name=:'Approved Draft-Object' limit 1][0];   
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setUseSignature(false);
                mail.setToAddresses(new String[] { Email });
                mail.setTemplateId(template.Id);
                OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                mail.setOrgWideEmailAddressId(owea.get(0).Id); 
                mail.setwhatid(ObjValue.Id);
                mail.setTargetObjectId(contactId);
                mail.setSaveAsActivity(true);
                
                mailList.add(mail);   
                
                Messaging.sendEmail(mailList); 
            }
        }catch(Exception ps){
            utility.sendException(ps,'APPROVE AND PUBLISH FAILED, MAIL IS NOT SEND TO THE CUSTOMER');
        }
    }       
}