public class searchProductController {
    @AuraEnabled
    //method used to search the product using searchkeword
    public static List<Product2> fetchProduct(String searchKeyword) {
        System.debug('Input passes from the helper - ' + searchKeyword);
       String queryString = 'Select Name,ProductCode,Description,Family,Insured_Value__c from Product2 where name like \'' + searchKeyword + '%\'';
        List<Product2> returnList = new List<Product2>();
        List<Product2> productList = Database.query(queryString);
        for (Product2 lstOfProduct : productList) {
              returnList.add(lstOfProduct);
        }
        return returnList;
	}
}