@isTest
public class BatchInviteLingsCustomerTest {
    
    testmethod public static void testBatchInviteLingsCustomerTest() { 
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        con.Email_Verified__c = True;
        con.Email = 'aishwarya@rangerinc.com';
        con.Register_Confirmed__c = True;
        con.SignUp__c = True;
        insert con;     
        DateTime threeDaysBack = System.now().addDays(-3);
        Test.setCreatedDate(con.Id, threeDaysBack);        
        
        Test.startTest();
        
        BatchInviteLingsCustomer batch = new BatchInviteLingsCustomer();
        DataBase.executeBatch(batch);
        
        Test.stopTest();        
    }    

}