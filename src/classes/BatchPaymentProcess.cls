//If invoice status is not success, then the payment object record creation will be undertaken by this batch class 
//Batch Should run after BatchInvoiceProcess 
global class BatchPaymentProcess implements Database.Batchable<Invoice__c>, Database.AllowsCallouts, Database.Stateful {
    Public List<Payment__c> successPaymentCreationList = new List<Payment__c>();
    Public List<Voucher_Contact__c> successVCUpdateList = new List<Voucher_Contact__c>();
    Public List<Voucher_Contact__c> failedVCUpdateList = new List<Voucher_Contact__c>();
    Public List<Invoice__c> successInvoiceUpdateList = new List<Invoice__c>();
    Public List<Invoice__c> failedInvoiceUpdateList = new List<Invoice__c>();
    Public List<Policy__c> successPolicyUpdateList = new List<Policy__c>();
    Public List<Policy__c> failedPolicyUpdateList = new List<Policy__c>();

    //Start Batchable
    global Iterable<Invoice__c> start(Database.BatchableContext BC){
        System.debug('BatchPaymentProcess START');
        List<Invoice__c> query = [SELECT Id, Name, Amount__c, Date__c, Contact__c, Transaction_Id__c,OKKPolicyAmount__c,RVGPolicyAmount__c,
                                  Voucher_Deductions__c, Card_Payment__c, Contact__r.Email, Transaction_Amount__c,
                                  Contact__r.VIU_Balance__c, Contact__r.Balance_Pending__c, Contact__r.Voucher_In_Use__c, Contact__r.Payment_Alias_ID__c 
                                  FROM Invoice__c WHERE Status__c = 'New' ORDER BY Date__c];
        return query;
    }

    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Invoice__c> scope) {
        System.debug('BatchPaymentProcess EXECUTE');
        List<Voucher_Contact__c> vcList = new List<Voucher_Contact__c>();
        List<Contact> conList = new List<Contact>();
        List<Voucher_Contact__c> successVCList = new List<Voucher_Contact__c>();
        List<Contact> successConList = new List<Contact>();
        List<String> respList = new List<String>();
        Map<String, Invoice__c> invMap = new Map<String, Invoice__c>();
        Map<String, String> getStatus = new Map<String, String>();      //LM-719--To get the Payment status -- 10OCT2019
        Set<Id> successInvoiceIds = new Set<Id>();
        
        for(Invoice__c inv : scope) {            
            //Payment Process            
            Decimal voucherAmtToDeduct = 0.0;
            Decimal availableVoucherBalance = 0.0;
            Decimal remainingVoucherBalance = 0.0;
            if(inv.RVGPolicyAmount__c == null){
                inv.RVGPolicyAmount__c = 0.0;
            }
            if(inv.OKKPolicyAmount__c == null){
                inv.OKKPolicyAmount__c = 0.0;
            }
            
            // MR - 08JUN19 - Added to avoid NULL Pointer Exception
            if(inv.Contact__r.Voucher_In_Use__c != NULL) {
                availableVoucherBalance += inv.Contact__r.VIU_Balance__c;
            }
            
            if(inv.Contact__r.Balance_Pending__c != NULL) {
                availableVoucherBalance += inv.Contact__r.Balance_Pending__c;
            }
            
            
            Decimal invAmtWOOKKANDRVG = 0.0; // to store the invoice amount without OKKPolicy and RVGPolicy amount
            
            // Reva -- LM-1116 and LM-1306 - To do the calculation separately for the OKKPolicy and RVGPolicy amount
            if(inv.OKKPolicyAmount__c > 0.0 || inv.RVGPolicyAmount__c > 0.0){
                // to store the invAmt with OKK policy and RVG policy amount
                invAmtWOOKKANDRVG = inv.Amount__c - (inv.RVGPolicyAmount__c + inv.OKKPolicyAmount__c);
            }
            
            if(inv.OKKPolicyAmount__c > 0.0 || inv.RVGPolicyAmount__c > 0.0){
                Decimal tempInvAmt = 0.0;
                // if available voucher balance is enough to pay invAmtWOOKKANDRVG
                if(invAmtWOOKKANDRVG <= availableVoucherBalance){
                	voucherAmtToDeduct = invAmtWOOKKANDRVG;
                    tempInvAmt = (inv.RVGPolicyAmount__c + inv.OKKPolicyAmount__c);
                } else { // if voucher balance is not enough to pay invAmtWOOKKANDRVG
                	voucherAmtToDeduct = invAmtWOOKKANDRVG - availableVoucherBalance;
                    tempInvAmt = voucherAmtToDeduct + (inv.RVGPolicyAmount__c + inv.OKKPolicyAmount__c);
                }
                // Charge Credit Card for Difference
                if(inv.Contact__r.Payment_Alias_ID__c != null) {
                    // Need to do the external payment for remaining amount to be paid
                    respList.add(Utility.processPayment(inv.Id, 
                                                        tempInvAmt.setscale(2), 
                                                        inv.Name, 
                                                        inv.Contact__r.Payment_Alias_ID__c));
                } else {
                    inv.Status__c = 'MissingCreditCard';
                }
            } else if(inv.Amount__c <= availableVoucherBalance && inv.OKKPolicyAmount__c <= 0.0
                     && inv.RVGPolicyAmount__c <= 0.0) {// Step 03: Process the Payment
                // No need for external payment
                voucherAmtToDeduct = inv.Amount__c;
                inv.Status__c = 'Success';
                successInvoiceIds.add(inv.Id);
            } else if(inv.Amount__c > availableVoucherBalance && inv.OKKPolicyAmount__c <= 0.0
                     && inv.RVGPolicyAmount__c <= 0.0){
                // Charge Credit Card for Difference
                if(inv.Contact__r.Payment_Alias_ID__c != null) {
                    // 06JUN19 - Corrected amount to deduct from Credit Card
                    respList.add(Utility.processPayment(inv.Id, 
                                                        (inv.Amount__c - availableVoucherBalance).setscale(2), 
                                                        inv.Name, 
                                                        inv.Contact__r.Payment_Alias_ID__c));
                } else {
                    inv.Status__c = 'MissingCreditCard';
                }
                
                voucherAmtToDeduct = availableVoucherBalance;
            }
            
            remainingVoucherBalance = availableVoucherBalance - voucherAmtToDeduct;
            
            // Clear the Balance from Voucher Contact
            if(availableVoucherBalance > 0) {
                Contact cont = new Contact(id=inv.Contact__c);  
                // MR - 06JUN19 - Condition to check the Balance Pending
                if(inv.Contact__r.Balance_Pending__c != NULL && inv.Contact__r.Balance_Pending__c >= voucherAmtToDeduct) {
                    cont.Balance_Pending__c = inv.Contact__r.Balance_Pending__c - voucherAmtToDeduct;
                    conList.add(cont);
                    getStatus.put(inv.Contact__c, 'Success'); //LM-1113--added this map to update the voucher contact when only the voucher deduction is done without CC--10FEB2020
                } else if(inv.Contact__r.Voucher_In_Use__c != null) {
					if(remainingVoucherBalance <= 0.0) {
                        cont.Balance_Pending__c = 0.0;
                        cont.Voucher_In_Use__c = null;
                        conList.add(cont);
                        
                        // Clear out the Balance in Voucher Contact
                        Voucher_Contact__c viu = new Voucher_Contact__c(id=inv.Contact__r.Voucher_In_Use__c);
                        viu.Balance__c = 0;
                        viu.External_ID__c = inv.Contact__c; 
                        vcList.add(viu); 
                        
                        getStatus.put(inv.Contact__c, 'Success'); //LM-1113--added this map to update the voucher contact when only the voucher deduction is done without CC--10FEB2020
                    } else if(remainingVoucherBalance < inv.Contact__r.VIU_Balance__c) {
                        Voucher_Contact__c viu = new Voucher_Contact__c(id=inv.Contact__r.Voucher_In_Use__c);
                        viu.Balance__c = remainingVoucherBalance;
                        //Changed from Contact to external id since contact is not writeable --06NOV2019.
                        viu.External_ID__c = inv.Contact__c;  //LM-719 --Adding contact Id in the list -- 10OCT2019
                        vcList.add(viu); 
                        // Balance Pending needs to be Cleared out
                        cont.Balance_Pending__c = 0.0;
                        conList.add(cont);
                        
                        getStatus.put(inv.Contact__c, 'Success'); //LM-1113--added this map to update the voucher contact when only the voucher deduction is done without CC--10FEB2020
                    }  
                }
                
                inv.Voucher_Deductions__c = voucherAmtToDeduct;
            }
            
            invMap.put(inv.id, inv);             
        }
        
        // Process the Callout Responses
        for(String resp: respList) {
            if(resp != '') {
                resp = resp.replaceAll('"Transaction"', '"txn"');              

                UIPaymentWrapper.SPPaymentResponse apiResp = (UIPaymentWrapper.SPPaymentResponse) JSON.deserialize(resp, UIPaymentWrapper.SPPaymentResponse.class);                               
                Invoice__c invObj = invMap.get(apiResp.responseHeader.requestId);

                // Create New Payment Record
                Payment__c paymentObj = new Payment__c();
                paymentObj.Contact__c = invObj.Contact__c;
                paymentObj.Date__c = System.today();
                // Check for the txn - on positive response
                if(apiResp.txn != null) {
                    paymentObj.Status__c = String.valueOf(apiResp.txn.status);
                    paymentObj.Paid__c = Decimal.valueOf(apiResp.txn.amount.value)/100;
                    invObj.Status__c = 'Success';
                    successInvoiceIds.add(invObj.Id);

                    //13MAY2019 --Transaction Capture.
                    if(apiResp.txn.Id != '') {
                        //15MAY2019 -- To capture the tnx Id and the tnx amount in Invoice.
                        invObj.Transaction_Id__c = String.valueOf(apiResp.txn.Id);
                        invObj.Transaction_Amount__c = Decimal.valueOf(apiResp.txn.amount.value)/100;
                        invObj.Email__c = invObj.Contact__r.Email;
                        
                        String reqBody = '{ "RequestHeader": { "SpecVersion": "' +System.Label.SaferPayVersion + '", "CustomerId": "' + 
                            Constants.SP_CUSTOMER_ID + '","RequestId": "'+
                            invObj.Id+'123abc'+'", "RetryIndicator": 0 }, "TransactionReference": { "TransactionId": "' + apiResp.txn.Id + '"}}';
                    
                        HttpRequest spReq = new HttpRequest();
                        spReq.setBody(reqBody);
                        spReq.setEndpoint(Constants.SP_Tnx_Capture_EP);
                        spReq.setMethod('POST');
                        spReq.setHeader('Authorization', Constants.SP_BASIC_AUTH);
                        spReq.setHeader('Content-Type', 'application/json');                
                        
                        HttpResponse TxnCaptureresp = new Http().send(spReq);
                        String CaptureResp = TxnCaptureresp.getBody();
                        CaptureResp = CaptureResp.replaceAll('"Date"', '"CaptureDate"'); 
                        UIPaymentWrapper.SPTxnCapture TxnCaptureapiResp = (UIPaymentWrapper.SPTxnCapture) JSON.deserialize(CaptureResp, UIPaymentWrapper.SPTxnCapture.class);            

                        if(TxnCaptureresp.getStatusCode() == 200) {   
                            paymentObj.Status__c = TxnCaptureapiResp.Status;
                            invObj.Status__c = 'Success';
                        } else {
                            paymentObj.Status__c = String.valueOf(TxnCaptureapiResp.ErrorMessage);
                            invObj.Status__c = 'TxnCaptureFailed';
                        }        
                    }        
                } else { // Negative Response
                    paymentObj.Status__c = String.valueOf(apiResp.ErrorMessage);
                    paymentObj.Paid__c = 0.00;
                    invObj.Status__c = 'CalloutFailed';                   
                }
                getStatus.put(paymentObj.Contact__c, invObj.Status__c); //LM-719--To get the Payment status -- 10OCT2019
                paymentObj.API_Response__c = resp;
                paymentObj.Invoice__c = invObj.Id;      // 23APR19 - MR - Missing Invoice Id in the Payment

                try {
                    insert paymentObj;
                    successPaymentCreationList.add(paymentObj);
                    invObj.Card_Payment__c = paymentObj.Id;   
                    invMap.put(invObj.id, invObj);
                } catch(Exception pe) {
                    Utility.sendException(pe, 'Error Creating the Payment Record for ' + paymentObj.Contact__c);
                }
            }
        }
        
        // Update the Contact Balances-17APR2019
        if(conList.size() > 0) {
            //if(!Test.isRunningTest()) {
                try {
                    for(Contact con : conList){
                        if(getStatus.get(con.Id) == 'Success') { //LM-719--To get the Payment status and update record accordingly-- 10OCT2019
                            successConList.add(con);                            
                        }
                    }                  
                    Update successConList;
                } catch(Exception cue) {
                    Utility.sendException(cue, 'Error Updating the Contact Record for ' + JSON.serialize(conList));
                }
            //}
        } 
        
        if(vcList.size() > 0){            
            try{
                for(Voucher_Contact__c vc : vcList) {
                    if(getStatus.get(vc.External_ID__c) == 'Success') { //LM-719--To get the Payment status and update record accordingly-- 10OCT2019
                        successVCList.add(vc);                        
                    }
                }
                Update successVCList;
            } catch(Exception vce) {
                Utility.sendException(vce, 'Error Updating the Voucher Contact Record for ' + JSON.serialize(vcList)); 
            }
        }
        
        // Update the invoice Status
        if(invMap.size() > 0){
            List<Invoice__c> tempInvList = invMap.values();
            List<Database.SaveResult> srUpdate = Database.update(invMap.values());
            Integer invIndex = 0;  
            for(Database.SaveResult sr: srUpdate) {
                Invoice__c inv = tempInvList[invIndex];
                if(sr.isSuccess()) {
                    successInvoiceUpdateList.add(inv);  
                } else {
                    failedInvoiceUpdateList.add(inv);
                }
                
                invIndex++;
            }
        }  
        
        // Renew the Policies with revised Invoice Start Date
        List<Policy__c> polList = new List<Policy__c>();
        for(Policy__c pol: [SELECT Id, Insurance_Type__c, Invoice_Start_Date__c, Active__c, Invoice__r.Date__c, Start_Date__c, End_Date__c   
                            FROM Policy__c 
                            WHERE Invoice__c IN :successInvoiceIds 
                            AND Active__c = true]) {
            // MR - 06JUN19 - Updated to handle all the policy types
            if(pol.Insurance_Type__c.equalsIgnoreCase(Constants.ONDEMAND_STRING)) {
                pol.Invoice_Start_Date__c = pol.Invoice__r.Date__c.addDays(1);
            } else if(pol.Insurance_Type__c.equalsIgnoreCase(Constants.FlatRate_STRING)) {
                Date invoiceEndDate = pol.Invoice__r.Date__c;
                Date insStartDate = pol.Start_Date__c.date();
                Date insEndDate = pol.End_Date__c.date();

                if(pol.Invoice_Start_Date__c > insStartDate && insEndDate > invoiceEndDate && invoiceEndDate.daysBetween(insEndDate) > Utility.numOfDaysForEndDate(invoiceEndDate)) {
                    Integer monthsToReduce = insEndDate.month() - invoiceEndDate.month();
                    while(monthsToReduce > 0) {
                        Date prevMonthDate = insEndDate.addMonths(-1);
                        insEndDate = insEndDate.addDays(-1 * (Utility.numOfDaysForEndDate(prevMonthDate) + 1));
                        monthsToReduce--;
                    }

                    pol.Invoice_Start_Date__c = insEndDate + 1;
                }
            }
            
            // MR - 06JUN19 - Invoice should have the value retained
            //pol.Invoice__c = null;
            polList.add(pol);
        }
        
        if(polList.size() > 0){
            List<Database.SaveResult> srUpdate = Database.update(polList);
            Integer polIndex = 0;  
            for(Database.SaveResult sr: srUpdate) {
                Policy__c pol = polList[polIndex];
                if(sr.isSuccess()) {
                    successPolicyUpdateList.add(pol);  
                } else {
                    failedPolicyUpdateList.add(pol);
                }
                
                polIndex++;
            }
        }  
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchPaymentProcess FINISH');

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String>{'riyaz.mohamed@outlook.com'});
        mail.setSubject('BatchPaymentProcess ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);  
        
        String body = 'BatchPaymentProcess Status on Updating Policy, Invoice, VoucherContact and Creating Payment records<br/>';
        
        // Processing Policy Updation   
        body += '<br/> ' + (successPolicyUpdateList.size() + failedPolicyUpdateList.size()) + ' number of Policy records update processing were completed.'; 
        if(successPolicyUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy updation details <br/>';
            
            for(Policy__c successList: successPolicyUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy updation happened.';
        }         
        if(failedPolicyUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Policy updation details <br/>';
            
            for(Policy__c failedList: failedPolicyUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Policy updation happened.';
        }
        
        // Processing Invoice Updation   
        body += '<br/> ' + (successInvoiceUpdateList.size() + failedInvoiceUpdateList.size()) + ' number of Policy records update processing were completed.'; 
        if(successInvoiceUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Invoice updation details <br/>';
            
            for(Invoice__c successList: successInvoiceUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Invoice updation happened.';
        }         
        if(failedInvoiceUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Invoice updation details <br/>';
            
            for(Invoice__c failedList: failedInvoiceUpdateList) {
                body += '<br/> ' + failedList;
            }
        } else {
            body += '<br/> No Failed Invoice updation happened.';
        }
        
        // Processing VoucherContact Updation   
        body += '<br/> ' + (successVCUpdateList.size() + failedVCUpdateList.size()) + ' number of VoucherContact records update processing were completed.'; 
        if(successVCUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Invoice updation details <br/>';
            
            for(Voucher_contact__c successList: successVCUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No VoucherContact updation happened.';
        }         
        if(failedVCUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed VoucherContact updation details <br/>';
            
            for(Voucher_contact__c failedList: failedVCUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed VoucherContact updation happened.';
        }
        
        // Processing Payment Creation
        body += '<br/> ' + successPaymentCreationList.size() + ' total number of new Payment records Creation processing were completed.'; 
        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);       
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedVCUpdateList.size() > 0 || failedInvoiceUpdateList.size() > 0 || failedPolicyUpdateList.size() > 0) {
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchPaymentProcess';
        ab.Track_message__c = body;
        database.insertImmediate(ab);         
        }
    }
}