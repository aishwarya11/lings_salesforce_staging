@isTest
public class UtilityTest {
    
    List<String> ErrorDetailList = new List<String>(); 
    class MyObject
    {
        Id id;
        String name;
        String Alias;
        String Email;
        String EmailEncodingKey;
        String FirstName;
        String LastName;
        String LanguageLocaleKey;
        String LocaleSidKey;
        String UserName;
        Id UserRoleId;
        String CommunityNickname;
        String ConLastName;
        Id ConOwnerId;
        String ConPassword;
        public SPPaymentResponse paymentResp;
        public SPTxnPaymentResponse txnResp;
    }
    
    public class SPPaymentResponse{
        public SPResponseHeader responseHeader;
        public SPAlias alias;   
        public SPPaymentInfo paymentMeans;
        public String ErrorMessage = 'This will show the Error message';
        public String Behavior = 'Good';
        public String ErrorName = 'To show error'; 
        //ErrorDetailList.add('errorvalue');
        
    }
    
    public class SPTxnPaymentResponse{
        public SPResponseHeader responseHeader;
        public SPTransaction txn;
        public SPPaymentInfo paymentMeans;
        public SPPayer payer;
        public SPRegistrationResult registrationResult;
        public SPLiability liability;        
        public String ErrorMessage = 'This will show the Error message';
        public String Behavior = 'Good';
        public String ErrorName = 'To show error'; 
        //ErrorDetailList.add('errorvalue');
        
    }    
    
    public class SPResponseHeader{
        public String SpecVersion = '2.0';
        public String RequestId = System.Label.TestContact;        
    }
    
    public class SPPaymentInfo {
        public SPBrandInfo brand;
        public String displayText = 'sample text';
        public SPCardInfo card;
    }
    
    public class SPBrandInfo {
        public String PaymentMethod = 'sample payment method';
        public String Name = 'sample payment name';
    }
    public class SPCardInfo {
        public String MaskedNumber = '125454';
        public String ExpYear = '2019';
        public String ExpMonth = 'may';
        public String HolderName = 'RSK';
        public String CountryCode = 'sample code';
    }
    
    public class SPPayer {
        public String ipAddress = 'TestId';
        public String ipLocation = 'TestLocation';
    }    
    
    public class SPRegistrationResult {
        public boolean success = true;
        public SPAlias alias; 
        public SPError error;  
    }
    
    public class SPError {
        public String errorName = 'TestName';
        public String errorMessage = 'TestMessage';
    }    
    
    public class SPAlias {
        public String id = 'test';
        public Integer lifetime = 2000;
    }    
    
    public class SPTransaction {
        public String type = 'Test';
        public String status = 'Good';
        public String id ='test Id';
        public String txnDate = 'Today';
        public SPAmount amount;
        public String acquirerName ='test';
        public String acquirerReference ='test';
        public String sixTransactionReference ='test';
        public String approvalCode ='test';
    }
    
    public class SPLiability {
        public boolean liabilityShift = true;
        public String liableEntity = 'Test';
        public SP3D threeDs;
        public SPFraudFree fraudFree;
    }
    
    public class SP3D {
        public boolean authenticated = true;
        public boolean liabilityShift = true;
        public String xid = 'Test';
        public String verificationValue = 'TestValue';
    }
    
    public class SPFraudFree {
        public String id = 'TestId';
        public boolean liabilityShift = true;
        public decimal score = 2.5;
        public List<String> investigationPoints;
        public String errorMessage = 'ErrorTest';
    }    
    
    public class SPAmount {
        public String value = 'Test';
        public String currencyCode = 'CHF';
    } 
    
    testmethod public static void TestUtility() {
        
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.demo';
        
        /*UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
insert r;*/
        
        Account acc = new Account (Name = 'newAcc1');  
        insert acc;
        
        Contact con = new Contact(lastname='lastname1', AccountId = acc.id, Password__c='123pwd4598' );
        con.Token__c = 'testTokenValue';
        con.Token_created__c = 'TestDate';
        con.Others__c = 'test';
        con.Payment_Alias_ID__c = System.label.TestAlaisId;
        con.Recommendation__c = 'Test';
        insert con;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User'];
        
        
        String contactId=p.Id;
        String param1=contactId;
        Exception ee;
        
        User u = new User();
        System.runAs ( thisUser ) {
            //,UserRoleId = r.Id
            u = new User(Email='standarduser@testorg.com',
                         EmailEncodingKey='UTF-8' , FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                         LocaleSidKey='en_US', ProfileId = p.Id,Others__c = 'Test',
                         TimeZoneSidKey='America/Los_Angeles',Recommendation__c = 'Test',contactId=con.Id,CommunityNickname='Lings32184',
                         UserName=uniqueUserName);
            insert u;
        }
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {  
            
            MyObject instance = new MyObject();
            instance.ConLastName = con.LastName;
            instance.ConOwnerId =  con.OwnerId;
            instance.ConPassword = con.Password__c;
            instance.id = u.Id;
            instance.Alias=u.firstName.subString(0,1) + u.lastName.substring(2,5);
            instance.Email=u.Email;
            instance.EmailEncodingKey=u.EmailEncodingKey;
            instance.FirstName=u.FirstName;
            instance.Lastname=u.LastName;
            instance.LanguageLocaleKey=u.LanguageLocaleKey;
            instance.LocaleSidKey=u.LocaleSidKey;
            instance.UserName=u.email + userNameSuffix;
            instance.CommunityNickname=u.CommunityNickname;
            instance.UserRoleId = u.UserRoleId;
            String myJSON = JSON.serialize(instance);
            
            Premium__c prevalue = new Premium__c();
            prevalue.Name='smartphone';
            prevalue.On_Demand__c=12;
            prevalue.FlatRate__c=32;
            insert prevalue;
            
            Premium__c prevalue1 = new Premium__c();
            prevalue1.Name='Laptop';
            prevalue1.On_Demand__c=12;
            prevalue1.FlatRate__c=32;
            insert prevalue1;
            
            Premium__c prevalue2 = new Premium__c();
            prevalue2.Name='Camera incl. Accessories';
            prevalue2.On_Demand__c=12;
            prevalue2.FlatRate__c=32;
            insert prevalue2;
            //declaring the required test data for apex class utility 
            DB_Brack__c DBB1 = new DB_Brack__c(Name='ABC',Category_2__c='Foto und Videografie');
            DB_Fotichaeschtli__c DBF1 = new DB_Fotichaeschtli__c(Name='ABC',Product_Type__c= 'Camera incl. Accessories');
            List<Product2> PdtList = new List<Product2>();
            Product2 pdt1 = new Product2(Name=DBF1.Name,Product_Type__c='Camera incl. Accessories');
            insert pdt1;
            Product2 pdt2 = new Product2(Name=DBF1.Name,Product_Type__c='Laptop');
            insert pdt2;
            Product2 pdt3 = new Product2(Name=DBF1.Name,Product_Type__c='Smartphone');
            insert pdt3;
            Product2 pdt4 = new Product2(Name=DBF1.Name,Product_Type__c='');
            insert pdt4;           
            
            SPResponseHeader SPRespInfoinstance = new SPResponseHeader();
            String SPRespInfoJSON = JSON.serialize(SPRespInfoinstance,true);    
            
            SPAlias SPAlaisInfoinstance = new SPAlias();
            String SPAlaisInfoJSON = JSON.serialize(SPAlaisInfoinstance,true); 
            
            SPCardInfo SPCardInfoinstance = new SPCardInfo();
            String SPCardInfoJSON = JSON.serialize(SPCardInfoinstance,true);
            
            SPBrandInfo SPBrandInfoinstance = new SPBrandInfo();
            String SPBrandInfoJSON = JSON.serialize(SPBrandInfoinstance,true);
            
            SPPaymentInfo SPPaymentInfoinstance = new SPPaymentInfo();
            SPPaymentInfoinstance.brand = SPBrandInfoinstance;
            SPPaymentInfoinstance.card = SPCardInfoinstance; 
            String SPPaymentInfoJSON = JSON.serialize(SPPaymentInfoinstance,true);
            
            SPPayer SPPayerinstance = new SPPayer();
            String SPPayerInfoJSON = JSON.serialize(SPPayerinstance,true);
            
            SPError SPErrorinstance = new SPError();
            String SPErrorInfoJSON = JSON.serialize(SPErrorinstance,true);            
            
            SPRegistrationResult SPRRinstance = new SPRegistrationResult();
            SPRRinstance.alias = SPAlaisInfoinstance;
            SPRRinstance.error = SPErrorinstance;
            String SPRRJSON = JSON.serialize(SPRRinstance,true); 
            
            SP3D SP3Dinstance = new SP3D();
            String SP3DInfoJSON = JSON.serialize(SP3Dinstance,true);
            
            SPFraudFree SPFraudFreeinstance = new SPFraudFree();
            String SPFFInfoJSON = JSON.serialize(SPFraudFreeinstance,true);
            
            SPAmount SPAmountinstance = new SPAmount();
            String SPAmountJSON = JSON.serialize(SPAmountinstance, true);
            
            SPLiability SPLiabilityinstance = new SPLiability();
            SPLiabilityinstance.threeDs = SP3Dinstance; 
            SPLiabilityinstance.fraudFree =SPFraudFreeinstance; 
            String SPLiabilityJSON = JSON.serialize(SPLiabilityinstance,true); 
            
            SPTransaction SPTransactioninstance = new SPTransaction();
            SPTransactioninstance.amount = SPAmountinstance;
            String SPTransactionJSON = JSON.serialize(SPTransactioninstance,true);
            
            
            SPTxnPaymentResponse SPTxnPaymentInstance = new SPTxnPaymentResponse();
            SPTxnPaymentInstance.responseHeader = SPRespInfoinstance;
            SPTxnPaymentInstance.txn = SPTransactioninstance;
            SPTxnPaymentInstance.paymentMeans = SPPaymentInfoinstance;
            SPTxnPaymentInstance.registrationResult = SPRRinstance;
            SPTxnPaymentInstance.liability = SPLiabilityinstance;
            String SPTxnPaymentJSON = JSON.serialize(SPTxnPaymentInstance,true); 
            
            SPPaymentResponse SPPaymentInstance = new SPPaymentResponse();
            SPPaymentInstance.responseHeader = SPRespInfoinstance;
            SPPaymentInstance.alias = SPAlaisInfoinstance;
            SPPaymentInstance.paymentMeans = SPPaymentInfoinstance;
            String SPPaymentJSON = JSON.serialize(SPPaymentInstance,true); 
            
            MyObject Objectinstance = new MyObject();
            Objectinstance.paymentResp = SPPaymentInstance;
            String ObjectJSON = JSON.serialize(Objectinstance,true);    
            
            MyObject Objectinstance1 = new MyObject();
            Objectinstance1.txnResp = SPTxnPaymentInstance;
            String ObjectJSON1 = JSON.serialize(Objectinstance1,true);
            
            Premium__c prevalue3 = new Premium__c();
            prevalue3.Name='laptop';
            prevalue3.On_Demand__c=12;
            prevalue3.FlatRate__c=32;
            insert prevalue3;
            
            Product2 pro= new Product2();
            pro.Name='sample product';
            pro.Insured_Value__c = 200.00;
            pro.Price__c= pro.Insured_Value__c.setScale(2);
            pro.Price_Customer__c = 100;
            pro.Old_Price__c=pro.Price__c;        
            pro.Searchable__c = TRUE;
            pro.OnDemand_Premium__c = 2.3;
            pro.Flatrate_Premium__c = 5.6;
            pro.IsDeleted__c = False;
            pro.Status__c = 'Closed';
            pro.Manufacturer__c = 'Test Product';
            pro.Price_Customer__c = 56;
            pro.Product_Type__c = 'Laptop';
            insert pro;
            
            Product2 pro1= new Product2();
            pro1.Name='sample product';
            pro1.Insured_Value__c = 200.00;
            pro1.Price__c= pro.Insured_Value__c.setScale(2);
            pro1.Price_Customer__c = 100;
            pro1.Old_Price__c=pro.Price__c;        
            pro1.Searchable__c = TRUE;
            pro1.OnDemand_Premium__c = 2.3;
            pro1.Flatrate_Premium__c = 5.6;
            pro1.IsDeleted__c = False;
            pro1.Status__c = 'Closed';
            pro1.Manufacturer__c = 'Test Product';
            pro1.Price_Customer__c = 56;
            pro1.Product_Type__c = 'Laptop';
            
            Object__c obj = new Object__c();
            obj.Product__c = Pro.Id;
            obj.Customer__c = con.Id;
            obj.Insured_Value__c = 100;
            obj.Is_Insured__c =true;
            obj.Insurance_Start_Date__c = System.today();
            obj.Insurance_End_Date__c = System.today()+2;
            obj.Coverage_Type__c = 'OnDemand';
            insert obj;
            
            Policy__c pol = new Policy__c();
            pol.Object__c = obj.Id;
            pol.Contact__c = con.Id;
            pol.Start_Date__c = System.today();
            pol.Premium__c = 12;
            
            insert pol;
            
            obj.Policy__c = pol.Id;
            update obj;
            
            Invoice__c inv = new Invoice__c();
            inv.Name = 'testInvoice';
            inv.Amount__c = 5;
            inv.Contact__c = con.Id;
            insert inv;
            
            //For OKK
            Contact con4 = new Contact(lastname='lastname1',Password__c='123pwd4598');
            con4.email='test@test.com';
            insert con4;          
            
            Premium_CustomSetting__c pcs = new Premium_CustomSetting__c();
            pcs.Name = 'SampleName';
            pcs.PRICEPERDAY__c = 2.40;
            pcs.Pricetotal__c = 5.0;
            pcs.Duration_days__c = 1;
            insert pcs;
            
            Premium__c prevalue4 = new Premium__c();
            prevalue4.Name='travel insurance';
            prevalue4.On_Demand__c=5.00;
            prevalue4.FlatRate__c=5.00;
            insert prevalue4;
            
            Product2 pro4 = new Product2(Id=System.label.OKKProductId);
            pro4.Price__c = 100;
            pro4.Insured_Value__c = 100;
            pro4.Product_Type__c = 'Travel Insurance';
            upsert pro4;
            
            List<Event__c> eveList = new List<Event__c>();
            Event__c eve = new Event__c();
            eve.Name='sample event';
            eve.Start_Date__c=System.today();
            eve.End_Date__c=System.today();
            eve.Contact__c=con4.Id;
            eve.External_ID__c = 'Local_12345688';
            eve.Comments__c = 'Created Event with OKK';
            //insert eve;
            //eveList.add(eve);
            //insert eveList;            
            
            Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
            Utility.setSPAlias(SPPaymentJSON);
            Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
            Utility.updateProduct(Pro.Id);
            Utility.createEvent(eve, eve.External_ID__c);
            Utility.setSPPayment(SPPaymentJSON);
            Utility.setSPToken(con.id, 'TestToken');
            Utility.convertToProductDB1(DBB1,pdt1);
            Utility.convertToProductDB1(DBB1, pro1);
            Utility.convertToProductDB2(DBF1,pdt1);
            Utility.convertToProductDB2(DBF1, pro1);
            Utility.convertToProductDB1(DBB1,pdt4);
            Utility.numOfDays(null);
            Utility.getCategory(pdt1.Product_Type__c);
            Utility.getCategory('laptop');
            Utility.getCategory('Mobiltelefonie');
            Utility.getCategory(pdt4.Product_Type__c);
            Utility.isValidCategory(pdt2.Product_Type__c);
            Utility.isValidCategory(pdt1.Product_Type__c);
            Utility.getPremium('foto');
            Utility.getPremium('e-bike');
            Utility.getPremium('velo');
            Utility.getRoundedPremium(102);
            Utility.getRoundedPremium(11.92);
            Utility.registerContact(u.Id, 'TestPassword');
            Utility.generateToken(10);
            Utility.numOfDaysForEndDate(System.today());
            Utility.setSPToken(u.ContactId, 'token');
            Utility.sendInfo(con.Email, 'info', 'rec');
            Utility.sendError(con.Email, 'err', 'rec', 'actionNeeded'); 
            Utility.processPayment(inv.Id, inv.Amount__c, inv.Name, con.Payment_Alias_ID__c);
            Utility.setSPTxnAlias(SPTxnPaymentJSON,'Txn Completed');
            Utility.getMonthName(12);
            Utility.getMonthName(0);
            Utility.closeObjectPolicy(Obj.Id);
            Utility.trackObject(obj.Id,'ON');
        }
        try {
            //Utility.SendFCMNotification();
        } catch(Exception e){
            Utility.sendException2(e,'Sample Request String');
            Utility.errorString(e);
            Utility.errorHandler(e);
            Utility.sendError('riyaz.mohamed@outlook.com', 'err', 'rec', 'actionNeeded');  
        }
    }
}