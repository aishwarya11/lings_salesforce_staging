@isTest
global class LingsCalloutMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse resp = new HTTPResponse();
        UIPaymentWrapper.SPInsertResponse spResp = new UIPaymentWrapper.SPInsertResponse();
        String epStr = req.getEndpoint();
        if(req.getMethod() == 'POST') {
            if(epStr.equalsIgnoreCase(Constants.SP_Tnx_Initialize_EP)) {
                UIPaymentWrapper.SPTxnInitResponse txnResp = new UIPaymentWrapper.SPTxnInitResponse();
                resp.setStatusCode(200);
                resp.setStatus('OK');
                // Set the Response for the request
                String reqBody = req.getBody();
                String reqContactId = reqBody.substringBetween('"RequestId": "', '", "');
		        txnResp.responseHeader.RequestId = reqContactId;
                txnResp.token = '12345';
                txnResp.redirect = new UIPaymentWrapper.SPRedirect();
                txnResp.redirect.redirectURL = 'test';
                resp.setBody(JSON.serialize(txnResp));
            } else if(epStr.equalsIgnoreCase(Constants.SP_TXN_EP)) {
                 resp.setStatusCode(200);
                resp.setStatus('OK');
                // Set the Response for the request
                String reqBody = req.getBody();
                String reqContactId = reqBody.substringBetween('"RequestId": "', '", "');
		        spResp.responseHeader.RequestId = reqContactId;
                spResp.token = '12345';
                resp.setBody(JSON.serialize(spResp));
            } else if(epStr.equalsIgnoreCase(Constants.SP_Tnx_Cancel_EP)) {
                 resp.setStatusCode(200);
                resp.setStatus('OK');
                // Set the Response for the request
                String reqBody = req.getBody();
                String reqContactId = reqBody.substringBetween('"RequestId": "', '", "');
		        spResp.responseHeader.RequestId = reqContactId;
                spResp.token = '12345';
                resp.setBody(JSON.serialize(spResp));
            } else if(epStr.equalsIgnoreCase(Constants.SP_Tnx_Capture_EP)) {
                 resp.setStatusCode(200);
                resp.setStatus('OK');
                // Set the Response for the request
                String reqBody = req.getBody();
                String reqContactId = reqBody.substringBetween('"RequestId": "', '", "');
		        spResp.responseHeader.RequestId = reqContactId;
                spResp.token = '12345';
                resp.setBody(JSON.serialize(spResp));
            }
        } else if(epStr.equalsIgnoreCase(Constants.SP_Tnx_Authorize_EP)) {
                 resp.setStatusCode(200);
                resp.setStatus('OK');
                // Set the Response for the request
                String reqBody = req.getBody();
                String reqContactId = reqBody.substringBetween('"RequestId": "', '", "');
		        spResp.responseHeader.RequestId = reqContactId;
                spResp.token = '12345';
                resp.setBody(JSON.serialize(spResp));
            }
        
        return resp;
    }
}