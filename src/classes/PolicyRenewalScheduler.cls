global class PolicyRenewalScheduler implements Schedulable {
	//Renewal of Policies
    global void execute(SchedulableContext sc) {
        BatchPolicyRenewal batchRenewal = new BatchPolicyRenewal();
        database.executeBatch(batchRenewal, 5);
    }
}