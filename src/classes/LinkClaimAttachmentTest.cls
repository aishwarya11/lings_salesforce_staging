@isTest
public class LinkClaimAttachmentTest {
    @isTest static void TestLinkClaimAttachmentTest() { 
        String userNameSuffix = 'lings.staging';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];        
        
        User u = new User(Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                          UserName= 'reva@testorg.com',CommunityNickname='Lings32184');
        insert u;
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            Contact con = new Contact(lastname='lastname1',OwnerId=u.id,Password__c='123pwd4598' );
            con.email='test@test.com';
            con.Street__c = 'Test Location';
            con.Place__c = 'Chennai';
            con.Post_Code__c = '1000';
            con.Phone = '8056489524';
            insert con;     
            
            Premium__c pre = new Premium__c();
            pre.Name='laptop';
            pre.On_Demand__c=12;
            pre.FlatRate__c=32;
            insert pre;  
            
            Product2 PO= new Product2();
            PO.Name='sample product';
            PO.Insured_Value__c = 200.00;
            PO.Price__c= PO.Insured_Value__c.setScale(2);
            PO.Price_Customer__c = 100;
            PO.Old_Price__c=PO.Price__c;        
            PO.Searchable__c = TRUE;
            PO.OnDemand_Premium__c = 2.3;
            PO.Flatrate_Premium__c = 5.6;
            PO.IsDeleted__c = False;
            PO.Status__c = 'Closed';
            PO.Manufacturer__c = 'Test Product';
            PO.Price_Customer__c = 56;
            PO.Product_Type__c = 'Laptop';
            insert PO;
            
            Object__c obj = new Object__c();
            obj.Customer__c = con.Id;
            obj.Product__c = PO.Id;
            obj.Insured_Value__c=100;
            obj.Included_Date__c=System.today();
            obj.FlatRate_Savings__c=50;
            obj.Coverage_Type__c='OnDemand';
            obj.Premium_FlatRate__c=10.00;
            obj.Premium_OnDemand__c=6.00;
            obj.Flatrate_Renewal_Flag__c=true;
            obj.Insurance_Start_Date__c=System.today();
            obj.Insurance_End_Date__c=System.today()+1;
            obj.Is_Deleted__c = FALSE;
            obj.Approved__c = true;   
            obj.Acquired_Year__c = '2019';
            obj.Actual_amount_paid__c = 200.00;
            obj.Description__c = 'This is the test Object';
            obj.Designation__c = 'test';
            obj.Is_Insured__c = TRUE;
            obj.Own_Comment__c = 'This is a comment section';
            obj.Sort_Order__c = 1;
            obj.Shop_Name__c = 'Test Object';
            insert obj;
            
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId= con.Id;
            insert attach;                
            
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S';         //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
            cVersion.PathOnClient = attach.Name;    //File name with extention
            cVersion.Origin = 'H';                  //C-Content Origin. H-Chatter Origin.
            cVersion.OwnerId = UserInfo.getUserID(); //getContactId();      //Owner of the file
            cVersion.Title = attach.Name;           //Name of the file
            cVersion.VersionData = attach.Body;     //File content
            Insert cVersion;
            
            Id conDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
            
            Claim__c newClaim = new Claim__c();
            newClaim.Damage_Type__c = 'theft-loss';
            newClaim.Damage_Date__c = System.today();
            newClaim.Description__c  = 'Sample description';
            newClaim.Object__c = obj.id;
            newClaim.IBAN__c = 'Sample IBAN';
            newClaim.Attachment_Id__c = conDocumentId;
            newClaim.Contact__c = con.Id;
            newClaim.First_Name__c = u.FirstName;
            newClaim.Last_Name__c = u.LastName;
            newClaim.User_Name__c = u.Username;
            newClaim.Email__c = u.Email;
            newClaim.User_Id__c = u.Id;
            newClaim.CurrencyIsoCode = 'CHF';
            newClaim.Reporting_Date__c = System.today();
            newClaim.Insurance_Amount__c = 13.00;
            insert newClaim;    
            
        }
    }
}