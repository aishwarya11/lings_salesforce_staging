@RestResource(urlMapping='/voucher/*')
global with sharing class MobileVoucherServices {
    @HttpPost
    global static String redeem(String langCode, String voucherCode) {
        return UIHomeController.redeemVoucher(langCode, voucherCode);
    }
}