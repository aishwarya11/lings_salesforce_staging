public class DataMasking {
    public class ContactWrapper{
        public Id conId;
        public String conFirstName;
        public String conLastName;
        public String conPhone;
        public String conEmail;
        public String paymentAliasId;
    }

    public static void contactMasking(){
        List<ContactWrapper> conListWrapper = new List<ContactWrapper>();
        Integer i = 3501;
        
        List<User> userList = [Select name,id,contactid from user where (NOT Name  like 'MemberUser-%') and contactid != NULL Limit 350];
        List<User> userUpdateList = new List<User>();
        for(User u : userList){
            if(u.ContactId != NULL && !(u.ContactId.equals(System.label.Contact_Henrik_1)) && !(u.ContactId.equals(System.label.Contact_Henrik_2))
               && !(u.ContactId.equals(System.label.Contact_Henrik_3))&& !(u.ContactId.equals(System.label.Contact_Henrik_4))
               && !(u.ContactId.equals(System.label.Contact_Henrik_5))&& !(u.ContactId.equals(System.label.Contact_Larissa_5))
               && !(u.ContactId.equals(System.label.Contact_Larissa_4))&& !(u.ContactId.equals(System.label.Contact_Larissa_3))
               && !(u.ContactId.equals(System.label.Contact_Larissa_2))&& !(u.ContactId.equals(System.label.Contact_Larissa_1))
               && !(u.ContactId.equals(System.label.Contact_Aishwarya_1)) && !(u.ContactId.equals(System.label.Contact_Aishwarya_2)) 
               && !(u.ContactId.equals(System.label.Contact_Afeela_1)) && !(u.ContactId.equals(System.label.Contact_Afeela_2))
               && !(u.ContactId.equals(System.label.Contact_Riyaz_1)) && !(u.ContactId.equals(System.label.Contact_Riyaz_2))
               && !(u.ContactId.equals(System.label.Contact_Roger_6))&& !(u.ContactId.equals(System.label.Contact_Roger_5))
               && !(u.ContactId.equals(System.label.Contact_Roger_4))&& !(u.ContactId.equals(System.label.Contact_Roger_3))
               && !(u.ContactId.equals(System.label.Contact_Roger_2))&& !(u.ContactId.equals(System.label.Contact_Roger))
               && !(u.ContactId.equals(System.label.Contact_Thomas_1))&& !(u.ContactId.equals(System.label.Contact_Thomas_2))
               && !(u.ContactId.equals(System.label.Contact_Thomas_3))&& !(u.ContactId.equals(System.label.Contact_Kai_1))
               && !(u.ContactId.equals(System.label.Contact_Kai_2))){
                u.FirstName = 'MemberUser-';
                u.LastName = ''+i;
                u.Email = u.firstname+u.lastname+'-test@lings.com';
                u.Phone = '00000000';
                u.Username = u.firstname+u.lastname+'-test@lings.comstaging';
                System.debug('user contact id = '+u.ContactId);
                userUpdateList.add(u);
                ContactWrapper cl = new ContactWrapper();
                cl.conId = u.ContactId;
                cl.conFirstName = u.FirstName;
                cl.conLastName = u.LastName;
                cl.conPhone = u.Phone;
                cl.conEmail = u.Email;
                cl.paymentAliasId = NULL;
                conListWrapper.add(cl);
            }
            i++;
        }
        update userUpdateList;
        System.debug('updation completed successfully for the user list of size = '+userUpdateList.size());
        System.debug('Contact list size in 1st method = '+conListWrapper.size());
        String contUpdJsonString = JSON.serialize(conListWrapper,true);   
        updateContactList(contUpdJsonString,i);
    }

    @future
    public static void updateContactList(String conJson,Integer count){
        List<ContactWrapper> CWL = (List<ContactWrapper>)System.JSON.deserialize(conJson, List<ContactWrapper>.class);
        List<Contact> contactUpdateList = new List<Contact>();
        
        for(ContactWrapper cw : CWL){
            Contact c = new Contact(id = cw.conId);
            c.FirstName = cw.conFirstName;
            c.LastName = cw.conLastName;
            c.Email = cw.conEmail;
            c.Phone = cw.conPhone;
            c.Payment_Alias_ID__c = cw.paymentAliasId;
            contactUpdateList.add(c);
        }
        update contactUpdateList;
        System.debug('updation completed successfully for the contact list 1 of size = '+contactUpdateList.size()+' and the contact update list 1 is ---> '+contactUpdateList);
		System.debug('Total number of Contact Updated: '+(contactUpdateList.size()));
    }
}