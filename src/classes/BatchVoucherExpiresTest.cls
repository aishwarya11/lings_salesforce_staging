@isTest
public class BatchVoucherExpiresTest {
    public static testmethod void voucherExpiresTest(){
        //The values required to test the batch
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=100;
        vc.From__c=System.today()-2;
        vc.To__c=System.today();
        vc.Fixed__c=System.today();
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        insert VCC;
        
        Test.startTest();
        
        BatchVoucherExpires ve = new BatchVoucherExpires(); // Add you Class Name
        DataBase.executeBatch(ve);
        
        Test.stopTest();
        
    }
    
}