public class UIResultWrapper {
    public static String CHF_STRING = 'CHF';
    public static String SRCH_KEYWORD = '';
    public static String FIRST_SRCH_KEYWORD = '';
    public static List<String> SRCH_COMBINED_KEYWORDS = new List<String>();
    public static Set<String> SRCH_KEYWORDS = new Set<String>();

    //fields used in the UI to define the product 
    public class ProductInfo implements Comparable {
        public String uuid;
        public String name;
        public String model;
        public String description;
        public String thumbnail;
        public String year;
        public boolean ebike;
        public String category;
        public Decimal price;
        public String manufacturer;
        //19July2019-- to get the additional details of the Lings Object.
        public String shopName ;
        public Decimal paidAmount;
        //public String state;
        public String serialNumber;
        public String comment;
        public Date purchasedDate;
        public Date warrantyDate;
        public String shopUrl;
        public String warrantyCard;
        public String fileContent1;
        public String receipt;
        public String fileContent2;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        String currencyCode = CHF_STRING;
        public Integer sortIndex = 0;
        public Decimal OKKPerDayPre = 0;
        public Decimal RVGPerDayPre = 0;
        public Decimal OKKTotalPre = 0;
        public Decimal RVGTotalPre = 0;
        public Date OKKEventStartDate;
        public Date OKKEventEndDate;
        public Date RVGEventStartDate;
        public Date RVGEventEndDate;

        //Initiailizing the CustomSettings
        public ProductInfo() {
            insuranceRate = new OnDemandInfo();
            flatRate = new FlatRateInfo();
            ebike = false;
        }

        // Add a Sorting logic for the list to return the data in below order
        // 1. srchKeyword Starting with Name
        // 2. srchKeyword containing Name exactly
        public Integer compareTo(Object compareTo) {
            ProductInfo next = (ProductInfo)compareTo;

            Integer returnValue = 0;            
            if(sortIndex == next.sortIndex) {
                returnValue = 0;
            } else if(sortIndex > next.sortIndex) {
                returnValue = -1;
            } else {
                returnValue = 1;
            }

            System.debug('Inside compareTo returing ' + returnValue + ' for [' + name + '(' + sortIndex + ')] && [' + next.name + '(' + next.sortIndex + ')]');
            return returnValue;
        }
    }
    
    //Definiton of the OnDemandInfo customsetting
    public class OnDemandInfo {
        public Decimal daily;
        public Decimal monthly;
        @testvisible String currencyCode = CHF_STRING;
        public boolean enabled;
    }
    
     //Definiton of the FlatRateInfo customsetting
    public class FlatRateInfo {
        public Decimal daily;
        public Decimal monthly;
        public Decimal savings;
        public Date periodFrom;
        public Date periodTo;
        public boolean enabled;
        public boolean renewable;        
        // MR - 29MAR19 - Changed from Literal to Function
        Integer duration = Utility.numOfDays(null);
        String currencyCode = CHF_STRING;
    }
    
    //Defining the EventInfo fields 
    public class EventInfo {
        public String uuid;
        public String name;
        public Date periodFrom;
        public Date periodTo;
    }
    
    //definition of EventInfo fields with the corresponding object name
    public class EventWithObject {
        public String uuid;
        public String name;
        public Date periodFrom;
        public Date periodTo;
        public List<MyObject> insuredProducts;
    }
   
    //Defining the RucksackInfo fields --26JULY2019
    public class SackInfo {
        public String uuid;
        public String name;
        public String state;        
    }
    
    //definition of SackInfo fields with the corresponding object name --26JULY2019
    public class SackWithObject {
        public String uuid;
        public String name;
        public String state; 
        public List<MyObject> insuredProducts;
    }    
    //Definition of BalanceInfo (i.e) ExpirationDate of the Amount already paid for the Insurance
    public class BalanceInfo {
        public Decimal amount;
        public Date expirationDate;
       @testvisible String currencyCode = CHF_STRING;

        // 06JUN19 - Initialization for Object Insurance Handling
        public BalanceInfo() {
            amount = 0.0;
            expirationDate = System.today();
        }
    }
    
    //Defining the MYObject section (i.e) Products owned by the customer 
    public class MyObject {
        public String uuid;
        public String name;
        public Decimal price;
        @testvisible String currencyCode = CHF_STRING;
        public String thumbnail;
        public boolean published;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        public List<EventInfo> events;
        public boolean insured;
        public boolean paid;    // MR - 21MAY19 - Added for LM437
        public boolean claimed;
        public String category;
        public boolean electricBike; //25JUNE2019--Added to capture the E-BIKE Category.        
        public String shopName;	//19July2019-- to get the additional details of the Lings Object.
        public Decimal paidAmount;
      //  public String state;
        public String serialNumber;
        public String comment;
        public Date purchasedDate;
        public Date warrantyDate;
        public String shopUrl;
        public String warrantyCard;
        public String fileContent1;
        public String receipt;
        public String fileContent2;        
        public String manufacturer;
          //Initialzing some of  the fields of MYObject section
        public MyObject() {
            insured = false;
            claimed = false;
            published = true;
            insuranceRate = new OnDemandInfo();
            flatRate = new FlatRateInfo();
            events = new List<EventInfo>();
        }
    }
    
    //Detail Information Page of the Customer 
    public class Profile {
        public String uuid;
        public String firstName;
        public String lastName;
        public Date dob;
        public String street;
        public String house;
        public String postalCode;
        public String city;
        public String phone;
        public String email;
        public String password;
        public String leadSource;
        public String others;
        public String recommendation;
        public boolean terms;
        public boolean freeze;
        public String ipAddress;
        public UIPaymentWrapper.SPPaymentInfo paymentMeans;

        // MR - 01AUG19 - Added for PolicyDues and Voucher Balance
        public decimal policyDues;
        public decimal voucherBalance;
        public boolean hasEvents;
        public boolean hasBags;
        public List<EventInfo> events;
        public List<SackInfo> sacks;
        
        public Profile(){
           events = new List<EventInfo>();
           sacks = new List<SackInfo>(); 
        }
    }
        //Claims Information
    public class ClaimWithObject {
        public String uuid;    // MR - 10AUG19 - Added for differentiating the Claim for Object and Bag
        public String damageReason;
        public String damageDate;
        public String damageDetails;
        public String iban;
        public List<String> attachments = new List<String>();
        //public List<String> fileContents;
        //public MyObject insuredProduct;
        
        public List<UIObjectWrapper.LingsObject> insuredProducts = new List<UIObjectWrapper.LingsObject>();
    }
    
    //Assinging the values from the UI to the EventInfo fields when it get starts 
    public static EventInfo toEventInfo(Event__c objEvent) {
        EventInfo wrapperEvent = new EventInfo();
        wrapperEvent.uuid = objEvent.id;    
        wrapperEvent.name = objEvent.name;
        wrapperEvent.periodFrom = objEvent.Start_Date__c;
        wrapperEvent.periodTo = objEvent.End_Date__c;
        return wrapperEvent;
    }
  //getting List of EventInfo records from the UI
    public static List<EventInfo> toEventInfoList(List<Event__c> objEvents) {
        List<EventInfo> wrapperList = new List<EventInfo>();
        
        if(objEvents != null && objEvents.size() > 0) {
            for(Event__c objEvent: objEvents) {
                wrapperList.add(toEventInfo(objEvent));
            }   
        }

        return wrapperList;
    }
    
   //Calculating the OnDemandInfo by passing the Premium Value 
    public static OnDemandInfo getOnDemandWrapper(Decimal premium) {
        OnDemandInfo rate = new OnDemandInfo();
        rate.daily = premium;
        rate.monthly = premium * Utility.numOfDays(null);
        rate.enabled = false;
        return rate;
    }
    
    //Calculating the FlatRateInfo by passing the Premium Value 
    public static FlatRateInfo getFlatRateWrapper(Decimal premium) {
        FlatRateInfo rate = new FlatRateInfo();
        rate.daily = premium;
        // LM399 - MR - 22APR19 - Removed setScale
        rate.monthly = Utility.getRoundedPremium(premium * Utility.numOfDays(null));    
        if(rate.monthly == 0.00) {
            rate.monthly = 0.05;
        }
        rate.enabled = false;
        rate.renewable = false;
        rate.periodFrom = System.today();
        // 22APR19 - LM398 - MR - Fix for FlatRate EndDate
        rate.periodTo = System.today() + Utility.numOfDaysForEndDate(null);
        return rate;
    }


    // MR - 06APR19 - Function to return indexValue for the Name with Keyword
    public static Integer getIndexValue(String inpString) {
        Integer val = 0;
        for(String keyStr: SRCH_KEYWORDS) {
            if(inpString.equalsIgnoreCase(keyStr)) {
                val+=100;
            } else if(inpString.startsWithIgnoreCase(keyStr)) {
                val+=50;
            } else if(inpString.containsIgnoreCase(keyStr)) {
                val+=25;
            }
        }
        
        if(inpString.equalsIgnoreCase(SRCH_KEYWORD)) {
            val+=200;
        }else if(inpString.startsWithIgnoreCase(SRCH_KEYWORD)) {
            val+=100;
        } else if(inpString.containsIgnoreCase(SRCH_KEYWORD)) {
            val+=50;
        }

        System.debug('Inside getIndexValue returing ' + val + ' for [' + inpString + ']. Kyeword = ' + SRCH_KEYWORD + ' & Keywords = ' + SRCH_KEYWORDS);
        return val;
    }
}