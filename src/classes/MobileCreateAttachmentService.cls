@RestResource(urlMapping='/create/*')
global with sharing class MobileCreateAttachmentService {
    @HttpPost
    global static String attachment(String fileName, String fileContent, String getId){
        return UIHomeController.createAttachment(fileName, fileContent, getId);
    }
}