public interface DataHandler {
     void setContext(String context);   
     // List the records
     String getRecords();

     // Get the information of a particular record
     String readRecords(String inpJson);

     // Create a record
     String createRecords(String inpJson);

     // Update the records
     String updateRecords(String inpJson);

     // Delete the records
     String deleteRecords(String inpJson);
}