@isTest
public class EventBeforeTriggerTest {
    @isTest static void TestEventBeforeTriggerTest() { 
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;          
        
        Premium_CustomSetting__c pcs = new Premium_CustomSetting__c();
        pcs.Name = 'SampleName';
        pcs.PRICEPERDAY__c = 2.40;
        pcs.Pricetotal__c = 5.0;
        pcs.Duration_days__c = 1;
        insert pcs;
        
        Premium__c prevalue = new Premium__c();
        prevalue.Name='travel insurance';
        prevalue.On_Demand__c=5.00;
        prevalue.FlatRate__c=5.00;
        insert prevalue;
        
        Product2 pro = new Product2(Id=System.label.OKKProductId);
        pro.Price__c = 100;
        pro.Insured_Value__c = 100;
        pro.Product_Type__c = 'Travel Insurance';
        upsert pro;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        eve.External_ID__c = 'Local_12345688';
        eve.Comments__c = 'Created Event with OKK';
        insert eve;
        
    }
}