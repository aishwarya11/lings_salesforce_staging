@isTest
public class ClaimSettleTest {
    testmethod public static void testClaimSettleTest(){
        try{
            
        Product2 pdt = new Product2();
        pdt.Name = 'sample product';
        pdt.Product_Type__c = 'Laptop';
        pdt.Insured_Value__c = 10;
        insert pdt;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        insert con;

        Object__c obj1 = new Object__c();
        obj1.Product__c = pdt.Id;
        obj1.Customer__c = con.id;
        obj1.Insured_Value__c = 10;
        insert obj1;
        
        Object__c sc = obj1;
        sc.Id = obj1.Id;
        update sc;

        
        ClaimSettle.updateObject(obj1, obj1.id);
        ClaimSettle.sendEmail(obj1, obj1.id);
        }
        catch(Exception e){}
    }

}