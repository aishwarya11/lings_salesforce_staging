@isTest
public class PaymentInfoTest {
    
    testmethod public static void TestPolicyInfo() {
        //Contact details
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        //con.Voucher_In_Use__c =VCC.id;
        insert con;        
        
        //Invoice details
        Invoice__c inv = new Invoice__c();
        inv.Amount__c = 200;
        inv.Contact__c = con.Id;
        inv.Voucher_Deductions__c =100;
        inv.Status__c = 'Success';
        inv.Email__c = con.Email;
        insert inv;
       
        //Payment details
        Payment__c pay = new Payment__c();
        pay.Contact__c = con.Id;
        pay.Invoice__c = inv.Id;
        pay.Date__c = System.today();
        pay.Paid__c = 100;
        pay.Status__c = 'CAPTURED';
        insert pay;    
        
        //Invoice__c inv1 = new Invoice__c(id = inv.Id);
        inv.Card_Payment__c = pay.Id;
        update inv;

        
        PaymentInfo pi = new PaymentInfo();
        PaymentInfo.InvoiceDetails ID = new PaymentInfo.InvoiceDetails();
        ID.Amount = inv.Amount__c;
        ID.email = con.Email;
        ID.Status = pay.Status__c;
        pi.invoiceId = inv.Id;
        pi.getInvoiceList();

    }
    
}