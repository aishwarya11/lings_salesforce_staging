@isTest
public class ApprovedTest {
    Public static testmethod void ApproveProductCmp(){
        try{
        Product2 pdt = new Product2();
        pdt.Name = 'sample product';
        pdt.Product_Type__c = 'Bike';
        pdt.Insured_Value__c = 10;
        pdt.Status__c = 'New';
        insert pdt;
            
        Product2 pdt1 = new Product2();
        pdt1.Name = 'sample product123';
        pdt1.Product_Type__c = 'Laptop';
        pdt1.Insured_Value__c = 10;
        pdt1.Status__c = 'New';
        insert pdt1;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        insert con;
        
        //Updating object field
        Object__c obj = new Object__c();
        obj.Approved__c = false;
        obj.Product__c = pdt.Id;
        obj.Customer__c = con.id;
        obj.Insured_Value__c = 10;
        insert obj;
            
        //Updating object field
        Object__c obj1 = new Object__c();
        obj1.Approved__c = false;
        obj1.Product__c = pdt1.Id;
        obj1.Customer__c = con.id;
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        Approved.approveProduct(obj,obj.id);
        Approved.approveProduct(obj1,obj1.id);
        }catch(Exception ap){}
    }
}