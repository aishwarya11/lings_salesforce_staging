@RestResource(urlMapping='/getOkkPremium/*')
global with sharing class MobileGetOKKPremiumService {
    @HttpPost
    global static String OKKPre(String noOfDays){
        return PremiumCalculation.premium(noOfDays);
    }
}