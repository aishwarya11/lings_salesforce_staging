@RestResource(urlMapping='/product/*')
global with sharing class MobileProductServices {
    @HttpPost
    global static String search(String langCode, String srchKeyword, String category) {
        return UIHomeController.getProducts(langCode, srchKeyword, category);
    }
    
    @HttpGet
    global static void objectInfo() {
       
    }
}