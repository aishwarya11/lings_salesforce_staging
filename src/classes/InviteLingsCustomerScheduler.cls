global class InviteLingsCustomerScheduler implements Schedulable{     
    //Method to notify all the New user.
    global void execute(SchedulableContext sc) {
        // Instantiate PolicyExpires with parameters
        BatchInviteLingsCustomer batch = new BatchInviteLingsCustomer();
        database.executeBatch(batch, 5);
    } 
}