@isTest(SeeAllData=false)
public class BatchCustomerNotLoggedInTest {
    
    testmethod public static void testBatchCustomerNotLoggedInTest() { 		
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', IsActive = True,
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = true);
                
        insert u;    
        
		Test.setCreatedDate(u.Id, DateTime.newInstance(2019,06,31));
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con;         
        
        Test.startTest();
        
        BatchCustomerNotLoggedIn batch = new BatchCustomerNotLoggedIn();
        BatchCustomerNotLoggedIn.lastLogin = System.today().addMonths(-3);
        BatchCustomerNotLoggedIn.monthsBetween = 3;
        DataBase.executeBatch(batch, 20);      
        
        Test.stopTest();  
        
    }    

}