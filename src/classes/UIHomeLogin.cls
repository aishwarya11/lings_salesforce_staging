public without sharing class UIHomeLogin {
    public static String userNameSuffix = System.Label.UserNameSuffix;
    public String username {get; set;}
    public String password {get; set;}
    
    public UIHomeLogin(UIHomeController controller) {
        
    }
    //Login user
    @remoteAction
    public static String login(String langCode, String username, String password) {
         //LM-532 --Added new Parameter language code--20JULY2019
        boolean errFlag = false;
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        // Check whether the Site Login is enabled
        try{ 
            // MR - 20MAY19 - LM438 - Commented the below line for Incorrect Login error message
            //username = username.replaceAll(userNameSuffix, '');
            username = username + userNameSuffix;
            
            // Check user email verified or not
            User usr = [SELECT Id, Contact_Email_Verified__c, Email, Firstname, First_Login__c, UserName, ContactId, ResetPasswordURL__c FROM User WHERE UserName =:username LIMIT 1];
            
            if(!usr.First_Login__c){ 
                //06APR2019- To use the Forget password page itself for the FirstLogin- LM-353
                //For first Login Customer
                jsonGen.writeStringField('message', Constants.ERROR_FIRST_LOGIN.get('de'));
                //System.resetPassword(usr.Id , true);  --13MAY2019 To avoid the default reset Password page in Salesforce.
                //System.setPassword(usr.Id, 'LingsFamily2019');
                // Update the Contact with new password
                Date CreatedTime;
                Contact uContact = new Contact(Id=usr.ContactId);             
                uContact.Token__c = Utility.generateToken(32);
                uContact.Token_created__c = String.valueOf(System.now());                                             
                uContact.Password__c = passwordSet(usr.Id);
                update uContact;
                
                // Send Email
                List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
                //EmailTemplate emailTemp = [Select id from EmailTemplate where name='Reset Password FirstLogin' limit 1][0];
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setUseSignature(false);
                mail.setToAddresses(new String[] {Usr.Email});
                //mail.setTemplateId(emailTemp.Id);
                OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
                //mail.setWhatId(usr.ContactId);
                //LM-1269--FirstLogin cross reference Exception handeling--30MAR2020
                mail.setSubject('LINGS - Passwort zurück setzen');
                String messageBody = '<html><body>Salut '+Usr.FirstName+'<br><br>'+
                    'Wie bereits kommuniziert haben wir ein Update durchgeführt. Somit wird LINGS für dich in Zukunft noch schneller und noch sicherer. Damit alles reibungslos über die Bühne geht, musst du dein Passwort noch einmal neu setzen. Danach läuft für dich alles wieder wie gewohnt. <br><br>'+
                        'Dein Passwort kannst du <a href = "'+System.label.Lings_Community_link+'/de/members/resetting/'+uContact.Token__c+'">Hier</a>'+
                        '<br><br>Beste Grüsse<br>Larissa von LINGS<br><br>'+
            			'<img src = "https://lingscrm--c.documentforce.com/sfc/dist/version/download/?oid=00D1t000000vePK&ids=0681t00000FxK8PAAV&d=%2Fa%2F1t000000Xs7W%2FyJM.KOzqm..v3KBCb.p3zCHikmDzqcax_2MlAEoJYFs&operationContext=DELIVERY&viewId=05H1t000001G3iMEAS&dpt="/><br/>'+
           				'<br>'+'Generali Versicherungen'+'<br>'+'Soodmattenstrasse 4'+'<br>'+'8134 Adliswil'+'<br>'+'http://www.lings.ch<br>'+
            			'<br>'+'LINGS ist eine Marke der Generali Allgemeine Versicherungen AG, Avenue Perdtemps 23, 1260 Nyon'+'<br></body> </html>';
                mail.setHtmlBody(messageBody);                
                mail.setTargetObjectId(usr.ContactId);
                mail.setSaveAsActivity(true);
                
                mailList.add(mail);   
                
                Messaging.sendEmail(mailList); 
            } else if(!usr.Contact_Email_Verified__c) {
                errFlag = true;
                // Set Error Response
                jsonGen.writeStringField('error', 'Verify Email Address and Try logging in ');
                
            } else if(!Site.isLoginEnabled()) {
                errFlag = true;
                // Set Error Response
                jsonGen.writeStringField('error', 'Login is not enabled for this Community');
            } else {
                PageReference pgRef = Site.login(username, password, null);                 
                if(pgRef == null) {
                    // Set Error Response
                    errFlag = true;                                        
                    //26JUNE2019--This is used to track the Number of attempt remaining for the User per day
                    Map<String, Integer> getValue = new Map<String, Integer>();
                    getValue = numberOfAttempts(Usr.Id);
                    Integer Count = getValue.get('Triedattempt');
                    Integer LeftAttempt = getValue.get('Leftattempt');
                    //28June2019--When the attempt is 10 PasswordLockout error will be displayed--LM-465
                    if(Count == 0){
                        String LockedUser = 'SELECT Id, UserId , IsPasswordLocked  FROM UserLogin where IsPasswordLocked = true and UserId = \'' + usr.Id + '\'';                        
                        UserLogin PasswordLocked = database.query(LockedUser);
                        if(PasswordLocked != null){
                            jsonGen.writeStringField('message', Constants.ERROR_Locked_User.get('de'));  
                        } 
                    }else{
                        //jsonGen.writeStringField('message', Constants.ERROR_Login_Attempts.get('de')+Count+''+Constants.ERROR_Attempts_Remaining.get('de')+LeftAttempt);    
                    }                     
                    
                    //30MAY2019--This will happen for the Password Locked User--LM-465
                    /* if(Count > 9){
String LockedUser = 'SELECT Id, UserId , IsPasswordLocked  FROM UserLogin where IsPasswordLocked = true and UserId = \'' + usr.Id + '\'';                        
UserLogin PasswordLocked = database.query(LockedUser);
if(PasswordLocked != null){
jsonGen.writeStringField('message', Constants.ERROR_Locked_User.get('de'));  
}   
}   */                                                    
                } else {   
                    // Save the user password in Contact
                    Map<String, System.Cookie> cookieMap = pgRef.getCookies();
                    for(String keyCookie: cookieMap.keySet()) {
                        System.Cookie tempCookie = cookieMap.get(keyCookie);
                    } 
                    jsonGen.writeStringField('redirectURL', pgRef.getUrl());
                    Contact con = new Contact(id = usr.ContactId, Password__c = password);
                    Update con;
                }                  
            } 
            
            jsonGen.writeBooleanField('success', !errFlag);
            jsonGen.writeStringField('status', (errFlag ? 'failure' : 'success'));
            jsonGen.writeObjectField('user', UserInfo.getUserName());
            jsonGen.writeEndObject();
        } catch(Exception e) {
            jsonGen.writeStringField('message', Constants.ERROR_INCORRECT_DATA.get('de'));
            jsonGen.writeStringField('Exception', e.getMessage());
            Utility.sendException(e, 'USER CANNOT LOGIN IN THE UIHOMELOGIN');
        }
        return jsonGen.getAsString(); 
        
    }
    
    // MR - 27MAY19 - Dynamic Password
    public static String passwordSet(String UserId){    
        String dynamicPassword = '';
        String chars = 'abcdefghijklmnopqrstuz12345678910';
        
        Integer idx = 0;
        for(Integer i=0; i<16; i++) {
            idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            dynamicPassword += chars.substring(idx, idx+1);            
        }        
        
        System.setPassword(UserId, dynamicPassword); 
        
        return dynamicPassword;      
    }
    
    //LM-465--This is used to track the Number of attempt remaining for the User per day--26JUNE2019
    public static Map<String, Integer> numberOfAttempts(String UsrId){ 
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        Map<String, Integer> getValue = new Map<String, Integer>();
        List<LoginHistory> LoginAttempt = [SELECT Id, LoginTime, LoginType, LoginUrl, NetworkId, Status, UserId FROM LoginHistory where UserId = :UsrId ];
        Integer Count = 0;
        Integer leftOutAttempts = 0;
        Integer m = 0;
        for(LoginHistory attempt : LoginAttempt){
            
            if(attempt.Status == 'Invalid Password'){
                Count = Count + 1; 
                m = math.mod(Count, 10);
                leftOutAttempts = 10 - m;   
            }else if(attempt.Status == 'Success'){
                Count = 0;
                leftOutAttempts = 10;
                m = 0;
            }                 
        }         
        getValue.put('Triedattempt', m);
        getValue.put('Leftattempt', leftOutAttempts);
        return getValue;
    }
    //Forget Password
    @remoteAction
    public static String resetPassword(String langCode, String username) {
        //LM-532 --Added new Parameter language code--20JULY2019
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject(); 
        Boolean errFlag = false;
        Date CreatedTime;
        
        try {//1.ExceptionHandling when Username is null and contactId is null. 
            // Read the User and use that contact id in below query
            String userNamewithSuffix = username + userNameSuffix;
            if(userNamewithSuffix != null) {
                // 27MAY19 - Changed for Customer directly doing the ResetPassword instead of Login
                String query = 'Select Id, ContactId, First_Login__c from User where username= \'' +userNamewithSuffix + '\'limit 1';
               //LM-709--Invalid E-Mail Address --01OCT2019
                List<User> usrCont = Database.query(query);
                if(usrCont.size() == 0){
                    errFlag = true;
                   jsonGen.writeStringField('message', Constants.ERROR_USERNAME.get('de')); 
                }
                User usrContact = usrCont[0]; //LM-709--Changed to list to check the query size --04OCT2019
                String usrContactId = usrContact.ContactId;
                
                // 1. Get Contact info using the username
                String queryString = 'SELECT id, name, FirstName, Token__c, Token_created__c,Password__c, Email, Reset_Password__c,Reset_Password_URL__c from Contact WHERE id = \'' + usrContactId + '\' LIMIT 1';
                contact cont = Database.query(queryString);
                if(cont.Id != null) {
                    // If Password field is null make the user to reset the password
                    // 27MAY19 - Changed for Customer directly doing the ResetPassword instead of Login
                    if(cont.Password__c == null || !usrContact.First_Login__c) { //16MAY2019 --When Contact Password is null set default Password 
                        //System.resetPassword(UserInfo.getUserId(), true);
                        //27MAY2019 -- To create a dynamic password for the each User.
                        cont.Password__c = passwordSet(usrContact.Id);      // 30MAY19 - Setting password for right user
                    }
                    // 2. Update the Token and Flag in the Contact
                    //26JUNE2019--One Token creation for one password change.
                    
                    if(cont.Token__c == null || cont.Token_created__c == null){
                        cont.Token__c = Utility.generateToken(32);
                        cont.Token_created__c = String.valueOf(System.now());   
                    } else { //LM-788 -- Generating the token when the token created time is greater than one day --20NOV2019
                        CreatedTime = Date.valueOf(cont.Token_created__c);                        
                    }                   
                    //LM-788 -- Generating the token when the token created time is greater than one day --20NOV2019
                    if(CreatedTime != NULL && System.today() > CreatedTime) {
                        cont.Token__c = Utility.generateToken(32);
                        cont.Token_created__c = String.valueOf(System.now());                         
                    }
                    //cont.Reset_Password__c = true;
                    update cont;
                    //LM-474--Send FORGET PASSWORD email to the Customer From Process Builder to apex --26AUG2019.
                    
                    List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
                   // EmailTemplate template = [Select id from EmailTemplate where name=:'Password-reset' LIMIT 1];   
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setUseSignature(false);
                    mail.setToAddresses(new String[] { cont.Email });
                    //mail.setTemplateId(template.Id);
                    OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                    mail.setOrgWideEmailAddressId(owea.get(0).Id); 
                    mail.setSubject('LINGS - Passwort zurück setzen');
                    String messageBody = '<html><body>Salut '+cont.FirstName+'<br><br>Du möchtest dein Passwort zurücksetzen? <br><br>'+
                        'Auch das funktioniert bei uns mit nur einem Klick. Und zwar <a href = "'+System.label.Lings_Community_link+'/de/members/resetting/'+cont.Token__c+'">hier</a>'+
                        '<br><br>Liebe Grüsse<br>Larissa von LINGS<br><br>'+
            			'<img src = "https://lingscrm--c.documentforce.com/sfc/dist/version/download/?oid=00D1t000000vePK&ids=0681t00000FxK8PAAV&d=%2Fa%2F1t000000Xs7W%2FyJM.KOzqm..v3KBCb.p3zCHikmDzqcax_2MlAEoJYFs&operationContext=DELIVERY&viewId=05H1t000001G3iMEAS&dpt="/><br/>'+
           				'<br>'+'Generali Versicherungen'+'<br>'+'Soodmattenstrasse 4'+'<br>'+'8134 Adliswil'+'<br>'+'http://www.lings.ch<br>'+
            			'<br>'+'LINGS ist eine Marke der Generali Allgemeine Versicherungen AG, Avenue Perdtemps 23, 1260 Nyon'+'<br></body> </html>';
                    mail.setHtmlBody(messageBody);
                    mail.setSaveAsActivity(true);                    
                    //mail.setTargetObjectId(usrContactId);
                    //mail.setSaveAsActivity(true);
                    mailList.add(mail);   
                    try{
                        Messaging.sendEmail(mailList); 
                        jsonGen.writeStringField('status', 'success');
                        jsonGen.writeStringField('message', 'Mail is send to '+ username);                        
                    }catch(Exception cl){
                        jsonGen.writeStringField('status', 'failure');
                        jsonGen.writeStringField('user', UserInfo.getUserName());
                        utility.sendException(cl, 'FORGET PASSWORD MAIL FAILED TO SEND '+usrContactId+'  '+cl.getStackTraceString());
                    }                                                                    
                }
            }
        } catch(Exception e) {
            jsonGen.writeStringField('status', 'failure');
            if(errFlag == FALSE) { //LM-709--To display the Correct error message -- 04OCT2019
                jsonGen.writeStringField('message', e.getMessage());
                Utility.sendException(e, 'PASSWORD RESET FAILED for the Customer');
            }                        
        }
        
        jsonGen.writeEndObject();
        return jsonGen.getAsString();     
    }
}