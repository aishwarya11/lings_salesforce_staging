//This Batch will run at the Start of the Month to renew the OnDemand policy. This Batch should run after the BatchPolicyExpire.
global class BatchOnDemandRenewal implements Database.Batchable<Object__c>, Database.Stateful {
    // MR - 05SEP19 - LM-660 Fix
    public Date lastMonthEndDate = null;
    public Datetime lastMonthEndDT = null;
    public List<Policy__c> successPolicyCloseList = new List<Policy__c>();
    public List<Policy__c> failedPolicyCloseList = new List<Policy__c>();
    public List<Policy__c> successPolicyCreateList = new List<Policy__c>();    
    public List<Policy__c> failedPolicyCreateList = new List<Policy__c>();
    public List<Object__c> successObjectUpdateList = new List<Object__c>();
    public List<Object__c> failedObjectUpdateList = new List<Object__c>();
    
    //Start Batchable
    global Iterable<Object__c> start(Database.BatchableContext BC) {
        System.debug('BatchOnDemandRenewal START');
        // MR - 05SEP19 - LM-660 Fix
        lastMonthEndDate = System.today().toStartofMonth().addDays(-1);
        lastMonthEndDT = Datetime.newInstance(lastMonthEndDate, Utility.policyEndTime);
        
        List<Object__c> query =[Select Id, Insured_value__c, Premium_OnDemand__c, Premium_flatrate__c, Customer__c,
                                Flatrate_savings__c, Comments__c, Product__c, Product__r.Insured_value__c, Product__r.Source__c,
                                Product__r.Ondemand_premium__c, Product__r.flatrate_premium__c, 
                                Product__r.flatrate_savings__c, Flatrate_renewal_flag__c, Coverage_Type__c,
                                Policy__c, Policy__r.Active__c, Policy__r.Start_date__c, Policy__r.End_date__c,
                                Policy__r.Status__c, Policy__r.Premium__c, Policy__r.Insured_value__c, Policy__r.Insurance_type__c,
                                Policy__r.Invoice_start_date__c, Insurance_Start_Date__c,Insurance_End_Date__c from Object__c 
                                where Coverage_Type__c = 'OnDemand' AND Is_Deleted__c = false AND policy__c != '' AND 
                                Policy__r.Active__c = TRUE AND Policy__r.Status__c = 'Insured' AND 
                                Policy__r.Invoice_Start_Date__c = LAST_MONTH AND 
                                (Policy__r.End_date__c = NULL OR Policy__r.End_date__c >= LAST_MONTH)
                                //Reva - To ignore the renewal of OKK and RVG Policies from OnDemandRenewal process
                               	AND Product__c <>: System.Label.OKKProductId AND Product__c <>: System.Label.RVGProductId];
        return query;
    }
    
    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Object__c> scope) {
        System.debug('BatchOnDemandRenewal EXECUTE');
        
        Map<Id, Object__c> updatedObjectMap = new Map<Id, Object__c>();
        List<Policy__c> closePolicyList = new List<Policy__c>();
        List<Policy__c> createPolicyList = new List<Policy__c>();
        DateTime ed;
        Integer diffDays;
        Integer chargedDays; 
        
        for(Object__c obj : scope) {            
            //LingsObject Should not be insured in the Flatrate type
            if(obj.Coverage_Type__c == Constants.ONDEMAND_STRING) {
                //Closing the already existing Policy for the On-Demand and creating the new Policy- 06APR2019
                Policy__c pol = new Policy__c(id = obj.Policy__c);
                pol.Active__c = false;
                pol.End_Date__c = lastMonthEndDT;                                           // MR - 05SEP19 - LM-660 Fix
                pol.Status__c = Constants.POL_STATUS_EXPIRED;
                pol.Comments__c = 'BatchOnDemandRenewal Closing Policy for last Month';     // MR - 05SEP19 - LM-660 Fix
                closePolicyList.add(pol);
                
                //Create a new Policy
                Policy__c newPol = new Policy__c();
                newPol.Object__c = obj.Id;
                newPol.Contact__c = obj.Customer__c;
                newPol.Active__c = True;
                newPol.Insurance_Type__c = Constants.ONDEMAND_STRING;
                newPol.Status__c = Constants.POL_STATUS_INSURED;
                DateTime getDate = pol.End_Date__c.addDays(1);
                newPol.Start_Date__c = Datetime.newInstance(getDate.date(), Utility.policyStartTime);
                newPol.Invoice_Start_Date__c = getDate.date();
                
                //when end date is not null update the end date - This will apply for the event LM-362 09APR2019
                if(obj.Policy__r.End_date__c != null){
                    newPol.End_Date__c = Datetime.newInstance(obj.Policy__r.End_date__c.date(), Utility.policyEndTime);
                    newPol.Policy_Source__c = 'Event';
                    ed = newPol.End_Date__c;
                } else{
                    newPol.Policy_Source__c = 'Object';
                    ed = newPol.Start_Date__c.date().addMonths(1).toStartofMonth().addDays(-1); 
                }
                
                //18JULY2019--Calculate the Policy Charges.
                diffDays =  newPol.Invoice_Start_Date__c.daysBetween(ed.Date());
                chargedDays = diffDays + 1;                                                    
                
                Obj.Insurance_Start_Date__c = newPol.Start_Date__c.date();
                if(newPol.End_Date__c != null) {
                    obj.Insurance_End_Date__c = newPol.End_Date__c.date();   
                }                
                
                //Ignore the Price Update of the Product when it is Bike.
                if(obj.Product__r.Source__c != 'DB Bike'){
                    obj.Insured_value__c = obj.Product__r.Insured_value__c;
                    obj.Premium_OnDemand__c = obj.Product__r.Ondemand_premium__c;
                    obj.Premium_flatrate__c = obj.Product__r.flatrate_premium__c;
                    obj.Flatrate_savings__c = obj.Product__r.flatrate_savings__c;
                }              
                
                updatedObjectMap.put(obj.Id, obj);
                newPol.Insured_Value__c = obj.Insured_Value__c;
                newPol.Premium__c = obj.Premium_OnDemand__c;   
                //LM-695--Rounded the policy charges--25SEPT2019                               
                newPol.Policy_Charges__c = Utility.getRoundedPremium(newPol.Premium__c * chargedDays); 
                newPol.Comments__c = 'BatchOnDemandRenewal Creating Policy for this Month';    // MR - 05SEP19 - LM-660 Fix
                createPolicyList.add(newPol);                    
            }                
        }
        try{
            if(closePolicyList.size() > 0){
                List<Database.SaveResult> srUpdate = Database.update(closePolicyList);
                Integer polIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {            
                    Policy__c pol = closePolicyList[polIndex];
                    if(sr.isSuccess()) {
                        successPolicyCloseList.add(pol);  
                        System.debug('Policy is Closed Successfully!!');
                    } else {
                        failedPolicyCloseList.add(pol);
                    }
                    
                    polIndex++;
                }
            }
        }catch(Exception cp){
            utility.sendException(cp,'CLOSE POLICY FAILED IN ONDEMAND RENEWAL');
        }
        
        try{
            if(createPolicyList.size() > 0){
                List<Database.SaveResult> srInsert = Database.insert(createPolicyList);
                Integer polIndex = 0;  
                for(Database.SaveResult sr: srInsert) {
                    Policy__c pol = createPolicyList[polIndex];
                    if(sr.isSuccess()) {
                        // Update Object with the new Policy - 12APR2019
                        Object__c updobj = updatedObjectMap.get(pol.Object__c);
                        updobj.Policy__c = sr.getId();   
                        updobj.Is_Insured__c = true;    // MR - 03SEP19 - LM-639 Fix
                        updObj.Comments__c = 'BatchOnDemandRenewal updating latest policy in the object';
                        
                        updatedObjectMap.put(updObj.Id, updobj);
                        System.debug('New Policy is Created Successfully');
                        successPolicyCreateList.add(pol);                    
                    } else {
                        failedPolicyCreateList.add(pol);
                    }
                    
                    polIndex++;
                }
            }
        }catch(Exception np){
            utility.sendException(np, 'NEW POLICY CREATION FAILED IN THE ONDEMAND RENEWAL');
        }
        
        try{
            if(updatedObjectMap.size() > 0){
                List<Object__c> objList = updatedObjectMap.values();
                List<Database.SaveResult> srUpdate = Database.update(objList);
                Integer objIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Object__c obj = objList[objIndex];
                    if(sr.isSuccess()) {
                        successObjectUpdateList.add(obj);  
                        System.debug('Object Updated Successfully !!');
                    } else {
                        failedObjectUpdateList.add(obj);
                    }
                    
                    objIndex++;
                }
            }        
        } catch(Exception uo) {
            utility.sendException(uo, 'OBJECT UPDATE FAILED IN ONDEMAND RENEWAL');
        }
    }    
    // Finish Batchable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchOnDemandRenewal FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 22MAY19 - Code Optimization
        mail.setSubject('BatchOnDemandRenewal ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         
        
        String body = 'BatchOnDemandRenewal Status on Closing/Creating Policies and Updating Object <br/>';
        body += '<br/> ' + (successPolicyCloseList.size() + failedPolicyCloseList.size()) + ' number of Policy Closure records processing were completed.'; 
        if(successPolicyCloseList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy Closure details <br/>';
            
            for(Policy__c successList: successPolicyCloseList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy Closure happened.';
        }
        
        if(failedPolicyCloseList.size() > 0) {
            body += '<br/> Refer the below list of Failed Policy Closure details <br/>';
            
            for(Policy__c failedList: failedPolicyCloseList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed Policy Closure processing happened.';
        }
        
        // New Policy Creation List   
        body += '<br/> ' + (successPolicyCreateList.size() + failedPolicyCreateList.size()) + ' number of new Policy creation processing were completed.'; 
        
        if(successPolicyCreateList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy Creation details <br/>';
            
            for(Policy__c successList: successPolicyCreateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy Creation happened.';
        }
        
        if(failedPolicyCreateList.size() > 0){
            body += '<br/> Refer the below list of Failed Policy Creation details <br/>';
            
            for(Policy__c failedList: failedPolicyCreateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Policy Creation happened.';
        }
        
        // Object Updation List
        body += '<br/> ' + (successObjectUpdateList.size() + failedObjectUpdateList.size()) + ' number of Object update records processing were completed.'; 
        if(successObjectUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Object Updation details <br/>';
            
            for(Object__c successList: successObjectUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Objects update processing happened.';
        }
        
        if(failedObjectUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Object Updation details <br/>';
            
            for(Object__c failedList: failedObjectUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Objects update processing happened.';
        }
        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);       
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedObjectUpdateList.size() > 0 || failedPolicyCreateList.size() > 0 || failedPolicyCloseList.size() > 0) {
                Messaging.sendEmail(mails);
            }
            //LM-699--Populate big object --26SEPT2019
            Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
            ab.Audit_Name__c = 'BatchOnDemandRenewal';
            ab.Track_message__c = body;
            database.insertImmediate(ab);
        }
    }         
}