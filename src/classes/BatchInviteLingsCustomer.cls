//This batch is used to send Email to the customer after 3 days they create a Lings account --Lings 2.0
global class BatchInviteLingsCustomer implements Database.Batchable<Contact>{
    public Integer totalMailProcessedCount = 0;
    public Integer failedMailProcessedCount = 0;
    public Integer successMailCount = 0;    
    //Start batchable
    global Iterable<Contact> start(Database.BatchableContext BC) {
        System.debug('BatchInviteLingsCustomer START');
        
        List<Contact> query = [SELECT Id, Name, SignUp__c, Register_confirmed__c, Email_verified__c, CreatedDate,
                               Email, Activated_Three_Days_Ago__c FROM Contact WHERE CreatedDate = LAST_N_DAYS:3
                               AND SignUp__c = TRUE AND Email_verified__c = TRUE and Activated_Three_Days_Ago__c = TRUE];	//LM-689--Created a formula field and added to the query--24SEPT2019
        return query;
    }    
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        System.debug('BatchInviteLingsCustomer EXECUTE');
        //LM-689--Removed the condition from the Query and added it in the if Condition--23SEPT2019
        //Date mailAfterThreeDays = System.today().addDays(-3);
        for(Contact con : scope) {
            String contactId = con.Id;
            EmailTemplate template = [Select id from EmailTemplate where name=:'Mail to customers after 3 day' limit 1][0];
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { con.Email });
            mail.setTemplateId(template.Id);
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
            mail.setTargetObjectId(contactId);
            mail.setSaveAsActivity(true);
            
            mailList.add(mail);
            successMailCount++;
            
            try {
                totalMailProcessedCount += mailList.size();
                // Send the emails
                if(!Test.isRunningTest()) {
                    Messaging.sendEmail(mailList); 
                }
            } catch(Exception er) {
                failedMailProcessedCount++;
                // Send exception email to Admin
                Utility.sendException(er, 'BatchInviteLingsCustomer MAIL NOT SEND');
            }                                            
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        If(Test.isRunningTest()){
            ID jobID = Database.executeBatch(new BatchInviteLingsCustomer(), 200);
            System.abortJob(jobID);
        }          
        System.debug('BatchInviteLingsCustomer FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 23MAY19 - Code Optimization
        mail.setSubject('BatchInviteLingsCustomer ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);   
        
        String body = 'BatchInviteLingsCustomer Status on Sending email to the user<br/>';
        
        // Processing Email list 
        body += '<br/> ' + totalMailProcessedCount + ' number of Email processing were completed.';  
        
        // Email Processing for BatchInviteLingsCustomer.
        if(successMailCount > 0) {
            body += '<br/> successful Emails were processed for the BatchInviteLingsCustomer = '+successMailCount+'<br/>';
        } else {
            body += '<br/> No Email processing were happened for the BatchInviteLingsCustomer';
        }       
        // Failed Email Processing
        if(failedMailProcessedCount > 0) {
            body += '<br/> Failed Email processing count = '+ failedMailProcessedCount + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing were happened.';
        }
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedMailProcessedCount > 0 ) {
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchInviteLingsCustomer';
        ab.Track_message__c = body;
        database.insertImmediate(ab);        
        }
    }    
}