//Remainder E-mail to the customer before the 1 Weeks of the Warranty Expires.Lings 2.0
global class BatchWarrantyRemainder implements Database.Batchable<Object__c>{
public Integer totalMailProcessedCount = 0;
public Integer failedMailProcessedCount = 0;
public Integer successMailCount = 0;
//Start batchable
global Iterable<Object__c> start(Database.BatchableContext BC) {
    System.debug('BatchWarrantyRemainder START');      
    
    List<Object__c> query = [SELECT Id, Name, Warranty_date__c, Customer__c, Product__c, 
                                Customer__r.Email, Is_Deleted__c, Approved__c, Claim__c 
                                FROM Object__c WHERE Warranty_date__c != NULL AND Is_Deleted__c = FALSE];
    return query;
} 

//Execute batchable
global void execute(Database.BatchableContext BC, List<Object__c> scope) {
    System.debug('BatchWarrantyRemainder EXECUTE');
    Date getDate;
    List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
    for(Object__c obj : scope){
        //Sent the remainder email before 7 days.
        getDate = obj.Warranty_Date__c.addDays(-7);	
        System.debug('Object-->'+ obj);
        System.debug('getDate-->'+ getDate);
        System.debug('Date'+System.today());            
        if(getDate == System.today()){
            System.debug('getDate-->'+ getDate);
            System.debug('Date'+System.today());
            // Add template and send email
            EmailTemplate emailTemp = [Select id from EmailTemplate where name='Warranty running out' limit 1][0];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { obj.Customer__r.Email });
            mail.setTemplateId(emailTemp.Id);
            mail.setWhatId(obj.Id);
            System.debug('Object id-->'+ obj.id);
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
            mail.setTargetObjectId(obj.Customer__c);
            System.debug('Object customer-->'+ obj.Customer__c);
            mail.setSaveAsActivity(true);
            
            mailList.add(mail);   
            successMailCount++;
        }
    } 
    try {
        totalMailProcessedCount += mailList.size();
        // Send the emails
        if(!Test.isRunningTest()) {
            Messaging.sendEmail(mailList); 
        }
    } catch(Exception er) {
        failedMailProcessedCount++;
        // Send exception email to Admin
        Utility.sendException(er, 'WARRANTY REMAINDER MAIL NOT SEND');
    }         
}

// Finish Batachable
global void finish(Database.BatchableContext BC) {
    System.debug('BatchWarrantyRemainder FINISH');
    
    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 23MAY19 - Code Optimization
    mail.setSubject('BatchWarrantyRemainder ran on ' + System.now());
    //17JUNE2019--Sender Name in the Batch status Mail. LM-496
    OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
    mail.setOrgWideEmailAddressId(owea.get(0).Id);   
    
    String body = 'BatchWarrantyRemainder Status on Sending email to the user<br/>';
    
    // Processing Email list 
    body += '<br/> ' + totalMailProcessedCount + ' number of Email processing were completed.';  
    
    // Email Processing for BatchWarrantyRemainder.
    if(successMailCount > 0) {
        body += '<br/> successful Emails were processed for the BatchWarrantyRemainder = '+successMailCount+'<br/>';
    } else {
        body += '<br/> No Email processing were happened for the BatchWarrantyRemainder';
    }       
    // Failed Email Processing
    if(failedMailProcessedCount > 0) {
        body += '<br/> Failed Email processing count = '+ failedMailProcessedCount + ' <br/>';
    } else {
        body += '<br/> No Failed Email processing were happened.';
    }
    body += '<br/> <br/> Thanks';
    mail.setHtmlBody(body);
    mails.add(mail);         
    
    //LM-696--Send mail only when there is a failure --25SEPT2019
    if(!Test.isRunningTest()) {
        if (failedMailProcessedCount > 0) {
            Messaging.sendEmail(mails);
        }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchWarrantyRemainder';
        ab.Track_message__c = body;
        database.insertImmediate(ab);          
    }
}
}