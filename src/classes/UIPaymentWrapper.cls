//Apex class as UIPaymentWrapper class
public class UIPaymentWrapper {
    Public UIPaymentWrapper() {
        
    }
    
    //InnerClass1
    public class SPInsertResponse {
        public SPResponseHeader responseHeader;
        public String token;
        public Datetime expiration;
        public String redirectURL;
        
        public SPInsertResponse() {
            responseHeader = new SPResponseHeader();
        }
    }

    public class SPTxnInitResponse {
        public SPResponseHeader responseHeader;
        public String token;
        public Datetime expiration;
        public SPRedirect redirect;
        
        public SPTxnInitResponse() {
            responseHeader = new SPResponseHeader();
        }
    }
    
    //InnerClass2
    public class SPAliasResponse {
        public SPResponseHeader responseHeader;
        public SPAlias alias;   
        public SPPaymentInfo paymentMeans;
        public String ErrorMessage;
        public String Behavior;
        public String ErrorName;
        public List<String> ErrorDetail;
        
        public SPAliasResponse() {
            responseHeader = new SPResponseHeader();
        }
    }
    
    //InnerClass3
    public class SPPaymentResponse {
        public SPResponseHeader responseHeader;
        public SPTransaction txn;   
        public SPPaymentInfo paymentMeans;
        public String ErrorMessage;
        public String Behavior;
        public String ErrorName;
        public List<String> ErrorDetail;
        
        public SPPaymentResponse() {
            responseHeader = new SPResponseHeader();
        }
    }

    public class SPTxnPaymentResponse {
        public SPResponseHeader responseHeader;
        public SPTransaction txn;   
        public SPPaymentInfo paymentMeans;
        public SPPayer payer;
        public SPRegistrationResult registrationResult;
        public SPLiability liability;
        public String ErrorMessage;
        public String Behavior;
        public String ErrorName;
        public List<String> ErrorDetail;
        
        public SPTxnPaymentResponse() {
            responseHeader = new SPResponseHeader();
        }
    }
    
    //02MAY2019-Transaction Cancel
    public class SPTransactionCancel{
        public SPResponseHeader responseHeader;
        public String TransactionId;
        public String OrderId;
      //  public DateTime Date;
    }
    
    //13MAY2019 --Transaction Capture
    public class SPTxnCapture{
        public SPResponseHeader responseHeader; 
        public String CaptureId;
        public String Status;
        public String CaptureDate;
        public String ErrorMessage;
        public String Behavior;
        public String ErrorName;
        public List<String> ErrorDetail;        
    }
    
    //InnerClass4
    public class SPResponseHeader {
        public String SpecVersion;
        public String RequestId;
    }
    
    //InnerClass5
    public class SPAlias {
        public String id;
        public Integer lifetime;
    }
    
    //InnerClass6
    public class SPPaymentInfo {
        public SPBrandInfo brand;
        public String displayText;
        public SPCardInfo card;
    }
    
    //InnerClass7
    public class SPBrandInfo {
        public String PaymentMethod;
        public String Name;
    }
    
    //InnerClass8
    public class SPCardInfo {
        public String MaskedNumber;
        public String ExpYear;
        public String ExpMonth;
        public String HolderName;
        public String CountryCode;
    }
    
    //InnerClass9
    public class SPTransaction {
        public String type;
        public String status;
        public String id;
        public String txnDate;
        public SPAmount amount;
        public String acquirerName;
        public String acquirerReference;
        public String sixTransactionReference;
        public String approvalCode;
    }
    
    //InnerClass10
    public class SPAmount {
        public String value;
        public String currencyCode = 'CHF';
    } 
           
    public class SPRedirect {
        public String redirectURL;
        public boolean paymentMeansRequired;
    }

    public class SPPayer {
        public String ipAddress;
        public String ipLocation;
    }

    public class SPRegistrationResult {
        public boolean success;
        public SPAlias alias; 
        public SPError error;  
    }

    public class SPError {
        public String errorName;
        public String errorMessage;
    }

    public class SPLiability {
        public boolean liabilityShift;
        public String liableEntity;
        public SP3D threeDs;
        public SPFraudFree fraudFree;
    }

    public class SP3D {
        public boolean authenticated;
        public boolean liabilityShift;
        public String xid;
        public String verificationValue;
    }

    public class SPFraudFree {
        public String id;
        public boolean liabilityShift;
        public decimal score;
        public List<String> investigationPoints;
        public String errorMessage;
    }
}