@isTest
public class BatchPolicyRenewalTest {
    
    testmethod public static void testBatchPolicyRenewalTest() {
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        insert con;
        
       Product2 PO= new Product2();
		PO.Name='sample product';
		PO.Price__c=36;
		PO.Insured_Value__c=100;
		insert PO;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;

        
        Object__c obj = new Object__c();
        obj.Product__c=PO.Id;
        obj.Customer__c=con.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c= 'FlatRate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today();
        //obj.Claim__c = CL.Id;
        insert obj;
        
        
        Claim__c CL = new Claim__c();
        CL.Contact__c = con.Id;
        CL.Object__c = obj.Id;
        insert CL;
        
        Object__c obj1 = new Object__c();
        obj1.Product__c=PO.Id;
        obj1.Customer__c=con.Id;
        obj1.Insured_Value__c=100;
        obj1.Included_Date__c=System.today();
        obj1.FlatRate_Savings__c=50;
        obj1.Coverage_Type__c='FlatRate';
        obj1.Premium_FlatRate__c=10.00;
        obj1.Premium_OnDemand__c=6.00;
        obj1.Flatrate_Renewal_Flag__c=false;
        obj1.Insurance_Start_Date__c=System.today();
        obj1.Insurance_End_Date__c=System.today();
        obj1.Claim__c = CL.Id;
        insert obj1;
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Start_Date__c=System.today()- 1;
        pol.Invoice_Start_Date__c = System.today() -1;
        pol.End_Date__c=System.today();
        pol.Active__c=true;
        pol.Status__c='Insured';
        pol.Insurance_Type__c='FlatRate';
        pol.Premium__c=100;
        pol.Object__c=obj.id;
        insert pol;
        
        Policy__c pol1 = new Policy__c();
        pol1.Contact__c=con.Id;
        pol1.Start_Date__c=System.today() -1 ;
        pol1.Invoice_Start_Date__c = System.today() -1;
        pol1.End_Date__c=System.today();
        pol1.Active__c=false;
        pol1.Status__c = Constants.POL_STATUS_EXPIRED;
        pol1.Insurance_Type__c='FlatRate';
        pol1.Premium__c=100;
        pol1.Object__c=obj1.id;
        insert pol1;                    
        
        Test.startTest();
        
        BatchPolicyRenewal BCE = new BatchPolicyRenewal(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        
    }
    
    testmethod public static void testBatchPolicyRenewalTest1() {
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        insert con;
        
        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
        
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='FlatRate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;      
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.Premium__c = 10.00;   
        pol.Start_Date__c = System.today();
        pol.End_Date__c= System.today();
        pol.Invoice_Start_Date__c = System.today();
        pol.Active__c=true;        
        pol.Insurance_Type__c='FlatRate';
        pol.Premium__c=100;
        insert pol; 
                     
        
        Test.startTest();
        
        BatchPolicyRenewal BCE = new BatchPolicyRenewal(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        
    }
    
}