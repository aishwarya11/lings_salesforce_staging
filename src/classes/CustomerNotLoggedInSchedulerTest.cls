@isTest
public class CustomerNotLoggedInSchedulerTest {
    testmethod public static void testUniqueUserName() {
        
        //Instantiate the InviteLingsCustomerScheduler apex class
        CustomerNotLoggedInScheduler CNL = new CustomerNotLoggedInScheduler();
        CNL.execute(null);
    }

}