@isTest
public class FreezeUserAccountTest {

    testmethod public static void TestFreezeUserAccountTest() { 
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        con.Freeze__c = False;
        insert con;         
    
        FreezeUserAccount.freezeAccount(con, con.Id);
    }
    
    testmethod public static void TestFreezeUserAccountTest1() { 
    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        con.Freeze__c = True;
        insert con;         
    
        FreezeUserAccount.freezeAccount(con, con.Id);
    }    
}