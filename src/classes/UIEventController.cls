public class UIEventController implements DataHandler {
    @testvisible private String contactId = '';
    
    public UIEventController() {
        
    }
    
    public UIEventController(String contact) {
        this.contactId = contact;
    }
        
    
    public void setContext(String cId) {
        this.contactId = cId;
    }
    
    //get the event__c records which are all have enddate on today or greater than today
    public String getRecords() {
        List<UIObjectWrapper.EventWithObject> respList = new List<UIObjectWrapper.EventWithObject>();
        String queryString = 'Select Id, IsOKKObjPresent__c,IsRVGObjPresent__c, Name, Start_Date__c, End_Date__c, Contact__r.Freeze__c, ' + 
            ' (SELECT Id, Object__c, Object__r.Product_Name__c, Object__r.Premium_OnDemand__c, ' +
            'Object__r.Premium_FlatRate__c, Object__r.Policy__c, Object__r.Coverage_Type__c, Object__r.Claim__c,' +
            'Object__r.Product__r.DisplayURL from Event_Objects__r WHERE Object__r.Is_Deleted__c = FALSE)' +
            ' FROM Event__c ' +
            ' WHERE Contact__c = \'' + this.contactId + '\' AND End_Date__c >= TODAY order by Start_date__c';
        // "insuranceRate":{"daily":0.4,"monthly":12,"currencyCode":"CHF"}
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();

        for(Event__c event: Database.query(queryString)) {
            UIObjectWrapper.EventWithObject eoWrap = new UIObjectWrapper.EventWithObject();
            eoWrap.uuid = event.Id;
            eoWrap.Name = event.Name;
            eoWrap.periodFrom = event.Start_Date__c;
            eoWrap.periodTo = event.End_Date__c;
            eoWrap.IsOKKObjPresent = event.IsOKKObjPresent__c;
            eoWrap.IsRVGObjPresent = event.IsRVGObjPresent__c;
            eoWrap.insuredProducts = new List<UIObjectWrapper.LingsObject>();
            for(Event_Object__c eo: event.Event_Objects__r) {
                UIObjectWrapper.LingsObject objWrap = new UIObjectWrapper.LingsObject();
                objWrap.uuid = eo.Object__c;
                objWrap.name = eo.Object__r.Product_Name__c;
                objWrap.thumbnail = eo.Object__r.Product__r.DisplayURL;
                objWrap.insuranceRate.daily = eo.Object__r.Premium_OnDemand__c;
                objWrap.flatRate.daily = eo.Object__r.Premium_FlatRate__c;
                String typ = eo.Object__r.Coverage_Type__c;

                if(typ.equalsIgnoreCase('ondemand')) {
                    objWrap.insuranceRate.enabled = true;
                } else if(typ.equalsIgnoreCase('flatrate')) {
                    objWrap.flatRate.enabled = true;
                } 
                eoWrap.insuredProducts.add(objWrap);
            }
            respList.add(eoWrap);
        }
        
        jsonGen.writeObjectField('events', respList);
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString();    
    }
    
    //read the records of the event__c
    public String readRecords(String inputJSON) {
        UIObjectWrapper.EventWithObject eoWrap = new UIObjectWrapper.EventWithObject();
        String queryString = 'Select Id,OKKChildEventId__c, Name, Start_Date__c, End_Date__c, Contact__r.Freeze__c,'+
            'RVGChildEventId__c,OKKTVId__c, ' + 
            '(SELECT Id, Object__c, Object__r.Product_Name__c, Object__r.Premium_OnDemand__c, Object__r.Claim__c,' +
            'Object__r.Premium_FlatRate__c, Object__r.Policy__c, Object__r.Coverage_Type__c, ' +
            'Object__r.Product__r.DisplayURL from Event_Objects__r WHERE Object__r.Is_Deleted__c = FALSE)' +
            ' FROM Event__c ' +
            ' WHERE Id = \'' + inputJSON + '\'';
        // "insuranceRate":{"daily":0.4,"monthly":12,"currencyCode":"CHF"}
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
	        
        for(Event__c event: Database.query(queryString)) {
            eoWrap.uuid = event.Id;
            eoWrap.Name = event.Name;
            eoWrap.periodFrom = event.Start_Date__c;
            eoWrap.periodTo = event.End_Date__c;
            eoWrap.insuredProducts = new List<UIObjectWrapper.LingsObject>();
            //Reva - this flag is used to show the OKK and RVG info message in Event View page on First View
            if(event.OKKChildEventId__c != null && event.OKKChildEventId__c != ''){
                eoWrap.isParentOfOKK = true;
                eoWrap.isParentOfRVG = false;
            }
            if(event.RVGChildEventId__c != null && event.RVGChildEventId__c != ''){
                eoWrap.isParentOfRVG = true;
                eoWrap.isParentOfOKK = false;
            }
            
            // Reva - To get the OKK Event info along with RVG Event info when customer try to get the first view of an Event with OKK and RVG alone
            if(event.OKKTVId__c != '' && event.OKKTVId__c != null){
                Event_Object__c eveObj = [SELECT Id, Object__c, Object__r.Product_Name__c, Object__r.Premium_OnDemand__c,
                                          Object__r.Claim__c,Object__r.Premium_FlatRate__c, Object__r.Policy__c,
                                          Object__r.Coverage_Type__c,Object__r.Product__r.DisplayURL from Event_Object__c 
                                          WHERE Object__r.Is_Deleted__c = FALSE And Event__c=:event.OKKTVId__c limit 1];
                
                UIObjectWrapper.LingsObject objWrap = new UIObjectWrapper.LingsObject();
                objWrap.uuid = eveObj.Object__c;
                objWrap.name = eveObj.Object__r.Product_Name__c;
                objWrap.thumbnail = eveObj.Object__r.Product__r.DisplayURL;
                objWrap.insuranceRate.daily = eveObj.Object__r.Premium_OnDemand__c;
                objWrap.flatRate.daily = eveObj.Object__r.Premium_FlatRate__c;
                String typ = eveObj.Object__r.Coverage_Type__c; 
                if(typ.equalsIgnoreCase('ondemand')) {
                    objWrap.insuranceRate.enabled = true;
                }
                eoWrap.insuredProducts.add(objWrap);
            }
            
            for(Event_Object__c eo: event.Event_Objects__r) {
                UIObjectWrapper.LingsObject objWrap = new UIObjectWrapper.LingsObject();
                objWrap.uuid = eo.Object__c;
                objWrap.name = eo.Object__r.Product_Name__c;
                objWrap.thumbnail = eo.Object__r.Product__r.DisplayURL;
                objWrap.insuranceRate.daily = eo.Object__r.Premium_OnDemand__c;
                objWrap.flatRate.daily = eo.Object__r.Premium_FlatRate__c;
                String typ = eo.Object__r.Coverage_Type__c;
                //To set the Claim flag --30AUG2019
                if(eo.Object__r.Claim__c != NULL) {
                    objWrap.claimed = True;
                } else {
                    objWrap.claimed = False;
                }                
                if(typ.equalsIgnoreCase('ondemand')) {
                    objWrap.insuranceRate.enabled = true;
                } else if(typ.equalsIgnoreCase('flatrate')) {
                    objWrap.flatRate.enabled = true;
                } 
                eoWrap.insuredProducts.add(objWrap);
            }
            jsonGen.writeObjectField('event', eoWrap);
        }

        
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString();
    }
    
    //create event__c records
    public String createRecords(String inputJSON) {
        boolean travelInsuranceFlag = false;
        boolean OKKObjtempFlag = false;
        boolean RVGObjtempFlag = false;
        boolean tvIdFlag = false;
        List<Object__c> OKKObj = new List<Object__c>();
        List<Object__c> RVGObj = new List<Object__c>();
        UIObjectWrapper.EventWithObject eventWrapper = (UIObjectWrapper.EventWithObject)JSON.deserialize(inputJSON, UIObjectWrapper.EventWithObject.class);
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        // creating a separate event for OKK   
        Event__c OkkEvent = new Event__c();
        
        // If createOKKFlag is true then need to create OKKObject as well as separate Event,EventObject for the customer 
        if(eventWrapper.createOKKFlag == true){
            System.debug('CreateOKKFlag true Loop');
            System.debug('Inside the creation of OKKObj');
            Product2 pro = [Select id,name,Description,Product_Type__c,DisplayUrl,Insured_Value__c from Product2 
                            where id=:System.label.OKKProductId];
            UIResultWrapper.ProductInfo instance = new UIResultWrapper.ProductInfo();  
            instance.uuid = System.label.OKKProductId;
            instance.category = pro.Product_Type__c;
            instance.name = pro.name;
            instance.thumbnail = pro.DisplayUrl;
            instance.price = pro.Insured_Value__c;
            instance.OKKPerDayPre = eventWrapper.OKKPerDayPre;
            instance.OKKTotalPre = eventWrapper.OKKTotalPre;
            instance.OKKEventStartDate = eventWrapper.periodFrom;
            instance.OKKEventEndDate = eventWrapper.periodTo;
            String myJSON = JSON.serialize(instance, true);
            
            UIObjectController UIOC = new UIObjectController(this.contactId);
            UIOC.createObject(myJSON);
            
            
            System.debug('After the insertion of OKKObject');
            OKKObj = [select id,name,product__c,Is_Deleted__c from Object__c where product__c=:System.label.OKKProductId and 
                      Customer__c=:this.contactId order by createddate desc limit 1];
            if(OKKObj!=null){
                System.debug('After Querying OKKObj and its id = '+OKKObj[0].Id);
            }
            
            OkkEvent.Start_Date__c = eventWrapper.periodFrom;
            OkkEvent.End_Date__c   = eventWrapper.periodTo;
            OkkEvent.Name = eventWrapper.name;
            OkkEvent.Contact__c = this.contactId;
            OKKEvent.IsOKKObjPresent__c = true;
            try{
                insert OkkEvent;
                if(OKKObj.size() > 0){
                	jsonGen.writeObjectField('OKKObjId', OKKObj[0].id);
                    System.debug('After the insertion of newEvent, querying OKKObj and its id is = '+OKKObj[0].Id);
                    Event_Object__c eo = new Event_Object__c();
                    eo.Event__c = OkkEvent.Id;
                    eo.Object__c = OKKObj[0].id;
                    insert eo;
                }
                List<UIObjectWrapper.LingsObject> lingsObjList = new List<UIObjectWrapper.LingsObject>();
                lingsObjList = eventWrapper.insuredProducts;
                if(lingsObjList == null || lingsObjList.size() <= 0){
                    OKKObjtempFlag = true;
                    eventWrapper.uuid = OkkEvent.Id;
                    
                    //To show the OKK Warning info in UI while creating Event with OKK Obj alone LM-1169
                    OKKEvent.OKKChildEventId__c = 'SELF';
                    update OKKEvent;
                    // Reva - To check whether OKK Event is created along with RVG Obj
                    if(eventWrapper.createRVGFlag == false){ 
                        jsonGen.writeBooleanField('isOnlyOKK', true);
                        jsonGen.writeStringField('status', 'success');
                        jsonGen.writeObjectField('event', eventWrapper);
                        return jsonGen.getAsString(); // Reva - need to return the OKK Obj alone to the UI
                    }
                }
            } catch(Exception ee) {
                Utility.sendException(ee, 'ERROR IN CREATING OKK OBJECT INSIDE UIEVENTCONTROLLER WHILE CREATING EVENT'); 
            }
        }
        
        
        
        // creating a separate event for RVG   
        Event__c RVGEvent = new Event__c();
        
        // If createRVGFlag is true then need to create RVGObject as well as separate Event,EventObject for the customer 
        if(eventWrapper.createRVGFlag == true){
            System.debug('createRVGFlag true Loop');
            System.debug('Inside the creation of RVGObj');
            Product2 pro = [Select id,name,Description,Product_Type__c,DisplayUrl,Insured_Value__c,OnDemand_Premium__c
                            from Product2 where id=:System.label.RVGProductId];
            UIResultWrapper.ProductInfo instance = new UIResultWrapper.ProductInfo();  
            instance.uuid = System.label.RVGProductId;
            instance.category = pro.Product_Type__c;
            instance.name = pro.name;
            instance.thumbnail = pro.DisplayUrl;
            instance.price = pro.Insured_Value__c;
            instance.RVGPerDayPre = pro.OnDemand_Premium__c;
            instance.RVGEventStartDate = eventWrapper.periodFrom;
            instance.RVGEventEndDate = eventWrapper.periodTo;
            String myJSON = JSON.serialize(instance, true);
            
            UIObjectController UIOC = new UIObjectController(this.contactId);
            UIOC.createObject(myJSON);
            
            
            System.debug('After the insertion of RVGObject');
            RVGObj = [select id,name,product__c,Is_Deleted__c from Object__c where product__c=:System.label.RVGProductId and 
                      Customer__c=:this.contactId order by createddate desc limit 1];
            if(RVGObj!=null){
                System.debug('After Querying RVGObj and its id = '+RVGObj[0].Id);
            }
            
            RVGEvent.Start_Date__c = eventWrapper.periodFrom;
            RVGEvent.End_Date__c   = eventWrapper.periodTo;
            RVGEvent.Name = eventWrapper.name;
            RVGEvent.Contact__c = this.contactId;
            RVGEvent.IsRVGObjPresent__c = true;
            try{
                insert RVGEvent;
                if(RVGObj.size() > 0){
                	jsonGen.writeObjectField('RVGObjId', RVGObj[0].id);
                    System.debug('After the insertion of newEvent, querying RVGObj and its id is = '+RVGObj[0].Id);
                    Event_Object__c eo = new Event_Object__c();
                    eo.Event__c = RVGEvent.Id;
                    eo.Object__c = RVGObj[0].id;
                    insert eo;
                }
                
                List<UIObjectWrapper.LingsObject> lingsObjList = new List<UIObjectWrapper.LingsObject>();
                lingsObjList = eventWrapper.insuredProducts;
                if(lingsObjList == null || lingsObjList.size() <= 0){
                    RVGObjtempFlag = true;
                    eventWrapper.uuid = RVGEvent.Id;
                    
                    //Reva - To show the RVG Warning info in UI while creating Event with RVG Obj
                    RVGEvent.RVGChildEventId__c = 'SELF';
                    //Reva - used for showing First view of EventCreation Page when customer creates event with OKK and RVG alone
                    RVGEvent.OKKTVId__c = OkkEvent.Id;
                    update RVGEvent;
                    
                    // Reva - To check whether OKK Event is created along with RVG Obj
                    if(eventWrapper.createOKKFlag == false){ 
                        jsonGen.writeBooleanField('isOnlyRVG', true);
                        jsonGen.writeStringField('status', 'success');
                        jsonGen.writeObjectField('event', eventWrapper);
                        return jsonGen.getAsString(); //Reva - need to return RVG alone to UI
                    }
                }
            } catch(Exception ee) {
                Utility.sendException(ee, 'ERROR IN CREATING RVG OBJECT INSIDE UIEVENTCONTROLLER WHILE CREATING EVENT'); 
            }
        }
        
        //Reva - To pass the both OKK and RVG objs without normal objs to UI
        if(OKKObjtempFlag && RVGObjtempFlag){
            try{
                jsonGen.writeBooleanField('isBothTVOnly', true);
                jsonGen.writeStringField('tvId', OkkEvent.Id);
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeObjectField('event', eventWrapper);
                return jsonGen.getAsString();
            } catch(Exception e){
                Utility.sendException(e, 'ERROR IN CREATING RVG AND OKK OBJ ALONE INSIDE UIEVENTCONTROLLER WHILE CREATING EVENT'); 
            }
        }
        
        //eventWrapper.createRVGFlag == true
        // Reva - To create Event with normal object after creating TravelInsurance Event alone
        If(OKKObjtempFlag == false || RVGObjtempFlag == false){
            Event__c newEvent = new Event__c();
            newEvent.Start_Date__c = eventWrapper.periodFrom;
            newEvent.End_Date__c   = eventWrapper.periodTo;
            newEvent.Name = eventWrapper.name;
            newEvent.Contact__c = this.contactId;
            //Reva - To store the splited OKK event Id as child Event Id in the newEvent with normalObjs
            //if(OkkEvent != null && OkkEvent.Id != '' && OkkEvent.Id != null){
                newEvent.OKKChildEventId__c = OkkEvent.Id;
            //}
            //Reva - To store the splited RVG event Id as child Event Id in the newEvent with normalObjs
            //if(RVGEvent != null && RVGEvent.Id != '' && RVGEvent.id != null){
                newEvent.RVGChildEventId__c = RVGEvent.Id;
            //}
            
            try {
                insert newEvent;
                
                List<Event_Object__c> eveObjList = new List<Event_Object__c>();
                if(eventWrapper.insuredProducts != null){
                    for(UIObjectWrapper.LingsObject obj: eventWrapper.insuredProducts){
                        Event_Object__c eo = new Event_Object__c();
                        eo.Event__c = newEvent.Id;
                        eo.Object__c = obj.uuid;
                        eveObjList.add(eo);
                    } 
                }      
                
                if(eveObjList.size() > 0) {
                    try {
                        insert eveObjList;
                        System.debug('Successfully Inserted EventObject');
                    } catch(Exception e) {
                        // Not able to create Event Objects
                    }
                }
                
                eventWrapper.uuid = newEvent.Id;
                //26MAR2019 - Event batch will run Only if the Start date is today
                //10Jul2019--added extra condition to avoid creating policy when the account is freezed.         
                if(newEvent.Start_Date__c == System.today()){
                    // to process the batch if event start date is today
                    BatchEventPolicy be = new BatchEventPolicy(System.today());
                    Database.executeBatch(be, 100);    
                }
                
                jsonGen.writeBooleanField('OKKWithAllObj', true);
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeObjectField('event', eventWrapper);
            } catch(Exception ee) {
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', ee.getMessage());            
            }
        }
        System.debug('OKK with RVG CreateRecord response ===>'+jsonGen.getAsString());
        return jsonGen.getAsString(); 
    }
    
    //update event__c records
    public String updateRecords(String inputJSON) {
        UIObjectWrapper.EventWithObject eventWrapper = (UIObjectWrapper.EventWithObject)JSON.deserialize(inputJSON, UIObjectWrapper.EventWithObject.class);
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        Event__c eventObj = new Event__c(Id=eventWrapper.uuid);
        eventObj.Start_Date__c = eventWrapper.periodFrom;
        eventObj.End_Date__c   = eventWrapper.periodTo;
        eventObj.Name = eventWrapper.name;
        
        Set<String> uiObjects = new Set<String>();
        for(UIObjectWrapper.LingsObject obj: eventWrapper.insuredProducts) {
            uiObjects.add(obj.uuid);
        }
        
        Map<String, String> beObjectsMap = new Map<String, String>();
        for(Event_Object__c eobj: [SELECT Id, Object__c FROM Event_Object__c WHERE Event__c = :eventWrapper.uuid]) {
            beObjectsMap.put(eObj.Object__c, eObj.Id);
        }
        
        // Get Existing Objects
        List<Event_Object__c> insertList = new List<Event_Object__c>();
        List<Event_Object__c> deleteList = new List<Event_Object__c>();
        
        try {
            update eventObj;      
            
            // Add all the newly selected Objects
            for(String uiObjUUID: uiObjects) {
                if(!beObjectsMap.containsKey(uiObjUUID)) {
                    Event_Object__c eo = new Event_Object__c();
                    eo.Event__c = eventObj.Id;
                    eo.Object__c = uiObjUUID;
                    insertList.add(eo);                        
                }
            }
            
            if(insertList.size() > 0) {
                try {
                    insert insertList;
                } catch(Exception e) {
                    // Not able to create Event Objects
                    Utility.sendException(e, 'ERROR IN CREATING EVENT OBJECTS'); 
                }
            }
            
            // Remove all the unselected Objects
            for(String beObjUUID: beObjectsMap.keySet()) {
                if(!uiObjects.contains(beObjUUID)) {
                    Event_Object__c eo = new Event_Object__c(Id=beObjectsMap.get(beObjUUID));
                    //11Jul2019--To Check if the Object Has a Policy, while unselecting Close the Policy.--LM-551
                    Utility.closeObjectPolicy(beObjUUID);
                    deleteList.add(eo);
                }
            }
            
            if(deleteList.size() > 0) {
                try {
                    delete deleteList;
                } catch(Exception e) {
                    // Not able to delete Event Objects
                    Utility.sendException(e, 'ERROR removing event objects'); 
                }
            }
            //26MAR2019 - Event batch will run Only if the Start date is today
            if(eventObj.Start_Date__c == System.today()){
                // TO PROCESS THE EVENT
                BatchEventPolicy be = new BatchEventPolicy(System.today());
                Database.executeBatch(be, 100);       
            }
            
            jsonGen.writeStringField('status', 'success');
            jsonGen.writeObjectField('event', eventWrapper);
        } catch(Exception ee) {
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', ee.getMessage());            
        }
        
        
        return jsonGen.getAsString(); 
    }
    
    //delete event__c object records
    public String deleteRecords(String inputJSON) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        try {
            Event__c eve = new Event__c(id=inputJSON); 
            //11Jul2019--This is Used to close the Policy when the Event is deleted--LM-551
            List<Event_Object__c> EOList = [SELECT id, Name, Event__c, Object__c from Event_Object__c WHERE Event__c = :eve.Id];
            for(Event_Object__c eo : EOList){
                Utility.closeObjectPolicy(eo.Object__c);
            }
            // Reva -- LM-1223 - To avoid the exception during deleteEvent multiclick on UI
            if(EOList.size() > 0){
                delete eve;
            }
            
            jsonGen.writeStringField('status', 'success');
        } catch (Exception e) {
            Utility.sendException(e, 'Batch Deletion failed!!');
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', e.getStackTraceString());
        }
        
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString(); 
    }
}