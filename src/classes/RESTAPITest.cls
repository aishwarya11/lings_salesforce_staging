@isTest
global class RESTAPITest {

    testmethod global static void TestRESTAPITest(){
        RestContext.request = new RestRequest();
        RestContext.request.requestURI = '/Migrate/*'; 
        RestContext.request.requestBody = Blob.valueOf('[]');

        RestContext.response = new RestResponse();

        RestContext.request.params.put('type', 'Contact'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Event_Object__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Policy__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Voucher_Contact__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Product2'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Object__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Event__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Claim__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'Invoice__c'); RESTAPI.insertChildRecords();
        RestContext.request.params.put('type', 'dummy__c'); RESTAPI.insertChildRecords();

        RestContext.request.requestBody = Blob.valueOf('[{"firstname": "test"}]');
        RestContext.request.params.put('type', 'User'); RESTAPI.insertChildRecords();

        RestContext.request.requestBody = Blob.valueOf('[{"Name": "Test", "End_Date__c": "123"}]');
        RestContext.request.params.put('type', 'Voucher__c'); RESTAPI.insertChildRecords();
    }

    global class ResponseWrapper{
        public Boolean isSuccess;
        public Map<String,String> successMap;
        //public List<Error> errors;
    }
}