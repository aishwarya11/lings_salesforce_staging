global class CouponExpireScheduler implements Schedulable {
    
//method to trigger Reminder before the Coupon Expires for all user
    global void execute(SchedulableContext sc) {
        BatchCouponExpire remainder = new BatchCouponExpire();
        database.executeBatch(remainder);
    }
    
    //public CouponExpireScheduler(){}
}