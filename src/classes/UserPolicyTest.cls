@isTest
public class UserPolicyTest {        
    public static String CHF_STRING = 'CHF';
    
    public class OnDemandInfo {
        public Decimal daily=1.00;
        public Decimal monthly=5.00;
        @testvisible String currencyCode = CHF_STRING;
        public boolean enabled=true;
    }
    
    //Definiton of the FlatRateInfo customsetting
    public class FlatRateInfo {
        public Decimal daily=1.00;
        public Decimal monthly=5.00;
        public Integer savings=30;
        public Date periodFrom=System.today();
        public Date periodTo=System.today()+30;
        public boolean enabled=true;
        public boolean renewable=true;        
        Integer duration = 30;
        String currencyCode = CHF_STRING;
    }
    
    public class ProductInfo {
        public String uuid;
        public String name;
        public String model;
        public String description;
        public String thumbnail;
        public String year;
        public boolean ebike;
        public String category;
        public Decimal price;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        String currencyCode = CHF_STRING;
        
        //Initiailizing the CustomSettings
        public ProductInfo() {
            insuranceRate = new OnDemandInfo();
            flatRate = new FlatRateInfo();
            ebike = false;
        }
    }
    
    testmethod public static void TestUserPolicy() {
        try{
            
            //declaring the required test data 
            Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
            con.email='test@test.com';
            insert con;
            
            List<Object__c> objList = new List<Object__c>();
            
            Product2 pro1 = new Product2();
            //pro1.Product_Type__c = 'Laptop';
            pro1.Name='Product test';
            pro1.Price__c = 100;
            //pro1.Price_Customer__c = 100;
            pro1.Insured_Value__c = 100;
            insert pro1;
            
            Object__c obj1 = new Object__c(); 
            obj1.Coverage_Type__c =  'Flatrate' ;
            obj1.Product__c= pro1.Id;
            obj1.Customer__c = con.id;
            obj1.Premium_FlatRate__c = 5;
            obj1.Premium_OnDemand__c = 10;        
            obj1.Insured_Value__c = 100;
            insert obj1;
            objList.add(obj1);
            
            Object__c obj2 = new Object__c();
            obj2.Coverage_Type__c =  'OnDemand' ;
            obj2.Customer__c = con.id;
            obj2.Product__c= pro1.Id;
            obj2.Insured_Value__c = 50;
            obj2.Premium_FlatRate__c = 12;
            obj2.Premium_FlatRate__c = 10;
            
            insert obj2;
            objList.add(obj2);
            
            Policy__c pol = new Policy__c();
            pol.Contact__c = con.Id;
            pol.Object__c = obj1.Id;
            insert pol;
            
            
            //Instantiate the UserPolicy apex class
            UserPolicy UP1 = new UserPolicy();
            UP1.cont=con;
            UP1.contactId=con.Id;
            UP1.email=con.Email;
            UP1.PolTime=''+System.now();
            UP1.Poldate=''+System.today();
            UP1.obj = objList;
            
            //Instantiate the inner class PolicyWrapper from UserPolicy class
            UserPolicy.PolicyWrapper UP = new UserPolicy.PolicyWrapper();
            UP.name='ABC';
            UP.Category= 'smartphone';
            UP.Value=01;
            UP.type='Flatrate';
            UP.Premium=10;
            UP.month=02;
            UP.Total=100;
            
            
            UserPolicy.LastRowWrapper LRW = new UserPolicy.LastRowWrapper();
            LRW.polCategory  = 'Flatrate';
            LRW.polmonth = 04;
            LRW.polname = pol.Name;
            LRW.polPremium = 12.00;
            LRW.polTotal = 112;
            LRW.poltype = 'Flatrate';
            LRW.polValue = 11.23;
            
            
            List<UserPolicy.PolicyWrapper> UPL = new List<UserPolicy.PolicyWrapper>();
            Up1.polList = UPL;
            
            Map<String,UserPolicy.PolicyWrapper> UPM = new Map<String,UserPolicy.PolicyWrapper>();
            UP1.polWrapperMap=UPM;
            UP1.sendPdf();
            
            
            
            UserPolicy UP2 = new UserPolicy(con.Id);
            
            //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            UserPolicy testAccPlan = new UserPolicy(sc);
            testAccPlan.processPolicies(); 
            UP2.processPolicies();
            UserPolicy UPinstance = new UserPolicy();
            
        }
        catch(Exception e){
            
        }
    }
    testmethod public static void TestUserPolicy1() {
        try{
            
            //declaring the required test data 
            Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
            con.email='test@test.com';
            insert con;
            
            List<Object__c> objList = new List<Object__c>();
            
            Product2 pro1 = new Product2();
            pro1.Product_Type__c = 'camera incl. accessories';
            pro1.Name='Product test';
            pro1.Price__c = 100;
            pro1.Price_Customer__c = 100;
            pro1.Insured_Value__c = 100;
            insert pro1;
            
            Object__c obj1 = new Object__c(); 
            obj1.Coverage_Type__c =  'Flatrate' ;
            obj1.Product__c= pro1.Id;
            obj1.Customer__c = con.id;
            obj1.Premium_FlatRate__c = 5;
            obj1.Premium_OnDemand__c = 10;        
            obj1.Insured_Value__c = 100;
            insert obj1;
            objList.add(obj1);
            
            Object__c obj2 = new Object__c();
            obj2.Coverage_Type__c =  'OnDemand' ;
            obj2.Customer__c = con.id;
            obj2.Product__c= pro1.Id;
            obj2.Insured_Value__c = 50;
            obj2.Premium_FlatRate__c = 12;
            obj2.Premium_FlatRate__c = 10;
            
            insert obj2;
            objList.add(obj2);
            
            Policy__c pol = new Policy__c();
            pol.Contact__c = con.Id;
            pol.Object__c = obj1.Id;
            insert pol;
            
            
            //Instantiate the UserPolicy apex class
            UserPolicy UP1 = new UserPolicy();
            UP1.cont=con;
            UP1.contactId=con.Id;
            UP1.email=con.Email;
            UP1.PolTime=''+System.now();
            UP1.Poldate=''+System.today();
            UP1.obj = objList;
            
            //Instantiate the inner class PolicyWrapper from UserPolicy class
            UserPolicy.PolicyWrapper UP = new UserPolicy.PolicyWrapper();
            UP.name='ABC';
            UP.Category= 'smartphone';
            UP.Value=01;
            UP.type='Flatrate';
            UP.Premium=10;
            UP.month=02;
            UP.Total=100;
            
            
            UserPolicy.LastRowWrapper LRW = new UserPolicy.LastRowWrapper();
            LRW.polCategory  = 'Flatrate';
            LRW.polmonth = 04;
            LRW.polname = pol.Name;
            LRW.polPremium = 12.00;
            LRW.polTotal = 112;
            LRW.poltype = 'Flatrate';
            LRW.polValue = 11.23;
            
            
            List<UserPolicy.PolicyWrapper> UPL = new List<UserPolicy.PolicyWrapper>();
            Up1.polList = UPL;
            
            Map<String,UserPolicy.PolicyWrapper> UPM = new Map<String,UserPolicy.PolicyWrapper>();
            UP1.polWrapperMap=UPM;
            UP1.sendPdf();
            
            
            
            UserPolicy UP2 = new UserPolicy(con.Id);
            
            //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            UserPolicy testAccPlan = new UserPolicy(sc);
            testAccPlan.processPolicies(); 
            UP2.processPolicies();
            UserPolicy UPinstance = new UserPolicy();
        }
        catch(Exception e){
            
        }
    }
    testmethod public static void TestUserPolicy2() {
        try{
            
            //declaring the required test data 
            Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
            con.email='test@test.com';
            insert con;
            
            List<Object__c> objList = new List<Object__c>();
            
            OnDemandInfo onclass = new OnDemandInfo();
            String myJSON3 = JSON.serialize(onclass,true);
            FlatRateInfo frclass = new FlatRateInfo();
            String myJSON4 = JSON.serialize(frclass,true);    
            
            ProductInfo instance = new ProductInfo();  
            //instance.uuid = ;
            instance.category = 'Foto und Videografie';
            instance.name = 'sample product';
            instance.model = 'sample model';
            instance.description = 'sample description';
            instance.thumbnail = 'sample thumbnail';
            instance.year = '2019';
            instance.ebike = true;
            instance.price = 100.00;
            instance.currencyCode = 'CHF';
            instance.insuranceRate = onclass;
            instance.flatRate = frclass;
            String myJSON = JSON.serialize(instance, true);
            
            Product2 pro1 = new Product2(id = instance.uuid);
            pro1.Name = instance.name;
            pro1.Price__c = instance.price;
            //pro1.Product_Type__c = instance.category;
            pro1.Insured_Value__c = instance.price;
            insert pro1;
            
            Object__c obj1 = new Object__c(); 
            obj1.Coverage_Type__c =  'OnDemand' ;
            obj1.Product__c= pro1.Id;
            obj1.Customer__c = con.id;
            obj1.Premium_FlatRate__c = 5;
            obj1.Premium_OnDemand__c = 10;        
            obj1.Insured_Value__c = 100;
            insert obj1;
            objList.add(obj1);
            
            Object__c obj2 = new Object__c();
            obj2.Coverage_Type__c =  'OnDemand' ;
            obj2.Customer__c = con.id;
            obj2.Product__c= pro1.Id;
            obj2.Insured_Value__c = 50;
            obj2.Premium_FlatRate__c = 12;
            obj2.Premium_FlatRate__c = 10;
            
            insert obj2;
            objList.add(obj2);
            
            Policy__c pol = new Policy__c();
            pol.Contact__c = con.Id;
            pol.Object__c = obj1.Id;
            pol.Insurance_Type__c = 'OnDemand';
            pol.Active__c = TRUE;
            insert pol;
            
            
            //Instantiate the UserPolicy apex class
            UserPolicy UP1 = new UserPolicy();
            UP1.cont=con;
            UP1.contactId=con.Id;
            UP1.email=con.Email;
            UP1.PolTime=''+System.now();
            UP1.Poldate=''+System.today();
            UP1.obj = objList;
            
            //Instantiate the inner class PolicyWrapper from UserPolicy class
            UserPolicy.PolicyWrapper UP = new UserPolicy.PolicyWrapper();
            UP.name='ABC';
            UP.Category= 'smartphone';
            UP.Value=01;
            UP.type='OnDemand';
            UP.Premium=10;
            UP.month=02;
            UP.Total=100;
            
            
            UserPolicy.LastRowWrapper LRW = new UserPolicy.LastRowWrapper();
            LRW.polCategory  = 'OnDemand';
            LRW.polmonth = 04;
            LRW.polname = pol.Name;
            LRW.polPremium = 12.00;
            LRW.polTotal = 112;
            LRW.poltype = 'OnDemand';
            LRW.polValue = 11.23;
            
            
            List<UserPolicy.PolicyWrapper> UPL = new List<UserPolicy.PolicyWrapper>();
            UPL.add(UP);
            Up1.polList = UPL;
            
            Map<String,UserPolicy.PolicyWrapper> UPM = new Map<String,UserPolicy.PolicyWrapper>();
            UP1.polWrapperMap=UPM;
            UP1.sendPdf();
            
            
            
            UserPolicy UP2 = new UserPolicy(con.Id);
            
            //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            UserPolicy testAccPlan = new UserPolicy(sc);
            testAccPlan.processPolicies(); 
            UP2.processPolicies();
            UserPolicy UPinstance = new UserPolicy();
            
        }
        catch(Exception e){
            
        }
    }
    
    testmethod public static void TestUserPolicy3() {
        //try{
        
        //declaring the required test data 
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
        con.email='test@test.com';
        insert con;
        
        List<Object__c> objList = new List<Object__c>();
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON3 = JSON.serialize(onclass,true);
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON4 = JSON.serialize(frclass,true);    
        
        ProductInfo instance = new ProductInfo();  
        //instance.uuid = ;
        instance.category = 'Foto und Videografie';
        instance.name = 'sample product';
        instance.model = 'sample model';
        instance.description = 'sample description';
        instance.thumbnail = 'sample thumbnail';
        instance.year = '2019';
        instance.ebike = true;
        instance.price = 100.00;
        instance.currencyCode = 'CHF';
        instance.insuranceRate = onclass;
        instance.flatRate = frclass;
        String myJSON = JSON.serialize(instance, true);
        
        Product2 pro1 = new Product2(id = instance.uuid);
        pro1.Name = instance.name;
        pro1.Price__c = instance.price;
        //pro1.Product_Type__c = instance.category;
        pro1.Insured_Value__c = instance.price;
        insert pro1;
        
        Product2 pro = new Product2();
        pro.Name = 'sample product';
        pro.Product_Type__c = 'camera incl. accessories';
        insert pro;
        
        Object__c obj1 = new Object__c(); 
        obj1.Coverage_Type__c =  'OnDemand' ;
        obj1.Product__c= pro.Id;
        obj1.Customer__c = con.id;
        obj1.Premium_FlatRate__c = 5;
        obj1.Premium_OnDemand__c = 10;        
        obj1.Insured_Value__c = 100;
        insert obj1;
        objList.add(obj1);
        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c = con.Id;
        pol.Object__c = obj1.Id;
        pol.Insurance_Type__c = 'OnDemand';
        pol.Active__c = TRUE;
        insert pol;
        
        
        //Instantiate the UserPolicy apex class
        UserPolicy UP1 = new UserPolicy();
        UP1.cont=con;
        UP1.contactId=con.Id;
        UP1.email=con.Email;
        UP1.PolTime=''+System.now();
        UP1.Poldate=''+System.today();
        UP1.obj = objList;
        
        //Instantiate the inner class PolicyWrapper from UserPolicy class
        UserPolicy.PolicyWrapper UP = new UserPolicy.PolicyWrapper();
        UP.name='ABC';
        UP.Category= 'smartphone';
        UP.Value=01;
        UP.type='OnDemand';
        UP.Premium=10;
        UP.month=02;
        UP.Total=100;
        
        
        UserPolicy.LastRowWrapper LRW = new UserPolicy.LastRowWrapper();
        LRW.polCategory  = 'OnDemand';
        LRW.polmonth = 04;
        LRW.polname = pol.Name;
        LRW.polPremium = 12.00;
        LRW.polTotal = 112;
        LRW.poltype = 'OnDemand';
        LRW.polValue = 11.23;
        
        
        List<UserPolicy.PolicyWrapper> UPL = new List<UserPolicy.PolicyWrapper>();
        UPL.add(UP);
        Up1.polList = UPL;
        
        Map<String,UserPolicy.PolicyWrapper> UPM = new Map<String,UserPolicy.PolicyWrapper>();
        UP1.polWrapperMap=UPM;
        UP1.sendPdf();
        
        
        
        UserPolicy UP2 = new UserPolicy(con.Id);
        
        //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        UserPolicy testAccPlan = new UserPolicy(sc);
        testAccPlan.processPolicies(); 
        UP2.processPolicies();
        UserPolicy UPinstance = new UserPolicy();
        
        //}
        //catch(Exception e){}
    }
    
    testmethod public static void TestUserPolicy4() {
        //try{
        
        //declaring the required test data 
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
        con.email='test@test.com';
        insert con;
        
        List<Object__c> objList = new List<Object__c>();
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON3 = JSON.serialize(onclass,true);
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON4 = JSON.serialize(frclass,true);    
        
        ProductInfo instance = new ProductInfo();  
        //instance.uuid = ;
        instance.category = 'Foto und Videografie';
        instance.name = 'sample product';
        instance.model = 'sample model';
        instance.description = 'sample description';
        instance.thumbnail = 'sample thumbnail';
        instance.year = '2019';
        instance.ebike = true;
        instance.price = 100.00;
        instance.currencyCode = 'CHF';
        instance.insuranceRate = onclass;
        instance.flatRate = frclass;
        String myJSON = JSON.serialize(instance, true);
        
        Product2 pro1 = new Product2(id = instance.uuid);
        pro1.Name = instance.name;
        pro1.Price__c = instance.price;
        //pro1.Product_Type__c = instance.category;
        pro1.Insured_Value__c = instance.price;
        insert pro1;
        
        Product2 pro = new Product2();
        pro.Name = 'sample product';
        pro.Product_Type__c = 'Bike';
        insert pro;
        
        Object__c obj1 = new Object__c(); 
        obj1.Coverage_Type__c =  'FlatRate' ;
        obj1.Product__c= pro.Id;
        obj1.Customer__c = con.id;
        obj1.Premium_FlatRate__c = 5;
        obj1.Premium_OnDemand__c = 10;        
        obj1.Insured_Value__c = 100;
        insert obj1;
        objList.add(obj1);
        
        Object__c obj2 = new Object__c();
        obj2.Coverage_Type__c =  'OnDemand' ;
        obj2.Customer__c = con.id;
        obj2.Product__c= pro.Id;
        obj2.Insured_Value__c = 50;
        obj2.Premium_FlatRate__c = 12;
        obj2.Premium_FlatRate__c = 10;
        
        insert obj2;
        objList.add(obj2);
        
        Policy__c pol = new Policy__c();
        pol.Contact__c = con.Id;
        pol.Object__c = obj1.Id;
        pol.Insurance_Type__c = 'OnDemand';
        pol.Active__c = TRUE;
        insert pol;
        
        
        //Instantiate the UserPolicy apex class
        UserPolicy UP1 = new UserPolicy();
        UP1.cont=con;
        UP1.contactId=con.Id;
        UP1.email=con.Email;
        UP1.PolTime=''+System.now();
        UP1.Poldate=''+System.today();
        UP1.obj = objList;
        
        //Instantiate the inner class PolicyWrapper from UserPolicy class
        UserPolicy.PolicyWrapper UP = new UserPolicy.PolicyWrapper();
        UP.name='ABC';
        UP.Category= 'smartphone';
        UP.Value=01;
        UP.type='OnDemand';
        UP.Premium=10;
        UP.month=02;
        UP.Total=100;
        
        
        UserPolicy.LastRowWrapper LRW = new UserPolicy.LastRowWrapper();
        LRW.polCategory  = 'OnDemand';
        LRW.polmonth = 04;
        LRW.polname = pol.Name;
        LRW.polPremium = 12.00;
        LRW.polTotal = 112;
        LRW.poltype = 'OnDemand';
        LRW.polValue = 11.23;
        
        
        List<UserPolicy.PolicyWrapper> UPL = new List<UserPolicy.PolicyWrapper>();
        UPL.add(UP);
        Up1.polList = UPL;
        
        Map<String,UserPolicy.PolicyWrapper> UPM = new Map<String,UserPolicy.PolicyWrapper>();
        UP1.polWrapperMap=UPM;
        UP1.sendPdf();
        
        
        
        UserPolicy UP2 = new UserPolicy(con.Id);
        
        //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        UserPolicy testAccPlan = new UserPolicy(sc);
        testAccPlan.processPolicies(); 
        UP2.processPolicies();
        UserPolicy UPinstance = new UserPolicy();
        
        //}
        //catch(Exception e){}
    }
    testmethod public static void TestUserPolicy5() {
        //try{
        
        //declaring the required test data 
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
        con.email='test@test.com';
        insert con;
        
        List<Object__c> objList = new List<Object__c>();
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON3 = JSON.serialize(onclass,true);
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON4 = JSON.serialize(frclass,true);    
        
        ProductInfo instance = new ProductInfo();  
        //instance.uuid = ;
        instance.category = 'Foto und Videografie';
        instance.name = 'sample product';
        instance.model = 'sample model';
        instance.description = 'sample description';
        instance.thumbnail = 'sample thumbnail';
        instance.year = '2019';
        instance.ebike = true;
        instance.price = 100.00;
        instance.currencyCode = 'CHF';
        instance.insuranceRate = onclass;
        instance.flatRate = frclass;
        String myJSON = JSON.serialize(instance, true);
        
        Product2 pro1 = new Product2(id = instance.uuid);
        pro1.Name = instance.name;
        pro1.Price__c = instance.price;
        //pro1.Product_Type__c = instance.category;
        pro1.Insured_Value__c = instance.price;
        insert pro1;
        
        Product2 pro = new Product2();
        pro.Name = 'sample product';
        pro.Product_Type__c = 'smartphone';
        insert pro;
        
        Object__c obj1 = new Object__c(); 
        obj1.Coverage_Type__c =  'OnDemand' ;
        obj1.Product__c= pro.Id;
        obj1.Customer__c = con.id;
        obj1.Premium_FlatRate__c = 5;
        obj1.Premium_OnDemand__c = 10;        
        obj1.Insured_Value__c = 100;
        insert obj1;
        objList.add(obj1);
        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c = con.Id;
        pol.Object__c = obj1.Id;
        pol.Insurance_Type__c = 'OnDemand';
        pol.Active__c = TRUE;
        insert pol;
        
        
        //Instantiate the UserPolicy apex class
        UserPolicy UP1 = new UserPolicy();
        UP1.cont=con;
        UP1.contactId=con.Id;
        UP1.email=con.Email;
        UP1.PolTime=''+System.now();
        UP1.Poldate=''+System.today();
        UP1.obj = objList;
        
        //Instantiate the inner class PolicyWrapper from UserPolicy class
        UserPolicy.PolicyWrapper UP = new UserPolicy.PolicyWrapper();
        UP.name='ABC';
        UP.Category= 'smartphone';
        UP.Value=01;
        UP.type='OnDemand';
        UP.Premium=10;
        UP.month=02;
        UP.Total=100;
        
        
        UserPolicy.LastRowWrapper LRW = new UserPolicy.LastRowWrapper();
        LRW.polCategory  = 'OnDemand';
        LRW.polmonth = 04;
        LRW.polname = pol.Name;
        LRW.polPremium = 12.00;
        LRW.polTotal = 112;
        LRW.poltype = 'OnDemand';
        LRW.polValue = 11.23;
        
        
        List<UserPolicy.PolicyWrapper> UPL = new List<UserPolicy.PolicyWrapper>();
        UPL.add(UP);
        Up1.polList = UPL;
        
        Map<String,UserPolicy.PolicyWrapper> UPM = new Map<String,UserPolicy.PolicyWrapper>();
        UP1.polWrapperMap=UPM;
        UP1.sendPdf();
        
        
        
        UserPolicy UP2 = new UserPolicy(con.Id);
        
        //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        UserPolicy testAccPlan = new UserPolicy(sc);
        testAccPlan.processPolicies(); 
        UP2.processPolicies();
        UserPolicy UPinstance = new UserPolicy();
        
        //}
        //catch(Exception e){}
    }
    
    
    testmethod public static void TestUserPolicy6() {
        //try{
        
        //declaring the required test data 
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
        con.email='test@test.com';
        insert con;
        
        List<Object__c> objList = new List<Object__c>();
                
        Premium__c prevalue = new Premium__c();
        prevalue.Name='travel insurance';
        prevalue.On_Demand__c=5.00;
        prevalue.FlatRate__c=5.00;
        insert prevalue;
        
        
        Product2 pro = new Product2(Id=System.label.OKKProductId);
        pro.Price__c = 100;
        pro.Insured_Value__c = 100;
        pro.Product_Type__c = 'Travel Insurance';
        upsert pro;
        
        Object__c obj1 = new Object__c(); 
        obj1.Coverage_Type__c =  'Flatrate' ;
        obj1.Product__c= pro.Id;
        obj1.Customer__c = con.id;
        obj1.Premium_FlatRate__c = 5;
        obj1.Premium_OnDemand__c = 10;        
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        Object__c obj = new Object__c();
        obj.Product__c=Pro.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='flatrate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj1.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        UserPolicy.sendOKKPolicyPdf(obj1.Id, con.Id,'CreatePDF'); 
        UserPolicy.sendOKKPolicyPdf(obj1.Id, con.Id,'CancelPDF'); 
        UserPolicy.sendOKKPolicyPdf(con.Id, con.Id,'CancelPDF'); 
        UserPolicy.sendOKKPolicyPdf(obj.Id, con.Id,'CancelPDF');
        /*}
        catch(Exception e){
            
        }*/
    }
    
    
    testmethod public static void TestUserPolicy7() {
        //try{
        
        //declaring the required test data 
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
        con.email='test@test.com';
        insert con;
        
        List<Object__c> objList = new List<Object__c>();
                
        Premium__c prevalue = new Premium__c();
        prevalue.Name='travel insurance';
        prevalue.On_Demand__c=5.00;
        prevalue.FlatRate__c=5.00;
        insert prevalue;
        
        
        Product2 pro = new Product2(Id=System.label.RVGProductId);
        pro.Price__c = 100;
        pro.Insured_Value__c = 100;
        pro.Product_Type__c = 'Travel Insurance';
        upsert pro;
        
        Object__c obj1 = new Object__c(); 
        obj1.Coverage_Type__c =  'Flatrate' ;
        obj1.Product__c= pro.Id;
        obj1.Customer__c = con.id;
        obj1.Premium_FlatRate__c = 5;
        obj1.Premium_OnDemand__c = 10;        
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        Object__c obj = new Object__c();
        obj.Product__c=Pro.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='flatrate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj1.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        UserPolicy.sendOKKPolicyPdf(obj1.Id, con.Id,'CreatePDF'); 
        UserPolicy.sendOKKPolicyPdf(obj1.Id, con.Id,'CancelPDF'); 
        UserPolicy.sendOKKPolicyPdf(con.Id, con.Id,'CancelPDF'); 
        UserPolicy.sendOKKPolicyPdf(obj.Id, con.Id,'CancelPDF');
        /*}
        catch(Exception e){
            
        }*/
    }
    
    
    testmethod public static void TestUserPolicy8() {
        //try{
        
        //declaring the required test data 
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598' );
        con.email='test@test.com';
        insert con;
        
        List<Object__c> objList = new List<Object__c>();
                
        Premium__c prevalue = new Premium__c();
        prevalue.Name='travel insurance';
        prevalue.On_Demand__c=5.00;
        prevalue.FlatRate__c=5.00;
        insert prevalue;
        
        
        Product2 pro = new Product2(Id=System.label.RVGProductId);
        pro.Price__c = 100;
        pro.Insured_Value__c = 100;
        pro.Product_Type__c = 'Travel Insurance';
        upsert pro;
        
        Object__c obj1 = new Object__c(); 
        obj1.Coverage_Type__c =  'Flatrate' ;
        obj1.Product__c= pro.Id;
        obj1.Customer__c = con.id;
        obj1.Premium_FlatRate__c = 5;
        obj1.Premium_OnDemand__c = 10;        
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        Object__c obj = new Object__c();
        obj.Product__c=Pro.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='flatrate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj1.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        //Instantiate the constructor with apex page controller parameter from the UserPolicy class 
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        UserPolicy.sendOKKPolicyPdf(obj1.Id, con.Id,'CreatePDF'); 
        UserPolicy.sendOKKPolicyPdf(obj1.Id, con.Id,'CancelPDF'); 
        UserPolicy.sendOKKPolicyPdf(con.Id, con.Id,'CreatePDF');  
        UserPolicy.sendOKKPolicyPdf(con.Id, con.Id,'CancelPDF'); 
        UserPolicy.sendOKKPolicyPdf(obj.Id, con.Id,'CreatePDF');
        UserPolicy.sendOKKPolicyPdf(obj.Id, con.Id,'CancelPDF');
        /*}
        catch(Exception e){
            
        }*/
    }
}