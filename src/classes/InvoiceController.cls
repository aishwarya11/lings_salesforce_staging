global class InvoiceController {
    global Static list<Messaging.Emailfileattachment> attachFile = new List<Messaging.Emailfileattachment>();
    public InvoiceController(){
        
    }
    public InvoiceWrapper invWrapper { get; set; }
	
    @testvisible private Id contactId = null;

    // MR - 06JUN19 - Added to get the Correct Days and Amount in Invoice
    @testvisible Transient private Date invoiceEndDate;
    @testvisible Transient private Datetime invoiceEndDateTime;
    @testvisible Transient private String invoiceName;
    @testvisible Transient private Boolean vfPageFlag = false;
    
    // Set of Policy Ids for Updating the Invoice
    Set<Id> invPolicySet = new Set<Id>();
    
    // Wrapper for the Complete Invoice Processing
    public class InvoiceWrapper {
        public Id invId { get; set; }
        public String invName { get; set; }
        public String invMonth { get; set; }
        public String invMonthYear { get; set; }
        Transient public Integer totalDays { get; set; }
        Transient public Decimal totalCost { get; set; }
        // Reva -- LM-1116 - Calculating total cost of OKK individually
        Transient public Decimal totalOKKCost { get; set; }
        // Reva -- LM-1306 - Calculating total cost of RVG individually
        Transient public Decimal totalRVGCost { get; set; }
        // MR - 06JUM19 Added for Txn Amount Value
        Transient  public Decimal viuAmount { get; set; }
        Transient  public Decimal txnAmount { get; set; }
	    Transient  public String email { get; set; }
        
        public List<InsuranceWrapper> insList { get; set; }
        public List<SummaryWrapper> summaryList { get; set; }
        
        public InvoiceWrapper() {
            this.totalDays = 0;
            this.totalCost = 0.0;
            this.totalOKKCost = 0.0;
            this.totalRVGCost = 0.0;
            // MR - 06JUM19 Added for Txn Amount Value
            this.viuAmount = 0.0;
            this.txnAmount = 0.0;

            this.insList = new List<InsuranceWrapper>();
            this.summaryList = new List<SummaryWrapper>();
        }
    }
    
    public class InsuranceWrapper {
        public String name { get; set; }
        public String sdate { get; set; }
        public String edate { get; set; }
        public Decimal days { get; set; }
        public Decimal cost { get; set; }
        public String InsuranceType { get; set; }
        public Decimal Premium { get; set; }
    }
    
    public class SummaryWrapper {
        public String leftSideText { get; set; }
        public String rightSideText { get; set; }
    }
        
    public InvoiceController(ApexPages.StandardController controller) {
        vfPageFlag = true;
	
        contactId = controller.getId();

        if(ApexPages.currentPage().getParameters().containsKey('invDate')) {
            invoiceEndDate = Date.valueOf(ApexPages.currentPage().getParameters().get('invDate'));
        } else {
            invoiceEndDate = System.today(); 
    
            // MR - 06JUN19 - Added to get the Correct Days and Amount in Invoice
            invoiceEndDateTime = invoiceEndDate;
            invoiceName = 'Invoice ' + invoiceEndDateTime.format('dd.MM.yyyy') + 'Constructor';
        }
        
        // Get the Policies List
        List<Policy__c> polList = getPolicies(contactId, invoiceEndDate);
        invWrapper = processPolicies(polList);
    }
    
    public InvoiceController(String inputContactId) {
        contactId = inputContactId;
        // MR - 06JUN19 - Added to get the Correct Days and Amount in Invoice
        invoiceEndDate = System.today();
        invoiceEndDateTime = invoiceEndDate;
        invoiceName = 'Invoice ' + invoiceEndDateTime.format('dd.MM.yyyy');
    }

    // MR - 06JUN19 - Added to get the Correct Days and Amount in Invoice
    public InvoiceController(String invContactId, Date invEndDate) {
        contactId = invContactId;
        invoiceEndDate = invEndDate;
        invoiceEndDateTime = invoiceEndDate.addDays(1);
        invoiceName = 'Invoice 01.' + invoiceEndDateTime.format('MM.yyyy');
    }

//read the policies when the invoice status is not success
    public List<Policy__c> getPolicies(String conId, Date invEndDate) {
        return [Select id, name, Insurance_Type__c, Premium__c,
			Object__r.Product_Name__c, Start_Date__c, Invoice_Start_Date__c,Object__r.OKKTotalPremium__c,
            Object__r.RVGTotalPremium__c,Object__r.RVGEventEndDate__c,Object__r.RVGEventStartDate__c,Object__r.Product__c,
            Object__r.OKKEventEndDate__c,Object__r.OKKEventStartDate__c,End_Date__c, Number_of_days_Insured__c, Dates__c, 
            Contact__r.Voucher_In_Use__c,Contact__r.VIU_End_date__c, Contact__r.VIU_Balance__c, Contact__r.Balance_Pending__c, 
			Invoice__c, Invoice__r.Status__c, Invoice__r.Date__c, Contact__r.email 
			from Policy__c 
			where Contact__c = :conId 
              AND Object__c != NULL 
              AND Status__c <> 'SplitSource' 
              AND Invoice_Start_Date__c < :invEndDate 
              AND (Invoice__c = NULL OR Invoice__r.Status__c != 'Success') 
        	ORDER BY Start_Date__c ASC];
    }
    
    public InvoiceWrapper processPolicies(List<Policy__c> inpPolicies) {
        InvoiceWrapper respWrapper = new InvoiceWrapper();
        
		Map<String, InsuranceWrapper> insWrapperMap = new Map<String, InsuranceWrapper>();
        String key = '';
        Decimal premium = 0.0;
        Date viuEndDate = null;
        Decimal viuBalance = 0.0;
        Decimal Total = 0.0;
        String invoiceStatus = '';
        String Charges = 'Current Total Premium(incl.5% Stamp duty)';
        
        InsuranceWrapper insWrap = null;
        // MR - 21MAY19 - Added the flags for correct summary calculation
        Integer polDays = 0;
        Decimal polCost = 0.0;
        Decimal OKKPolCost = 0.0;
        Decimal RVGPolCost = 0.0;

        // 07JUN19 - Changed to show right 
        Integer invMonthNum = 0;
        Integer invYearNum = 0;
        if(invoiceEndDate != null) { 
            invMonthNum = invoiceEndDate.addMonths(-1).month(); 
            invYearNum = invoiceEndDate.addMonths(-1).year();
        }
        respWrapper.invMonth = Utility.getMonthName(invMonthNum) + ' ' + invYearNum;

        respWrapper.invMonthYear = '(' + (invMonthNum > 9 ? '' : '0') + invMonthNum + '/' + invYearNum + ')';

        // Process all the policies
        for (Policy__c ins: inpPolicies) {
			respWrapper.email = ins.Contact__r.email;
            
            //To check the voucher-in-use
            if(viuEndDate == null) {
                // MR - 06JUN19 - Added to avoid null pointer exception
                viuEndDate = System.today();
                if(ins.Contact__r.Voucher_In_Use__c != null) {
                    viuEndDate = ins.Contact__r.VIU_End_date__c;
                    viuBalance = ins.Contact__r.VIU_Balance__c;
                }

                // MR - 20MAY19 - TO check the previous vouchers
                if(ins.Contact__r.Balance_Pending__c != null) {
                    viuBalance += ins.Contact__r.Balance_Pending__c;
                }
            }
            
            invPolicySet.add(ins.Id);
            
            try {
                premium = ins.Premium__c;
            } catch(Exception e) {
                premium = 0.0;
            }
            
            key = '' + ins.Id + premium + ins.Insurance_Type__c + ins.Invoice_Start_Date__c;
            
            // MR - 06JUN19 - Get Correct Policy Days
            Date insEndDate = (ins.End_Date__c == NULL ? invoiceEndDate : ins.End_Date__c.date());
            if(ins.Insurance_Type__c.equalsIgnoreCase(Constants.ONDEMAND_STRING)) {
                   if(insEndDate >= invoiceEndDate) {
                       insEndDate = invoiceEndDate.addDays(-1);
                   }
                   polDays = ins.Invoice_Start_Date__c.daysBetween(insEndDate) + 1;
               }
            
            // To process the Policy with FlatRate type when it is not an OKK and a RVG Obj
            else if(ins.Insurance_Type__c.equalsIgnoreCase(Constants.FlatRate_STRING)) {
                             if(insEndDate > invoiceEndDate && invoiceEndDate.daysBetween(insEndDate) > Utility.numOfDaysForEndDate(invoiceEndDate)) {
                                 Integer monthsToReduce = insEndDate.month() - invoiceEndDate.month();
                                 while(monthsToReduce > 0) {
                                     Date prevMonthDate = insEndDate.addMonths(-1);
                                     insEndDate = insEndDate.addDays(-1 * (Utility.numOfDaysForEndDate(prevMonthDate) + 1));
                                     monthsToReduce--;
                                 }
                             }
                             polDays = ins.Invoice_Start_Date__c.daysBetween(insEndDate) + 1;
                         } 
            // Reva -- LM-1152 - To get the OKKPol and RVGPol in a single invoice even though it is in Month Overlapping scenario
            /*else if(ins.Insurance_Type__c.equalsIgnoreCase(Constants.ONDEMAND_STRING) && 
                                   (ins.Object__r.Product__c == System.Label.OKKProductId ||
                   				   ins.Object__r.Product__c == System.Label.RVGProductId)){
                                       polDays = ins.Invoice_Start_Date__c.daysBetween(insEndDate) + 1;
                                   }*/
            
            // MR - 06JUN19 - Get Correct Policy Cost
            // To get the Normal Obj total Premium value
            if(ins.Object__r.Product__c != System.Label.OKKProductId &&
              ins.Object__r.Product__c != System.Label.RVGProductId){
                polCost = Utility.getRoundedPremium(polDays * premium);
            }
           // Reva -- LM-1152 - To get the OKK Obj total Premium value
            else if(ins.Object__r.Product__c == System.Label.OKKProductId){
                OKKPolCost = ins.Object__r.OKKTotalPremium__c.setScale(2);
                polCost = ins.Object__r.OKKTotalPremium__c.setScale(2);
            }
           // Reva -- LM-1306 - To get the RVG Obj total Premium value
            else if(ins.Object__r.Product__c == System.Label.RVGProductId){
                RVGPolCost = ins.Object__r.RVGTotalPremium__c.setScale(2);
                polCost = ins.Object__r.RVGTotalPremium__c.setScale(2);
            }

            if(insWrapperMap.containsKey(key)) {
                insWrap = insWrapperMap.get(key);
                insWrap.days += polDays;
                insWrap.cost += polCost;
            } else {
                insWrap = new InsuranceWrapper();
                Transient Integer d = ins.Invoice_Start_Date__c.day();
                Transient Integer mo = ins.Invoice_Start_Date__c.month();
                Transient Integer yr = ins.Invoice_Start_Date__c.year();
                
                Transient DateTime DT = DateTime.newInstance(yr, mo, d);
                insWrap.sdate = DateTime.newInstance(yr, mo, d).format('dd.MM');
                
                insWrap.days = polDays;
                insWrap.cost = polCost;
            }

            // MR - MR 06JUN19 - Fix for Invoice Page Dates
            insWrap.edate = '- ' + Datetime.newInstance(insEndDate.year(), insEndDate.month(), insEndDate.day()).format('dd.MM');
            
            insWrap.name = ins.Object__r.Product_Name__c;
            //07JUNE2019 --To display the On-Demand and Flatrate text as requested by Henrik.
            if(ins.Insurance_Type__c.equalsIgnoreCase(Constants.ONDEMAND_STRING)){
                insWrap.InsuranceType = 'On-Demand';                
            }else if(ins.Insurance_Type__c.equalsIgnoreCase(Constants.FlatRate_STRING)) {
                insWrap.InsuranceType = 'Flatrate';
            }
            //insWrap.InsuranceType = ins.Insurance_Type__c;
            insWrap.Premium = ins.Premium__c.setScale(2);
            
            insWrapperMap.put(key, insWrap);
            
            //rollup summary
            respWrapper.totalDays += Integer.valueOf(polDays);
            if(ins.Object__r.Product__c == System.label.OKKProductId){
                respWrapper.totalOKKCost += polCost;
            }else if(ins.Object__r.Product__c == System.label.RVGProductId){
                respWrapper.totalRVGCost += polCost;
            }
            
            respWrapper.totalCost += polCost;

            // MR - 21MAY19 - Reset the flags
            polDays = 0;
            polCost = 0.0;
        }        
        
        // Wrapper for the last row
        insWrap = new InsuranceWrapper();
        insWrap.name = 'Aktuelle Gesamtprämie (inkl. 5% Stempelsteuer)';
        insWrap.sdate = '';
        insWrap.edate = '';
        insWrap.days = respWrapper.totalDays;
        insWrap.cost = respWrapper.totalCost;
        
        insWrapperMap.put('Total', insWrap);
        
        respWrapper.insList = insWrapperMap.values();
                               
        // Summary List        
        // First Row
        SummaryWrapper row1 = new SummaryWrapper();
        row1.leftSideText = 'Angefallene Gebühren diesen Monat:';
        row1.rightSideText = 'CHF ' + respWrapper.totalCost;
        respWrapper.summaryList.add(row1);
        
        // Second Row
       	SummaryWrapper row2 = new SummaryWrapper();
        // Reva -- LM-1116 - To handle the OKKPol and RVGPol amount individually in the invoice
        Decimal WOOKKAndRVGPolDue = 0.0;
        if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) > 0){
            // to store the polDue value by excluding OKK and RVG Policy due
            WOOKKAndRVGPolDue = respWrapper.totalCost - (respWrapper.totalOKKCost + respWrapper.totalRVGCost);
        }
        if (viuBalance > 0) {
            row2.leftSideText = 'Abzüglich Gutschein / Guthaben:';
            // when OKK and RVG present and Voucher amt is enough for remaining polDue
            if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) > 0.0 && viuBalance >= WOOKKAndRVGPolDue){
                row2.rightSideText = 'CHF -' + WOOKKAndRVGPolDue;
                // MR - 06AUG19 - Added for LM-575
                respWrapper.viuAmount = WOOKKAndRVGPolDue;
            }
            // when OKK and RVG present and Voucher amt is not enough for remaining polDue OR 
            // when OKK and RVG not present and Voucher amt is not enough for remaining polDue
            else if(((respWrapper.totalOKKCost + respWrapper.totalRVGCost) > 0.0 && viuBalance < WOOKKAndRVGPolDue) ||
                     ((respWrapper.totalOKKCost + respWrapper.totalRVGCost) <= 0.0 && viuBalance < respWrapper.totalCost)){
                row2.rightSideText = 'CHF -' + viuBalance;                
                // MR - 06AUG19 - Added for LM-575
                respWrapper.viuAmount = viuBalance;
            }
            // when OKK and RVG not present and Voucher amt is enough for remaining polDue
            else if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) <= 0.0 && viuBalance >= respWrapper.totalCost){
                row2.rightSideText = 'CHF -' + respWrapper.totalCost;
                // MR - 06AUG19 - Added for LM-575
                respWrapper.viuAmount = respWrapper.totalCost;
            }
            
            respWrapper.summaryList.add(row2);
        }
        
        //Third Row 
        SummaryWrapper row3 = new SummaryWrapper();
        row3.leftSideText = 'Übertrag letzte Periode(n):';
        row3.rightSideText = 'CHF 0.00';
        respWrapper.summaryList.add(row3);
        
        
		// Reva -- LM-1116 - To handle the OKKPol and RVGPol amount individually in the invoice
		// when OKK and RVG present and Voucher amt is enough for remaining polDue
        if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) > 0.0 && viuBalance >= WOOKKAndRVGPolDue){
            Total = (respWrapper.totalOKKCost + respWrapper.totalRVGCost);
        }
        // when OKK and RVG present and Voucher amt is not enough for remaining polDue
        else if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) > 0.0 && viuBalance < WOOKKAndRVGPolDue){
            Total = (WOOKKAndRVGPolDue - viuBalance)+(respWrapper.totalOKKCost + respWrapper.totalRVGCost);
        }
        // when OKK and RVG not present and Voucher amt is enough for remaining polDue
        else if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) <= 0.0 && viuBalance >= respWrapper.totalCost){
            Total = 0.0;
        }
        // when OKK and RVG not present and Voucher amt is not enough for remaining polDue
        else if((respWrapper.totalOKKCost + respWrapper.totalRVGCost) <= 0.0 && viuBalance < respWrapper.totalCost){
            Total = respWrapper.totalCost - viuBalance;
        }   
        
        // MR - 06JUN19 - Added to Txn Amount
        respWrapper.txnAmount = Total;
        //respWrapper.viuAmount = viuBalance; // MR - 06AUG19 - Commented for LM-575

        SummaryWrapper row4 = new SummaryWrapper();
        row4.leftSideText = 'Total Betrag:';
        row4.rightSideText ='CHF ' + Total;
        respWrapper.summaryList.add(row4);
        
        return respWrapper;
    }
    
    // Create Invoice records for Payment Tracking    
    public Invoice__c createInvoice(id contact, decimal amt,decimal OKKAmt,decimal RVGAmt, decimal voucherDeduction) {
        Invoice__c inv = new Invoice__c();
        inv.Name = invoiceName;
        inv.Contact__c = contact;
        inv.Amount__c = amt;
        inv.OKKPolicyAmount__c = OKKAmt;
        inv.RVGPolicyAmount__c = RVGAmt;
        inv.Voucher_Deductions__c = voucherDeduction;
        inv.Related_Policy_Charges__c = amt; //12July2019-- This field will Track all the Policy Charges related to the Invoice.
        inv.Date__c = invoiceEndDate; //System.today();
        inv.Status__c = 'New';
        
        // MR - 17JUL19 Added for tracking the template in test invoices
        String queryString = 'Select id, Name, Status__c,OKKPolicyAmount__c,RVGPolicyAmount__c, Date__c, Contact__c from Invoice__c where Contact__c = \'' + contactId + '\' and createddate < this_month order by createddate desc limit 1';
        Invoice__c lastInvoice = null;
        try {
            lastInvoice = Database.query(queryString);
        } catch(Exception ire) {
            lastInvoice = null;
        }
        
        if(lastInvoice != null && lastInvoice.Status__c != 'Success') {
        	inv.Template_to_be_used__c = 'Past Month Failed Invoices';
        } else {
         	inv.Template_to_be_used__c = 'Monthly-Receipt';
        }
        return inv;
    }
    
    // Create Attachment
    public String createAttachmentRecord(Id contactId, Id invId) {
        PageReference pdf = Page.InvoicePage;

        // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('id', contactId);
        pdf.getParameters().put('invDate', ''+invoiceEndDate);

        // the contents of the attachment from the pdf
        Blob body = null;
        
        try {
            body = pdf.getContentAsPdf();
        } catch (VisualforceException e) {
            body = Blob.valueOf('No Data Available!!');
        }
        
        // Create attachment
        Attachment att = new Attachment(ParentId = invId,
                                        Body = body,
                                        Name = invWrapper.invName + '.pdf');
        
        try {
	        insert att;
	        return att.Id;
        } catch (Exception aie) {
            // Send Exception email to Admin
            Utility.sendException(aie, 'Error creating Attachment for ' + contactId);
        }
        
		return null;
    }
    
    // Generate Email with Attachment
    public Messaging.SingleEmailMessage generateInvoiceEmail(String contactId, String receiverAddr, String attachmentId) {
        //16JULY2019--New template is added.
        String templateId;
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();  
        String queryString = 'Select id, Name, Status__c, Date__c, Contact__c from Invoice__c where Contact__c = \'' + contactId + '\' and createddate < this_month order by createddate desc limit 1';
        Invoice__c lastInvoice = null;
        try {
            lastInvoice = Database.query(queryString);
        } catch (Exception lie) {
            lastInvoice = null;
        }
        if(queryString != '' && lastInvoice != null && lastInvoice.Status__c != 'Success') {
            //template for Lastmonth failed Invoice.
        	EmailTemplate template = [Select id from EmailTemplate where name=:'Past Month Failed Invoices'];
            templateId = template.Id;
        } else {
         	//template for the Invoice.
        	EmailTemplate template = [Select id from EmailTemplate where name=:'Monthly-receipt'];           
            templateId = template.Id;
        }

        //Set email file attachments
        Attachment att = [Select Id, name, body from Attachment WHERE Id=: attachmentId];            
        efa.setFileName(att.Name);
        efa.setBody(att.Body);

        Messaging.SingleEmailMessage invEmail = new Messaging.SingleEmailMessage();
        invEmail.setUseSignature(false);
        invEmail.setToAddresses(new String[] { receiverAddr });
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        invEmail.setOrgWideEmailAddressId(owea.get(0).Id); 
     	invEmail.setFileAttachments(new List<Messaging.Emailfileattachment>{ efa }); 
        invEmail.setTargetObjectId(contactId);
        invEmail.setTemplateId(templateId);
        invEmail.setSaveAsActivity(true);
        
        return invEmail;
    }
    
    //To generate as pdf
    public PageReference sendPdf() {
        // Create the Invoice 
        Invoice__c inv = createInvoice(contactId, this.invWrapper.totalCost,this.invWrapper.totalOKKCost,this.invWrapper.totalRVGCost, this.invWrapper.viuAmount);

        // MR 06JUN19 - Added for Voucher and Txn Amount
        //inv.Voucher_Deductions__c = this.invWrapper.viuAmount;
        //inv.Transaction_Amount__c = this.invWrapper.txnAmount;

        try {
			insert inv;   
        } catch (Exception invE) {
            // Send exception email to Admin
            Utility.sendException(invE, 'Error Creating Invoice Record');
			if(vfPageFlag) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Creating Invoice Record')); }
        	return null;
        }
        
        invWrapper.invId = inv.Id;
        // MR - 06JUN19 - Setting the date to first of the Month
        invWrapper.invName = invoiceName;
        
        String attId = createAttachmentRecord(contactId, inv.Id);
        
        // Update Policy with the Id
        List<Policy__c> polList = new List<Policy__c>();
        for(String polId: invPolicySet) {
            Policy__c pol = new Policy__c(Id=polId);
            pol.Invoice__c = inv.Id;
            polList.add(pol);
        }
        
        if(polList.size() > 0) {
            try {
		        update polList;   
            } catch (Exception pole) {
                Utility.sendException(pole, 'Error Updating the Policies with InvoiceId');
                // Delete Attachment and Invoice
                if(vfPageFlag) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Updating the Policies with InvoiceId. ' + JSON.serialize(polList))); }
        		return null;
            }
        }
        
        // MR - 06JUN19 - Added Condition to Send Email based on the Label
        if(System.label.Send_Invoice_Email.equalsIgnoreCase('yes')) {
            Messaging.SingleEmailMessage email = generateInvoiceEmail(contactId, this.invWrapper.email, attId);
            
            try{
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            } catch(Exception bounceAdd){
                Utility.sendException(bounceAdd, 'Error Sending the invoice to the customer');
                if(vfPageFlag) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Sending the invoice to the customer. ' + bounceAdd.getMessage())); }
                return null;
            }
            
            if(vfPageFlag) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with PDF sent to ' + this.invWrapper.email)); }
        } else {
            if(vfPageFlag) { ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email NOT sent to ' + this.invWrapper.email)); }
        }
        
        return null;
    }   
}