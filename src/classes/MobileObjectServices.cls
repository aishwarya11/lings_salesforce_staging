@RestResource(urlMapping='/object/*')
global with sharing class MobileObjectServices {
    @HttpPost
    global static String create(String inpContact, String inpJson) {
        if(inpContact == '') {
            User loginUser = [Select ContactId from User where id = :UserInfo.getUserId()];
            inpContact = loginUser.ContactId;
        }
        UIObjectController objInstance = new UIObjectController(inpContact);
        return objInstance.createObject(inpJson);
    }
}