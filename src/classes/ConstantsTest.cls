@isTest
public class ConstantsTest {
    
    testmethod public static void testConstantsTest() {
        Constants.FLATRATE_STRING =  'OnDemand';
        Constants.ONDEMAND_STRING =  'FlatRate';
        Constants.SP_ALIAS_ASSERT_EP =  'https://test.saferpay.com/api/Payment/v1/Alias/AssertInsert';
        Constants.SP_ALIAS_INSERT_EP =   'https://test.saferpay.com/api/Payment/v1/Alias/Insert';
        Constants.SP_CUSTOMER_ID =  '245912';
        Constants.SP_TERMINAL_ID = '17935452';
        Constants.SP_TXN_EP = 'https://test.saferpay.com/api/Payment/v1/Transaction/AuthorizeDirect';
    }

}