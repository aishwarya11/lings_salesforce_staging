@isTest
public class BatchPaymentFailureTest {
    testmethod public static void testBatchPaymentFailureTest() {
        
        //Contact details
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        insert con;        
        
        //Invoice details
        Invoice__c inv = new Invoice__c();
        inv.Amount__c = 200;
        inv.Contact__c = con.Id;
        inv.Voucher_Deductions__c =100;
        inv.Status__c = 'Failed';
        inv.Email__c = con.Email;  
        inv.Date__c = System.today();
        insert inv;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 200;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Failed';
        inv1.Email__c = con.Email;
        inv1.Date__c = System.today();
        insert inv1;        
        
        //Payment details
        Payment__c pay = new Payment__c();
        pay.Contact__c = con.Id;
        pay.Invoice__c = inv.Id;
        pay.Date__c = System.today();
        pay.Paid__c = 100;
        pay.Status__c = 'Card expired';
        insert pay;    
        
        Payment__c pay1 = new Payment__c();
        pay1.Contact__c = con.Id;
        pay1.Invoice__c = inv.Id;
        pay1.Date__c = System.today();
        pay1.Paid__c = 100;
        pay1.Status__c = 'Transaction declined by acquirer';
        insert pay1;      
        
        inv.Card_Payment__c = pay.Id;
        update inv;    
        
        inv1.Card_Payment__c = pay1.Id;
        update inv1;   
        
        Test.startTest();
        // Instantiate apex batch class
        BatchPaymentFailure BCE = new BatchPaymentFailure(); // Add you Class Name
        DataBase.executeBatch(BCE, 10);
        Test.stopTest();        
    }
    
}