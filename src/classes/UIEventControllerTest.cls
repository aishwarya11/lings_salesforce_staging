@isTest
public class UIEventControllerTest {
    @testSetup static void setup() {
        Account acc = new Account();
        acc.Name = 'Test Account 1';
        insert acc;
        
        List<Contact> conList = new List<Contact>();
        
        Contact con1 = new Contact(lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con1.email='test1@test.com';
        con1.Token__c ='samplevalue';
        con1.AccountId = acc.Id;
        conList.add(con1);
        
        Contact con2 = new Contact(lastname='lastname2',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con2.email='test2@test.com';
        con2.Token__c ='samplevalue';
        con2.AccountId = System.Label.AccountId;
        conList.add(con2);
        
        insert conList;
    }
    
    public class EventWithObject {
        public String uuid;
        public String name;
        public Date periodFrom;
        public Date periodTo;
        public List<Object__c> insuredProducts;
        // new element to create OKKObject -- 14Jan2020
        public Boolean createOKKFlag;
        public Boolean createRVGFlag;
        public Decimal OKKPerDayPre = 0;
        public Decimal OKKTotalPre = 0;
        public Boolean IsOKKObjPresent;
        public Boolean isParentOfOKK;
    }
    
    testmethod public static void TestUserPolicy() {
        UIEventController UIEC = new UIEventController();
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;        
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Flatrate_Premium__c=10.00;
        PO.OnDemand_Premium__c=6.00;
        PO.DisplayUrl='https://image.url';
        insert PO;
        
        List<Object__c> objlist = new List<Object__c>();
        Object__c obj = new Object__c();
        obj.Product__c=PO.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='ondemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        objlist.add(obj);
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.End_Date__c=System.today();
        insert pol;        
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        EventWithObject  EveJson = new EventWithObject(); 
        EveJson.uuid = eve.id;
        EveJson.name = eve.name;
        EveJson.periodFrom = System.today();
        EveJson.periodTo = System.today()+1;
        EveJson.insuredProducts = objlist;
        String EveJson1 = JSON.serialize(EveJson);
        
        UIEventController UIEC1 = new UIEventController(con.Id);
        UIEC.setContext(con.Id);
        UIEC.getRecords();
        UIEC.createRecords(EveJson1);
        UIEC.readRecords(eve.Id);
        UIEC.updateRecords(EveJson1);
        UIEC.deleteRecords(eve.id);
        
    }
    testmethod public static void TestUserPolicy2() {
        UIEventController UIEC = new UIEventController();
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;        
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Flatrate_Premium__c=10.00;
        PO.OnDemand_Premium__c=6.00;
        PO.DisplayUrl='https://image.url';
        insert PO;
        
        List<Object__c> objlist = new List<Object__c>();
        Object__c obj = new Object__c();
        obj.Product__c=PO.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='flatRate';
        obj.Flatrate_Renewal_Flag__c = true;
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        objlist.add(obj);
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.End_Date__c=System.today();
        insert pol;            
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        EventWithObject  EveJson = new EventWithObject(); 
        //EveJson.uuid = eve.id;
        EveJson.name = eve.name;
        EveJson.periodFrom = System.today();
        EveJson.periodTo = System.today()+1;
        EveJson.insuredProducts = objlist;
        String EveJson1 = JSON.serialize(EveJson);
        
        delete obj;
        
        UIEventController UIEC1 = new UIEventController(con.Id);
        UIEC.setContext(con.Id);
        UIEC.getRecords();
        UIEC.createRecords(EveJson1);
        UIEC.readRecords(eve.Id);
        UIEC.updateRecords(EveJson1);
        UIEC.deleteRecords(eve.id);
        
    }
    testmethod public static void TestUserPolicy3() {
        UIEventController UIEC = new UIEventController();
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Flatrate_Premium__c=10.00;
        PO.OnDemand_Premium__c=6.00;
        PO.DisplayUrl='https://image.url';
        insert PO;
        
        List<Object__c> objlist = new List<Object__c>();
        Object__c obj = new Object__c();
        obj.Product__c=PO.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='ondemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        objlist.add(obj);          
        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.End_Date__c=System.today();
        insert pol;          
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        EventWithObject  EveJson = new EventWithObject(); 
        EveJson.uuid = eve.id;
        EveJson.name = eve.name;
        EveJson.periodFrom = System.today();
        EveJson.periodTo = System.today()+1;
        EveJson.insuredProducts = objlist;
        String EveJson1 = JSON.serialize(EveJson);
        
        UIEventController UIEC1 = new UIEventController(con.Id);
        UIEC.setContext(con.Id);
        UIEC.getRecords();
        UIEC.createRecords(EveJson1);
        UIEC.readRecords(eve.Id);
        UIEC.updateRecords(EveJson1);
        UIEC.deleteRecords(eve.id);
        
    }
    testmethod public static void TestUserPolicy4() {
        UIEventController UIEC = new UIEventController();
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Flatrate_Premium__c=10.00;
        PO.OnDemand_Premium__c=6.00;
        PO.DisplayUrl='https://image.url';
        insert PO;
        
        List<Object__c> objlist = new List<Object__c>();
        Object__c obj = new Object__c();
        obj.Product__c=PO.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='flatrate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        objlist.add(obj); 
        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.End_Date__c=System.today();
        insert pol;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        EventWithObject  EveJson = new EventWithObject(); 
        EveJson.uuid = eve.id;
        EveJson.name = eve.name;
        EveJson.periodFrom = System.today();
        EveJson.periodTo = System.today()+1;
        EveJson.insuredProducts = objlist;
        String EveJson1 = JSON.serialize(EveJson);
        
        UIEventController UIEC1 = new UIEventController(con.Id);
        UIEC.setContext(con.Id);
        UIEC.getRecords();
        UIEC.createRecords(EveJson1);
        UIEC.readRecords(eve.Id);
        UIEC.updateRecords(EveJson1);
        UIEC.deleteRecords(eve.id);
        
    }
    
    testmethod public static void TestUserPolicy5() {
        try{
            UIEventController UIEC = new UIEventController();
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User'];
            List<Contact> contactList = [Select Id, Name from Contact where Token__c = 'samplevalue'];
            
            UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert r;
            
            List<User> uList = new List<User>();
            
            User u = new User(Email='test1@test.com',
                              EmailEncodingKey='UTF-8', FirstName='User1', LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', 
                              UserName='test1@test.comtest', CommunityNickname='Lings32184');
            u.City = 'chennai';
            u.Street = 'Ashok Nagar';
            u.Phone = '988456897';
            u.DOB__c = System.today();
            u.House_Number__c = '100';
            u.PostalCode = '1000';
            u.ContactId = contactList[0].Id;
            System.runAs (new User(Id = UserInfo.getUserId())) {
                insert u;
            }  
            
            System.runAs (new User(Id = u.Id)) {
            
            Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
            insert con;  
                
                Premium__c prevalue = new Premium__c();
                prevalue.Name='travel insurance';
                prevalue.On_Demand__c=5.00;
                prevalue.FlatRate__c=5.00;
                insert prevalue;
                
                Product2 pro = new Product2(Id=System.label.OKKProductId);
                pro.Price__c = 100;
                pro.Insured_Value__c = 100;
                pro.Product_Type__c = 'Travel Insurance';
                upsert pro;
                
                Product2 PO = [SELECT id,name from Product2 where id=:System.label.OKKProductId];
                
                List<Object__c> objlist = new List<Object__c>();
                Object__c obj = new Object__c();
                obj.Product__c=PO.Id;
                obj.Customer__c=con.Id;
                obj.Insured_Value__c = 100;
                insert obj;
                objlist.add(obj);
                
                EventWithObject  EveJson = new EventWithObject(); 
                EveJson.name = 'Sample Event 123';
                EveJson.periodFrom = System.today();
                EveJson.periodTo = System.today()+1;
                EveJson.createOKKFlag = true;
                EveJson.OKKPerDayPre = 1.20;
                EveJson.OKKTotalPre = 5.00;
                String EveJson1 = JSON.serialize(EveJson);
                
                UIEventController UIEC1 = new UIEventController(con.Id);
                UIEC.createRecords(EveJson1);
            }
       }
        catch(Exception e){}
    }
    
    testmethod public static void TestUserPolicy6() {
        UIEventController UIEC = new UIEventController();
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;        
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Flatrate_Premium__c=10.00;
        PO.OnDemand_Premium__c=6.00;
        PO.DisplayUrl='https://image.url';
        insert PO;
        
        List<Object__c> objlist = new List<Object__c>();
        Object__c obj = new Object__c();
        obj.Product__c=PO.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='ondemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        objlist.add(obj);
        
        Object__c obj1 = new Object__c();
        obj1.Product__c=PO.Id;
        obj1.Customer__c=con.Id;
        obj1.Coverage_Type__c='ondemand';
        obj1.Premium_FlatRate__c=10.00;
        obj1.Premium_OnDemand__c=6.00;
        obj1.Insured_Value__c = 100;
        insert obj1;
        objlist.add(obj1);
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.End_Date__c=System.today();
        insert pol;      
        
        Policy__c pol1 = new Policy__c();
        pol1.Contact__c=con.Id;
        pol1.Object__c = obj1.Id;
        pol1.Start_Date__c=System.today();
        pol1.End_Date__c=System.today();
        insert pol1;   
        
        Event__c eve1 = new Event__c();
        eve1.Name='sample event';
        eve1.Start_Date__c=System.today();
        eve1.End_Date__c=System.today();
        eve1.Contact__c=con.Id;
        insert eve1;        
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        eve.OKKChildEventId__c = eve1.Id;
        eve.RVGChildEventId__c = eve1.Id;
        eve.OKKTVId__c = eve1.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        Event_Object__c EO1 = new Event_Object__c();
        EO1.Object__c=obj1.Id;
        EO1.Event__c=eve1.Id;
        insert EO1;
        
        EventWithObject  EveJson = new EventWithObject(); 
        EveJson.uuid = eve.id;
        EveJson.name = eve.name;
        EveJson.periodFrom = System.today();
        EveJson.periodTo = System.today()+1;
        EveJson.insuredProducts = objlist;
        String EveJson1 = JSON.serialize(EveJson);
        
        UIEventController UIEC1 = new UIEventController(con.Id);
        UIEC.setContext(con.Id);
        UIEC.getRecords();
        UIEC.createRecords(EveJson1);
        UIEC.readRecords(eve.Id);
        UIEC.updateRecords(EveJson1);
        UIEC.deleteRecords(eve.id);
        
    }
    
    testmethod public static void TestUserPolicy7() {
        UIEventController UIEC = new UIEventController();
        
        RVGPolicyNumber__c rp = new RVGPolicyNumber__c();
        rp.Name = '20200';
        insert rp;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.email='test@test.com';
        insert con;        
        
        Product2 PO= new Product2(Id=System.Label.RVGProductId);
        PO.Name = 'Sample name';
        PO.Price__c = 250000;
        PO.Insured_Value__c = 250000;
        PO.Flatrate_Premium__c = 1;
        PO.OnDemand_Premium__c = 0.25;
        update PO;
        
        List<Object__c> objlist = new List<Object__c>();
        Object__c obj = new Object__c();
        obj.Product__c = PO.Id;
        obj.Customer__c=con.Id;
        obj.Coverage_Type__c='ondemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Insured_Value__c = 100;
        insert obj;
        objlist.add(obj);
        
        Object__c obj1 = new Object__c();
        obj1.Product__c=PO.Id;
        obj1.Customer__c=con.Id;
        obj1.Coverage_Type__c='ondemand';
        obj1.Premium_FlatRate__c=10.00;
        obj1.Premium_OnDemand__c=6.00;
        obj1.Insured_Value__c = 100;
        insert obj1;
        objlist.add(obj1);
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.End_Date__c=System.today();
        insert pol;      
        
        Policy__c pol1 = new Policy__c();
        pol1.Contact__c=con.Id;
        pol1.Object__c = obj1.Id;
        pol1.Start_Date__c=System.today();
        pol1.End_Date__c=System.today();
        insert pol1;   
        
        Event__c eve1 = new Event__c();
        eve1.Name='sample event';
        eve1.Start_Date__c=System.today();
        eve1.End_Date__c=System.today();
        eve1.Contact__c=con.Id;
        insert eve1;        
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        eve.OKKChildEventId__c = eve1.Id;
        eve.RVGChildEventId__c = eve1.Id;
        eve.OKKTVId__c = eve1.Id;
        insert eve;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c=obj.Id;
        EO.Event__c=eve.Id;
        insert EO;
        
        Event_Object__c EO1 = new Event_Object__c();
        EO1.Object__c=obj1.Id;
        EO1.Event__c=eve1.Id;
        insert EO1;
        
        EventWithObject  EveJson = new EventWithObject(); 
        EveJson.uuid = eve.id;
        EveJson.name = eve.name;
        EveJson.periodFrom = System.today();
        EveJson.periodTo = System.today()+1;
        EveJson.insuredProducts = objlist;
        EVeJson.createRVGFlag = true;
        String EveJson1 = JSON.serialize(EveJson);
        
        UIEventController UIEC1 = new UIEventController(con.Id);
        UIEC.setContext(con.Id);
        UIEC.getRecords();
        try{
            UIEC.createRecords(EveJson1);
        }catch(Exception e){}
        UIEC.readRecords(eve.Id);
        UIEC.updateRecords(EveJson1);
        UIEC.deleteRecords(eve.id);
        
    }
}