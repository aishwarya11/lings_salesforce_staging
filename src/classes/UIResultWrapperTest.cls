@isTest
public class UIResultWrapperTest {
    
    testmethod public static void testUIResultWrapper() {
        //declaring required test data 
        List<Event__c> eve = new List<Event__c>();
        for(Integer i=5;i<10;i++) {
            Contact con = new Contact(lastname='lastname'+i,Password__c='123pwd4598'+i);
            insert con;
            Event__c a = new Event__c(Name='TestEvent__c' + i,Contact__c=con.id);
            eve.add(a);
        }
        insert eve;
        
        //calling the inner class called ProductInfo from the UIResultWrapper
        UIResultWrapper.ProductInfo page = new UIResultWrapper.ProductInfo();
        page.uuid = 'test';
        page.thumbnail = 'Sample Image';
        page.model = 'test';
        page.name = 'Hello';
        page.description = 'This is the Test Class';
        page.price = 20;
        page.ebike = false;
        page.year = '2019';
        page.category = 'Laptop';
        page.compareTo(page);
        
        //calling the inner class called Profile from the UIResultWrapper
        UIResultWrapper.Profile page1 = new UIResultWrapper.Profile();
        page1.firstName = 'Hello';
        
        //calling the inner class called BalanceInfo from the UIResultWrapper
        UIResultWrapper.BalanceInfo page2 = new UIResultWrapper.BalanceInfo();
        page2.currencyCode = 'CHF';
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.MyObject page3 = new UIResultWrapper.MyObject();
        page3.currencyCode = 'CHF';
        
        //calling methods
        UIResultWrapper.toEventInfoList(eve);
        UIResultWrapper.getOnDemandWrapper(0);
        UIResultWrapper.getFlatRateWrapper(0.00);
        UIPaymentWrapper UIPW = new UIPaymentWrapper();
        UIPaymentWrapper.SPAlias SPA = new UIPaymentWrapper.SPAlias();
        UIResultWrapper.getIndexValue('Apple');            
        UIResultWrapper.getIndexValue('b');
        UIPaymentWrapper.SPAmount SPAAmount = new UIPaymentWrapper.SPAmount();
        SPAAmount.currencyCode='CHF';
    }
    
    testmethod public static void testUIResultWrapper1() {
        //declaring required test data 
        List<Event__c> eve = new List<Event__c>();
        for(Integer i=5;i<10;i++) {
            Contact con = new Contact(lastname='lastname'+i,Password__c='123pwd4598'+i);
            insert con;
            Event__c a = new Event__c(Name='TestEvent__c' + i,Contact__c=con.id);
            eve.add(a);
        }
        insert eve;
        
        //calling the inner class called ProductInfo from the UIResultWrapper
        UIResultWrapper.ProductInfo page = new UIResultWrapper.ProductInfo();
        page.uuid = 'test';
        page.thumbnail = 'Sample Image';
        page.model = 'test';
        page.name = 'Hello';
        page.description = 'This is the Test Class';
        page.price = 20;
        page.ebike = false;
        page.year = '2019';
        page.category = 'Laptop';
        page.compareTo(page);
        
        //calling the inner class called Profile from the UIResultWrapper
        UIResultWrapper.Profile page1 = new UIResultWrapper.Profile();
        page1.firstName = 'Hello';
        
        //calling the inner class called BalanceInfo from the UIResultWrapper
        UIResultWrapper.BalanceInfo page2 = new UIResultWrapper.BalanceInfo();
        page2.currencyCode = 'CHF';
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.MyObject page3 = new UIResultWrapper.MyObject();
        page3.currencyCode = 'CHF';
        
        
        //calling methods
        List<String> attList = new List<String>();
        List<UIObjectWrapper.LingsObject> insPdt = new List<UIObjectWrapper.LingsObject>();
        
        
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.ClaimWithObject page4 = new UIResultWrapper.ClaimWithObject();
        page4.attachments = attList;
        page4.insuredProducts = insPdt;
        
        
        
        Set<String> SRCH_KEYWORDS_Set = new Set<String>();
        SRCH_KEYWORDS_Set.add('Apple');
        SRCH_KEYWORDS_Set.add('C');
        SRCH_KEYWORDS_Set.add('ash');
        UIResultWrapper.SRCH_KEYWORDS = SRCH_KEYWORDS_Set;
        UIResultWrapper.toEventInfoList(eve);
        UIResultWrapper.getOnDemandWrapper(0);
        UIResultWrapper.getFlatRateWrapper(0.00);
        UIPaymentWrapper UIPW = new UIPaymentWrapper();
        UIPaymentWrapper.SPAlias SPA = new UIPaymentWrapper.SPAlias();
        UIResultWrapper.getIndexValue('Apple');            
        UIResultWrapper.getIndexValue('b');
        UIResultWrapper.getIndexValue('Cat'); 
        UIResultWrapper.getIndexValue('dash');     
        UIPaymentWrapper.SPAmount SPAAmount = new UIPaymentWrapper.SPAmount();
        SPAAmount.currencyCode='CHF';
    }
    
    
    
    testmethod public static void testUIResultWrapper2() {
        //declaring required test data 
        List<Event__c> eve = new List<Event__c>();
        for(Integer i=5;i<10;i++) {
            Contact con = new Contact(lastname='lastname'+i,Password__c='123pwd4598'+i);
            insert con;
            Event__c a = new Event__c(Name='TestEvent__c' + i,Contact__c=con.id);
            eve.add(a);
        }
        insert eve;
        
        //calling the inner class called Profile from the UIResultWrapper
        UIResultWrapper.Profile page1 = new UIResultWrapper.Profile();
        page1.firstName = 'Hello';
        
        //calling the inner class called BalanceInfo from the UIResultWrapper
        UIResultWrapper.BalanceInfo page2 = new UIResultWrapper.BalanceInfo();
        page2.currencyCode = 'CHF';
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.MyObject page3 = new UIResultWrapper.MyObject();
        page3.currencyCode = 'CHF';
        
        
        //calling methods
        List<String> attList = new List<String>();
        List<UIObjectWrapper.LingsObject> insPdt = new List<UIObjectWrapper.LingsObject>();
        
        
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.ClaimWithObject page4 = new UIResultWrapper.ClaimWithObject();
        page4.attachments = attList;
        page4.insuredProducts = insPdt;
        
        UIResultWrapper.SRCH_KEYWORD = 'Apple';
        
        //calling the inner class called ProductInfo from the UIResultWrapper
        UIResultWrapper.ProductInfo page = new UIResultWrapper.ProductInfo();
        page.uuid = 'test';
        page.thumbnail = 'Sample Image';
        page.model = 'test';
        page.name = 'Hello';
        page.description = 'This is the Test Class';
        page.price = 20;
        page.ebike = false;
        page.year = '2019';
        page.sortIndex = 2;
        page.category = 'Laptop';
        page.compareTo(page);
        
        UIResultWrapper.toEventInfoList(eve);
        UIResultWrapper.getOnDemandWrapper(0);
        UIResultWrapper.getFlatRateWrapper(0.00);
        UIPaymentWrapper UIPW = new UIPaymentWrapper();
        UIPaymentWrapper.SPAlias SPA = new UIPaymentWrapper.SPAlias();
        UIResultWrapper.getIndexValue('Apple');            
        UIResultWrapper.getIndexValue('b');
        UIResultWrapper.getIndexValue('Cat'); 
        UIResultWrapper.getIndexValue('ash');     
        UIPaymentWrapper.SPAmount SPAAmount = new UIPaymentWrapper.SPAmount();
        SPAAmount.currencyCode='CHF';
    }
    
    testmethod public static void testUIResultWrapper3() {
        //declaring required test data 
        List<Event__c> eve = new List<Event__c>();
        for(Integer i=5;i<10;i++) {
            Contact con = new Contact(lastname='lastname'+i,Password__c='123pwd4598'+i);
            insert con;
            Event__c a = new Event__c(Name='TestEvent__c' + i,Contact__c=con.id);
            eve.add(a);
        }
        insert eve;
        
        //calling the inner class called Profile from the UIResultWrapper
        UIResultWrapper.Profile page1 = new UIResultWrapper.Profile();
        page1.firstName = 'Hello';
        
        //calling the inner class called BalanceInfo from the UIResultWrapper
        UIResultWrapper.BalanceInfo page2 = new UIResultWrapper.BalanceInfo();
        page2.currencyCode = 'CHF';
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.MyObject page3 = new UIResultWrapper.MyObject();
        page3.currencyCode = 'CHF';
        
        
        //calling methods
        List<String> attList = new List<String>();
        List<UIObjectWrapper.LingsObject> insPdt = new List<UIObjectWrapper.LingsObject>();
        
        
        
        //calling the inner class called MyObject from the UIResultWrapper
        UIResultWrapper.ClaimWithObject page4 = new UIResultWrapper.ClaimWithObject();
        page4.attachments = attList;
        page4.insuredProducts = insPdt;
        
        UIResultWrapper.SRCH_KEYWORD = 'ash';
        
        //calling the inner class called ProductInfo from the UIResultWrapper
        UIResultWrapper.ProductInfo page = new UIResultWrapper.ProductInfo();
        page.uuid = 'test';
        page.thumbnail = 'Sample Image';
        page.model = 'test';
        page.name = 'Hello';
        page.description = 'This is the Test Class';
        page.price = 20;
        page.ebike = false;
        page.year = '2019';
        page.sortIndex = 2;
        page.category = 'Laptop';
        page.compareTo(page);
        
        UIResultWrapper.toEventInfoList(eve);
        UIResultWrapper.getOnDemandWrapper(0);
        UIResultWrapper.getFlatRateWrapper(0.00);
        UIPaymentWrapper UIPW = new UIPaymentWrapper();
        UIPaymentWrapper.SPAlias SPA = new UIPaymentWrapper.SPAlias();
        UIResultWrapper.getIndexValue('Apple');            
        UIResultWrapper.getIndexValue('b');
        UIResultWrapper.getIndexValue('Cat'); 
        UIResultWrapper.getIndexValue('dash');     
        UIPaymentWrapper.SPAmount SPAAmount = new UIPaymentWrapper.SPAmount();
        SPAAmount.currencyCode='CHF';
    }
    
}