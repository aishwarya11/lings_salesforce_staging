//Policy__c object fields Start date and End Date are get renewed for next 30 days, if the user doesn’t off their Flatrate policies even after the enddate.
@TestVisible
global class BatchPolicyRenewal implements Database.Batchable<Policy__c>, Database.Stateful {
    public List<Policy__c> successPolicyUpdateList = new List<Policy__c>();
    public List<Policy__c> failedPolicyUpdateList = new List<Policy__c>();
    public List<Object__c> successObjectUpdateList = new List<Object__c>();
    public List<Object__c> failedObjectUpdateList = new List<Object__c>();
    //Start Batchable
    global Iterable<Policy__c> start(Database.BatchableContext BC){
        System.debug('BatchPolicyRenewal START');
        
        // 15JUL19 - MR - Added filter conditions to avoid Duplicate policies
        List<Policy__c> query = [SELECT Id, Name, Invoice__c, Object__c, Invoice_Start_Date__c, Contact__c,
                                 Start_Date__c, End_Date__c, Object__r.Flatrate_Renewal_Flag__c, Active__c,
                                 Object__r.Product_Source__c, Premium__c, Status__c, Insured_value__c, 
                                 Object__r.Insured_Value__c, Object__r.Premium_Flatrate__c,
                                 Insurance_type__c, Policy_Charges__c
                                 FROM Policy__c 
                                 WHERE Object__r.Flatrate_Renewal_Flag__c = True 
                                 AND Status__c <> 'Claimed' 
                                 AND Status__c <> 'SplitSource'
                                 AND End_Date__c = TODAY 
                                 AND Insurance_type__c = 'FlatRate'];	// 22JULY2019--added insurance type condition to avoid Ondemand.
        return query;
    }
    
    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Policy__c> scope) {
        System.debug('BatchPolicyRenewal EXECUTE');
        
        List<Policy__c> policyRenewal = new List<Policy__c>();
        List<Policy__c> policyUpdates = new List<Policy__c>();
        List<Object__c> updateObject = new List<Object__c>();
        Map<Id, Object__c> updateObjectMap = new Map<Id, Object__c>();
        //LM-362 To have a separate query for the Object 10APR2019
        Set<Id> objIds = new Set<Id>();
        Integer diffDays;
        Integer chargedDays;        
        for(Policy__c pol : scope) {
            objIds.add(pol.Object__c);
        }
        
        // Now querying all the Product details from the Object
        // MR 07JUN19 - changed to avoid exception on copying the price, premium from brands
        for(Object__c obj : [SELECT id, Name, Insured_Value__c, Insurance_End_Date__c, Premium_FlatRate__c, 
                             Premium_OnDemand__c, FlatRate_Savings__c, Is_Insured__c, Product__r.Insured_value__c, 
                             Product__r.Flatrate_Premium__c, Product__r.OnDemand_Premium__c, Product__r.Source__c,
                             Product__r.Flatrate_savings__c FROM Object__c WHERE ID = :objIds]) {
                                 //To check the Product's Insured value and the Object's Insured Value.
                                 if(obj.Product__r.Source__c != 'DB Bike'){		//18JULY2019--We ignore the update when the Source is Bike.
                                     obj.Insured_Value__c = obj.Product__r.Insured_value__c;
                                     obj.Premium_FlatRate__c = obj.Product__r.Flatrate_Premium__c;
                                     obj.Premium_OnDemand__c = Obj.Product__r.OnDemand_Premium__c;
                                     obj.FlatRate_Savings__c = obj.Product__r.Flatrate_savings__c;                                                                          
                                 }     
                                 updateObjectMap.put(obj.id, obj);   //LM-701--Map placed outside the if Cond. --04OCT2019
                             }
        
        for(Policy__c pol : scope) {
            // MR - 06JUN19 - Updated Invoice Start Date based on the Invoice Status           
            // 05JUL Create new Policy at the Month-end.
            Policy__c newPol = new Policy__c();
            newPol.Active__c = true;
            newPol.Contact__c = pol.Contact__c;
            newPol.Object__c = pol.Object__c;
            newPol.Insurance_Type__c = Constants.FLATRATE_STRING;
            DateTime getDate = pol.End_Date__c.addDays(1);
            newPol.Start_Date__c = Datetime.newInstance(getDate.date(), Utility.policyStartTime);
            newPol.Invoice_Start_Date__c = newPol.Start_Date__c.date();
            newPol.Status__c = Constants.POL_STATUS_INSURED;
            newPol.Policy_Source__c = 'Object';
            Date ed = System.Today() + 1;
            // 22APR19 - LM398 - MR - Fix for FlatRate EndDate
            // LM-721-- Changed the input value for the below methos from 'NULL' to getDate --04OCT2019
            newPol.End_Date__c = Datetime.newInstance(ed.addDays(Utility.numOfDaysForEndDate(getDate.date())), Utility.policyEndTime);
            //12July2019--To calculate Policy Charges on Creating the Policy.
			diffDays =  newPol.Invoice_Start_Date__c.daysBetween(newPol.End_Date__c.Date());
			chargedDays = diffDays + 1;                     
                    
            // MR - 27MAY19 - Made Changes for updating the Object End Date - START
            Object__c polObj = new Object__c(Id=pol.Object__c);
            // MR - 27MAY19 - Made Changes for updating the Object End Date - END
            // LM- 652--On new Policy Creation for the Bike Premium and Insured Value were updated as Null--06SEPT2019
            if(updateObjectMap.containsKey(pol.Object__c)) { 
                polObj = updateObjectMap.get(pol.Object__c);
                newPol.Insured_Value__c = polObj.Insured_Value__c;
                newPol.Premium__c = polObj.Premium_FlatRate__c;  
            } 
            
            if(newPol.Premium__c != NULL){
                //LM-695--Rounded the policy charges--25SEPT2019
            	newPol.Policy_Charges__c = Utility.getRoundedPremium(newPol.Premium__c * chargedDays);    
            }
            
            // MR - 27MAY19 - Made Changes for updating the Object End Date
            polObj.Policy__c = newPol.id;
            polObj.Insurance_Start_Date__c = newPol.Start_Date__c.date();
            polObj.Insurance_End_Date__c = newPol.End_Date__c.date();
            polObj.Is_Insured__c = true;    // MR - 03SEP19 - LM-639 Fix
            updateObjectMap.put(polObj.id, polObj);
            
            policyRenewal.add(newPol);
            
            // 15JUL19 - MR - Added to closing out the old policy
            pol.Status__c = 'Expired';
            pol.Active__c = false;
            policyUpdates.add(pol);
        }             
        
        
        try {
            if(policyRenewal.size() > 0){
                List<Database.SaveResult> srUpdate = Database.insert(policyRenewal);
                Integer polIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Policy__c pol = policyRenewal[polIndex];
                    if(sr.isSuccess()) {
                        Object__c updobj = updateObjectMap.get(pol.Object__c);
                        updobj.Policy__c = sr.getId();   
                        updObj.Is_Insured__c = true;    // MR - 03SEP19 - LM-639 Fix
                        System.debug('Policy Inserted Successfully!!');
                        updateObjectMap.put(updObj.Id, updobj);                        
                        successPolicyUpdateList.add(pol);  
                    } else {
                        failedPolicyUpdateList.add(pol);
                    }
                    polIndex++;
                }
            }
        } catch(Exception pn) {
            //Send Exception to Admin
            Utility.sendException(pn, 'FAILED TO RENEW THE POLICY IN THE POLICY RENEWAL BATCH');   
        }
        
        // 15JUL19 - MR - Added to closing out the old policy
        try {
            update policyUpdates;
        } catch(Exception pn) {
            //Send Exception to Admin
            Utility.sendException(pn, 'FAILED TO RENEW THE POLICY IN THE POLICY RENEWAL BATCH');   
        }
        
        updateObject = updateObjectMap.values();
        
        try {      
            if(updateObject.size() > 0){
                List<Database.SaveResult> srUpdate = Database.update(updateObject);
                Integer objIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Object__c obj = updateObject[objIndex];
                    if(sr.isSuccess()) {
                        System.debug('Object Updated Successfully !!');
                        successObjectUpdateList.add(obj);  
                    } else {
                        failedObjectUpdateList.add(obj);
                    }
                    objIndex++;
                }
            }
        } catch(Exception uo) {
            //Send Exception to Admin
            Utility.sendException(uo, 'FAILED TO UPDATE THE OBJECT IN THE POLICY RENEWAL BATCH'); 
        }       
        
    }   
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchPolicyRenewal FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 22MAY19 - Code Optimization
        mail.setSubject('BatchPolicyRenewal ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         
        
        String body = 'BatchPolicyRenewal Status on Updating Policy and Object records<br/>';
        
        // Updating Policy List   
        body += '<br/> ' + (successPolicyUpdateList.size() + failedPolicyUpdateList.size()) + ' number of Policy records update processing were completed.'; 
        if(successPolicyUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy updation details <br/>';
            
            for(Policy__c successList: successPolicyUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy updation happened.';
        }         
        if(failedPolicyUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Policy updation details <br/>';
            
            for(Policy__c failedList: failedPolicyUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Policy updation happened.';
        }
        
        // Updating Object List   
        body += '<br/> ' + (successObjectUpdateList.size() + failedObjectUpdateList.size()) + ' number of Object records update processing were completed.'; 
        if(successObjectUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Object updation details <br/>';
            
            for(Object__c successList: successObjectUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Object updation happened.';
        }         
        if(failedObjectUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Object updation details <br/>';
            
            for(Object__c failedList: failedObjectUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Object updation happened.';
        }
        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);     
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedObjectUpdateList.size() > 0 || failedPolicyUpdateList.size() > 0) {
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchPolicyRenewal';
        ab.Track_message__c = body;
        database.insertImmediate(ab);     
        }
    }
}