@RestResource(urlMapping='/delete/*')
global with sharing class MobileDeleteAttachmentService {
    @HttpPost
    global static String attachment(String ContentDocumentId){
        return UIHomeController.deleteAttachment(ContentDocumentId);
    }
}