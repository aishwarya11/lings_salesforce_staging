public with sharing class UIRegisterExt {

    @testvisible  private static Id PORTAL_ACCOUNT_ID = System.Label.AccountId;
    private static String communityNickname = 'lings';
    private String username;

    public UIRegisterExt(UIHomeController controller) {
        
    }
    
    //defining the user profile data structure 
    public class Profile {
        public String firstName;
        public String lastName;
        public Date	dob;
        public String street;
        public String houseNumber;
        public String zipCode;
        public String city;
        public String phoneNumber;
        public String email;
        public String leadSource;
        public boolean terms;
        public String password;
    }
    
    //method to register the user by passing their input in json structure 
    public String registerUser(String inpParam) {
		Profile usrProfile = (Profile)JSON.deserialize(inpParam, Profile.class);

        username = usrProfile.firstname + '.' + usrProfile.lastname + '@lings.demo';
            
        User u = new User();
        u.Username = username;
        u.Email = usrProfile.email;
		u.City = usrProfile.city;
        u.CommunityNickname = communityNickname;
        
        String accountId = PORTAL_ACCOUNT_ID;

        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        // lastName is a required field on user, but if it isn't specified, we'll default it to the username
        String userId = Site.createPortalUser(u, accountId, usrProfile.password);
        if (userId != null) { 
            if (usrProfile.password != null && usrProfile.password.length() > 1) {
                // On success show login page
                jsonGen.writeStringField('status', 'success');
            }
            else {
                // On failure show register page again
                jsonGen.writeStringField('status', 'failure');
            }
        }
        
        jsonGen.writeEndObject();
        return jsonGen.getAsString();
    }
}