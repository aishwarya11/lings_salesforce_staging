public without sharing class Utility {
    public static Time policyStartTime = Time.newInstance(0, 1, 0, 0);
    public static Time policyEndTime = Time.newInstance(23, 59, 0, 0);
    
    //method to verify whether it is valid category or not
    public static boolean isValidCategory(String catStr) {
        for(Premium__c category: Premium__c.getall().values()) {
            if(category.name.equalsIgnoreCase(catStr)) {
                return true;
            }
        }
        return false;
    }     
    
    //27MAR2020--Create new okk event
    public static void createEvent(Event__c neweve, String externalID){
       
        String eventId = '';
        String okkProductId = System.label.OKKProductId;
        Integer noOfDays = 0;
        String contactId = '';
        List<Object__c> updObjList = new List<Object__c>();
       //if (neweve.size() > 0 ) {
           insert neweve;        
       //}
        //Step 1: Get Event Id.
        String queryString = 'SELECT ID, Name, OKKChildEventId__c, Start_date__c, End_date__c, Contact__c, Comments__c FROM Event__c WHERE Comments__c=  \'' + externalId + '\'';
        Event__c updEve = Database.query(queryString);
        eventId = updEve.Id;
        contactId = updEve.Contact__c;
        noOfDays = (updEve.Start_Date__c.daysBetween(updEve.End_Date__c)) + 1;
        
        //Calculating the Premium for OKK
        Premium_CustomSetting__c  premiumCalc = new Premium_CustomSetting__c();
        String query = 'select id,Duration_days__c,Priceperday__c,Pricetotal__c from Premium_CustomSetting__c where Duration_days__c =' + noOfDays;
        premiumCalc = Database.query(query);        
        
        
        //Step 2: Create a Okk Lings Object
        Product2 pro = [Select id,name,Description,Product_Type__c,DisplayUrl,Insured_Value__c, OnDemand_Premium__c from Product2 
                            where id=:System.label.OKKProductId];
        
        Object__c newObj = new Object__c();
        newObj.Product__c = okkProductId;
        newObj.Approved__c = true;
        newObj.Customer__c = contactId;
        newObj.OKKEventStartDate__c = updEve.Start_Date__c;
        newObj.OKKEventEndDate__c = updEve.End_Date__c;
        newObj.Insured_Value__c = pro.Insured_Value__c;
        newObj.Premium_OnDemand__c = premiumCalc.Priceperday__c;
        newObj.OKKTotalPremium__c = premiumCalc.Pricetotal__c; 
        newObj.Comments__c = 'Inserted From Mobile App';
        //updObjList.add(newObj);
        
        insert newObj;
             
        //Step 3: Create a Event Object.
        Event_Object__c newEO = new Event_Object__c();
        newEO.Event__c = eventId;
        newEO.Object__c = newObj.Id;
        //newEO.Premium__c = premiumCalc.Priceperday__c;
        
        insert newEO;
    }
    
    //24DEC2019--Sending notification to the mobile device.
    @Future(callout=true)
    public static void SendFCMNotification(){
        
         string message ='test msg2';
         string title ='Hello test 123';
        
        Messaging.PushNotification msg = new Messaging.PushNotification();
        
        //For android device.
        Map<String, Object> androidPayload = new Map<String, Object>();
        
        androidPayload.put('message', message); 
        androidPayload.put('Title', title);
        msg.setPayload(androidPayload);
        
        //For Apple device.
        Map<String, Object> payload = Messaging.PushNotificationPayload.apple('message :' +message+
                                                                              'Title :' +title , '', null, null);
        
        // Adding the assembled payload to the notification
        msg.setPayload(payload);
        
        
        msg.setTtl(100);
        String userId = '0051t000002eMEn';  //'0051X000002RmDL';   //For now hard coded User Id
        
        // Adding recipient users to list
        Set<String> users = new Set<String>();
        
        users.add(userId); 
        
        msg.send('Lings_Mobile_App', users);  
    }
    
    @future
    public static void trackObject (String objId, String state){
     
        //LM-698 -- Tracking the ON and OFF of the Object -- 14NOV2019
        string queryStr = 'SELECT Id, Name, Track_button__c FROM Object__c WHERE Id  =  \'' + objId + '\'';
        Object__c obj = Database.query(queryStr);
        obj.Track_button__c = state +'  --' +System.now();
        try{
            update obj;
        } catch (Exception uo) {
            Utility.sendException(uo, 'OBJECT TRACKING FAILED!!'+uo.getStackTraceString());
        }          
    }
    
    //06MAY2019 - Isdeleted flag in the Draft Product LM-372.
    public static void updateProduct(String ProId){
        
        String queryString = 'Select id, Status__c, IsDeleted__c, Source__c, Searchable__c from Product2 where id = \'' + ProId + '\'';    
        Product2 proValue = Database.query(queryString);
        if(proValue.Source__c == 'Draft' || provalue.Source__c == 'DB Bike'){  //When the source is Bike and the reconnection happens for the bike Product.
            proValue.Status__c = 'Closed';
            proValue.IsDeleted__c = true;  
            proValue.Searchable__c = false;
            System.debug('Updated Successfully'); 
            update proValue;
        }        
    }
    
    //11Jul2019--This method is used when the Object is deleted from the event list/ when the event is deleted.
    public static void closeObjectPolicy(String objectId){
        Integer chargedDays;
        Integer diffDays;
        
        List<Object__c> ObjList = new List<Object__c>();
        String queryString = 'SELECT Id, Name, Is_Insured__c, Policy__c, Policy__r.Start_date__c, Policy__r.Premium__c, Insurance_Start_date__c, Insurance_End_date__c, Coverage_type__c FROM Object__c Where Policy__c != NULL AND Id = \'' + objectId + '\'Limit 1';
        List<Object__c> obj = database.query(queryString);
        
        if(obj.size() > 0 ){
            Object__c updObj = obj[0];
            if(updObj.Policy__c != NULL && updObj.Coverage_Type__c == 'OnDemand'){
                updObj.Is_Insured__c = FALSE;   //When the Event is deleted Update the Insurance flag. 14AUG2019
                updObj.Insurance_End_Date__c = System.today();            
                ObjList.add(updObj);  
            }
            Policy__c policy = new Policy__c(Id=updObj.Policy__c);
            policy.Active__c = false;
            policy.End_Date__c = System.now(); 
            policy.Status__c = Constants.POL_STATUS_EXPIRED;  
            diffDays =  updObj.Policy__r.Start_date__c.date().daysBetween(policy.End_Date__c.Date());
            chargedDays = diffDays + 1;
            //LM-695--Rounded the policy charges--25SEPT2019
            policy.Policy_Charges__c = Utility.getRoundedPremium(updObj.Policy__r.Premium__c * chargedDays); 
            //Try catch Block to find the error. 06JUNE2019
            try{        
                update policy;            
            }catch(Exception cp){            
                Utility.sendException(cp, 'FAILED IN CLOSING POLICY FOR THIS POLICY :'+policy.Id);
            }                    
        }
        if(ObjList.size() > 0) {
            try {
                Update ObjList;
            } catch(Exception o) {
                Utility.sendException(o, 'ERROR in updating object when deleting event objects'); 
            }
        }        
    }    
    
    public static Premium__c getPremium(String catStr) {
        if(catStr.containsIgnoreCase('velo')) {
            catStr = 'Bike';   
        } else if(catStr.startsWith('e')) {
            catStr = 'E-Bike';
        } else if(catStr.equalsIgnoreCase('photo') || catStr.equalsIgnoreCase('foto')) {
            catStr = 'Camera incl. Accessories';
        }
        
        for(Premium__c category: Premium__c.getall().values()) {
            if(category.name.equalsIgnoreCase(catStr)) {
                return category;
            }
        }
        
        return (new Premium__c(name=catStr));
    }
    
    //evaluate and categories the products by DB_Brack__c, DB_Fotichaeschtli__c fields Category_2__c, Product_Type__c respectively
    public static String getCategory(String inpStr) {
        inpStr = inpStr.trim().replaceAll('\\s+', ' ');
        if(inpStr.startsWithIgnoreCase('foto') ||
           inpStr.equals('camera incl. accessories') || //this condition is added to get the upper case value for category sort--21AUG2019.
           inpStr.containsIgnoreCase('foto') || 
           inpStr.containsIgnoreCase('videografie')) {
               return 'Camera incl. Accessories';
           } else if(inpStr.startsWithIgnoreCase('IT') ||
                     inpStr.equals('laptop') ||     //21AUG2019 --For category sort order
                     inpStr.containsIgnoreCase('Hardware') || 
                     inpStr.startsWithIgnoreCase('PC') ||
                     inpStr.startsWithIgnoreCase('Computer') ||
                     inpStr.containsIgnoreCase('Notebook')) {
                         return 'Laptop';
                     } else if(inpStr.equalsIgnoreCase('Mobiltelefonie') ||
                               inpStr.equals('smartphone') ||   //21AUG2019 --For category sort order
                               inpStr.startsWithIgnoreCase('mobil') ||
                               inpStr.startsWithIgnoreCase('Telefonie')){
                                   return 'Smartphone';
                               } else if (inpStr.equals('drone') || 
                                          inpStr.equalsIgnoreCase('drohnes') ||
                                          inpStr.equalsIgnoreCase('RC & Modellbau')) { //LM-756--Added german name of drone--29OCT2019
                                              return 'Drone';
                                          } else if (inpStr.equalsIgnoreCase('travel insurance')) { // Reva - LM-1210  - Error handling while inserting new OKK obj for a customer
                                              return 'Travel Insurance';
                                          }
        return 'NA';
    }        
    
    //Rounding up the Premium value
    public static decimal getRoundedPremium(decimal inpVal) {
        // 22MAR19 - MR - Added for rouding up the correct 0.05
        if(inpVal > 0 && inpVal < 0.05) { return 0.05; }
        
        // LM399 - MR - 22APR19 - Rounding Off Issue Fix      
        inpVal = inpVal.divide(1, 2, System.RoundingMode.UP);
        
        decimal diffAmount = 0.00;
        decimal remMod = Decimal.valueOf(math.mod(integer.valueof(inpVal*100),5))/100.00;
        if (remMod > 0){
            diffAmount = 0.05 - remMod;
            diffAmount = diffAmount.setScale(2);
        }
        return (inpVal + diffAmount).setScale(2);
    }
    
    public static String errorHandler(Exception e) {
        return '{"status":"failure", ' + 
            '"message":"' + e.getMessage() + '", ' +
            '"type":"' + e.getTypeName() + '", ' +
            '"line":"' + e.getLineNumber() + '", ' +
            '"cause":"' + e.getCause() + '", ' +
            '"stack":"' + e.getStackTraceString() + '", ' +
            '"user":"' + UserInfo.getUserId()+ '"}';
    }
    
    public static String errorString(Exception e) {
        return e.getMessage() + ' \n ' +
            + e.getTypeName() + ' \n ' +
            + e.getLineNumber() + ' \n ' +
            + e.getCause() + ' \n ' +
            + e.getStackTraceString() + '"}';
    }
    
    // to calculate no of days in current dynamically
    public static integer numOfDays(date ipDate){
        if (ipDate == null){
            ipDate = system.today();
        }
        Integer m = ipDate.month();
        Integer y = ipDate.year();
        
        return Date.daysInMonth(y, m);
    }
    
    // 22APR19 - LM398 - MR - Added new Function for End Date Calculation
    public static integer numOfDaysForEndDate(date ipDate){
        return (numOfDays(ipDate) - 1);
    }
    
    //creating  records from DB_Brack__c in product2 object
    public static Product2 convertToProductDB1(DB_Brack__c db1, product2 product1) {
        product1.DB_Brack__c = DB1.Id;
        product1.EAN_Code__c =  DB1.EAN_Code__c;
        product1.Price__c = DB1.UVP__c;
        
        // MR - 27JAN19 - Commented for Migration
        // product1.Product_ID__c = DB1.Item_Number_2__c;
        product1.ProductCode = DB1.Item_Number_2__c;
        product1.Manufacturer_Number__c = DB1.Manufacturer_Number__c;
        product1.Manufacturer__c =  DB1.Manufacturer__c;
        
        //product1.Insured_Value__c = DB1.Price_incl_VAT__c;    // 03JUL19 Commented for avoiding Insured Value Update
        
        // MR - 31AUG19 - No need to update these 3 fields on Price Change / only on insert
        if(product1.id == null) {
            product1.Description  = DB1.Web_Text__c;  //LM-780 -- No need to update the description field.Updates only on insert --13NOV2019
            product1.Name =  DB1.Designation__c;
            product1.Searchable__c = true;
            product1.DisplayUrl = DB1.WWW_Link__c;    
        }
        
        if(DB1.Category_2__c != null && DB1.Category_2__c != ''){
            product1.Product_Type__c = getCategory(DB1.Category_2__c);
        }
        product1.Family = product1.Product_Type__c;
        return product1;
    }
    
    //creating  records from DB_Fotichaeschtli__c in product2 object
    public static Product2 convertToProductDB2(DB_Fotichaeschtli__c db2, product2 prod2) {
        prod2.DB_Fotichaeschtli__c = DB2.Id;
        prod2.EAN_Code__c =  DB2.EAN__c;
        prod2.Price__c = DB2.Price__c;
        // MR - 27JAN19 - Commented for Migration
        // prod2.Product_ID__c = DB2.ID__c;
        prod2.ProductCode  = DB2.ID__c;
        
        prod2.Manufacturer_Number__c = DB2.Manufacturer_Parts_Number__c;
        prod2.Manufacturer__c =  DB2.Brand__c;        
        if (DB2.Product_Type__c != '' || DB2.Product_Type__c != null) {
            prod2.Product_Type__c= 'Camera incl. Accessories';
        }
        prod2.Family  = prod2.Product_Type__c;
        
        // MR - 31AUG19 - No need to update these 3 fields on Price Change or only on insert
        if(prod2.id == null) {
            prod2.Description  = DB2.Description__c; //LM-780 -- No need to update the description field.Updates only on insert --13NOV2019
            prod2.Name =  DB2.Title__c;
            prod2.DisplayUrl = DB2.Image_URL__c;
            prod2.Searchable__c = true;
        }
        
        return prod2;
    }
    
    
    // Function to set the password, token and register flag in the Contact
    @future
    public static void registerContact(String userId, String password) {
        //To generate random numbers
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';  
        String randStr = '';
        
        while (randStr.length() < 32) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        
        // Get the ContactId from the UserId
        User newUser = [Select Id, FirstName, ContactId, Others__c,Recommendation__c, Email, Register_confirm_URL__c  from User where id = :userId];
        
        if(newUser.ContactId != null) {
            
            try {
                // Update Contact with the Password, Token__c
                Contact newContact = new Contact(Id=newUser.ContactId);  
                newContact.password__c = password;
                newContact.Token__c = randStr;
                newContact.Token_created__c = String.valueOf(System.now());
                newContact.Others__c = newUser.Others__c;
                newContact.Recommendation__c  = newUser.Recommendation__c;
                
                // Send the Email Verification Email
                newContact.SignUp__c = True;
                update newContact;
            } catch (Exception ncException) {
                // Email the Admin about missing Contact
                sendException(ncException, 'ONBOARDING CONTACT NOT UPDATED');
            }
            //Send verification email to the Customer From Process Builder to apex -- 23AUG2019.
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
            //EmailTemplate template = [Select id from EmailTemplate where name=:'Register-confirm' LIMIT 1];   
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { newUser.Email });
            // mail.setTemplateId(template.Id);
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
            mail.setSubject('Bitte bestätige dein LINGS Profil');
            String messageBody = '<html><body>Salut '+newUser.FirstName+'<br><br>Nur noch ein Klick, und du gehörst zur LINGS-Familie: '+
                '<a href = "'+System.label.Lings_Community_link+'/de/auth/registrieren/'+randStr+'">hier</a> bestätigst du dein Benutzerkonto.'+
                '<br><br>Wir freuen uns auf dich,<br>Larissa von LINGS<br><br>'+
                '<img src = "https://lingscrm--c.documentforce.com/sfc/dist/version/download/?oid=00D1t000000vePK&ids=0681t00000FxK8PAAV&d=%2Fa%2F1t000000Xs7W%2FyJM.KOzqm..v3KBCb.p3zCHikmDzqcax_2MlAEoJYFs&operationContext=DELIVERY&viewId=05H1t000001G3iMEAS&dpt="/><br/>'+
                '<br>'+'Generali Versicherungen'+'<br>'+'Soodmattenstrasse 4'+'<br>'+'8134 Adliswil'+'<br>'+'http://www.lings.ch<br>'+
                '<br>'+'LINGS ist eine Marke der Generali Allgemeine Versicherungen AG, Avenue Perdtemps 23, 1260 Nyon'+'<br></body> </html>';
            mail.setHtmlBody(messageBody);            
            //mail.setTargetObjectId(newUser.ContactId);
            mail.setSaveAsActivity(true);
            
            mailList.add(mail);   
            try{
                Messaging.sendEmail(mailList);     
            }catch(Exception cl){
                utility.sendException(cl, 'EMAIL VERIFICATION MAIL FAILED TO SEND '+newUser.ContactId);
            }        
        } else {
            // Email the Admin about missing Contact
        }
    }
    
    // Function to store the safer pay token
    public static void setSPToken(String contactId, String token) {
        try {
            // Update Contact with the Token__c
            Contact newContact = new Contact(Id=contactId);  
            newContact.Token__c = token;
            newContact.Token_created__c = String.valueOf(System.now());
            
            update newContact;
        } catch (Exception ncException) {
            // Email the Admin about missing Contact
            sendException(ncException, 'Setting SaferPay Token [' + token + '] in ' + contactId);
        }
    }
    
    // Function to store the safer pay alias
    @future
    public static void setSPAlias(String inpResp) { //String contactId, String alias) {
        UIPaymentWrapper.SPAliasResponse apiResp = (UIPaymentWrapper.SPAliasResponse) JSON.deserialize(inpResp, UIPaymentWrapper.SPAliasResponse.class);            
        
        Contact newContact = new Contact(Id=apiResp.responseHeader.RequestId); 
        try {
            // Update Contact with the Token__c
            newContact.Payment_Alias_Id__c = apiResp.alias.Id;
            newContact.Payment_Info__c = JSON.serialize(apiResp.paymentMeans);
            newContact.Token__c = '';
        } catch (Exception ncException) {
            // Email the Admin about missing Contact
            newContact.Payment_Info__c = apiResp.ErrorMessage;
            if(!Test.isRunningTest()){
                sendException(ncException, 'Setting SaferPay Alias [' + apiResp.alias.Id + '] in ' + apiResp.responseHeader.RequestId);
            }
        }
        
        update newContact;
    }
    
    // Function to store the safer pay alias
    //@future
    public static void setSPTxnAlias(String inpResp, String message) { //String contactId, String alias) {
        UIPaymentWrapper.SPTxnPaymentResponse apiResp = (UIPaymentWrapper.SPTxnPaymentResponse) JSON.deserialize(inpResp, UIPaymentWrapper.SPTxnPaymentResponse.class);                    
        Contact newContact = new Contact(Id=apiResp.responseHeader.RequestId); 
        
        try {
            //Card details will be Updated Only when the response message is Success --28AUG2019.
            if(message.equalsIgnoreCase('Txn Completed')){             
                // Update Contact with the Token__c
                newContact.Payment_Alias_Id__c = apiResp.registrationResult.alias.Id;
                newContact.Payment_Info__c = JSON.serialize(apiResp.paymentMeans);
                newContact.Payment_Response__c = JSON.serialize(apiResp);                   
                newContact.Token__c = '';                
            }          
            newContact.Card_error__c = message +'--'+System.now(); 
        } catch (Exception ncException) {
            newContact.Payment_Response__c = JSON.serialize(apiResp);
            if(!Test.isRunningTest()){
                sendException(ncException, 'Setting SaferPay Alias [' + apiResp.responseHeader.RequestId + '].');
            }
        }
        
        update newContact;
    }
    
    // Function to handle the SaferPay Payment Response
    public static void setSPPayment(String inpPaymentResp) {
        System.debug('Payment Response from SaferPay - ' + inpPaymentResp);
        UIPaymentWrapper.SPPaymentResponse apiResp = (UIPaymentWrapper.SPPaymentResponse) JSON.deserialize(inpPaymentResp, UIPaymentWrapper.SPPaymentResponse.class);            
        
        try {
            // Create New Payment Record
            Payment__c paymentObj = new Payment__c();
            paymentObj.Date__c = System.today();
            paymentObj.Paid__c = Decimal.valueOf(apiResp.txn.amount.value);
            paymentObj.API_Response__c = inpPaymentResp;
            insert paymentObj;                
            
            // Read & Update the Invoice
            Invoice__c invObj = new Invoice__c(Id=apiResp.responseHeader.requestId);
            invObj.Status__c = 'Success';
            // Create Payment Field
            update invObj;
            
            // Update the Contact
        } catch (Exception npException) {
            // Email the Admin about missing Contact
            sendException(npException, 'Setting SaferPay Payment for ' + apiResp.responseHeader.RequestId);
        }
    }
    
    // Generate Token for Email Verification and Forgot Password
    public static String generateToken(Integer size) {
        String token = '';
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuz';
        
        Integer idx = 0;
        for(Integer i=0; i<size; i++) {
            idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            token += chars.substring(idx, idx+1);
        }
        
        return token;
    }   
    
    // Process the Payment with SaferPay
    public static String processPayment(String invId, Decimal txnAmount, String invName, String aliasId) {
        Contact con = [Select Id, Email, Payment_Alias_ID__c From Contact Where Contact.Payment_Alias_ID__c = :aliasId];
        String receiverAddr = con.Email;
        Id contactId = con.Id;
        
        // Form the Request for Payment Transaction
        String reqBody = '{"RequestHeader": { "SpecVersion": "' + System.Label.SaferPayVersion + '", "CustomerId": "' + 
            Constants.SP_CUSTOMER_ID + '", "RequestId": "' + 
            invId + '", "RetryIndicator": 0 }, "TerminalId": "' + 
            Constants.SP_TERMINAL_ID + '", "Payment": { "Amount": {"Value": "' + 
            Integer.valueOf(txnAmount*100) + '", "CurrencyCode": "CHF"}, "Description": "' + 
            invName + '", "PayerNote": "' + System.Label.SaferPayNote + '" }, "PaymentMeans": { "Alias": { "Id": "' + 
            aliasId + '"} } }';
        
        HttpRequest spReq = new HttpRequest();
        spReq.setBody(reqBody);
        // 23APR19 - MR - Riyaz - Removal of URL HardCoding
        //spReq.setEndpoint('System.Label.SaferPay/api/Payment/v1/Transaction/AuthorizeDirect');
        //spReq.setHeader('Authorization', 'Basic QVBJXzI0NTkxMl8wNjM4MTc3MjpMaW5nc19EZW1vX1Bhc3N3b3JkXzE5');
        spReq.setEndpoint(Constants.SP_TXN_EP);
        spReq.setMethod('POST');
        spReq.setHeader('Authorization', Constants.SP_BASIC_AUTH);
        spReq.setHeader('Content-Type', 'application/json');
        
        //Payment Failed
        EmailTemplate template = [Select id from EmailTemplate where name=:'Payment Failure' limit 1];
        Messaging.SingleEmailMessage paymentEmail = new Messaging.SingleEmailMessage();
        paymentEmail.setUseSignature(false);
        paymentEmail.setToAddresses(new String[] { receiverAddr });
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        paymentEmail.setOrgWideEmailAddressId(owea.get(0).Id);                 
        paymentEmail.setTargetObjectId(contactId);
        String[] sendingToBccAdd = new String[]{'support@lings.ch'};
            paymentEmail.setBccAddresses(sendingToBccAdd);
        paymentEmail.setTemplateId(template.id);
        paymentEmail.setSaveAsActivity(true);       
        
        if(!Test.isRunningTest()){
            HttpResponse resp = new Http().send(spReq);
            if(resp.getStatusCode() != 200) {         
                // Send the email 12June2019 --Hiding payment failure as requested by henrik
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { paymentEmail });            
            }
            
            return resp.getBody();
        }
        
        return '';
    }
    
    // MR - 06JUN19 - Added function to return the Month in native language
    public static String getMonthName(Integer monthNumber) {
        switch on monthNumber {
            when 1 { return 'Januar'; }
            when 2 { return 'Februar'; }
            when 3 { return 'März'; }
            when 4 { return 'April'; }
            when 5 { return 'Mai'; }
            when 6 { return 'Juni'; }
            when 7 { return 'Juli'; }
            when 8 { return 'August'; }
            when 9 { return 'September'; }
            when 10 { return 'Oktober'; }
            when 11 { return 'November'; }
            when 12 { return 'Dezember'; }
        }
        
        return '';
    }
    
    // Variable to trace the execution flow
    public static List<String> traceStack = new List<String>();
    
    // emails to get the exception as stored in the custom label - ErrorEmails
    public static void sendInfo(String req, String info, String rec) {
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //LM-688--Added OrgWideAddress --25SEPT2019
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        for(String eAddr: System.Label.ErrorEmails.split(',')) {
            sendTo.add(eAddr);  
        }
        
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        //mail.setSenderDisplayName('Lings Support Admin');
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject('Lings.ch Information on ' + req);
        String body = 'Kindly note the below info generated from CRM System.';
        body += '<br/>Information: ' + info;
        body += '<br/>Record Details: ' + rec;
        body += '<br/><br/>Error Handler Stack info ' + traceStack;
        mail.setHtmlBody(body);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        // if(Test.isRunningTest()) { return; }
        try{
            if(!Test.isRunningTest()){
                Messaging.sendEmail(mails);   
            }
        }catch(Exception err){}
        
    }
    
    //LM-1172--Exception mails will be send with the Support@lings.ch mail --27FEB2020
    public static void sendError(String req, String err, String rec, String actionNeeded) {
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //LM-688--Added OrgWideAddress --25SEPT2019
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        for(String eAddr: System.Label.ErrorEmails2.split(',')) {
            sendTo.add(eAddr);  
        }
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        //mail.setSenderDisplayName('Lings Support Admin');
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject('Lings.ch Error on ' + req);
        String body = 'Error details are mentioned below';
        body += '<br/>' + err + '.';
        body += '<br/>Record Details : ' + rec;
        body += '<br/> Kindly do the ' + actionNeeded;
        
        body += '<br/><br/>Error Handler Stack info ' + traceStack;
        mail.setHtmlBody(body);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        //if(Test.isRunningTest()) { return; }
        try{
            if(!Test.isRunningTest()){
                Messaging.sendEmail(mails); 
            }
        }catch(Exception err1){}
        
    }
    
    public static void sendException(Exception e, String req) {
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        //LM-688--Added OrgWideAddress --25SEPT2019
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        for(String eAddr: System.Label.ErrorEmails.split(',')) {
            sendTo.add(eAddr);  
        }
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        //mail.setSenderDisplayName('Lings Support Admin');
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject('Lings.ch Exception on ' + req);
        String body = 'Exception details are mentioned below';
        body += '<br/> Error occured while processing - ' + e.getMessage() + '.';
        body += '<br/> Exception Type: ' + e.getTypeName();
        body += '<br/> Line Number: ' + e.getLineNumber();
        body += '<br/> Exception Stack Trace: ' + e.getStackTraceString();
        body += '<br/><br/>Error Handler Stack info ';
        for(String trace: traceStack) {
            body += '<br/>' + trace;
        }
        mail.setHtmlBody(body);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        //if(Test.isRunningTest()) { return; }
        try{
            if(!Test.isRunningTest()){
                Messaging.sendEmail(mails);
            }
        }catch(Exception err2){}
        
    }
    
    //LM-1172--Exception mails will be send with the Support@lings.ch mail --27FEB2020
    public static void sendException2(Exception e, String req) {
        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        //LM-688--Added OrgWideAddress --25SEPT2019
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        for(String eAddr: System.Label.ErrorEmails2.split(',')) {
            sendTo.add(eAddr);  
        }
        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        //mail.setSenderDisplayName('Lings Support Admin');
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject('Lings.ch Exception on ' + req);
        String body = 'Exception details are mentioned below';
        body += '<br/> Error occured while processing - ' + e.getMessage() + '.';
        body += '<br/> Exception Type: ' + e.getTypeName();
        body += '<br/> Line Number: ' + e.getLineNumber();
        body += '<br/> Exception Stack Trace: ' + e.getStackTraceString();
        body += '<br/><br/>Error Handler Stack info ';
        for(String trace: traceStack) {
            body += '<br/>' + trace;
        }
        mail.setHtmlBody(body);
        
        // Step 5. Add your email to the master list
        mails.add(mail);
        
        // Step 6: Send all emails in the master list
        //if(Test.isRunningTest()) { return; }
        try{
            if(!Test.isRunningTest()){
                Messaging.sendEmail(mails);
            }
        }catch(Exception err2){}
        
    }
}