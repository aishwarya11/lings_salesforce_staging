Global class WelcomeEmailScheduler implements Schedulable {
	//To send the Welcome email
    global void execute(SchedulableContext sc) {
        BatchWelcomeEmail Confirm = new BatchWelcomeEmail();
        database.executeBatch(Confirm, 5);
    }
}