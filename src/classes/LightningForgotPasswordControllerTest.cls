@IsTest(SeeAllData = true)
public with sharing class LightningForgotPasswordControllerTest {

 /* Verifies that ForgotPasswordController handles invalid usernames appropriately */
 @IsTest
 static void testLightningForgotPasswordControllerInvalidUserName() {
  System.assertEquals(LightningForgotPasswordController.forgotPassword('fakeUser', 'http://a.com'), 'Bitte fehlende Angaben ausfüllen.');
  System.assertEquals(LightningForgotPasswordController.forgotPassword(null, 'http://a.com'), 'Bitte fehlende Angaben ausfüllen.');
  System.assertEquals(LightningForgotPasswordController.forgotPassword('a', '/home/home.jsp'), 'Bitte fehlende Angaben ausfüllen.');
 }
 @IsTest
 static void setExperienceId() {
     Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
     User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
     EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
     LocaleSidKey='en_US',ProfileId = p.Id,
     TimeZoneSidKey='America/Los_Angeles', 
     UserName='dbds@mcode.llp',CommunityNickname='Lings9865465');
        insert u;
      LightningForgotPasswordController.setExperienceId(u.id);
  //System.assertEquals('passing null value', LightningForgotPasswordController.setExperienceId());
 }
 /* Verifies that null checkEmailRef url throws proper exception. */
 @IsTest
 static void testLightningForgotPasswordControllerWithNullCheckEmailRef() {
  System.assertEquals(LightningForgotPasswordController.forgotPassword('a', 'a123.com'), 'Bitte fehlende Angaben ausfüllen.');
  //System.assertEquals(LightningForgotPasswordController.forgotPassword('a@salesforce.com', 'a123.com'), 'Bitte fehlende Angaben ausfüllen.');
 }

 /* Verifies that LightningForgotPasswordController object is instantiated correctly. */
 @IsTest
 static void LightningForgotPasswordControllerInstantiation() {
  LightningForgotPasswordController controller = new LightningForgotPasswordController();
  System.assertNotEquals(controller, null);
 }
}