public class ContentController {
    public blob file { get; set; }
    
    public ContentController(){}
    
// Code we will invoke on page load.   
    public PageReference upload() {
        
        //Intialize and define values for ContentVersion Object
        ContentVersion v = new ContentVersion();
        v.versionData = file;
        v.title = 'testing upload';
        v.pathOnClient ='/somepath.txt';
        insert v;
        //returns to the ContentController vfpage
        return new PageReference('/' + v.id);
    }
}