//send  a remainder regarding the Coupon expiration to the User on the Coupon End Date in Email format
// Batch to run in the Afternoon/Evening
global class BatchCouponExpire implements Database.Batchable<Voucher_Contact__c>, Database.Stateful {
        public Integer failed_mail_count = 0;
    	public Integer invalidEmailcount = 0;
        public Integer totalEmailsProcessed = 0;
        public Integer errorInEmailTemplateCount = 0;
        public List<Voucher_Contact__C> successVCUpdateList = new List<Voucher_Contact__C>();
        public List<Voucher_Contact__C> failedVCUpdateList = new List<Voucher_Contact__C>();
        public Integer mailListWithETSize = 0;
    //Start Batchable
    global Iterable<Voucher_Contact__c> start(Database.BatchableContext BC){
        List<Voucher_Contact__c> query =[SELECT Id, Name, End_date__c,Balance__c, 
                                         Contact__c, Contact__r.Payment_Alias_ID__c, Contact__r.Email,
                                         Contact__r.Voucher_In_Use__c
                                         FROM Voucher_Contact__c 
                                         WHERE End_date__c = TOMORROW OR End_date__c = TODAY];
        return query;
    }
    
    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Voucher_Contact__c> scope) {
        System.debug('EXECUTE');
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Voucher_Contact__C> vcList = new List<Voucher_Contact__C>();
        
        for(Voucher_Contact__c vc : scope) {
            Id ContactId = vc.contact__c;
            
            if(vc.End_date__c == System.today() + 1) {
                 
                //Send Email
                EmailTemplate eTemplate = null;
                if(vc.Contact__r.Payment_Alias_Id__c <> null) {
                    // Contact has added CC
                    eTemplate = [Select id from EmailTemplate where name='Coupon-expires-soon-has-cc' limit 1][0];
                } else {
                    // Contact yet to add CC
                    eTemplate = [Select id from EmailTemplate where name='Coupon-expires-soon' limit 1][0];
                }
                
                if(eTemplate <> null) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    totalEmailsProcessed++;
                    mail.setUseSignature(false);
                    mail.setToAddresses(new String[] { vc.Contact__r.Email });
                    mail.setTemplateId(eTemplate.Id);
                    OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);                     
                    mail.setTargetObjectId(contactId);
                    mail.setSaveAsActivity(true);
                    mailList.add(mail);
                } else {
                    errorInEmailTemplateCount++;
                    // Send email to Admin   
                    Utility.sendError('BatchCouponExpire', 'Missing Email Template', ContactId, 'Ensure the Templates are available');
                }
            }
            if(vc.End_Date__c == System.today()){
                //Clear the voucher from the contact when it expired
                 vc.Contact__r.Voucher_In_Use__c = NULL;
                vcList.add(vc);  
                System.debug('Voucher Value'+vc.Contact__r.Voucher_In_Use__c);
            }
            //Updating Voucher_Contact__c List       
        if(vcList.size() > 0){
            List<Database.SaveResult> srUpdate = Database.update(vcList);
            Integer vcIndex = 0;  
            for(Database.SaveResult sr: srUpdate) {
                Voucher_Contact__c vcon = vcList[vcIndex];
                if(sr.isSuccess()) {
                    successVCUpdateList.add(vcon);  
                } else {
                    failedVCUpdateList.add(vcon);
                }

                vcIndex++;
            }
        }
     }
        try{
            try{
                mailListWithETSize = mailList.Size();
             // Send the emails
            Messaging.sendEmail(mailList);  
            }catch(Exception bounceadd){invalidEmailcount++;}
        }catch(Exception ce){
            failed_mail_count++;
            // Send exception email to Admin
           Utility.sendException(ce, 'COUPON EXPIRES EMAIL NOT SEND');
        }
    }
     // Finish Batchable
    global void finish(Database.BatchableContext BC){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String>{'riyaz.mohamed@outlook.com'});
        mail.setSubject('BatchCouponExpire ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id); 
        String body = 'BatchCouponExpire Status on Sending email to the user and Updating VoucherContact <br/>';
        
        // Processing Email list 
        body += '<br/> ' + totalEmailsProcessed + ' number of Email processing were completed.'; 
        body += '<br/> ' + mailListWithETSize + ' number of Email processing along with valid EmailTemplate were completed.';
        if(failed_mail_count > 0){
            body += '<br/> Failed Email processing for the Emails with valid EmailTemplate count = '+ failed_mail_count + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing for the Emails with valid EmailTemplate were happened.';
        }
        if(invalidEmailcount > 0){
              body += '<br/> Failed Email processing for the Emails with invalid Email Address count = '+ invalidEmailcount + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing for the Emails with invalid Email Address were happened.';
        }
        if(errorInEmailTemplateCount > 0){
            body += '<br/> Failed Email processing for the Email with Invalid EmailTemplate count = '+ errorInEmailTemplateCount + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing for the Email with Invalid EmailTemplate were happened.';
        }
        
        
        // New Policy Creation List   
        body += '<br/> ' + (successVCUpdateList.size() + failedVCUpdateList.size()) + ' number of VoucherContact records update processing were completed.'; 
        if(successVCUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success VoucherContact updation details <br/>';
            
            for(Voucher_Contact__c successList: successVCUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No VoucherContact updation happened.';
        }         
        if(failedVCUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed VoucherContact updation details <br/>';
            
            for(Voucher_Contact__c failedList: failedVCUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed VoucherContact updation happened.';
        }

        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);
		//LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest() && (failedVCUpdateList.size() > 0 || errorInEmailTemplateCount > 0 || invalidEmailcount > 0 || failed_mail_count > 0)){
            Messaging.sendEmail(mails);

            System.debug('BatchCouponExpire FINISH');
        }

    }
}