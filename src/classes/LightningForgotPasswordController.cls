global class LightningForgotPasswordController {
	public static String userNameSuffix = System.Label.UserNameSuffix;
    public LightningForgotPasswordController() {}

    @AuraEnabled
    public static String forgotPassword(String username, String checkEmailUrl) {
        try {
            String mail = username;
            //Added the Username suffix by default while logging in --20OCT2019.
            username = username + userNameSuffix;                                                
            //Site.forgotPassword(username);
			UIHomeLogin.resetPassword('de', mail); //LM-783-- This will generate the token and do an normal validation --14NOV2019                      
            //String shref = ApexPages.currentPage().getURL();
            ApexPages.PageReference checkEmailRef = new PageReference(checkEmailUrl);                          
            if(!Site.isValidUsername(mail)) {
                return 'Bitte fehlende Angaben ausfüllen.';
            }
            aura.redirect(checkEmailRef);
            return null;
        }
        catch (Exception ex) {
            utility.sendException(ex, 'Error in Mobile forgot password page '+ex.getStackTraceString());
            return 'Bitte fehlende Angaben ausfüllen.';
        }
    }

    @AuraEnabled
    global static String setExperienceId(String expId) {    
        // Return null if there is no error, else it will return the error message 
        try {
            String shref = ApexPages.currentPage().getURL();
            if (expId != null) {
                Site.setExperienceId(expId);               
            }
            return null; 
        } catch (Exception ex) {
            return ex.getMessage();            
        }        
    } 
}