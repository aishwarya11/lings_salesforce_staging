global class WarrantyRemainderScheduler implements Schedulable{     
    //Method to notify the user about the warranty run out.
    global void execute(SchedulableContext sc) {
        // Instantiate PolicyExpires with parameters
        BatchWarrantyRemainder batch = new BatchWarrantyRemainder();
        database.executeBatch(batch, 5);
    } 
}