public class OKKController {
    
    public List<object__c> lingsObjectList { get; set; }
    public List<Event_Object__c> eveObjList { get; set; }
    public List<Event__c> eveList { get; set; }
    @testvisible public Id lingsObjectId = null;
    public List<String> ins {get; set; }
    public String date1 { get; set; }
    public String conFirstName { get; set; }
    public String conLastName { get; set; }
    public String conStreet { get; set; }
    public String conPostalCode { get; set; }
    public String conHouseNumber { get; set; }
    public String conCity { get; set; }
    public String insuranceStartDate { get; set; }   
    public String insuranceEndDate { get; set; }   
    public String numberOfDaysInsured { get; set; }
    public String numberOfDaysInsuredInEN { get; set; }
    public String totalPremium { get; set; }
    public String creditCardType { get; set; }
    public String nameDetails { get; set; }
    public String todayDateValue {get; set; }
    public String todayDateValueInEN {get; set;}
    
    
    
    public OKKController(ApexPages.StandardController controller){
        try{ 
            
            
            integer monthValue = System.today().month();
            integer dayValue = System.today().Day();
            integer yearValue = System.today().year();
            String monthText = Utility.getMonthName(monthValue);
            String monthTextInEN = System.now().format('MMMMM');
            String dob = '';
            
			todayDateValue = dayValue+'. '+monthText+' '+yearValue;  
            todayDateValueInEN = dayValue+'. '+monthTextInEN+' '+yearValue;  
            
            lingsObjectId = controller.getId();
            //lingsObjectId = 'a080E000003rFc1QAE';
            
            lingsObjectList =[select id,OKKTotalPremium__c,OKKEventEndDate__c,OKKEventStartDate__c,
                              DisplayURL__c,Customer__r.name,Customer__r.firstname,Customer__r.lastname,customer__r.Birthdate,
                              customer__r.Street__c,customer__r.House_Number__c,customer__r.Post_Code__c,
                              customer__r.Payment_Info__c,customer__r.Place__c,Insurance_Start_Date__c,Insurance_End_Date__c
                              from object__c where Id =:lingsObjectId ]; 
            
            eveObjList = [select id,name,Object__c,Event__c from Event_Object__c where Object__c=:lingsObjectId];
            eveList = [select id,name,start_date__c,end_date__c,createddate from Event__c where id=:eveObjList[0].Event__c];
            object__c contact = lingsObjectList[0];                
            Date d = System.today();
            System.debug('Event StartDate = '+d);
            //Reva -- To validate whether Birthdate is null or not to avoid null error
            if(String.valueOf(contact.customer__r.Birthdate) != null && String.valueOf(contact.customer__r.Birthdate) != '' ){             
                d = contact.customer__r.Birthdate;
                System.debug('Event StartDate = '+d);
                dob = DateTime.newInstance(d.year(),d.month(),d.day()).format('dd.MM.YYYY');
                date1 = dob;
            }
            conFirstName = contact.customer__r.firstname;
            conLastName = contact.customer__r.lastname;
            conStreet = contact.customer__r.Street__c;
            conPostalCode = contact.customer__r.Post_Code__c;
            conHouseNumber = contact.Customer__r.House_Number__c;
            conCity = contact.Customer__r.Place__c;
            
            string insurancestart = string.valueof(contact.Insurance_Start_Date__c);
            //Reva - To also correct the format of InsuranceStartDate field when it is not null
            if(insurancestart  != '' && insurancestart != null){
                Date temp = contact.Insurance_Start_Date__c;
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
                insurancestart = string.valueof(dt);
            }
            else if(insurancestart == null || insurancestart == ''){
                Date temp = eveList[0].start_date__c;
                System.debug('Event StartDate = '+d);
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
            	insurancestart = string.valueof(dt);
            }
            insuranceStartDate = insurancestart;
            
            string insuranceend = string.valueof(contact.Insurance_End_Date__c);
             //Reva - To also correct the format of InsuranceEndDate field when it is not null
            if(insuranceend  != '' && insuranceend != null){
                Date temp = contact.Insurance_End_Date__c;
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
                insuranceend = string.valueof(dt);
            }
            else if(insuranceend == null || insuranceend == ''){
                Date temp = eveList[0].end_date__c;
                System.debug('Event StartDate = '+d);
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
            	insuranceend = string.valueof(dt);
            }
            insuranceEndDate = insuranceend; 
            
            Integer noOfDays = eveList[0].start_date__c.daysBetween(eveList[0].end_date__c)+1;
            if(noOfDays > 1){
            	numberOfDaysInsured = '('+noOfDays+' Tage)';
                numberOfDaysInsuredInEN = '('+noOfDays+' Days)';
            }else if(noOfDays == 1){
                numberOfDaysInsured = '('+noOfDays+' Tag)';
                numberOfDaysInsuredInEN = '('+noOfDays+' Day)';
            }
            totalPremium = 'CHF '+contact.OKKTotalPremium__c.setScale(2);
            String ccTempString = contact.Customer__r.Payment_Info__c.substringAfter('"brand":{"PaymentMethod":"').substringBefore('","Name":"');
            creditCardType = ccTempString;
            //nameDetails = contact.Customer__r.name + ', ' + contact.Customer__r.firstname + ', ' + dob;
            nameDetails = contact.Customer__r.lastname + ' ' + contact.Customer__r.firstname + ' ' + dob;
        }catch(Exception error){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {'revathy@rangerinc.cloud'});
            mail.setSubject('OKKPdf Error'); 
            String body ='Cause=>'+ error.getCause() +', ' +'getMessage=>'+ error.getMessage()+', '+'getStackTraceString=>'+error.getStackTraceString()+', '+'getTypeName=>'+error.getTypeName();     
            mail.setHtmlBody(body);  
            mails.add(mail);
            Messaging.sendEmail(mails);
            
        }
    }
    
}