//removes policy for an object on its End date.
// Late Night job to remove the policy
Global class BatchPolicyExpires implements Database.Batchable<Object__c>, Database.Stateful {
    public Set<Id> objIds = new Set<Id>();
    public Set<Id> polIds = new Set<Id>();
    public Integer totalCount = 0;
    public Integer failedCount = 0;
    public Integer successCount = 0;    
    
    //Start batchable
    global Iterable<Object__c> start(Database.BatchableContext BC){
        System.debug('BatchPolicyExpires START');
        
        // Get all the Object Policies that are expiring today
        // MR - 24MAY19 - LM-458 - Updated query
        List<Object__c> query = [SELECT id, name, Policy__c, Policy__r.End_date__c, Policy__r.Active__c, Is_Insured__c,
                                 Customer__c, Insurance_End_Date__c, Flatrate_Renewal_Flag__c, Insured_value__c, 
                                 Premium_OnDemand__c, Premium_flatrate__c, Flatrate_savings__c,Product__c, 
                                 Product__r.Insured_value__c, Product__r.Ondemand_premium__c, Product__r.flatrate_premium__c,
                                 Product__r.flatrate_savings__c, Insurance_Start_Date__c, Customer__r.name, Policy__r.Premium__c,
                                 Policy__r.Status__c, Policy__r.Invoice_Start_date__c, Policy__r.Policy_Charges__c
                                 FROM Object__c 
                                 WHERE Policy__c != NULL AND Flatrate_Renewal_Flag__c = FALSE AND 
                                 (Insurance_End_Date__c <= TODAY OR Policy__r.End_date__c <= TODAY)];
        return query;
    }
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Object__c> scope) {
        System.debug('BatchPolicyExpires EXECUTE');
        
        List<Object__c> updatedObjectList = new List<Object__c>();
        List<Policy__c> expiredPolicyList = new List<Policy__c>();
        
        Integer chargedDays = 0;
        Integer diffDays = 0;
        
        for(Object__c obj : scope) {
            // Update Policy end date if its null
            if(obj.Policy__r.End_date__c == null || obj.Policy__r.Active__c || obj.Policy__r.Status__c == 'Insured') { //01AUG2019 --To update the Status field for the Flatrate.
                Policy__c updPolicy = new Policy__c(id=Obj.Policy__c);
                //24MAR19 - MR - Changed to actual end data instead of today
                updPolicy.end_date__c = Datetime.newInstance(obj.Insurance_End_Date__c, Utility.policyEndTime);
                //Added Policy Charges on 26SEPT2019
                diffDays =  obj.Policy__r.Invoice_Start_Date__c.daysBetween(updPolicy.end_date__c.date());	
                chargedDays = diffDays + 1;                  
                updPolicy.Policy_Charges__c = Utility.getRoundedPremium(obj.Policy__r.Premium__c * chargedDays);                 
                updPolicy.Active__c = false;
                updPolicy.Status__c = Constants.POL_STATUS_EXPIRED;
                expiredPolicyList.add(updPolicy);
                polIds.add(updPolicy.Id);
            }
            
            //When policy Expires update the object
            obj.Policy__c = NULL;  
            obj.Coverage_Type__c = 'NA';
            obj.Insurance_Start_Date__c = NULL;
            obj.Insurance_End_Date__c = NULL;
            obj.Is_Insured__c = FALSE;	//LM-633--Uncheck the Is Insured Flag -- 29AUG2019.
            //Reva - To avoid the OKK or RVG object rendered in the MyObject section when its corresponding Event get ends
            if(obj.Product__c == System.Label.OKKProductId){
            	obj.IsOKKEventEnds__c = TRUE;
            }
            //07-APR-2020 - Reva - LM-1329 Issue Update
            if(obj.Product__c == System.Label.RVGProductId){
            	obj.IsRVGEventEnds__c = TRUE;
            }
            //Updating the Insured value for the Flatrate reactivation is not covered yet            
            updatedObjectList.add(obj);
            objIds.add(obj.Id);
            totalCount++;
        }
        
        try {
            if(expiredPolicyList.size() > 0) {
                update expiredPolicyList;
                successCount++;
            }
        } catch(Exception pe) {
            failedCount++;
            // Send exception email to Admin
            Utility.sendException(pe, 'ERROR IN UPDATING POLICIES IN POLICY EXPIRES BATCH');  
        }
        
        try {           
            if(updatedObjectList.size() > 0) {
                update updatedObjectList;    
                successCount++;
            }            
        } catch(Exception up) {
            failedCount++;
            // Send exception email to Admin
            Utility.sendException(up, 'ERROR IN UPDATING OBJECTS IN POLICY EXPIRES BATCH');  
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC){
        System.debug('BatchPolicyExpires FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 22MAY19 - Code Optimization
        mail.setSubject('BatchPolicyExpires Status' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         
        
        String body = 'BatchPolicyExpires Status on Updating Policies and Objects <br/>';
        
        //Inserting, updating policies
        if(objIds.size() > 0) {
            body += '<br/> ' + objIds.size() + ' number of records processing were completed.'; 
            body += '<br/> Refer the below list of Success Object details <br/>';
            
            body += '<br/> ' + JSON.serialize(objIds);
        } else {
            body += '<br/> No Objects processing happened.';
        }
        
        if(polIds.size() > 0) { 
            body += '<br/> ' + polIds.size() + ' number of records processing were completed.'; 
            body += '<br/> Refer the below list of Success Policy details <br/>';
            
            body += '<br/> ' + JSON.serialize(polIds);
        } else {
            body += '<br/> No Policy processing happened.';
        }
        
        if(successCount > 0){
            body += '<br/> ' + successCount + ' records are completed successfully.'; 
        }
        
        if(failedCount > 0){
            body += '<br/> ' + failedCount + ' records are not completed successfully.'; 
        }        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);               
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedCount > 0) {
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchPolicyExpires';
        ab.Track_message__c = body;
        database.insertImmediate(ab);         
        }
    }
}