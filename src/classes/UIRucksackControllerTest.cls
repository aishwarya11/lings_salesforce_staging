@isTest
public class UIRucksackControllerTest {
    public static String CHF_STRING = 'CHF';
    
    public class SackWithObject {
        public String uuid;
        public String name = 'Test Bag';
        public String state = 'Insured';           
        public boolean toggle = TRUE;          
        public boolean hasClaim = FALSE ;        
        public boolean customerOn = TRUE;      
        public integer objectsCount = 2;
        public decimal premiumTotal = 2.56;
        public List<LingsObject> insuredProducts;
    }
    
    public with sharing class LingsObject{

        public String uuid;
        public String name = 'Test Object';
        public Decimal price = 5.20;
        @testvisible String currencyCode = CHF_STRING;
        public String thumbnail = 'www.dummyImage.com';
        public boolean published = True;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        public List<EventInfo> events;
        public List<SackInfo> sacks;
        public boolean insured = True;
        public boolean paid = True; 
        public boolean claimed = False;
        public String category = 'smartPhone';
        public boolean electricBike = False;
        public String shopName = 'Test Shop'; 
        public Decimal paidAmount = 25.00;
      //  public String state;
        public String serialNumber = 'LINGSTEST';
        public String comment = 'This is used for the code Coverage';
        public Date purchasedDate = System.today();
        public Date warrantyDate = System.today().addDays(50);
        public String shopUrl = 'www.thisistestshop.com';
        public String warrantyCard = 'TestCard';
        public String fileContent1 = 'Test source 1';
        public String receipt = 'Test receipt';
        public String fileContent2 = 'Test source 2';        
        public String manufacturer = 'Test company';
        public String questionOnOff = 'Off';
        public String removeQuestionOnOff = 'Off';
        public boolean click = False;
       
    }
    
    public class OnDemandInfo {
        public Decimal daily = 0.15;
        public Decimal monthly = 2.00;
        public boolean enabled = True;
    }
    
    public class FlatRateInfo {
        public Decimal daily = 0.15;
        public Decimal monthly = 2.00;
        public Decimal savings = 0.25;
        public Date periodFrom = System.today();
        public Date periodTo = System.today().addDays(2);
        public boolean enabled = True;
        public boolean renewable = True;        
        Integer duration = Utility.numOfDays(null);
        String currencyCode = CHF_STRING;
    }     
    
    public with sharing Class EventInfo{
        public String uuid;
        public String name = 'Test Event';
        public Date periodFrom = System.today();
        public Date periodTo = System.today().addDays(2);      
    }
    
    public with sharing Class SackInfo{
        public String uuid;
        public String name = 'Test Bag';
        public String state = 'Insured';        
        public boolean toggle = True;          
        public boolean hasClaim = False;      
        public integer objectsCount = 2;
        public decimal premiumTotal = 2.56;        
    }
    
    testmethod public static void testUIRucksackControllerTest() {

       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar', Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                              UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
            insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
		Product2 pro = new Product2();
        pro.Name = 'Test Product';
		pro.DisplayUrl = 'https://lingscrm--lingsdev.com';
        pro.Manufacturer__c = 'Test Brand';
        pro.Product_Type__c = 'SmartPhone';
		insert pro;

		Object__c obj = new Object__c();
		obj.Approved__c = True;
		obj.Customer__c = con.id;
		obj.Product__c = pro.Id;
        obj.Is_Deleted__c = False;
        obj.Is_Insured__c = TRUE;
        obj.Insured_Value__c = 250;
        obj.Premium_OnDemand__c = 0.25;
        obj.Premium_FlatRate__c = 2.00;
        obj.Coverage_Type__c = 'OnDemand';
        obj.Insurance_Start_Date__c = System.today();
        obj.Insurance_End_Date__c = System.today().addDays(2);
        insert obj;
        
        Policy__c pol = new Policy__c();
        pol.Contact__c = con.Id;
        pol.Object__c = obj.Id;
        pol.Active__c = True;
        pol.Start_Date__c = System.today();
        pol.End_Date__c = System.today().addDays(2);
        pol.Invoice_Start_Date__c = System.today();
        pol.Insured_Value__c = 250;
        pol.Premium__c = 0.25;
        pol.Insurance_Type__c = 'OnDemand';
        pol.Status__c = 'Insured';
        pol.Policy_Source__c = 'Object';
        insert pol;
        
        obj.Policy__c = pol.Id;
        update obj;
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.Id;
        sack.Insurance_Flag__c = true;
        sack.Name = 'Test bag';
        insert sack;
        
        Rucksack_Object__c sackObj = new Rucksack_Object__c();
        sackObj.Contact__c = con.Id;
        sackObj.Lings_Object__c = obj.Id;
        sackObj.Rucksack__c = sack.Id;
        insert sackObj;
        
		Object__c obj1 = new Object__c();
		obj1.Approved__c = True;
		obj1.Customer__c = con.id;
		obj1.Product__c = pro.Id;
        obj1.Is_Insured__c = False;
        obj1.Is_Deleted__c = False;
        obj1.Insured_Value__c = 250;
        obj1.Premium_OnDemand__c = 0.25;
        obj1.Premium_FlatRate__c = 2.00;
        obj1.Coverage_Type__c = 'FlatRate';
        obj1.Insurance_Start_Date__c = System.today();
        obj1.Insurance_End_Date__c = System.today().addDays(2);
        insert obj1;        
        
        Rucksack__c sack1 = new Rucksack__c();
        sack1.Contact__c = con.Id;
        sack1.Insurance_Flag__c = false;
        sack1.Name = 'Test bag';
        insert sack1;
        
        Rucksack_Object__c sackObj1 = new Rucksack_Object__c();
        sackObj1.Contact__c = con.Id;
        sackObj1.Lings_Object__c = obj1.Id;
        sackObj1.Rucksack__c = sack1.Id;
        insert sackObj1;        
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true);
        
        List<SackInfo> SackInfolist = new List<SackInfo>();
        SackInfo sackclass = new SackInfo();
        sackclass.uuid = sack.Id; 
        SackInfolist.add(sackclass);
        String sackinfoJSON3 = JSON.serialize(sackclass,true);  
        
        List<LingsObject> objectList = new List<LingsObject>();
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        objectclass.sacks = SackInfolist;
        objectList.add(objectclass);
        String objectinfoJSON4 = JSON.serialize(objectclass, true);
        
        List<SackWithObject> sackwithObjectList = new List<SackWithObject>();
        SackWithObject sackObjJSON = new SackWithObject();
        sackObjJSON.uuid = sack.Id;
        sackObjJSON.insuredProducts = objectList;
        sackwithObjectList.add(sackObjJSON);
        String sackinfoJSON5 = JSON.serialize(sackObjJSON, true);
        String sackinfoJSON6 = JSON.serialize(sackwithObjectList, true);        
        
        List<SackWithObject> sackwithObjectList1 = new List<SackWithObject>();
        SackWithObject sackObjJSON1 = new SackWithObject();
        sackObjJSON1.uuid = sack1.Id;
       // sackObjJSON1.insuredProducts = objectList;
        sackwithObjectList1.add(sackObjJSON1);
        String sackinfoJSON7 = JSON.serialize(sackObjJSON1, true);
        String sackinfoJSON8 = JSON.serialize(sackwithObjectList1, true);
        
        UIRucksackController sackController = new UIRucksackController(con.Id);
        sackController.getRecords();
        sackController.readRecords(sack.Id);
        sackController.createRecords(sackinfoJSON6);
        sackController.updateRecords(sackinfoJSON6);
        sackController.updateRecords(sackinfoJSON8);
        sackController.deleteRecords(sack.Id);
        sackController.setContext(con.Id);
		        
    }
    
    testmethod public static void testUIRucksackControllerTest1() {

       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                              UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
            insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
		Product2 pro = new Product2();
        pro.Name = 'Test Product';
		pro.DisplayUrl = 'https://lingscrm--lingsdev.com';
        pro.Manufacturer__c = 'Test Brand';
        pro.Product_Type__c = 'SmartPhone';
		insert pro;
        
		Object__c obj = new Object__c();
		obj.Approved__c = True;
		obj.Customer__c = con.id;
		obj.Product__c = pro.Id;
        obj.Is_Deleted__c = False;
        obj.Insured_Value__c = 250;
        obj.Premium_OnDemand__c = 0.25;
        obj.Premium_FlatRate__c = 2.00;
        obj.Coverage_Type__c = 'OnDemand';
        obj.Insurance_Start_Date__c = System.today();
        obj.Insurance_End_Date__c = System.today().addDays(2);
        insert obj;
        
        Policy__c pol = new Policy__c();
        pol.Contact__c = con.Id;
        pol.Object__c = obj.Id;
        pol.Active__c = True;
        pol.Start_Date__c = System.today();
        pol.End_Date__c = System.today().addDays(2);
        pol.Invoice_Start_Date__c = System.today();
        pol.Insured_Value__c = 250;
        pol.Premium__c = 0.25;
        pol.Insurance_Type__c = 'OnDemand';
        pol.Status__c = 'Insured';
        pol.Policy_Source__c = 'Object';
        insert pol;
        
        obj.Policy__c = pol.Id;
        update obj;        
        
		Object__c obj1 = new Object__c();
		obj1.Approved__c = True;
		obj1.Customer__c = con.id;
		obj1.Product__c = pro.Id;
        obj1.Is_Deleted__c = False;
        obj1.Insured_Value__c = 250;
        obj1.Premium_OnDemand__c = 0.25;
        obj1.Premium_FlatRate__c = 2.00;
        obj1.Coverage_Type__c = 'FlatRate';
        obj1.Insurance_Start_Date__c = System.today();
        obj1.Insurance_End_Date__c = System.today().addDays(2);
        insert obj1;                  
        
        Policy__c pol1 = new Policy__c();
        pol1.Contact__c = con.Id;
        pol1.Object__c = obj1.Id;
        pol1.Active__c = True;
        pol1.Start_Date__c = System.today();
        pol1.End_Date__c = System.today().addDays(2);
        pol1.Invoice_Start_Date__c = System.today();
        pol1.Insured_Value__c = 250;
        pol1.Premium__c = 0.25;
        pol1.Insurance_Type__c = 'OnDemand';
        pol1.Status__c = 'Insured';
        pol1.Policy_Source__c = 'Object';
        insert pol1;
        
        obj1.Policy__c = pol1.Id;
        update obj1;                
        
        Rucksack__c sack1 = new Rucksack__c();
        sack1.Contact__c = con.Id;
        sack1.Insurance_Flag__c = false;
        sack1.Name = 'Test bag';
        insert sack1;

        Rucksack_Object__c sackObj = new Rucksack_Object__c();
        sackObj.Contact__c = con.Id;
        sackObj.Lings_Object__c = obj.Id;
        sackObj.Rucksack__c = sack1.Id;
        insert sackObj;
        
        Rucksack_Object__c sackObj1 = new Rucksack_Object__c();
        sackObj1.Contact__c = con.Id;
        sackObj1.Lings_Object__c = obj1.Id;
        sackObj1.Rucksack__c = sack1.Id;
        insert sackObj1;        
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true);
        
        List<SackInfo> SackInfolist = new List<SackInfo>();
        SackInfo sackclass = new SackInfo();
        sackclass.uuid = sack1.Id; 
        SackInfolist.add(sackclass);
        String sackinfoJSON3 = JSON.serialize(sackclass,true);            
        
        List<LingsObject> objectList = new List<LingsObject>();
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        objectclass.sacks = SackInfolist;
        objectList.add(objectclass);
        String objectinfoJSON4 = JSON.serialize(objectclass, true);                   
        
        List<SackWithObject> sackwithObjectList1 = new List<SackWithObject>();
        SackWithObject sackObjJSON1 = new SackWithObject();
        sackObjJSON1.uuid = sack1.Id;
        sackObjJSON1.insuredProducts = objectList;
        sackwithObjectList1.add(sackObjJSON1);
        String sackinfoJSON7 = JSON.serialize(sackObjJSON1, true);
        String sackinfoJSON8 = JSON.serialize(sackwithObjectList1, true);
        
        UIRucksackController sackController = new UIRucksackController(con.Id);
        sackController.getRecords();
        sackController.readRecords(sack1.Id);
        sackController.createRecords(sackinfoJSON8);
        sackController.updateRecords(sackinfoJSON8);
        sackController.deleteRecords(sack1.Id);
         
    }
}