public class Approved {
    @AuraEnabled 
    //Used in the Lightning Component to approve the Product in LingsObject.
    public static void approveProduct(Object__c inpobj, String objId){
        List <Object__c> updateObjectList = New List<Object__c>();
        List <Product2> updateProductList = New List<Product2>();
        for(Object__c obj:[SELECT id, Approved__c, Product__c, Customer__c, Product__r.Source__c, 
                           Product__r.Insured_Value__c, Product__r.Flatrate_Premium__c, 
                           Product__r.FlatRate_Savings__c, Product__r.Ondemand_Premium__c, 
                           Insured_Value__c, Premium_OnDemand__c, Premium_FlatRate__c FROM Object__c 
                           WHERE id = :objId LIMIT 1]){
                               obj.Approved__c = True;
                               
                               Product2 objProd = new Product2(id=obj.Product__c);
                               objProd.Status__c = 'Closed';
                               if(obj.Product__r.Source__c == 'DB Bike'){
                                   objProd.Flatrate_Premium__c = NULL;
                                   objProd.OnDemand_Premium__c = NULL;
                                   objProd.Insured_Value__c = NULL;
                                   objProd.Price__c = NULL;
                                   objProd.Old_Price__c = NULL;
                                   objProd.Price_Customer__c = NULL;
                               } else if (obj.Insured_Value__c != objProd.Insured_Value__c) {
                                   //LM-1171--Updating the Object Price with the Product --03MAR2020
                                   obj.Insured_Value__c = obj.Product__r.Insured_Value__c;
                                   obj.Premium_OnDemand__c = obj.Product__r.OnDemand_Premium__c;
                                   obj.Premium_FlatRate__c = obj.Product__r.Flatrate_Premium__c;
                                   obj.FlatRate_Savings__c = obj.Product__r.FlatRate_Savings__c;
                               }
                               updateProductList.add(objProd);                                                              
                               updateObjectList.add(obj); 
                           }
        Update updateObjectList;
        update updateProductList;
    }
}