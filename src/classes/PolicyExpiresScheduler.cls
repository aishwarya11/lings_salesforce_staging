global class PolicyExpiresScheduler implements Schedulable {       
    //Method to notify all user before their policy expires.
    global void execute(SchedulableContext sc) {
        // Instantiate PolicyExpires with parameters
        BatchPolicyExpires batchExpires = new BatchPolicyExpires();
        database.executeBatch(batchExpires, 5);
    }
}