@isTest
public class UIAmbassadorControllerTest {
    testmethod public static void TestUserPolicy() {
        //declaring the required test data for the apex class
        // Create Account - Ambassador
        Account acc = new Account(Name='ABC Pvt Ltd',Industry='A',Description='Sample Description',Background_URL__c='https://imageurl.com',Type='Analyst');
        acc.Key__c = 'samplekey';
        acc.DescriptionRichText__c = 'Introduction – Introduces the basic parts of the 5-paragraph essay.Topic sentence – It reveals the main idea of the specific paragraph and the way it relates to the thesis statement.';
 		acc.Description_en__c = 'test account';
        acc.Description_fr__c = 'Description in french';
        acc.Background_URL__c = 'www.Url.com';
        acc.Description_Bold_Para__c = 'test Description';
        acc.Description_Bold_Para_en__c = 'Test Description in English';
        acc.Description_Bold_Para_fr__c = 'Test Description in french'; 
        acc.DescriptionRichText__c = 'Rich Text description'; 
        acc.DescriptionRich_Text_en__c = 'Rich Text in English'; 
        acc.DescriptionRich_Text_fr__c = 'Rich Text in French';        
        
        insert acc;
        UIAmbassadorController UIAC = new UIAmbassadorController();
        UIAmbassadorController.languageCode = 'en';
        //calling the methods 
        UIAC.setContext(acc.Industry);
        UIAC.readRecords(acc.Key__c);
        UIAC.createRecords(acc.id);
        UIAC.getRecords();
        UIAC.category='Analyst';
        UIAC.updateRecords(null);
        UIAC.deleteRecords(null);

    }
    
        testmethod public static void TestUserPolicy1() {
        //declaring the required test data for the apex class
        // Create Account - Ambassador
        Account acc = new Account(Name='ABC Pvt Ltd',Industry='A',Description='Sample Description',Background_URL__c='https://imageurl.com',Type='Analyst');
        acc.Key__c = 'samplekey';
        acc.DescriptionRichText__c = 'Introduction – Introduces the basic parts of the 5-paragraph essay.Topic sentence – It reveals the main idea of the specific paragraph and the way it relates to the thesis statement.';
 		acc.Description_en__c = 'test account';
        acc.Description_fr__c = 'Description in french';
        acc.Background_URL__c = 'www.Url.com';
        acc.Description_Bold_Para__c = 'test Description';
        acc.Description_Bold_Para_en__c = 'Test Description in English';
        acc.Description_Bold_Para_fr__c = 'Test Description in french'; 
        acc.DescriptionRichText__c = 'Rich Text description'; 
        acc.DescriptionRich_Text_en__c = 'Rich Text in English'; 
        acc.DescriptionRich_Text_fr__c = 'Rich Text in French';        
        
        insert acc;
        UIAmbassadorController UIAC = new UIAmbassadorController();
        UIAmbassadorController.languageCode = 'fr';
        //calling the methods 
        UIAC.setContext(acc.Industry);
        UIAC.readRecords(acc.Key__c);
        UIAC.createRecords(acc.id);
        UIAC.getRecords();
        UIAC.category='Analyst';
        UIAC.updateRecords(null);
        UIAC.deleteRecords(null);

    }

            testmethod public static void TestUserPolicy2() {
        //declaring the required test data for the apex class
        // Create Account - Ambassador
        Account acc = new Account(Name='ABC Pvt Ltd',Industry='A',Description='Sample Description',Background_URL__c='https://imageurl.com',Type='Analyst');
        acc.Key__c = 'samplekey';
        acc.DescriptionRichText__c = 'Introduction – Introduces the basic parts of the 5-paragraph essay.Topic sentence – It reveals the main idea of the specific paragraph and the way it relates to the thesis statement.';
 		acc.Description_en__c = 'test account';
        acc.Description_fr__c = 'Description in french';
        acc.Background_URL__c = 'www.Url.com';
        acc.Description_Bold_Para__c = 'test Description';
        acc.Description_Bold_Para_en__c = 'Test Description in English';
        acc.Description_Bold_Para_fr__c = 'Test Description in french'; 
        acc.DescriptionRichText__c = 'Rich Text description'; 
        acc.DescriptionRich_Text_en__c = 'Rich Text in English'; 
        acc.DescriptionRich_Text_fr__c = 'Rich Text in French';        
        
        insert acc;
        UIAmbassadorController UIAC = new UIAmbassadorController();
        UIAmbassadorController.languageCode = 'de';
        //calling the methods 
        UIAC.setContext(acc.Industry);
        UIAC.readRecords(acc.Key__c);
        UIAC.createRecords(acc.id);
        UIAC.getRecords();
        UIAC.category='Analyst';
        UIAC.updateRecords(null);
        UIAC.deleteRecords(null);

    }
}