@isTest
public class UpdateFlagTest {
    testmethod public static void testUpdateFlagTest(){
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598');
        con.Email = 'aishwarya@rangerinc.cloud';
        insert con;
        
        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre; 
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;

        Object__c obj1 = new Object__c();
        obj1.Customer__c = con.id;
        obj1.Product__c = PO.Id;
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        UpdateFlag.approved(obj1, obj1.id);
        UpdateFlag.sendEmail(obj1, obj1.Id);
       
    }

}