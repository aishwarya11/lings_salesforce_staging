@isTest(seeAllData=false)
public class UIObjectWrapperTest {
    public static String CHF_STRING = 'CHF';
    
    public class LingsObject{
        
        public String uuid;
        public String name = 'Test Object';
        public Decimal price = 5.20;
        public String thumbnail = 'www.dummyImage.com';
        public boolean published = True;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        public List<EventInfo> events;
        public List<SackInfo> sacks;
        public boolean insured = True;
        public boolean paid = True; 
        public boolean claimed = False;
        public String category = 'Bike';
        public boolean electricBike = False;
        public String shopName = 'Test Shop'; 
        public Decimal paidAmount = 25.00;
        //  public String state;
        public String serialNumber = 'LINGSTEST';
        public String comment = 'This is used for the code Coverage';
        public Date purchasedDate = System.today();
        public Date warrantyDate = System.today().addDays(50);
        public String shopUrl = 'www.thisistestshop.com';
        public String warrantyCard = 'TestCard';
        public String fileContent1 = 'Test source 1';
        public String receipt = 'Test receipt';
        public String fileContent2 = 'Test source 2';        
        public String manufacturer = 'Test company';
        
    }
    
    public class OnDemandInfo {
        public Decimal daily = 0.15;
        public Decimal monthly = 2.00;
        public boolean enabled = True;
    }
    
    public class FlatRateInfo {
        public Decimal daily = 0.15;
        public Decimal monthly = 2.00;
        public Decimal savings = 0.25;
        public Date periodFrom = System.today();
        public Date periodTo = System.today().addDays(2);
        public boolean enabled = True;
        public boolean renewable = True;        
        Integer duration = Utility.numOfDays(null);
        String currencyCode = CHF_STRING;
    }     
    
    public with sharing Class EventInfo{
        public String uuid;
        public String name = 'Test Event';
        public Date periodFrom = System.today();
        public Date periodTo = System.today().addDays(2);      
    }
    public class BalanceInfo {
        public Decimal amount = 20;
        public Date expirationDate = System.today()+9;
        public String currencyCode = CHF_STRING;
    }
    
    public with sharing Class SackInfo{
        public String uuid;
        public String name = 'Test Bag';
        public String state = 'Insured';        
        public boolean toggle = True;          
        public boolean hasClaim = False;      
        public integer objectsCount = 2;
        public decimal premiumTotal = 2.56;        
    }    
    
    testmethod public static void testUIObjectWrapperTest() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
        Premium__c pre = new Premium__c();
        pre.Name='bike';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre; 
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Bike';
        insert PO;
        
        List<Object__c> objList = new List<Object__c>();
        List<Id> objIds = new List<Id>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c= Constants.ONDEMAND_STRING;
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today()-3;
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        objList.add(obj);
        objIds.add(obj.Id);
        
        Blob body = Blob.valueOf('No Data Available!!');
        //create attachment
        Attachment att = new Attachment(ParentId=obj.Id,
                                        Body=body,
                                        Name='Warranty');
        
        insert att;
        
        Attachment att1 = new Attachment(ParentId=obj.Id,
                                         Body=body,
                                         Name='Receipt');
        
        insert att1;
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.Id;
        sack.Insurance_Flag__c = true;
        sack.Name = 'Test bag';
        insert sack;
        
        Rucksack_Object__c sackObj = new Rucksack_Object__c();
        sackObj.Contact__c = con.Id;
        sackObj.Lings_Object__c = obj.Id;
        sackObj.Rucksack__c = sack.Id;
        insert sackObj;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;  
        
        Event_Object__c eo = new Event_Object__c ();
        eo.Event__c = eve.Id;
        eo.Object__c = obj.Id;
        insert eo;
        
        
        Policy__c pol = new Policy__c();
        pol.Object__c = obj.Id;
        pol.Contact__c = con.Id;
        pol.Active__c = True;
        pol.Insurance_Type__c = Constants.ONDEMAND_STRING;
        pol.Premium__c = 0.25;
        pol.Insured_Value__c = 250;
        pol.Status__c = 'Insured';
        pol.Start_Date__c = System.today()-3;
        pol.End_Date__c = System.today() + 1;
        pol.Invoice_Start_Date__c = System.today();        
        insert pol;
        
        obj.Policy__c = pol.Id;
        update obj; 
        
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true); 
        
        BalanceInfo biclass = new BalanceInfo();
        String myJSON3 = JSON.serialize(biclass, true);
        
        List<SackInfo> SackInfolist = new List<SackInfo>();
        SackInfo sackclass = new SackInfo();
        sackclass.uuid = sack.Id; 
        SackInfolist.add(sackclass);
        String sackinfoJSON3 = JSON.serialize(sackclass,true);
        
        List<EventInfo> EventInfolist = new List<EventInfo>();
        EventInfo eventclass = new EventInfo();
        eventclass.uuid = eve.Id; 
        EventInfolist.add(eventclass);
        String eventinfoJSON3 = JSON.serialize(eventclass,true);        
        
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        String objectinfoJSON4 = JSON.serialize(objectclass, true);
        
        UIObjectWrapper.getFlatRateWrapper(2.410);
        UIObjectWrapper.getFlatRateWrapper(0.000);
        UIObjectWrapper.getOnDemandWrapper(0.144);
        UIObjectWrapper.SackInfo sainfoclass = new UIObjectWrapper.SackInfo();
        sainfoclass.objectIds = objIds;
        UIObjectWrapper.BalanceInfo bal = new UIObjectWrapper.BalanceInfo();
        bal.amount = 20;
        bal.currencyCode = CHF_STRING;
        bal.expirationDate = System.today()+9;
        
        UIObjectWrapper.convertToWrapper(obj);             
    }   
    
    testmethod public static void testUIObjectWrapperTest1() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcodedas3.com',CommunityNickname='Lings48423', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
        Premium__c pre = new Premium__c();
        pre.Name='bike';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre; 
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Bike';
        insert PO;
        
        List<Object__c> objList = new List<Object__c>();
        List<Id> objIds = new List<Id>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c= Constants.FLATRATE_STRING;
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+31;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        objList.add(obj);
        objIds.add(obj.Id);
        
        Blob body = Blob.valueOf('No Data Available!!');
        //create attachment
        Attachment att = new Attachment(ParentId=obj.Id,
                                        Body=body,
                                        Name='Warranty');
        
        insert att;
        
        Attachment att1 = new Attachment(ParentId=obj.Id,
                                         Body=body,
                                         Name='Receipt');
        
        insert att1;
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.Id;
        sack.Insurance_Flag__c = true;
        sack.Name = 'Test bag';
        insert sack;
        
        Rucksack_Object__c sackObj = new Rucksack_Object__c();
        sackObj.Contact__c = con.Id;
        sackObj.Lings_Object__c = obj.Id;
        sackObj.Rucksack__c = sack.Id;
        insert sackObj;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;  
        
        Event_Object__c eo = new Event_Object__c ();
        eo.Event__c = eve.Id;
        eo.Object__c = obj.Id;
        insert eo;
        
        
        Policy__c pol = new Policy__c();
        pol.Object__c = obj.Id;
        pol.Contact__c = con.Id;
        pol.Active__c = True;
        pol.Insurance_Type__c = Constants.FLATRATE_STRING;
        pol.Premium__c = 0.25;
        pol.Insured_Value__c = 250;
        pol.Status__c = 'Insured';
        pol.Start_Date__c = System.today();
        pol.End_Date__c = System.today() + 31;
        pol.Invoice_Start_Date__c = System.today();        
        insert pol;
        
        obj.Policy__c = pol.Id;
        update obj; 
        
        
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true); 
        
        BalanceInfo biclass = new BalanceInfo();
        String myJSON3 = JSON.serialize(biclass, true);
        
        List<SackInfo> SackInfolist = new List<SackInfo>();
        SackInfo sackclass = new SackInfo();
        sackclass.uuid = sack.Id; 
        SackInfolist.add(sackclass);
        String sackinfoJSON3 = JSON.serialize(sackclass,true);
        
        List<EventInfo> EventInfolist = new List<EventInfo>();
        EventInfo eventclass = new EventInfo();
        eventclass.uuid = eve.Id; 
        EventInfolist.add(eventclass);
        String eventinfoJSON3 = JSON.serialize(eventclass,true);        
        
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        String objectinfoJSON4 = JSON.serialize(objectclass, true);
        
        UIObjectWrapper.getFlatRateWrapper(2.410);
        UIObjectWrapper.getFlatRateWrapper(0.000);
        UIObjectWrapper.getOnDemandWrapper(0.144);
        UIObjectWrapper.SackInfo sainfoclass = new UIObjectWrapper.SackInfo();
        sainfoclass.objectIds = objIds;
        UIObjectWrapper.BalanceInfo bal = new UIObjectWrapper.BalanceInfo();
        bal.amount = 20;
        bal.currencyCode = CHF_STRING;
        bal.expirationDate = System.today()+9;
        
        UIObjectWrapper.convertToWrapper(obj);             
    }   
    
    
    
    testmethod public static void testUIObjectWrapperTest2() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
        Premium__c pre = new Premium__c();
        pre.Name='travel insurance';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre; 
        
        Product2 PO = new Product2(Id=System.label.OKKProductId);
        PO.Price__c = 100;
        PO.Insured_Value__c = 100;
        PO.Product_Type__c = 'Travel Insurance';
        upsert PO;
        
        List<Object__c> objList = new List<Object__c>();
        List<Id> objIds = new List<Id>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c= Constants.ONDEMAND_STRING;
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today()-3;
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        
        Blob body = Blob.valueOf('No Data Available!!');
        //create attachment
        Attachment att = new Attachment(ParentId=obj.Id,
                                        Body=body,
                                        Name='Warranty');
        
        insert att;
        
        Attachment att1 = new Attachment(ParentId=obj.Id,
                                         Body=body,
                                         Name='Receipt');
        
        insert att1;
        
        UIObjectWrapper.convertToWrapper(obj);             
    }    
    
    
    
    testmethod public static void testUIObjectWrapperTest3() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
        Premium__c pre = new Premium__c();
        pre.Name='travel insurance';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre; 
        
        Product2 PO = new Product2(Id=System.label.OKKProductId);
        PO.Price__c = 100;
        PO.Insured_Value__c = 100;
        PO.Product_Type__c = 'Travel Insurance';
        upsert PO;
        
        List<Object__c> objList = new List<Object__c>();
        List<Id> objIds = new List<Id>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='NA';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today()-3;
        //obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        
        Blob body = Blob.valueOf('No Data Available!!');
        //create attachment
        Attachment att = new Attachment(ParentId=obj.Id,
                                        Body=body,
                                        Name='Warranty');
        
        insert att;
        
        Attachment att1 = new Attachment(ParentId=obj.Id,
                                         Body=body,
                                         Name='Receipt');
        
        insert att1;
        
        
        Object__c obj1 = new Object__c();
        obj1.Customer__c = con.Id;
        obj1.Product__c = PO.Id;
        obj1.Insured_Value__c=100;
        obj1.Included_Date__c=System.today();
        obj1.FlatRate_Savings__c=50;
        obj1.Coverage_Type__c=Constants.ONDEMAND_STRING;
        obj1.Premium_FlatRate__c=10.00;
        obj1.Premium_OnDemand__c=6.00;
        obj1.Flatrate_Renewal_Flag__c=false;
        obj1.Insurance_Start_Date__c=System.today()-3;
        //obj1.Insurance_End_Date__c=System.today()+1;
        obj1.Is_Deleted__c = FALSE;
        obj1.Approved__c = true;   
        obj1.Acquired_Year__c = '2019';
        obj1.Actual_amount_paid__c = 200.00;
        obj1.Description__c = 'This is the test Object';
        obj1.Designation__c = 'test';
        obj1.Is_Insured__c = TRUE;
        obj1.Own_Comment__c = 'This is a comment section';
        obj1.Sort_Order__c = 1;
        obj1.Shop_Name__c = 'Test Object';
        insert obj1;
        
        
        Policy__c pol = new Policy__c();
        pol.Object__c = obj.Id;
        pol.Contact__c = con.Id;
        pol.Active__c = True;
        pol.Insurance_Type__c = Constants.ONDEMAND_STRING;
        pol.Premium__c = 0.25;
        pol.Insured_Value__c = 250;
        pol.Status__c = 'Insured';
        pol.Start_Date__c = System.today()-3;
        pol.End_Date__c = System.today() + 1;
        pol.Invoice_Start_Date__c = System.today();        
        insert pol;
        
        obj.Policy__c = pol.Id;
        update obj; 
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;  
        
        Event_Object__c eo = new Event_Object__c ();
        eo.Event__c = eve.Id;
        eo.Object__c = obj.Id;
        insert eo;
        
        UIObjectWrapper.convertToWrapper(obj); 
        //UIObjectWrapper.convertToWrapper(obj1);            
    }
    
    testmethod public static void testUIObjectWrapperTest4() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con; 
        
        Premium__c pre = new Premium__c();
        pre.Name='travel insurance';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre; 
        
        product2 PO = new product2();
        PO.Name='sample product';
        PO.Product_Type__c='Travel Insurance';
        PO.Insured_Value__c=10.00;
        PO.Price__c=100;
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;
        PO.OnDemand_Premium__c = 5.00;
        PO.Flatrate_Premium__c = 10.00;
        PO.Searchable__c = true;
        insert PO;
        
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Premium_OnDemand__c = 3.50;
        obj.Premium_FlatRate__c = 5.00;
        obj.Included_Date__c=System.today()-3;
        obj.Coverage_Type__c= Constants.ONDEMAND_STRING;
        obj.Insurance_Start_Date__c=System.today()-3;
        obj.Insurance_End_Date__c=System.today()-1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Is_Insured__c = TRUE;
        insert obj;
        
        
        Policy__c pol = new Policy__c();
        pol.Object__c = obj.Id;
        pol.Contact__c = con.Id;
        pol.Active__c = false;
        pol.Insurance_Type__c = Constants.ONDEMAND_STRING;
        pol.Premium__c = 0.25;
        pol.Insured_Value__c = 250;
        pol.Start_Date__c = System.today()-3;
        pol.End_Date__c = System.today()-1;
        pol.Invoice_Start_Date__c = System.today()-3; 
        insert pol;
        
        obj.Policy__c = pol.Id;
        update obj;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;  
        
        Event_Object__c eo = new Event_Object__c ();
        eo.Event__c = eve.Id;
        eo.Object__c = obj.Id;
        insert eo;
        
        Blob body = Blob.valueOf('No Data Available!!');
        //create attachment
        Attachment att = new Attachment(ParentId=obj.Id,
                                        Body=body,
                                        Name='Warranty');
        
        insert att;
        
        Attachment att1 = new Attachment(ParentId=obj.Id,
                                         Body=body,
                                         Name='Receipt');
        
        insert att1;
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.Id;
        sack.Insurance_Flag__c = true;
        sack.Name = 'Test bag';
        insert sack;
        
        Rucksack_Object__c sackObj = new Rucksack_Object__c();
        sackObj.Contact__c = con.Id;
        sackObj.Lings_Object__c = obj.Id;
        sackObj.Rucksack__c = sack.Id;
        insert sackObj;
        
        UIObjectWrapper.convertToWrapper(obj);             
    } 
}