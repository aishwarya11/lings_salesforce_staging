//After the Coupon expired, if the user don’t have any credit card, then the insurance for all objects of the user will be disabled
// Runs early morning
global class BatchInsuranceDisable implements Database.Batchable<Contact>, Database.Stateful {
    Integer failed_mail_count = 0;
    Integer totalEmailsProcessed = 0;
    public List<Policy__c> successPolicyUpdateList = new List<Policy__c>();
    public List<Policy__c> failedPolicyUpdateList = new List<Policy__c>();
    public List<Object__c> successObjectUpdateList = new List<Object__c>();
    public List<Object__c> failedObjectUpdateList = new List<Object__c>();
    Set<string> targetIdsFailed = new Set<string>();
    //Start Batchable
    global Iterable<Contact> start(Database.BatchableContext BC){
        System.debug('BatchInsuranceDisable START');
        List<Contact> query = [SELECT Id, Name, Email, Balance_Pending__c, Payment_Alias_ID__c, 
                               VIU_End_date__c, VIU_Balance__c, Voucher_In_Use__c, 
                               (SELECT Id, Name, End_date__c, Start_date__c, Number_of_days_Insured__c,
                                Insurance_Type__c, Premium__c, Charges_Incurred_this_month__c, Dates__c,
                                Invoice_Start_Date__c, Invoice__c, Invoice__r.Status__c, Active__c,
                                Object__c, Policy_Charges__c  
                                FROM LingsContracts__r 
                                WHERE Object__c != NULL 
                                AND (Invoice__c = NULL OR Invoice__r.Status__c != 'Success' OR Invoice_Start_Date__c = THIS_MONTH)),
                               (SELECT name, Insurance_End_Date__c, Insurance_Start_Date__c, Coverage_type__c
                                FROM Objects__r) 
                               FROM Contact WHERE Voucher_In_Use__c != NULL];
        return query;
    }
    
    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        System.debug('BatchInsuranceDisable EXECUTE');
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Object__c> updatedObjectList = new List<Object__c>();
        List<Policy__c> expiredPolicyList = new List<Policy__c>();
        
        boolean insuranceDisableFlag = false;
        boolean couponExpiresFlag = false;
        boolean hasCreditCardFlag = false;
        
        Integer chargedDays = 0;
        Integer diffDays = 0;
        /*Date gmtToday = Date.valueOf(System.now());
Date gmtTomorrow = Date.valueOf(System.now()).addDays(+1);
Date viuEndDate = Date.valueOf(System.now());
Date polEndDate = Date.valueOf(System.now());*/
        
        Date gmtToday = System.today();
        Date gmtTomorrow = System.today().addDays(+1);
        Date viuEndDate = System.today();
        Date polEndDate = System.today();
        
        for(Contact con: scope) {
            // MR - 22MAY19 - Code Optimization
            if(con.Objects__r == NULL || con.LingsContracts__r == NULL) {
                continue;
            }
            
            Id ContactId = con.Id;
            // Initialize the Flags
            insuranceDisableFlag = false;
            couponExpiresFlag = false;
            hasCreditCardFlag = false;
            
            viuEndDate = con.VIU_End_date__c;
            if(con.Payment_Alias_Id__c != null) {
                hasCreditCardFlag = true;
            }
            
            if(con.Balance_Pending__c == null) {
                con.Balance_Pending__c = 0.0;
            }
            
            Decimal totalPendingCharges = 0.0;
            Decimal nextDayPolicyCharges = 0.0;
            Decimal allVoucherBalance = con.Balance_Pending__c + con.VIU_Balance__c;
            
            // Process and Send Email (if any policy exist)
            if(con.LingsContracts__r != NULL && con.LingsContracts__r.size() > 0) {
                // Get the total Charges Incurred from Policies
                for(Policy__c pol: con.LingsContracts__r) {
                    if(pol.End_Date__c != null) {
                        polEndDate = pol.End_Date__c.date();	//LM-588--To replace date.ValueOf to date()--12AUG2019
                    }
                    //Added Policy Charges on 26SEPT2019
                    diffDays =  pol.Invoice_Start_Date__c.daysBetween(polEndDate);	
                    chargedDays = diffDays + 1;                 
                    pol.Policy_Charges__c = Utility.getRoundedPremium(pol.Premium__c * chargedDays);                    
                    totalPendingCharges += pol.Charges_Incurred_this_month__c;
                    if(pol.Active__c) {
                        if(pol.Insurance_Type__c.equalsIgnoreCase('ondemand')) {
                            nextDayPolicyCharges += pol.Premium__c;
                        } else if(pol.Insurance_Type__c.equalsIgnoreCase('flatrate') && polEndDate == gmtToday) {
                            nextDayPolicyCharges += pol.Premium__c;
                        }
                    }
                }
            }
            
            if(!hasCreditCardFlag) {
                if(viuEndDate == gmtTomorrow) {
                    couponExpiresFlag = true;
                } else if(nextDayPolicyCharges > 0.0 && allVoucherBalance >= 0.0) {
                    // When we have active policies and voucher balance
                    Decimal availableBalance = allVoucherBalance - totalPendingCharges;
                    if(availableBalance < nextDayPolicyCharges) {
                        insuranceDisableFlag = true;
                    } else if(nextDayPolicyCharges >= availableBalance) {
                        couponExpiresFlag = true;
                    }
                }
            }
            
            
            /*System.debug('Contact[' + con.Id + ',' + con.Name + ']'  
+ 'VIU End Date - ' + viuEndDate
+ 'GMT Today - ' + gmtToday
+ 'GMT Tomorrow - ' + gmtTomorrow 
+ ' nextDayPolicyCharges - ' + nextDayPolicyCharges 
+ ' allVoucherBalance - ' + allVoucherBalance 
+ ' totalPendingCharges - ' + totalPendingCharges
+ ' Insurance Disable Flag - ' + insuranceDisableFlag 
+ ' Coupon Expires Flag - ' + couponExpiresFlag 
+ ' Credit Card Flag - ' + hasCreditCardFlag); */
            
            EmailTemplate Emailtemp = null;
            if(insuranceDisableFlag) {
                for(Policy__c pol : con.LingsContracts__r) {
                    if(pol.Active__c) {
                        Policy__c updPolicy = new Policy__c(id=pol.id);
                        updPolicy.Active__c = false;
                        if(pol.Insurance_Type__c.equalsIgnoreCase('ondemand')) {
                            updPolicy.end_date__c = Datetime.newInstance(System.today(), Utility.policyEndTime);
                            //Added Policy Charges on 26SEPT2019
                            diffDays =  pol.Invoice_Start_Date__c.daysBetween(updPolicy.end_date__c.date());	
                            chargedDays = diffDays + 1;                    
                            updPolicy.Policy_Charges__c = Utility.getRoundedPremium(pol.Premium__c * chargedDays);                            
                        }                         
                        updPolicy.Status__c = Constants.POL_STATUS_EXPIRED;
                        updPolicy.Object__c = pol.Object__c;    // MR - 03SEP19 - LM-639 Fix
                        expiredPolicyList.add(updPolicy);
                    }
                }
                
                Emailtemp = [Select id from EmailTemplate where name='Insurance-disabled' limit 1][0];
            } else if(couponExpiresFlag) {
                if(hasCreditCardFlag) {
                    Emailtemp = [Select id from EmailTemplate where name='Coupon-expires-soon-has-cc' limit 1][0];
                } else {
                    Emailtemp = [Select id from EmailTemplate where name='Coupon-expires-soon' limit 1][0];
                }
            }
            
            if(Emailtemp != null) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setUseSignature(false);
                mail.setToAddresses(new String[] { con.Email });
                OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
                mail.setTargetObjectId(ContactId);
                mail.setTemplateId(Emailtemp.Id);
                mail.setSaveAsActivity(true);
                
                mailList.add(mail);
            }
        }
        
        List<Object__c> objList = new List<Object__c>();
        
        try {
            if(expiredPolicyList.size() > 0){
                List<Database.SaveResult> srUpdate = Database.update(expiredPolicyList);
                Integer polIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Policy__c pol = expiredPolicyList[polIndex];
                    if(sr.isSuccess()) {
                        // MR - 03SEP19 - LM-639 Fix
                        Object__c updobj = new Object__c(Id = pol.Object__c);
                        updObj.Is_Insured__c = true;
                        objList.add(updObj);
                        successPolicyUpdateList.add(pol);  
                    } else {
                        failedPolicyUpdateList.add(pol);
                    }
                    
                    polIndex++;
                }
            }   
            
            try {
                totalEmailsProcessed += mailList.size();
                
                if(!Test.isRunningTest()) {
                    Messaging.sendEmail(mailList);
                }
            } catch(Exception bounceadd) {
                failed_mail_count++;
            }
        } catch(Exception Ins){
            failed_mail_count++;
            
            // Send exception email to Admin
            Utility.sendException(Ins, 'INSURANCE DISABLE AND MAILING FAILED');                  
        }
        
        // MR - 03SEP19 - LM-639 Fix
        try {      
            if(objList.size() > 0){
                List<Database.SaveResult> srUpdate = Database.update(objList);
                Integer objIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Object__c obj = objList[objIndex];
                    if(sr.isSuccess()) {
                        successObjectUpdateList.add(obj);  
                    } else {
                        failedObjectUpdateList.add(obj);
                    }
                    objIndex++;
                }
            }
        } catch(Exception uo) {
            //Send Exception to Admin
            Utility.sendException(uo, 'FAILED TO UPDATE THE OBJECT IN THE POLICY DISABLE BATCH'); 
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchInsuranceDisable FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 22MAY19 - Code Optimization
        mail.setSubject('BatchInsuranceDisable ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         
        
        String body = 'BatchInsuranceDisable Status on Updating Policies and Sending an Email to the User <br/>';
        body += '<br/> ' + (successPolicyUpdateList.size() + failedPolicyUpdateList.size()) + ' number of Policy update processing were completed.';
        
        // Processing Policy Update List
        if(successPolicyUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy Update details <br/>';
            
            for(Policy__c successList: successPolicyUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy updation happened.';
        }
        
        if(failedPolicyUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Failed Policy Update details <br/>';
            
            for(Policy__c failedList: failedPolicyUpdateList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed Policy Update processing happened.';
        }
        
        // Updating Object List   
        body += '<br/> ' + (successObjectUpdateList.size() + failedObjectUpdateList.size()) + ' number of Object records update processing were completed.'; 
        if(successObjectUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Object updation details <br/>';
            
            for(Object__c successList: successObjectUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Object updation happened.';
        }         
        if(failedObjectUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Object updation details <br/>';
            
            for(Object__c failedList: failedObjectUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Object updation happened.';
        }
        
        // Processing Email List
        body += '<br/> ' + totalEmailsProcessed + ' number of Email processing were completed.'; 
        body += '<br/> '+' Number of Email processing were completed successfully = '+(totalEmailsProcessed - failed_mail_count);
        body += '<br/> '+' Number of Email processing were not completed successfully = '+ failed_mail_count; 
        body += '<br/> Number of Error Emails occured = '+targetIdsFailed.size()+'<br/> Error Email Ids = '+targetIdsFailed;
        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);               
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()){
            if (failedObjectUpdateList.size() > 0 || failedPolicyUpdateList.size() > 0){
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchInsuranceDisable';
        ab.Track_message__c = body;
        database.insertImmediate(ab);         
        }
    }
}