@isTest
public class UIHomeControllerTest{
    @testSetup static void setup() {
        Account acc = new Account();
        acc.Name = 'Test Account 1';
        insert acc;

        List<Contact> conList = new List<Contact>();

        Contact con1 = new Contact(lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con1.email='test1@test.com';
        con1.Token__c ='samplevalue';
        con1.AccountId = acc.Id;
        conList.add(con1);

        Contact con2 = new Contact(lastname='lastname2',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con2.email='test2@test.com';
        con2.Token__c ='samplevalue';
        con2.AccountId = System.Label.AccountId;
        conList.add(con2);

        insert conList;
        
        Postal_Code__c code = new Postal_Code__c();
        code.Name = '1000';
		code.Abbreviazione__c = 'IN';
        code.Canton__c = 'test';
        code.Cantone__c = 'test2'; 
        code.Kanton__c = 'test 3';
        code.Land__c = 'India'; 
        code.Ort__c = 'Test4';
        code.Paese__c = 'test5';
        code.Pays__c = 'test 6';     
        insert code;

        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;   
        
        List<Product2> prodList = new List<Product2>();
        for(Integer i=0; i<5;i++) {
            Product2 prod = new Product2();
            prod.Name = 'Test Product'+ i;
            prodList.add(prod);
        }

        for(Integer i=0; i<5;i++) {
            Product2 prod = new Product2();
            prod.Name = 'Sample Product'+ i;
            prodList.add(prod);
        }

        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        prodList.add(PO);

        insert prodList;

        ControllerVersion__c profCV = new ControllerVersion__c();
        profCV.Name = 'profile';
        profCV.ClassName__c = 'UIProfileController';
        insert profCV;
    }

    class MyObject {
        Id id;
        String name;
        String Alias;
        String Email;
        String EmailEncodingKey;
        String FirstName;
        String LastName;
        String LanguageLocaleKey;
        String LocaleSidKey;
        String UserName;
        Id UserRoleId;
        String CommunityNickname;
        String ConLastName;
        Id ConOwnerId;
        String ConPassword;
    }
    
    testmethod public static void testUIHomeControllerclass() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.demo';
              
        MyObject instance = new MyObject();
        instance.ConLastName = 'Test Contact';
        instance.ConPassword = 'PassWord1';
        instance.Alias = 'test1';
        instance.Email = 'riyaz@test.com';
        instance.FirstName = 'First';
        instance.Lastname = 'Last';
        instance.UserName = instance.Email + userNameSuffix;
        instance.CommunityNickname = 'nick1';

        String myJSON = JSON.serialize(instance);

        UIHomeController.register('langCode',myJSON);
    }

    testMethod public static void userCreationTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User'];
        List<Contact> contactList = [Select Id, Name from Contact where Token__c = 'samplevalue'];
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;

        List<User> uList = new List<User>();

        User u = new User(Email='test1@test.com',
                          EmailEncodingKey='UTF-8', FirstName='User1', LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='test1@test.comtest', CommunityNickname='Lings32184');
        u.City = 'chennai';
        u.Street = 'Ashok Nagar';
        u.Phone = '988456897';
        u.DOB__c = System.today();
        u.House_Number__c = '100';
        u.PostalCode = '1000';
        u.ContactId = contactList[0].Id;
        System.runAs (new User(Id = UserInfo.getUserId())) {
            insert u;
        }  
        
        System.runAs (new User(Id = u.Id)) {
            UIHomeController.getContactId();

            String resp = UIHomeController.process('langCode','profile', 'read', '');
            String resp4 = UIHomeController.process('langCode','profile', 'create', '');
            String resp1 = UIHomeController.process('langCode','profile', 'update', '');
            String resp2 = UIHomeController.process('langCode','profile', 'delete', '');
            String resp3 = UIHomeController.process('langCode','profile', 'list', '');
            
            String resp5 = UIHomeController.process('langCode','ambassador', 'list', '');
            
            UIHomeController.getProfile('langCode');
            UIHomeController.sendPolicyEmail('langCode');
            UIHomeController.tokenLogin('langCode','samplevalue');
            UIHomeController.emailVerification('langCode','samplevalue');
            UIHomeController.getProducts('langCode','sample', '');
            UIHomeController.getProducts('langCode','test', '');
            UIHomeController.getProducts('langCode','complex', '');
            UIHomeController.getProducts('langCode','asadfasf', '');
            UIHomeController.sortObjects(1, 4, 'Laptop');
            UIHomeController.sortCategories('Category Order');
            //UIHomeController.createAttachment('File Name', 'fileContent');
            //UIHomeController.deleteAttachment('ContentDocumentId');
            UIHomeController.getAttachmentString('ObjectId','attach');
        }
    }
    
    testmethod public static void testUIHomeControllerclass1() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.demo';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                          UserName=uniqueUserName,CommunityNickname='Lings32184');
        u.City = 'chennai';
        u.Street = 'Ashok Nagar';
        u.Phone = '988456897';
        u.DOB__c = System.today();
        u.House_Number__c = '100';
        u.PostalCode = '1000';
        insert u;
        
        User u1 = new User(Email='user@tes.com',
                           EmailEncodingKey='UTF-8', FirstName='Us',LastName='te', Alias='Uste', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                           UserName='aishwarya01041997@test.com',CommunityNickname='Lings3');
        u1.City = 'chennai';
        u1.Street = 'Ashok Nagar';
        u1.Phone = '988456897';
        u1.DOB__c = System.today();
        u1.House_Number__c = '100';
        u1.PostalCode = '1000';
        insert u1;
        
        System.runAs (new User(Id = UserInfo.getUserId())) {
            Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
            con.OwnerId = u.Id;
            con.email='test@test.com';
            con.Token__c ='samplevalue';
            insert con;
            
            MyObject instance = new MyObject();
            instance.ConLastName = con.LastName;
            instance.ConOwnerId =  con.OwnerId;
            instance.ConPassword = con.Password__c;
            instance.id = u.Id;
            instance.Alias=u.firstName.subString(0,1) + u.lastName.substring(2,5);
            instance.Email=u.Email;
            instance.EmailEncodingKey=u.EmailEncodingKey;
            instance.FirstName=u.FirstName;
            instance.Lastname=u.LastName;
            instance.LanguageLocaleKey=u.LanguageLocaleKey;
            instance.LocaleSidKey=u.LocaleSidKey;
            instance.UserName=u.email + userNameSuffix;
            instance.CommunityNickname=u.CommunityNickname;
            instance.UserRoleId = u.UserRoleId;
            String myJSON = JSON.serialize(instance);
            
            MyObject instance1 = new MyObject();
            instance1.ConLastName = con.LastName;
            instance1.ConOwnerId =  con.OwnerId;
            instance1.ConPassword = con.Password__c;
            instance1.id = u1.Id;
            instance1.FirstName=u1.FirstName;
            instance1.Lastname=u1.LastName;
            instance1.Alias= Utility.generateToken(5);
            instance1.Email=u1.Email;
            instance1.EmailEncodingKey=u1.EmailEncodingKey;
            
            instance1.LanguageLocaleKey=u1.LanguageLocaleKey;
            instance1.LocaleSidKey=u1.LocaleSidKey;
            instance1.UserName=u1.email + userNameSuffix;
            instance1.CommunityNickname=u1.CommunityNickname;
            instance1.UserRoleId = u1.UserRoleId;
            String myJSON1 = JSON.serialize(instance1);            
            
            Voucher__c vch = new Voucher__c();
            vch.Name = 'VCH1';
            vch.Amount__c = 100;
            vch.Fixed__c = System.today()+30;
            vch.From__c = System.today()-30;
            vch.To__c = System.today()+30;
            vch.Usage_Limit__c = 10;
            insert vch;
            
            Product2 PO= new Product2();
            PO.Name='sample product';
            PO.Insured_Value__c = 200.00;
            PO.Price__c= PO.Insured_Value__c.setScale(2);
            insert PO;
            
            UIHomeController UIHC = new UIHomeController();
            UIHC.username = u.Username;
            UIHC.password = con.Password__c;
            UIHomeController.getContactId();    
            UIHomeController.readLoginUser();

            //UIHomeController.login('revathy@rangerinc.cloud', 'welcome2');
            //UIHomeController.login(u.Username, con.Password__c);
            UIHomeController.tokenLogin('langCode',con.Token__c);
            //Product with two input
            List<UIResultWrapper.ProductInfo> resp = new List<UIResultWrapper.ProductInfo>();
            UIHomeController.getProducts('langCode','sample product', 'velo');
            UIHomeController.getProducts('langCode','sample product', 'SmartPhone');
            UIResultWrapper.ProductInfo sr = new UIResultWrapper.ProductInfo();        
            UIHomeController.register('langCode',myJSON);
            UIHomeController.register('langCode',myJSON1);
            UIHomeController.updateProfile('langCode',myJSON);
            UIHomeController.newsletterSubscribe('langCode',u.FirstName, u.Email);
            UIHomeController.newsletterSubscribe('langCode','',u.Email);
            UIHomeController.getProfile('langCode');
            UIHomeController.sendPolicyEmail('langCode');
            UIHomeController.redeemVoucher('langCode',vch.id);
            UIHomeController.passwordChange('langCode','oldPassword', 'newPassword');
            UIHomeController.passwordChange('langCode','', 'newPassword');
            UIHomeLogin.resetPassword('langCode',con.Email);
            UIHomeLogin.resetPassword('langCode','');
            UIHomeController.emailVerification('langCode',con.Token__c);
            try{
                UIHomeController.getCardURL('langCode','sourceURL');
               UIHomeController.checkCardAdd('langCode','sourceURL');
            }catch(Exception hp){}

       UIProfileController UIPC = new UIProfileController(System.Label.TestContact);
            //UIHomeController.UIProfileController hp = new UIHomeController.UIProfileController(System.Label.TestContact);
            
        }
    }
      testmethod public static void testUIHomeControllerclass2() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.demo';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                          UserName=uniqueUserName,CommunityNickname='Lings32184');
        u.City = 'chennai';
        u.Street = 'Ashok Nagar';
        u.Phone = '988456897';
        u.DOB__c = System.today();
        u.House_Number__c = '100';
        u.PostalCode = '1000';
        insert u;
        
        User u1 = new User(Email='user@tes.com',
                           EmailEncodingKey='UTF-8', FirstName='Us',LastName='te', Alias='Uste', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                           UserName='aishwarya01041997@test.com',CommunityNickname='Lings3');
        u1.City = 'chennai';
        u1.Street = 'Ashok Nagar';
        u1.Phone = '988456897';
        u1.DOB__c = System.today();
        u1.House_Number__c = '100';
        u1.PostalCode = '1000';
        insert u1;
        
        System.runAs (new User(Id = UserInfo.getUserId())) {
            Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
            con.OwnerId = u.Id;
            con.email='test@test.com';
            con.Token__c ='samplevalue';
            insert con;
            
            MyObject instance = new MyObject();
            instance.ConLastName = con.LastName;
            instance.ConOwnerId =  con.OwnerId;
            instance.ConPassword = con.Password__c;
            instance.id = u.Id;
            instance.Alias=u.firstName.subString(0,1) + u.lastName.substring(2,5);
            instance.Email=u.Email;
            instance.EmailEncodingKey=u.EmailEncodingKey;
            instance.FirstName=u.FirstName;
            instance.Lastname=u.LastName;
            instance.LanguageLocaleKey=u.LanguageLocaleKey;
            instance.LocaleSidKey=u.LocaleSidKey;
            instance.UserName=u.email + userNameSuffix;
            instance.CommunityNickname=u.CommunityNickname;
            instance.UserRoleId = u.UserRoleId;
            String myJSON = JSON.serialize(instance);
            
            MyObject instance1 = new MyObject();
            instance1.ConLastName = con.LastName;
            instance1.ConOwnerId =  con.OwnerId;
            instance1.ConPassword = con.Password__c;
            instance1.id = u1.Id;
            instance1.FirstName=u1.FirstName;
            instance1.Lastname=u1.LastName;
            instance1.Alias= Utility.generateToken(5);
            instance1.Email=u1.Email;
            instance1.EmailEncodingKey=u1.EmailEncodingKey;
            
            instance1.LanguageLocaleKey=u1.LanguageLocaleKey;
            instance1.LocaleSidKey=u1.LocaleSidKey;
            instance1.UserName=u1.email + userNameSuffix;
            instance1.CommunityNickname=u1.CommunityNickname;
            instance1.UserRoleId = u1.UserRoleId;
            String myJSON1 = JSON.serialize(instance1);            
            
            Voucher__c vch = new Voucher__c();
            vch.Name = 'VCH1';
            vch.Amount__c = 100;
            vch.Fixed__c = System.today()+30;
            vch.From__c = System.today()-30;
            vch.To__c = System.today()+30;
            vch.Usage_Limit__c = 10;
            insert vch;
            
            Product2 PO= new Product2();
            PO.Name='sample product';
            PO.Insured_Value__c = 200.00;
            PO.Price__c= PO.Insured_Value__c.setScale(2);
            insert PO;
            
            UIHomeController UIHC = new UIHomeController();
            UIHC.username = u.Username;
            UIHC.password = con.Password__c;
            UIHomeController.getContactId();    
            UIHomeController.readLoginUser();

            //UIHomeController.login('revathy@rangerinc.cloud', 'welcome2');
            //UIHomeController.login(u.Username, con.Password__c);
            UIHomeController.tokenLogin('langCode',con.Token__c);
            //Product with two input
            List<UIResultWrapper.ProductInfo> resp = new List<UIResultWrapper.ProductInfo>();
            UIHomeController.getProducts('langCode','sample product', 'velo');
            UIHomeController.getProducts('langCode','sample product', 'SmartPhone');
            UIResultWrapper.ProductInfo sr = new UIResultWrapper.ProductInfo();        
            UIHomeController.register('langCode',myJSON);
            UIHomeController.register('langCode',myJSON1);
            UIHomeController.updateProfile('langCode',myJSON);
            UIHomeController.newsletterSubscribe('langCode',u.FirstName, u.Email);
            UIHomeController.newsletterSubscribe('langCode','',u.Email);
            UIHomeController.getProfile('langCode');
            UIHomeController.sendPolicyEmail('langCode');
            UIHomeController.redeemVoucher('langCode',vch.id);
            UIHomeController.passwordChange('langCode','oldPassword', 'newPassword');
            UIHomeController.passwordChange('langCode','', 'newPassword');
            UIHomeLogin.resetPassword('langCode',con.Email);
            UIHomeLogin.resetPassword('langCode','');
            UIHomeController.emailVerification('langCode',con.Token__c);
            try{
               UIHomeController.checkCardAdd('langCode','sourceURL');
            }catch(Exception hp){}

       UIProfileController UIPC = new UIProfileController(System.Label.TestContact);
            //UIHomeController.UIProfileController hp = new UIHomeController.UIProfileController(System.Label.TestContact);
            
        }
    }
    
}