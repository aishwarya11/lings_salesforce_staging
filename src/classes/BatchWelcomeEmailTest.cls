@isTest
public class BatchWelcomeEmailTest {
    
    testmethod public static void testBatchWelcomeEmailTest() {
        //try{
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.SignUp__c=true;
        con.Register_Confirmed__c=false;
        con.Email_Verified__c=true;
        insert con;
        
        List<Contact> c = new List<Contact>();
        c.add(con);
        
        Test.startTest();
        
        BatchWelcomeEmail BCE = new BatchWelcomeEmail(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        // }catch(Exception pi){}
    }
    
}