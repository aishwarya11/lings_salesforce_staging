@isTest
public class UIHomeLoginTest {
    
    @testSetup static void setup() {
        Account acc = new Account();
        acc.Name = 'Test Account 1';
        insert acc;
		List<Contact> conList = new List<Contact>();
        
        Contact con1 = new Contact(lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con1.email='test1@test.com';
        con1.Token__c ='samplevalue';
        con1.AccountId = acc.Id;
        con1.Email_Verified__c = true;
        conList.add(con1);

        insert conList;
        
    }
    testmethod public static void testUIHomeLoginTest() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.staging';
        try{             
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community Login User'];
        List<Contact> contactList = [Select Id, Name, Password__c from Contact where Token__c = 'samplevalue'];
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;

        List<User> uList = new List<User>();

        User u = new User(Email='test1@test.com',
                          EmailEncodingKey='UTF-8', FirstName='User1', LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='test1@test.comlings.staging', CommunityNickname='Lings32184');
        u.City = 'chennai';
        u.Street = 'Ashok Nagar';
        u.Phone = '988456897';
        u.DOB__c = System.today();
        u.House_Number__c = '100';
        u.First_Login__c = false;
        u.ContactId = contactList[0].Id;
        System.runAs (new User(Id = UserInfo.getUserId())) {
            insert u;
        }               
            
            UIHomeController UIHC = new UIHomeController();
            UIHomeLogin UIHL = new UIHomeLogin(UIHC);
            UIHL.username = u.UserName;
            UIHL.password = contactList[0].Password__c;
            
            UIHomeLogin.login('en','test1@test.com', 'welcome2');
            UIHomeLogin.resetPassword('en','test1@test.com');
            UIHomeLogin.passwordSet(u.Id);
            UIHomeLogin.numberOfAttempts(u.Id);
            
        }catch(Exception HL){}
    }
}