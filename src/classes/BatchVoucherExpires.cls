//This Batch runs Every day at night 11:30 PM
Global class BatchVoucherExpires implements Database.Batchable<Contact>, Database.Stateful {    
        List<Contact> successContactUpdateList = new List<Contact>();    
        List<Contact> failedContactUpdateList = new List<Contact>();
       //Start batchable
        global Iterable<Contact> start(Database.BatchableContext BC){
        // Get all the Object Policies that are expiring today
        List<Contact> query = [Select id, name, Voucher_in_use__c,VIU_End_date__c from Contact WHERE VIU_End_date__c = TODAY];
        return query;
    }
        //Execute batchable
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        System.debug('Number of records for processing - ' + scope.size());
        List<Contact> updatedVC = new List<Contact>();
         for(Contact vc : scope) {
            // Update Policy end date if its null
            if(vc.VIU_End_date__c == System.today()) {
                vc.Voucher_In_Use__c = NULL;
                updatedVC.add(vc);  
                System.debug('Voucher Value'+vc.Voucher_In_Use__c);
            }
         }
        //Updating Contact List
        //Update updatedVC;           
        if(updatedVC.size() > 0){
            List<Database.SaveResult> srUpdate = Database.update(updatedVC);
            Integer conIndex = 0;  
            for(Database.SaveResult sr: srUpdate) {
                Contact con = updatedVC[conIndex];
                if(sr.isSuccess()) {
                    successContactUpdateList.add(con);  
                } else {
                    failedContactUpdateList.add(con);
                }

                conIndex++;
            }
        }
    }
         // Finish Batachable
    global void finish(Database.BatchableContext BC){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String>{'riyaz.mohamed@outlook.com'});
        mail.setSubject('BatchVoucherExpires ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);   
        String body = 'BatchVoucherExpires Status on Updating Contact records<br/>';
        //Processing Contact List
        body += '<br/> ' + (successContactUpdateList.size() + failedContactUpdateList.size()) + ' number of Contact records Update processing were completed.'; 
        if(successContactUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Contact Updation details <br/>';
            
            for(Contact successList: successContactUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Contact updation happened.';
        }
        if(failedContactUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Failed Contact updation details <br/>';
            
            for(Contact failedList: failedContactUpdateList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed Contact Updation processing happened.';
        }

        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);
		//LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest() && failedContactUpdateList.size() > 0){
            Messaging.sendEmail(mails);

            System.debug('BatchVoucherExpires FINISH');
        }
        
    }   

}