public with sharing class UIAmbassadorController implements DataHandler {
    @testvisible private String category = '';
    public static String languageCode {get; set;}	//LM-532 --Multilingual for Ambassador--20JULY2019
    
    //defining the data structure 
    public class Wrapper {
        public String uuid;
        public String name;
        public String category;
        public String description;
        public String imageurl;
        public String headline;
        public List<String> content;
        public List<String> images;
        public Map<String, String> social;
    }
    
    public UIAmbassadorController() {
        
    }    
    
    //setting value for Account object's field Industry
    public void setContext(String catString) {
        this.category = catString;
    }
    
    //read value for Account object's field Industry
    public String getRecords() {
        // Read the Accounts of Type = Analyst
        List<Wrapper> wrapList = new List<Wrapper>();
        String langCode = languageCode;
        // Construct the response JSON
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        for(Account acc: [Select Id, Name,Account_name_en__c, Account_Name_fr__c, Industry, Description,Description_en__c,
                          Description_fr__c, Background_URL__c, Description_Bold_Para__c,Description_Bold_Para_en__c,
                          Description_Bold_Para_fr__c, DescriptionRichText__c, DescriptionRich_Text_en__c, 
                          DescriptionRich_Text_fr__c, Image_URL_1__c, Image_URL_2__c,
                          Image_URL_3__c, Image_URL_4__c, Image_URL_5__c, Image_URL_6__c, Website,
                          Facebook__c, Linkedin__c, Instagram__c, YouTube__c from Account WHERE Type = 'Analyst']) 
        {
            Wrapper wrap = new Wrapper();
            wrap.uuid = acc.Id;
            wrap.category = acc.Industry;
            wrap.imageurl = acc.Background_URL__c;
            
            if(langCode.equalsIgnoreCase('en')){	//LM-532 --Multilingual for Ambassador in English--20JULY2019
                wrap.name = acc.Account_Name_en__c;
                wrap.description = acc.Description_en__c;   
                wrap.headline = acc.Description_Bold_Para_en__c;
                
            }else if(langCode.equalsIgnoreCase('fr')){		//LM-532 --Multilingual for Ambassador in French--20JULY2019
                wrap.name = acc.Name;
                wrap.description = acc.Description_fr__c;     
                wrap.headline = acc.Description_Bold_Para_fr__c;  
                
            }else{											//LM-532 --Multilingual for Ambassador in German--20JULY2019
                wrap.name = acc.Name;
                wrap.description = acc.Description;
                wrap.headline = acc.Description_Bold_Para__c;
            }
            
            if(String.isBlank(this.category) || (String.isNotBlank(this.category) && this.category.equalsIgnoreCase(wrap.category))) {
                wrapList.add(wrap);                
            }
        }
        
        jsonGen.writeObjectField('data', wrapList);        
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        // MR - 20MAY19 - Sesssion Expired Flag for common case
        jsonGen.writeBooleanField('sessionExpired', (UserInfo.getUserName().containsIgnoreCase('Guest User') ? true : false));
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString();
    }
    
    public String readRecords(String inpName) {
        // Read the Accounts of Type = Analyst
        Wrapper wrap = new Wrapper();
        String langCode = languageCode;
        // Construct the response JSON
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        for(Account acc: [Select Id, Name,Account_name_en__c, Account_Name_fr__c, Industry, Description,Description_en__c,
                          Description_fr__c, Background_URL__c, Description_Bold_Para__c,Description_Bold_Para_en__c,
                          Description_Bold_Para_fr__c, DescriptionRichText__c, DescriptionRich_Text_en__c, 
                          DescriptionRich_Text_fr__c, Image_URL_1__c, Image_URL_2__c,
                          Image_URL_3__c, Image_URL_4__c, Image_URL_5__c, Image_URL_6__c, Website,
                          Facebook__c, Linkedin__c, Instagram__c, YouTube__c from Account WHERE Key__c = :inpName]) {
                              wrap.uuid = acc.Id;
                              jsonGen.writeStringField('LanguageCode', ''+langCode);	
                              //20JULY2019--multilingual 
                              if(langCode.equalsIgnoreCase('en')){		//LM-532 --Multilingual for Ambassador in English--20JULY2019
                                  wrap.name = acc.Account_Name_en__c;
                                  wrap.description = acc.Description_en__c;
                                  wrap.category = acc.Industry;
                                  wrap.imageurl = acc.Background_URL__c;
                                  wrap.headline = acc.Description_Bold_Para_en__c;
                                  
                                  wrap.content = new List<String>();
                                  if(String.isNotBlank(acc.DescriptionRich_Text_en__c)) {
                                      for(String contentString: acc.DescriptionRich_Text_en__c.split('\n')) {
                                          if(contentString.length() > 0) {
                                              wrap.content.add(contentString);   
                                          }
                                      }
                                  }                                  
                              }else if(langCode.equalsIgnoreCase('fr')){		//LM-532 --Multilingual for Ambassador in French--20JULY2019
                                  wrap.name = acc.Account_Name_fr__c;
                                  wrap.description = acc.Description_fr__c;
                                  wrap.category = acc.Industry;
                                  wrap.imageurl = acc.Background_URL__c;
                                  wrap.headline = acc.Description_Bold_Para_fr__c;
                                  
                                  wrap.content = new List<String>();
                                  if(String.isNotBlank(acc.DescriptionRich_Text_fr__c)) {
                                      for(String contentString: acc.DescriptionRich_Text_fr__c.split('\n')) {
                                          if(contentString.length() > 0) {
                                              wrap.content.add(contentString);   
                                          }
                                      }
                                  }                                      
                              }else{											//LM-532 --Multilingual for Ambassador in German--20JULY2019
                                  wrap.name = acc.Name;
                                  wrap.description = acc.Description;
                                  wrap.category = acc.Industry;
                                  wrap.imageurl = acc.Background_URL__c;
                                  wrap.headline = acc.Description_Bold_Para__c;
                                  
                                  wrap.content = new List<String>();
                                  if(String.isNotBlank(acc.DescriptionRichText__c)) {
                                      for(String contentString: acc.DescriptionRichText__c.split('\n')) {
                                          if(contentString.length() > 0) {
                                              wrap.content.add(contentString);   
                                          }
                                      }
                                  }       
                              }
                              wrap.images = new List<String>();
                              wrap.images.add(acc.Image_URL_1__c);
                              wrap.images.add(acc.Image_URL_2__c);
                              wrap.images.add(acc.Image_URL_3__c);
                              wrap.images.add(acc.Image_URL_4__c);
                              wrap.images.add(acc.Image_URL_5__c);
                              wrap.images.add(acc.Image_URL_6__c);
                              
                              wrap.social = new Map<String, String>();
                              if(String.isNotBlank(acc.Website)) { wrap.social.put('Website', acc.Website); }
                              if(String.isNotBlank(acc.Facebook__c)) { wrap.social.put('Facebook', acc.Facebook__c); }
                              if(String.isNotBlank(acc.YouTube__c)) { wrap.social.put('YouTube', acc.YouTube__c); }
                              if(String.isNotBlank(acc.Linkedin__c)) { wrap.social.put('LinkedIn', acc.Linkedin__c); }
                              if(String.isNotBlank(acc.Instagram__c)) { wrap.social.put('Instagram', acc.Instagram__c); }
                              
                          }
        jsonGen.writeStringField('uuid', ''+wrap.uuid);		
        jsonGen.writeStringField('status', 'success');   
        jsonGen.writeObjectField('ambassador', wrap); 
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeObjectField('input', inpName);        
        jsonGen.writeEndObject();
        
        
        return jsonGen.getAsString();
    }
    public String createRecords(String inputJSON) {
        return '{"status":"failure","message":"Create operation is not supported"}';
    }
    
    public String updateRecords(String inputJSON) {
        return '{"status":"failure","message":"Update operation is not supported"}';
    }
    
    public String deleteRecords(String inputJSON) {
        return '{"status":"failure","message":"Delete operation is not supported"}';
    }
}