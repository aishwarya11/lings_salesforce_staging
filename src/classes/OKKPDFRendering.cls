public without sharing class OKKPDFRendering {
    
    private ContentVersion createContentVersion(String name, Blob body){
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.ContentLocation = 'S'; // S = Stored in Salesforce
        contentVersion.PathOnClient = name;
        contentVersion.Title = name;
        contentVersion.VersionData = body;
        return contentVersion;
    }
    
    private ContentDocumentLink createContentDocumentLink(Id contentDocumentId, Id parentId){
        ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
        contentDocumentLink.ContentDocumentId = contentDocumentId;
        contentDocumentLink.LinkedEntityId = parentId;
        contentDocumentLink.ShareType = 'V'; // Inferred permission
        contentDocumentLink.Visibility = 'AllUsers';
        return contentDocumentLink;
    }
    
    private contentDistribution createContentDelivery(Id contentVersionId, String attName){
        contentDistribution cd = new contentDistribution();
        cd.ContentVersionId = contentVersionId;
        cd.Name = attName;
        cd.PreferencesLinkLatestVersion = True;
        cd.PreferencesNotifyOnVisit = False;
        return cd;
    }
    
    
    public String addAttachmentsToContract(String OKKObjId,String contactId) {
        String displayURL = '';
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        try{
            System.debug('Inside the method 1st line');
            
            PageReference pdf = ApexPages.currentPage();
            EmailTemplate template = new EmailTemplate();
            
            // Reva - To attach the AVB document according to the Object (i.e Whether it is OKK or RVG)
            Object__c traInsObj = [Select id,name,Product__c,Included_Date__c,Is_Deleted__c,Insurance_Start_Date__c,createdDate
                                   from Object__c where id=:OKKObjId limit 1];
                
            // Reva - To set the PDF File and Email Template depends on the Object 
            if(traInsObj.Product__c == System.label.OKKProductId){
                pdf = Page.OKKpdfpage;
            	template = [Select id from EmailTemplate where name='OKK Insurance Confirmation'];
            }else if(traInsObj.Product__c == System.label.RVGProductId){
                pdf = Page.RVGPDFPage;
            	template = [Select id from EmailTemplate where name='RVG Insurance Confirmation'];
            }
            
            
            // add parent id to the parameters for standardcontroller
            pdf.getParameters().put('id', OKKObjId);
            System.debug('After querying the template and it Id is '+template.Id);
            Attachment[] a = [Select ParentId From Attachment where parentId = :template.Id];
            System.debug('After Quering the Attachment');
            System.debug('Already generated attachment Id = '+template.Id);
            if(a.size() > 0){
                System.debug('List of attachment is available for this template');
                delete a;
            }
            
            // the contents of the attachment from the pdf
            Blob body;
            
            try {
                // returns the output of the page as a PDF
                body = pdf.getContentAsPdf();
                System.debug('Body of the Email = '+body);
            } catch (VisualforceException e) {
                body = Blob.valueOf('No Data Available!!');
                System.debug('Catch part');
            }
            
            Attachment att = new Attachment();
            
            // Reva - To set the PDF File and Email Template depends on the Object 
            if(traInsObj.Product__c == System.label.OKKProductId){
                att = new Attachment(ParentId=contactId,
                                     Body=body,
                                     Name='OKKPolicy ' + System.now().format('dd.MM.yyyy')+ '.pdf');
            }else if(traInsObj.Product__c == System.label.RVGProductId){
                att = new Attachment(ParentId=contactId,
                                     Body=body,
                                     Name='RVGPolicy ' + System.now().format('dd.MM.yyyy')+ '.pdf');
            }
            
            insert att;
            System.debug('Attachment Id = '+att.id);
            
            ContentVersion cv = createContentVersion(att.name, att.Body);
            insert cv;
            
            ContentVersion newCV = [SELECT id,title,contentdocumentid from contentversion where id = : cv.id];
            System.debug('ContentVersion id = '+newCV.Id + ' and the ContentDoument Id is = '+ newCV.ContentDocumentId);
            
            ContentDocumentLink cdl = createContentDocumentLink(newCV.ContentDocumentId, contactId);
            insert cdl;
            System.debug('ContentDocumentLink id = '+cdl.Id);
            
            contentDistribution cd = createContentDelivery(cv.Id,att.Name);
            insert cd;
            System.debug('ContentDistribution id = '+cd.Id);
            
            ContentDistribution cdNew = [Select id,contentdownloadUrl,DistributionPublicUrl from ContentDistribution where id=:cd.id];
            
            
            if(cdNew.contentdownloadUrl != null && cdNew.contentdownloadUrl !=''){
                Object__c obj = [Select id,name,DisplayURL__C from Object__c where id=:OKKObjId];
                obj.DisplayURL__c = cdNew.DistributionPublicUrl;
                update obj;
                displayURL = obj.DisplayURL__c;
                jsonGen.writeStringField('status', 'success');
            }
            
        }catch(Exception e){
            
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {'revathy@rangerinc.cloud'});
            mail.setSubject('OKKPdf Error in OKKPDFRendering'); 
            String body ='Cause=>'+ e.getCause() +', ' +'getMessage=>'+ e.getMessage()+', '+'getStackTraceString=>'+e.getStackTraceString()+', '+'getTypeName=>'+e.getTypeName();     
            mail.setHtmlBody(body);  
            mails.add(mail);
            Messaging.sendEmail(mails);
            
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', e.getMessage());
        }
        jsonGen.writeStringField('displayURL', displayURL);
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString();
    }
    
}