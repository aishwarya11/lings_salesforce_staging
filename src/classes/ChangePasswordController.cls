/**
 * An apex page controller that exposes the change password functionality
 */
public with sharing class ChangePasswordController {
    // Declare all parameters in the page
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}    
   	public ChangePasswordController() {}    

    // Code we will invoke on page load.    
    public PageReference changePassword() {
        return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
    }     
    

}