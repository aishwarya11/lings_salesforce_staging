@isTest
public class ProductBeforeTriggerTest {
        @isTest static void testTestProductBeforeTrigger() {
            //try{
            
            Premium__c pre = new Premium__c();
            pre.Name='smartphone';
            pre.On_Demand__c=12.00;
            pre.FlatRate__c=32.00;
            insert pre;
            
            Premium__c pre1 = new Premium__c();
            pre1.Name='laptop';
            pre1.On_Demand__c=12.00;
            pre1.FlatRate__c=32.00;
            insert pre1;
            
            product2 pdt = new product2();
            pdt.Name='sample product';
            pdt.Product_Type__c='smartphone';
            pdt.Insured_Value__c=10.00;
            pdt.Price__c=100;
            pdt.Price_Customer__c = 100;
            pdt.Old_Price__c=pdt.Price__c;
            pdt.OnDemand_Premium__c = 5.00;
            pdt.Flatrate_Premium__c = 10.00;
            pdt.Searchable__c = true;
            insert pdt;
            
            product2 pdt2 = new product2();
            pdt2.Id = pdt.Id;
            pdt2.Product_Type__c='laptop';
            pdt2.Insured_Value__c = 12;
            pdt2.Old_Price__c = 68;
            pdt2.Searchable__c = false;
            pdt2.Price__c = 85;
            pdt2.Price_Customer__c = 200;
            pdt2.OnDemand_Premium__c = 10.00;
            pdt2.Flatrate_Premium__c = 11.00;
            update pdt2;
            //}
            //catch(Exception e){}

}
            @isTest static void testTestProductBeforeTrigger1() {
            //try{
            
            Premium__c pre = new Premium__c();
            pre.Name='smartphone';
            pre.On_Demand__c=12.00;
            pre.FlatRate__c=32.00;
            insert pre;
            
            Premium__c pre1 = new Premium__c();
            pre1.Name='laptop';
            pre1.On_Demand__c=12.00;
            pre1.FlatRate__c=32.00;
            insert pre1;
            
            product2 pdt = new product2();
            pdt.Name='sample product';
            pdt.Product_Type__c='smartphone';
            pdt.Insured_Value__c=10.00;
            pdt.Price__c=100;
            pdt.Price_Customer__c = 100;
            pdt.Old_Price__c=pdt.Price__c;
            pdt.OnDemand_Premium__c = 5.00;
            pdt.Flatrate_Premium__c = 10.00;
            pdt.Searchable__c = true;
            pdt.Status__c = 'New';
            insert pdt;
            
            product2 pdt2 = new product2();
            pdt2.Id = pdt.Id;
            pdt2.Product_Type__c='laptop';
            pdt2.Insured_Value__c = 12;
            pdt2.Old_Price__c = 68;
            pdt2.Searchable__c = false;
            pdt2.Price__c = 200;
            pdt2.Price_Customer__c = 200;
            pdt2.OnDemand_Premium__c = 10.00;
            pdt2.Flatrate_Premium__c = 11.00;
            pdt2.Status__c = 'Closed';
            update pdt2;
            //}
            //catch(Exception e){}

}

}