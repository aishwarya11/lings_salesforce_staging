global class EventRemainderScheduler implements Schedulable {
    //method to trigger Reminder to all user before the Event gets start
    global void execute(SchedulableContext sc) {
        BatchEventRemainder remainder = new BatchEventRemainder();
        database.executeBatch(remainder, 5);
    }
}