global class CustomerNotLoggedInScheduler implements Schedulable{     
    //Method to notify all the users who have not logged in for long time.
    global void execute(SchedulableContext sc) {
        
        If(Test.isRunningTest()){
            ID jobID = Database.executeBatch(new BatchCustomerNotLoggedIn(), 200);
            System.abortJob(jobID);
        } else {
            // Instantiate PolicyExpires with parameters
            BatchCustomerNotLoggedIn batch = new BatchCustomerNotLoggedIn();
            database.executeBatch(batch, 5);            
        }        
    } 
}