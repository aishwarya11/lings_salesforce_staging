public class FreezeUserAccount {
       @AuraEnabled 
    public static boolean freezeAccount(Contact con, String cId){  
        Boolean flag;
        List<Contact> updCon = new List<Contact>();
        List<Contact> conList = [SELECT id, Name, Freeze__c, Email FROM Contact WHERE ID = : cId LIMIT 1];
        for(Contact c : conList){
            if(c.Freeze__c == FALSE){
                c.Freeze__c = TRUE;
                flag = c.Freeze__c;
            }else{
                c.Freeze__c = FALSE;
                flag = c.Freeze__c;
            }
            
            updCon.add(c);
        } 
        try{
            Update updCon;
        }catch(Exception uc){
            Utility.sendException(uc, 'Freeze account failed');
        }
        return flag;
    }  

}