public class UIHomeExtension {
    //Retrieving the Event Details  for a specific Contact by passing the corresponding ContactId from UI
    public  List<Event__c> readEvents(Id contactId) {
        String queryString = 'Select Id, Name, Start_Date__c, End_Date__c ' + 
            					' FROM Event__c ' +
								' WHERE Contact__c = \'' + contactId + '\'';
        return (List<Event__c>) Database.query(queryString);
    }
}