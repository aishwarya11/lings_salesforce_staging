//This batch is used to send Email to the customer if they have not logged in for the long time(3 Month) --Lings 2.0
global class BatchCustomerNotLoggedIn implements Database.Batchable<User>{
    public Integer totalMailProcessedCount = 0;
    public Integer failedMailProcessedCount = 0;
    public Integer successMailCount = 0;
         
    @TestVisible static Date lastLogin;
    @TestVisible static Integer monthsBetween;
    
    //Start batchable
    global Iterable<User> start(Database.BatchableContext BC) {
        List<User> query = new List<User>();
        System.debug('BatchCustomerNotLoggedIn START');
        if(Test.isRunningTest()){	//This if condition is placed beacuse of the more than one executive batch error--14SEPT2019.
            query = [SELECT Id, First_Login__c, IsActive, LastLoginDate, Email, 
                     ContactId FROM User where ContactId != NULL AND IsActive = True LIMIT 10];
        }
        else{
            query = [SELECT Id, First_Login__c, IsActive, LastLoginDate, Email, 
                     ContactId FROM User where LastLoginDate >= LAST_N_MONTHS:3 
                     AND ContactId != NULL AND IsActive = True AND LastLoginDate != NULL];
        }
        return query;
    }    
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<User> scope) {
        System.debug('BatchCustomerNotLoggedIn EXECUTE');

        for(User usr : scope) {
            //LM-662--Sent email to the customer at the three month difference--13SEPT2019
            if(!Test.isRunningTest()) {
                lastLogin = usr.LastLoginDate.date();
                monthsBetween = lastLogin.monthsBetween(System.today());
            }
            if((usr.Email).Startswith('memberuser-')){	//LM-662--To restrict Bounce email Exception for the masked User--10SEPT2019
                continue;
            }else{           
                //LM-662--Added if Condition to check month difference--13SEPT2019
                if(monthsBetween > 0 && Math.mod(monthsBetween,3) == 0){
                    String contactId = usr.contactId;
                    EmailTemplate template = [Select id from EmailTemplate where name=:'Customer not logging for long time' limit 1][0];
                    List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setUseSignature(false);
                    mail.setToAddresses(new String[] { usr.Email });             
                    mail.setTemplateId(template.Id);
                    mail.setWhatId(usr.Id);
                    OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
                    mail.setTargetObjectId(contactId);
                    mail.setSaveAsActivity(false);
                    
                    mailList.add(mail);  
                    successMailCount++;
                    
                    try {
                        totalMailProcessedCount += mailList.size();
                        // Send the emails
                        if(!Test.isRunningTest()) {
                            Messaging.sendEmail(mailList); 
                        }
                    } catch(Exception er) {
                        failedMailProcessedCount++;
                        // Send exception email to Admin
                        Utility.sendException(er, 'CustomerNotLoggedIn Batch MAIL NOT SEND');
                    }
                }
            }
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        If(Test.isRunningTest()){
            ID jobID = Database.executeBatch(new BatchCustomerNotLoggedIn(), 200);
            System.abortJob(jobID);
        }        
        System.debug('BatchCustomerNotLoggedIn FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 23MAY19 - Code Optimization
        mail.setSubject('BatchCustomerNotLoggedIn ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);   
        
        String body = 'BatchCustomerNotLoggedIn Status on Sending email to the user<br/>';
        
        // Processing Email list 
        body += '<br/> ' + totalMailProcessedCount + ' number of Email processing were completed.';  
        
        // Email Processing for Customer Not Logged in for a long time.
        if(successMailCount > 0) {
            body += '<br/> successful Emails were processed for the BatchCustomerNotLoggedIn = '+successMailCount+'<br/>';
        } else {
            body += '<br/> No Email processing were happened for the BatchCustomerNotLoggedIn criteria.';
        }       
        // Failed Email Processing
        if(failedMailProcessedCount > 0) {
            body += '<br/> Failed Email processing count = '+ failedMailProcessedCount + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing were happened.';
        }
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);      
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) { 
             if (failedMailProcessedCount > 0) {
                Messaging.sendEmail(mails);
             }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchCustomerNotLoggedIn';
        ab.Track_message__c = body;
        database.insertImmediate(ab);          
        }
    }    
}