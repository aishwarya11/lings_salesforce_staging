//LM-362--This Batch will run on the daily basis--04APR2019 
global class BatchUpdateObjectPrice implements Database.Batchable<Object__c>, Database.Stateful {
    public List<Policy__c> successPolicyCloseList = new List<Policy__c>();
    public List<Policy__c> failedPolicyCloseList = new List<Policy__c>();
    public List<Policy__c> successPolicyCreateList = new List<Policy__c>();    
    public List<Policy__c> failedPolicyCreateList = new List<Policy__c>();
    public List<Object__c> successObjectUpdateList = new List<Object__c>();
    public List<Object__c> failedObjectUpdateList = new List<Object__c>();
	public Date getDate;	//LM-651--To set the Static date--06SEPT2019.
    
    public BatchUpdateObjectPrice() {	//LM-651--To set the Static date--06SEPT2019.
        this.getDate = system.today();
    }
    
    //Start Batchable
    global Iterable<Object__c> start(Database.BatchableContext BC) {
        System.debug('BatchUpdateObjectPrice START');
        List<Object__c> query =[Select Id,Insured_value__c, Premium_OnDemand__c, Premium_flatrate__c, Customer__c,
                                Flatrate_savings__c,Product__c, Product__r.Insured_value__c, Is_Insured__c,
                                Product__r.Ondemand_premium__c, Product__r.flatrate_premium__c, 
                                Product__r.flatrate_savings__c, Flatrate_renewal_flag__c, Coverage_Type__c,Policy__c,
                                Policy__r.End_date__c,Insurance_Start_Date__c,Insurance_End_Date__c, Policy__r.Invoice_Start_date__c from Object__c 
                                where (Coverage_Type__c = 'OnDemand' OR Policy__c = NULL) //LM-771 -- Added the Objects without policy in the query -- 07NOV2019
                                AND Is_Deleted__c = false AND Price_Diff__c = true ORDER BY Coverage_Type__c];
        return query;
    }

    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Object__c> scope) {
        System.debug('BatchUpdateObjectPrice EXECUTE');

        Map<Id, Object__c> updatedObjectMap = new Map<Id, Object__c>();
        List<Policy__c> closePolicyList = new List<Policy__c>();
        List<Policy__c> createPolicyList = new List<Policy__c>();
        Integer diffDays = 0;
        Integer chargedDays = 0; 
        DateTime ed;
        Decimal premiumValue = 0.0;

        for(Object__c obj : scope) {
            //LingsObject Should not be insured in the Flatrate type
            if(obj.Coverage_Type__c == Constants.ONDEMAND_STRING && obj.Is_Insured__c == TRUE) {
                //LM-362--Closing the already existing Policy for the On-Demand and creating the new Policy- 06APR2019
                Policy__c pol = new Policy__c(id = obj.Policy__c);              
                pol.Active__c = false;
                if(pol.End_Date__c == null){
                    pol.End_Date__c = Datetime.newInstance(getDate, Utility.policyEndTime);    
                }
                //18JULY2019--Calculate the Policy Charges.
				diffDays =  obj.Policy__r.Invoice_Start_date__c.daysBetween(pol.End_Date__c.Date());
			    chargedDays = diffDays + 1;         
                //LM-695--Rounded the policy charges--25SEPT2019
                pol.Policy_Charges__c = Utility.getRoundedPremium(Obj.Premium_OnDemand__c * chargedDays);                  
                pol.Status__c = Constants.POL_STATUS_EXPIRED;
                closePolicyList.add(pol);
                
                //Create a new Policy
                Policy__c newPol = new Policy__c();
                newPol.Object__c = obj.Id;
                newPol.Contact__c = obj.Customer__c;
                newPol.Active__c = True;
                newPol.Insurance_Type__c = Constants.ONDEMAND_STRING;
                newPol.Status__c = Constants.POL_STATUS_INSURED;
                //LM-687--When premium is zero update with the minimum value--20SEPT2019
                if(Obj.Product__r.Ondemand_premium__c >= 0.00 && Obj.Product__r.Ondemand_premium__c < 0.05) {
                    premiumValue = 0.05;
                }else {
                    premiumValue = Obj.Product__r.Ondemand_premium__c;
                }                
                newPol.Premium__c = premiumValue;
                newPol.Insured_Value__c = Obj.Product__r.Insured_value__c;  
                // 11JUL19 - MR - Modified to set the correct start date for the new policy.  - START  
                newPol.Invoice_Start_Date__c = getDate + 1;              
                newPol.Start_Date__c = Datetime.newInstance(newPol.Invoice_Start_Date__c, Utility.policyStartTime);
                // 11JUL19 - MR - Modified to set the correct start date for the new policy.  - END
                //when end date is not null update the end date - This will apply for the event LM-362 09APR2019
                if(obj.Insurance_End_Date__c != null){
                    newPol.End_Date__c = Datetime.newInstance(obj.Insurance_End_Date__c, Utility.policyEndTime);  
                    newPol.Policy_Source__c = 'Event';
                    ed = newPol.End_Date__c;
                } else{
                    newPol.Policy_Source__c = 'Object';
                    ed = newPol.Start_Date__c.date().addMonths(1).toStartofMonth().addDays(-1); 
                }
                
                //18JULY2019--Calculate the Policy Charges.
				diffDays =  newPol.Invoice_Start_Date__c.daysBetween(ed.Date());
			    chargedDays = diffDays + 1;         
                //LM-695--Rounded the policy charges--25SEPT2019
                newPol.Policy_Charges__c = Utility.getRoundedPremium(newPol.Premium__c * chargedDays);                 

                createPolicyList.add(newPol);                    
            }

            obj.Insured_value__c = obj.Product__r.Insured_value__c;
            obj.Premium_OnDemand__c = obj.Product__r.Ondemand_premium__c;
            obj.Premium_flatrate__c = obj.Product__r.flatrate_premium__c;
            obj.Flatrate_savings__c = obj.Product__r.flatrate_savings__c;

            updatedObjectMap.put(obj.Id, obj);
        }
        
        if(closePolicyList.size() > 0){
            List<Database.SaveResult> srUpdate = Database.update(closePolicyList);
            Integer polIndex = 0;  
            for(Database.SaveResult sr: srUpdate) {            
                Policy__c pol = closePolicyList[polIndex];
                if(sr.isSuccess()) {
                    successPolicyCloseList.add(pol);  
                } else {
                    failedPolicyCloseList.add(pol);
                }
                
                polIndex++;
            }
        }
        
        if(createPolicyList.size() > 0){
            List<Database.SaveResult> srInsert = Database.insert(createPolicyList);
            Integer polIndex = 0;  
            for(Database.SaveResult sr: srInsert) {
                Policy__c pol = createPolicyList[polIndex];
                if(sr.isSuccess()) {
                    // Update Object with the new Policy - 12APR2019
                    Object__c updobj = updatedObjectMap.get(pol.Object__c);
                    updobj.Policy__c = sr.getId();   
                    updObj.Is_Insured__c = true;    // MR - 03SEP19 - LM-639 Fix

                    updatedObjectMap.put(updObj.Id, updobj);
                    successPolicyCreateList.add(pol);                    
                } else {
                    failedPolicyCreateList.add(pol);
                }
                
                polIndex++;
            }
        }
        
        if(updatedObjectMap.size() > 0){
            List<Object__c> objList = updatedObjectMap.values();
            List<Database.SaveResult> srUpdate = Database.update(objList);
            Integer objIndex = 0;  
            for(Database.SaveResult sr: srUpdate) {
                Object__c obj = objList[objIndex];
                if(sr.isSuccess()) {
                    successObjectUpdateList.add(obj);  
                } else {
                    failedObjectUpdateList.add(obj);
                }
                
                objIndex++;
            }
        }        
    }
    
    // Finish Batchable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchUpdateObjectPrice FINISH');

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 22MAY19 - Code Optimization
        mail.setSubject('BatchUpdateObjectPrice ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         
        
        String body = 'BatchUpdateObjectPrice Status on Closing/Creating Policies and Updating Object <br/>';
        body += '<br/> ' + (successPolicyCloseList.size() + failedPolicyCloseList.size()) + ' number of Policy Closure records processing were completed.'; 
        if(successPolicyCloseList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy Closure details <br/>';
            
            for(Policy__c successList: successPolicyCloseList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy Closure happened.';
        }
        
        if(failedPolicyCloseList.size() > 0) {
            body += '<br/> Refer the below list of Failed Policy Closure details <br/>';
            
            for(Policy__c failedList: failedPolicyCloseList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed Policy Closure processing happened.';
        }
        
        // New Policy Creation List   
        body += '<br/> ' + (successPolicyCreateList.size() + failedPolicyCreateList.size()) + ' number of new Policy creation processing were completed.'; 
        
        if(successPolicyCreateList.size() > 0) {
            body += '<br/> Refer the below list of Success Policy Creation details <br/>';
            
            for(Policy__c successList: successPolicyCreateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy Creation happened.';
        }
        
        if(failedPolicyCreateList.size() > 0){
            body += '<br/> Refer the below list of Failed Policy Creation details <br/>';
            
            for(Policy__c failedList: failedPolicyCreateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Policy Creation happened.';
        }
        
        // Object Updation List
        body += '<br/> ' + (successObjectUpdateList.size() + failedObjectUpdateList.size()) + ' number of Object update records processing were completed.'; 
        if(successObjectUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Object Updation details <br/>';
            
            for(Object__c successList: successObjectUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Objects update processing happened.';
        }
        
        if(failedObjectUpdateList.size() > 0){
            body += '<br/> Refer the below list of Failed Object Updation details <br/>';
            
            for(Object__c failedList: failedObjectUpdateList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Objects update processing happened.';
        }
        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);        
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedObjectUpdateList.size() > 0 || failedPolicyCreateList.size() > 0 || failedPolicyCloseList.size() > 0) {
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchUpdateObjectPrice';
        ab.Track_message__c = body;
        database.insertImmediate(ab);  
        }
    }            
}