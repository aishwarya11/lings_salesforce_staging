@isTest
public class ObjectUtilityTest {
    
    testmethod public static void TestObjectUtility() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(firstname ='firstname',lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con;                            
        
        voucher__c v = new voucher__c();
        v.Name = 'Test Voucher';
        v.Amount__c = 5.00;
        v.From__c = System.today();
        v.To__c = System.today()+2;
        v.Fixed__c = System.today()+2;
        v.Usage_Limit__c = 2;
        insert v;
        
        Voucher_Contact__c vc = new Voucher_Contact__c();
        vc.Contact__c = Con.Id;
        vc.Voucher__c = v.Id;
        insert vc;
 
        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;        
		
		Set<Id> objId = new Set<Id>();   
        Map<Id, Boolean> objMap = new Map<Id, Boolean>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='OnDemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        objId.add(obj.Id);        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.Premium__c = 10.00;   
        pol.Start_Date__c = System.today();
        pol.End_Date__c= System.today() + 1;
        pol.Invoice_Start_Date__c = System.today();
        pol.Active__c=true;
        pol.Insurance_Type__c='OnDemand';
        pol.Premium__c=100;
        insert pol;        
        objMap.put(con.Id, pol.Active__c);
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.id;
        sack.Name = 'Bag Test';
        sack.Bag_IsDelete__c = False;
        sack.Insurance_Flag__c = True;
        insert sack;
        
        Rucksack_Object__c sackObject = new Rucksack_Object__c();
        sackObject.Contact__c = Con.Id;
        sackObject.Is_Object_Insured__c = True;
        sackObject.Lings_Object__c = obj.Id;
        sackObject.Rucksack__c = sack.Id;
        insert sackObject;
        
        ObjectUtility.getCustomerObjectDetails(con.Id);
        ObjectUtility.getCustomerObjects(con.Id);
        ObjectUtility.getObjectDetails(objId);
        ObjectUtility.getObjects(objId);
        ObjectUtility.updateRucksackObjects(objMap);
        ObjectUtility.reOrder(con.Id , 1 , 3 , 'Laptop');
        ObjectUtility.reOrder(con.Id , 5 , 1 , 'Laptop');
        ObjectUtility.reOrder(con.Id , 5 , 1 , 'bags');
        ObjectUtility.reOrder(con.Id , 1 , 3 , 'bags');
    }
    
    testmethod public static void TestObjectUtility2() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(firstname ='firstname',lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con;                            
        
        voucher__c v = new voucher__c();
        v.Name = 'Test Voucher';
        v.Amount__c = 5.00;
        v.From__c = System.today();
        v.To__c = System.today()+2;
        v.Fixed__c = System.today()+2;
        v.Usage_Limit__c = 2;
        insert v;
        
        Voucher_Contact__c vc = new Voucher_Contact__c();
        vc.Contact__c = Con.Id;
        vc.Voucher__c = v.Id;
        insert vc;
 
        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;        
		
		Set<Id> objId = new Set<Id>();   
        Map<Id, Boolean> objMap = new Map<Id, Boolean>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='OnDemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 4;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        objId.add(obj.Id);        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.Premium__c = 10.00;   
        pol.Start_Date__c = System.today();
        pol.End_Date__c= System.today() + 1;
        pol.Invoice_Start_Date__c = System.today();
        pol.Active__c=true;
        pol.Insurance_Type__c='OnDemand';
        pol.Premium__c=100;
        insert pol;        
        objMap.put(obj.Id, pol.Active__c);
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.id;
        sack.Name = 'Bag Test';
        sack.Bag_IsDelete__c = False;
        sack.Insurance_Flag__c = True;
        sack.Sort_Order__c = 4;
        insert sack;
        
        Rucksack_Object__c sackObject = new Rucksack_Object__c();
        sackObject.Contact__c = Con.Id;
        sackObject.Is_Object_Insured__c = True;
        sackObject.Lings_Object__c = obj.Id;
        sackObject.Rucksack__c = sack.Id;
        sack.Sort_Order__c = 3;
        insert sackObject;
        
        ObjectUtility.getCustomerObjectDetails(con.Id);
        ObjectUtility.getCustomerObjects(con.Id);
        ObjectUtility.getObjectDetails(objId);
        ObjectUtility.getObjects(objId);
        ObjectUtility.updateRucksackObjects(objMap);
        //ObjectUtility.reOrder(con.Id , 1 , 3 , 'Laptop');
        //ObjectUtility.reOrder(con.Id , 5 , 1 , 'Laptop');
        ObjectUtility.reOrder(con.Id , 5 , 1 , 'bags');
        ObjectUtility.reOrder(con.Id , 4 , 1 , 'bags');
        ObjectUtility.reOrder(con.Id , 3 , 5 , 'bags');
        //ObjectUtility.reOrder(con.Id , 1 , 3 , 'bags');
    }
    
    testmethod public static void TestObjectUtility3() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                          UserName='abc@mcode.com',CommunityNickname='Lings48', First_Login__c = false);
        insert u;
        
        Contact con = new Contact(firstname ='firstname',lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0,OwnerId = u.Id);
        insert con;                            
        
        voucher__c v = new voucher__c();
        v.Name = 'Test Voucher';
        v.Amount__c = 5.00;
        v.From__c = System.today();
        v.To__c = System.today()+2;
        v.Fixed__c = System.today()+2;
        v.Usage_Limit__c = 2;
        insert v;
        
        Voucher_Contact__c vc = new Voucher_Contact__c();
        vc.Contact__c = Con.Id;
        vc.Voucher__c = v.Id;
        insert vc;
 
        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;        
		
		Set<Id> objId = new Set<Id>();   
        Map<Id, Boolean> objMap = new Map<Id, Boolean>();
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='OnDemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        objId.add(con.Id);        
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.Premium__c = 10.00;   
        pol.Start_Date__c = System.today();
        pol.End_Date__c= System.today() + 1;
        pol.Invoice_Start_Date__c = System.today();
        pol.Active__c=true;
        pol.Insurance_Type__c='OnDemand';
        pol.Premium__c=100;
        insert pol;        
        objMap.put(obj.Id, pol.Active__c);
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.id;
        sack.Name = 'Bag Test';
        sack.Bag_IsDelete__c = False;
        sack.Insurance_Flag__c = True;
        insert sack;
        
        Rucksack_Object__c sackObject = new Rucksack_Object__c();
        sackObject.Contact__c = Con.Id;
        sackObject.Is_Object_Insured__c = True;
        sackObject.Lings_Object__c = obj.Id;
        sackObject.Rucksack__c = sack.Id;
        insert sackObject;
        
        ObjectUtility.getCustomerObjectDetails(con.Id);
        ObjectUtility.getCustomerObjects(con.Id);
        ObjectUtility.getObjectDetails(objId);
        ObjectUtility.getObjects(objId);
        ObjectUtility.updateRucksackObjects(objMap);
        ObjectUtility.reOrder(con.Id , 1 , 3 , 'Laptop');
        ObjectUtility.reOrder(con.Id , 5 , 1 , 'Laptop');
        ObjectUtility.reOrder(obj.Id , 5 , 1 , 'bags');
        ObjectUtility.reOrder(con.Id , 1 , 3 , 'bags');
    }
}