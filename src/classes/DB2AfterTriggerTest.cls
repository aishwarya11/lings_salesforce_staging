@isTest
public class DB2AfterTriggerTest {
    @isTest static void TestDB2AfterTriggerTest() {
       //try{
        
        Premium__c pre = new Premium__c();
        pre.Name='camera incl. accessories';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        pre.ImageUrl__c = 'Testimg.com';
        insert pre;    
        
        DB_Fotichaeschtli__c DB2 = new DB_Fotichaeschtli__c();
        DB2.EAN__c = 'sample EAN';
        DB2.Name = 'sample name';
        DB2.Image_URL__c = 'https://sample.url';
        DB2.Price__c = 42;
        DB2.Title__c = 'adddwev21542545';
        DB2.ID__c = '45781141';
        DB2.Manufacturer_Parts_Number__c = '54500011';
        DB2.Brand__c = 'Canon';
        DB2.Description__c = 'sample description';
        DB2.Product_Type__c = 'Video > Schulterstativ';
        DB2.Condition__c = 'Neu';
        DB2.Stock__c = 'Auf Lager';
        DB2.Shipping__c = 'Test shipping';
        DB2.Link__c = 'Test@salesforce.com';
        insert DB2;
        
        DB_Fotichaeschtli__c DB = new DB_Fotichaeschtli__c();
        DB.Id = DB2.Id;
        DB.Name = 'sample';
        DB.Title__c = 'sample Title';
        DB2.Price__c = 50;
        update DB;
        
        //Utility.convertToProductDB2(DB2, new Product2());

       //}catch(Exception e){}
     }
}