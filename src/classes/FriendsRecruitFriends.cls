//LINGS 2.0 -- Friend recruit Friend--26AUG2019.
public class FriendsRecruitFriends {
    @AuraEnabled 
    public static void recruiter(Contact con, String cId){  
        //RecruitFriend
        List<Contact> updCon = new List<Contact>();        
        Map<Id, Contact> contMap = new Map<Id, Contact>();
        List<Voucher_Contact__c> vcList = new List<Voucher_Contact__c>();
        
        List<Contact> conList = [SELECT id, Name,FRF_Value__c, Voucher_In_Use__c, VIU_Balance__c,
                                 Voucher_In_Use__r.Balance__c,VIU_End_date__c, Email, Balance_Pending__c 
                                 FROM Contact WHERE ID = : cId LIMIT 1];
        
        Decimal balance = 0.0;
        for(Contact c : conList){
            balance = 10.00; 
            if(c.FRF_Value__c == NUll) {
                c.FRF_Value__c = 0.0;
            }
            if(c.Balance_Pending__c == NULL ) {
                c.Balance_Pending__c = 0.0;
            }       
            c.Balance_Pending__c += balance;
            c.FRF_Value__c += balance;
            
            updCon.add(c);
            
            //Create a new Voucher Contact for the referrer.
            Voucher_Contact__c vc = new Voucher_Contact__c();
            vc.Voucher__c = System.Label.FriendRecruitFriendId;
            vc.Contact__c = c.Id;
            vc.Balance__c = 0.0;
            vcList.add(vc); 
            
            
            // Send Email
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
            EmailTemplate template = [Select id from EmailTemplate where name=:'Friend Recruiter' limit 1][0];   
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { c.Email });
            mail.setTemplateId(template.Id);
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
            mail.setTargetObjectId(c.Id);
            mail.setSaveAsActivity(true);
            
            mailList.add(mail);   
            
            Messaging.sendEmail(mailList); 
            
        }               

        if(vcList.size() > 0){
            try{
                insert vcList;
            }catch(Exception vc){
                Utility.sendException(vc, 'VOUCHER CONTACT CREATION FAILED IN FRF');
            } 
        }  
	        
        //Update the Contact.
        if(updCon.size() > 0){
            try{
                Update updCon;
            }catch(Exception uc){
                Utility.sendException(uc, 'CONTACT UPDATE FAILED');
            }            
        }
    }      
    
    @AuraEnabled 
    public static void newUserRecruited(Contact con, String cId){  
        //RecruitFriend
        List<Contact> updCon = new List<Contact>();        
        Map<Id, Contact> contMap = new Map<Id, Contact>();
        List<Voucher_Contact__c> vcList = new List<Voucher_Contact__c>();
        
        List<Contact> conList = [SELECT id, Name,FRF_Value__c, Voucher_In_Use__c, VIU_Balance__c,
                                 Voucher_In_Use__r.Balance__c,VIU_End_date__c, Email, Balance_Pending__c 
                                 FROM Contact WHERE ID = : cId LIMIT 1];
        
        Decimal balance = 0.0;
        for(Contact c : conList){
            balance = 5.00; 
            if(c.FRF_Value__c == NUll) {
                c.FRF_Value__c = 0.0;
            }
            if(c.Balance_Pending__c == NULL ) {
                c.Balance_Pending__c = 0.0;
            }       
            c.Balance_Pending__c += balance;
            c.FRF_Value__c += balance;
            
            updCon.add(c);
            
            //Create a new Voucher Contact for the referrer.
            Voucher_Contact__c vc = new Voucher_Contact__c();
            vc.Voucher__c = System.Label.FriendRecruitFriendId;
            vc.Contact__c = c.Id;
            vc.Balance__c = 0.0;
            vcList.add(vc); 
            
            
            // Send Email
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
            EmailTemplate template = [Select id from EmailTemplate where name=:'New User recruited' limit 1][0];   
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { c.Email });
            mail.setTemplateId(template.Id);
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
            mail.setTargetObjectId(c.Id);
            mail.setSaveAsActivity(true);
            
            mailList.add(mail);   
            
            Messaging.sendEmail(mailList); 
            
        }               

        if(vcList.size() > 0){
            try{
                insert vcList;
            }catch(Exception vc){
                Utility.sendException(vc, 'VOUCHER CONTACT CREATION FAILED IN FRF');
            } 
        }  
	        
        //Update the Contact.
        if(updCon.size() > 0){
            try{
                Update updCon;
            }catch(Exception uc){
                Utility.sendException(uc, 'CONTACT UPDATE FAILED');
            }            
        }
    }     
}