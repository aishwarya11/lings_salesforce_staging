@isTest
public class PremiumCalculationTest {
    
    testmethod public static void testPremiumCalculation(){
        
        Premium_CustomSetting__c pcs = new Premium_CustomSetting__c();
        pcs.Name = 'SampleName';
        pcs.PRICEPERDAY__c = 2.40;
        pcs.Pricetotal__c = 5.0;
        pcs.Duration_days__c = 10;
        insert pcs;
        
        PremiumCalculation.premium(''+10);
        PremiumCalculation.premium(''+0);
        PremiumCalculation.premium(''+2);
    }
    
}