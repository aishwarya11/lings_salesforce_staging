@isTest
public class BatchEventPolicyTest {
    
    testmethod public static void testBatchEventPolicyTest() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName= 'john99956486@acme.com',CommunityNickname='Lings456654');
        insert u;
        
        Contact con = new Contact();
        con.lastname ='Name123';
        con.Password__c='123pwd4598';
        con.Ownerid=u.Id;
        con.Email='revathypandian97@gmail.com';
        insert con;
        
        Attachment objAtt = new Attachment();
     	objAtt.Name = 'Test';
     	objAtt.body = Blob.valueof('string');
    	objAtt.ParentId = con.Id;
     	insert objAtt;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 0;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Success';
		insert inv1;          
        
        Object__c obj1 = new Object__c();
        obj1.Policy__c=null;
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Insurance_Type__c = 'OnDemand';
        pol.Start_Date__c = System.today();
        pol.End_Date__c = System.today()+1;
        pol.Invoice__c = inv1.id;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        pol.Status__c = 'Insured';
        insert pol;
        polList.add(pol);
        
        
        Event__c EV = new Event__c();
        EV.Name='AAA';
        EV.Start_Date__c=System.today();
        EV.Contact__c=con.id;
        EV.End_Date__c=System.today() + 1;
        insert EV;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c = obj1.id;
        EO.Event__c = EV.id;
        insert EO;
        
        Test.startTest();
        
		// Instantiate beforeEventRemainder apex batch class
            BatchEventPolicy BCE = new BatchEventPolicy(); // Add you Class Name
            DataBase.executeBatch(BCE);
           
        Test.stopTest();
    } 
    
    testmethod public static void testBatchEventPolicyTest2() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName= 'john99956486@acme.com',CommunityNickname='Lings456654');
        insert u;
        
        Contact con = new Contact();
        con.lastname ='Name123';
        con.Password__c='123pwd4598';
        con.Ownerid=u.Id;
        con.Email='revathypandian97@gmail.com';
        insert con;
        
        Attachment objAtt = new Attachment();
     	objAtt.Name = 'Test';
     	objAtt.body = Blob.valueof('string');
    	objAtt.ParentId = con.Id;
     	insert objAtt;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 0;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Success';
		insert inv1;  
        
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Insurance_Type__c = 'OnDemand';
        pol.Start_Date__c = System.today();
        pol.Invoice__c = inv1.id;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        pol.Status__c = 'Insured';
        insert pol;
        polList.add(pol);      
        
        obj1.Policy__c = pol.Id;
        update obj1;
        
        Event__c EV = new Event__c();
        EV.Name='AAA';
        EV.Start_Date__c=System.today();
        EV.Contact__c=con.id;
        EV.End_Date__c=System.today() + 1;
        insert EV;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c = obj1.id;
        EO.Event__c = EV.id;
        insert EO;
        
        Test.startTest();
        
		// Instantiate beforeEventRemainder apex batch class
            BatchEventPolicy BCE = new BatchEventPolicy(); // Add you Class Name
            DataBase.executeBatch(BCE);
           
        Test.stopTest();
    } 
	testmethod public static void testBatchEventPolicyTest3() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName= 'john99956486@acme.com',CommunityNickname='Lings456654');
        insert u;
        
        Contact con = new Contact();
        con.lastname ='Name123';
        con.Password__c='123pwd4598';
        con.Ownerid=u.Id;
        con.Email='revathypandian97@gmail.com';
        insert con;
        
        Attachment objAtt = new Attachment();
     	objAtt.Name = 'Test';
     	objAtt.body = Blob.valueof('string');
    	objAtt.ParentId = con.Id;
     	insert objAtt;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 0;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Success';
		insert inv1;          
        
        Object__c obj1 = new Object__c();
        obj1.Policy__c=null;
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        Event__c EV = new Event__c();
        EV.Name='AAA';
        EV.Start_Date__c=System.today();
        EV.Contact__c=con.id;
        EV.End_Date__c=System.today() + 1;
        insert EV;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c = obj1.id;
        EO.Event__c = EV.id;
        insert EO;
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Object__c = Obj1.Id;
        pol.Insurance_Type__c = 'OnDemand';
        pol.Start_Date__c = System.today();
        pol.End_Date__c = System.today()+1;
        pol.Invoice__c = inv1.id;
        pol.Contact__c = con.id;
        pol.Status__c = 'Insured';
        insert pol;
        polList.add(pol);        
        
        Test.startTest();
        
		// Instantiate beforeEventRemainder apex batch class
            BatchEventPolicy BCE = new BatchEventPolicy(System.today()); // Add you Class Name
            DataBase.executeBatch(BCE);
           
        Test.stopTest();
    } 
    
    testmethod public static void testBatchEventPolicyTest4() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName= 'john99956486@acme.com',CommunityNickname='Lings456654');
        insert u;
        
        Contact con = new Contact();
        con.lastname ='Name123';
        con.Password__c='123pwd4598';
        con.Ownerid=u.Id;
        con.Email='revathypandian97@gmail.com';
        insert con;
        
        Attachment objAtt = new Attachment();
     	objAtt.Name = 'Test';
     	objAtt.body = Blob.valueof('string');
    	objAtt.ParentId = con.Id;
     	insert objAtt;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 0;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Success';
		insert inv1;          
        
        Object__c obj1 = new Object__c();
        obj1.Policy__c=null;
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;
        
        Event__c EV = new Event__c();
        EV.Name='AAA';
        EV.Start_Date__c=System.today();
        EV.Contact__c=con.id;
        EV.End_Date__c=System.today() + 1;
        insert EV;
        
        Event_Object__c EO = new Event_Object__c();
        EO.Object__c = obj1.id;
        EO.Event__c = EV.id;
        insert EO;       
               
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        polList.add(pol);   
        
        Object__c obj2 = new Object__c(id= obj1.Id);
        obj2.Policy__c = pol.id;
        update obj2;
        
        Test.startTest();
        
		// Instantiate beforeEventRemainder apex batch class
            BatchEventPolicy BCE = new BatchEventPolicy(System.today()); // Add you Class Name
            DataBase.executeBatch(BCE);
           
        Test.stopTest();
    } 
}