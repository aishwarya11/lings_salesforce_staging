//Create Event_Object__c records when event get started for any objects (i.e) event start date is today
// Run this batch at 12:00 AM
Global class BatchEventPolicy implements Database.Batchable<Event_Object__c>, Database.Stateful {
    //22MAR2019 LM-319
    public Date sdValue;
    public List<Policy__c> successPolicyList = new List<Policy__c>();
    public List<Object__c> successObjectList = new List<Object__c>();
    public List<Policy__c> failedPolicyList = new List<Policy__c>();
    public List<Object__c> failedObjectList = new List<Object__c>();
    
    public BatchEventPolicy() {
        this.sdValue = system.today();
    }
    
    public BatchEventPolicy(Date inpDate){
        this.sdValue = inpDate;     
    }
    
    global Iterable<Event_Object__c> start(Database.BatchableContext BC){
        System.debug('BatchEventPolicy START');

        List<Event_Object__c> query = [SELECT id, name, Object__r.Policy__c, Event__r.Contact__c, Object__r.Product__c,
                                    Object__r.name, Object__r.Coverage_type__c, Object__r.Product__r.Insured_value__c,
                                    Object__r.Insurance_Start_date__c, Object__r.Insurance_End_date__c,
                                    Object__r.Insured_Value__c, Object__r.Premium_OnDemand__c, Object__r.Product_Source__c,
                                    Object__r.Policy__r.Premium__c, Event__r.start_date__c, 
                                    Event__r.End_date__c FROM Event_Object__c 
                                    WHERE Event__r.start_date__c = :sdValue];
       
        return query;
    }
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Event_Object__c> scope) {
        System.debug('BatchEventPolicy EXECUTE');

        List<Policy__c> policyInsertList = new List<Policy__c>();
        List<Policy__c> policyUpdateList = new List<Policy__c>();
        List<Object__c> objectListInsert = new List<Object__c>();
        List<Object__c> objectListUpdate = new List<Object__c>();
        
        // Check all the existing ondemand policies for the objects
        Set<Id> objIds = new Set<Id>();
        Map<String, List<Policy__c>> objPoliciesMap = new Map<String, List<Policy__c>>();
        for(Event_Object__c eventObject : scope) {
            objIds.add(eventObject.Object__c);
        }

        List<Policy__c> polList = new List<Policy__c>();
        String keyStr = '';
        String sDateStr = '';
        String eDateStr = '';
        Set<Id> onDemandPolicies = new Set<Id>();

        for(Policy__c pol: [Select Id, Object__c,Object__r.name,Object__r.Product__c, Start_Date__c, End_Date__c, Premium__c from Policy__c where Object__c in :objIds]) {
            sDateStr = String.valueOf(date.newinstance(pol.Start_Date__c.year(), pol.Start_Date__c.month(), pol.Start_Date__c.day()));
            eDateStr = (pol.End_Date__c <> null ? String.valueOf(date.newinstance(pol.End_Date__c.year(), pol.End_Date__c.month(), pol.End_Date__c.day())) : '');
            keyStr = pol.Object__c + sDateStr + eDateStr;
            if(objPoliciesMap.containsKey(keyStr)) {
                polList = objPoliciesMap.get(keyStr);
            } else {
                polList = new List<Policy__c>();
            }
            polList.add(pol);
            objPoliciesMap.put(keyStr, polList);
        }
        Integer chargedDays = 0;
        Integer diffDays = 0;
        
        for(Event_Object__c eventObject : scope) {
            keyStr = eventObject.Object__c + String.valueOf(eventObject.Event__r.Start_Date__c) + String.valueOf(eventObject.Event__r.End_Date__c);

            if(!objPoliciesMap.containsKey(keyStr)) {
                if(eventObject.Object__r.Policy__c == null) {
                    // Create Policy when no other policy exist with the same Start and End Date
                    Policy__c newPolicy = new Policy__c();
                    newPolicy.object__c = eventObject.object__c;
                    newPolicy.Contact__c = eventObject.Event__r.Contact__c;
                    newPolicy.Start_Date__c = Datetime.newInstance(eventObject.Event__r.start_date__c, Utility.policyStartTime);
                    newPolicy.Invoice_Start_Date__c = eventObject.Event__r.start_date__c;
                    newPolicy.End_Date__c = Datetime.newInstance(eventObject.Event__r.End_date__c, Utility.policyEndTime);
                    newPolicy.Insurance_Type__c = Constants.ONDEMAND_STRING;
                    newPolicy.Active__c = true;
                    newPolicy.Policy_Source__c = 'Event';
                    newPolicy.Status__c = 'Insured';
                    //LM-720 -- Taking the Insured value from the Object not Product --04OCT2019
                    newPolicy.Insured_Value__c = eventObject.Object__r.Insured_value__c;                    
                    newPolicy.Premium__c = eventObject.Object__r.Premium_OnDemand__c;
                    //Included Policy Charges calculation at 13AUG2019
                    diffDays =  newPolicy.Invoice_Start_Date__c.daysBetween(newPolicy.End_Date__c.Date());	//LM-588--To replace date.ValueOf to date()--12AUG2019
                    chargedDays = diffDays + 1;    
                    //LM-695--Rounded the policy charges--25SEPT2019                
                    newPolicy.Policy_Charges__c = Utility.getRoundedPremium(newPolicy.Premium__c * chargedDays);
                    policyInsertList.add(newPolicy);
                } else {
                    //Update existing Policy end date
                    Policy__c updPolicy = new Policy__c(Id=eventObject.Object__r.Policy__c);
                    updPolicy.End_Date__c = Datetime.newInstance(eventObject.Event__r.End_date__c, Utility.policyEndTime);
                    updPolicy.Premium__c = eventObject.Object__r.Premium_OnDemand__c;               
                    //Included Policy Charges calculation at 13AUG2019
                    diffDays =  eventObject.Event__r.Start_Date__c.daysBetween(updPolicy.End_Date__c.Date());	//LM-588--To replace date.ValueOf to date()--12AUG2019
                    chargedDays = diffDays + 1;  
                    //LM-695--Rounded the policy charges--25SEPT2019                  
                    updPolicy.Policy_Charges__c = Utility.getRoundedPremium(updPolicy.Premium__c * chargedDays);                    
                    updPolicy.Active__c = true;
                    updPolicy.Status__c = 'Insured';
                    updPolicy.Object__c = eventObject.Object__c;
                    updPolicy.Insurance_Type__c = Constants.ONDEMAND_STRING;
                    policyUpdateList.add(updPolicy);
                }
            } else {
                //objPoliciesMap is satisfied.
				//LM-1050--Policy should get updated when the same object is used for the different Event(after del) on the same day --04MAR2020
                if (eventObject.Object__r.Policy__c != NULL) {
                    Policy__c updPolicy = new Policy__c(Id=eventObject.Object__r.Policy__c);
                    updPolicy.Premium__c = eventObject.Object__r.Premium_OnDemand__c;
                    updPolicy.Active__c = true;
                    updPolicy.Status__c = 'Insured';
                    updPolicy.Object__c = eventObject.Object__c;
                    policyUpdateList.add(updPolicy);
                }                                 
            }
        }

        if(policyUpdateList.size() > 0){
            try {
                List<Database.SaveResult> srUpdate = Database.update(policyUpdateList);
                Integer polIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Policy__c pol = policyUpdateList[polIndex];
                    if(sr.isSuccess()) {
                        successPolicyList.add(pol);
                        // Update Object with the new Policy
                        Object__c obj = new Object__c(Id=pol.Object__c);
						obj.Is_Insured__c = TRUE;	//Update this flag when the Object is added to bag and Insured via Event. 14AUG2019
                        if(pol.end_date__c <> null) {
                            obj.Insurance_End_date__c = pol.End_date__c.date();   //LM-588--To replace date.ValueOf to date()--12AUG2019
                        }
                        obj.Policy__c = sr.getId();
                        
                        objectListUpdate.add(obj);  
                    } else {
                        failedPolicyList.add(pol);
                    }
    
                    polIndex++;
                }
            } catch(Exception upe) {
                // Send exception email to Admin
                Utility.sendException(upe, 'ERROR IN UPDATING POLICY via BatchEventPolicy');            
            }
        }
        
        // After creating the Policy Ensure the related object is updated with the current policy
        if(policyInsertList.size() > 0) {
            try {
                List<Database.SaveResult> srList = Database.insert(policyInsertList);
                Integer polIndex = 0;
                for(Database.SaveResult sr: srList) {
                    Policy__c pol = policyInsertList[polIndex];
                    if(sr.isSuccess()) {
                        successPolicyList.add(pol);
                        // Update Object with the new Policy
                        Object__c obj = new Object__c(Id=pol.Object__c);
                        obj.Insurance_Start_date__c = pol.start_date__c.date();
                        if(pol.end_date__c <> null) {
                            obj.Insurance_End_date__c = pol.End_date__c.date();   
                        }
                        obj.Is_Insured__c = TRUE;//Update this flag when the Object is added to bag and Insured via Event. 14AUG2019
                        obj.Coverage_type__c = pol.Insurance_Type__c;
                        obj.Policy__c = sr.getId();
                        
                        objectListUpdate.add(obj);  
                        System.debug('object List-->'+obj);
                    } else {
                        failedPolicyList.add(pol);
                    }
    
                    polIndex++;
                }
            } catch(Exception upe) {
                // Send exception email to Admin
                Utility.sendException(upe, 'ERROR IN CREATING POLICY via BatchEventPolicy');            
            }
        }
        
        if(objectListUpdate.size() > 0) {
            try {
                List<Database.SaveResult> srList = Database.update(objectListUpdate);
                Integer objIndex = 0;
                for(Database.SaveResult sr: srList) {
                    Object__c obj = objectListUpdate[objIndex];
                    if(!sr.isSuccess()) {
                        failedObjectList.add(obj);
                    } else {
                        successObjectList.add(obj);
                    }
    
                    objIndex++;
                }
            } catch (Exception uoe) {
                Utility.sendException(uoe, 'ERROR IN Updating Objects via BatchEventPolicy'+uoe.getStackTraceString());   
            }
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC){
        System.debug('BatchEventPolicy FINISH');

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 23MAY19 - Code Optimization
        mail.setSubject('BatchEventPolicy[' + sdValue + '] Status ran on ' + System.now()); 
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);          

        String body = 'BatchEventPolicy Status on Creating/Updating Policies and Inserting Objects <br/>';
        //Inserting, updating policies
        if(successPolicyList.size() > 0) {
            body += '<br/> ' + successPolicyList.size() + ' number of records processing were completed.'; 
            body += '<br/> Refer the below list of Success Policy details <br/>';
            
            for(Policy__c successList: successPolicyList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Policy processing happened.';
        }

        if(successObjectList.size() > 0) { 
            body += '<br/> ' + successObjectList.size() + ' number of records processing were completed.'; 
            body += '<br/> Refer the below list of Success Object details <br/>';
            
            for(Object__c successList: successObjectList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Objects processing happened.';
        }
        if(failedPolicyList.size() > 0) {
            body += '<br/> ' + failedPolicyList.size() + ' number of records processing were completed.'; 
            body += '<br/> Refer the below list of Failed Policy details <br/>';
            
            for(Policy__c failedList: failedPolicyList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed Policy processing happened.';
        }
        if(failedObjectList.size() > 0){
            body += '<br/> ' + failedObjectList.size() + ' number of records processing were completed.'; 
            body += '<br/> Refer the below list of Failed Object details <br/>';
            
            for(Object__c failedList: failedObjectList) {
                body += '<br/> ' + failedList;
            }    
        } else {
            body += '<br/> No Failed Objects processing happened.';
        }

        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);       
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()){ 
            if (failedObjectList.size() > 0 || failedPolicyList.size() > 0){
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchEventPolicy';
        ab.Track_message__c = body;
        database.insertImmediate(ab);         
        }
    }
}