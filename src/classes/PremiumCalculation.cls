public without sharing class PremiumCalculation {
    public static Integer noOfDays = 0;
    
    public class premiumValues {
        Decimal perDay = 0.00;
        Decimal totalPremium = 0.00;
    }
    @remoteAction
    public static String premium(String days){
        System.debug('Days = '+days);
        noOfDays = Integer.valueOf(days);      
        Premium_CustomSetting__c  premiumCalc = new Premium_CustomSetting__c();
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        if(noOfDays != null && noOfDays >= 1){
            jsonGen.writeNumberField('NoOfDays',noOfDays);
            premiumValues pv = new premiumValues();
            try{
                
                String queryString = 'select id,Duration_days__c,Priceperday__c,Pricetotal__c from Premium_CustomSetting__c where Duration_days__c =' + noOfDays;
        		premiumCalc = Database.query(queryString);
                /*premiumCalc = [select id , Name,Duration_in_days__c,priceperday__c,Price_Total__c from PremiumCalculation__c 
                               where Duration_in_days__c=1];*/
        		System.debug('premiumvalue = '+premiumCalc.Priceperday__c);
                
                jsonGen.writeObjectField('premiumCalc',premiumCalc);
                
                /*Integer size = premiumCalc.size();
                Decimal totalPrice = 0;
                Decimal  perDayPrice = 0;
                system.debug('size' + size);
                for(Integer i=1; i <= size ; i++ ){
                    if ( i==noOfDays ){
                        perDayPrice = premiumCalc[i-1].Price_Total__c;
                        //perDayPrice = premiumCalc[i-1].price_per_day_OKK__c;
                        //totalPrice = premiumCalc[i-1].Duration_in_days__c ; // * (premiumCalc[i-1].price_per_day_OKK__c);
                    }
                }*/
                pv.perDay = premiumCalc.Priceperday__c.setScale(2);
                pv.totalPremium = premiumCalc.Pricetotal__c.setScale(2) ;
                system.debug('PerDay =>' + pv.perDay);
                system.debug('TotalPre =>' + pv.totalPremium);
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeObjectField('perDay', pv.perDay);
                jsonGen.writeObjectField('totalPremium', pv.totalPremium);
                jsonGen.writeObjectField('OKKObjPremium', pv);
                jsonGen.writeEndObject();
            }
            catch(Exception e){
                jsonGen.writeNumberField('NoOfDays',noOfDays);
                Utility.sendException(e, 'Query failed');
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', e.getStackTraceString());
                jsonGen.writeStringField('ErrorMessage', e.getMessage());
            }
        }else{
            jsonGen.writeNumberField('NoOfDays',noOfDays);
            jsonGen.writeStringField('status', 'failure');
        }
        
        return jsonGen.getAsString();
    }
}