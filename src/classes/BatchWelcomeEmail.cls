// Run at the end of the day 11:45 PM
global class BatchWelcomeEmail implements Database.Batchable<Contact>, Database.Stateful {   
    List<Contact> successContactUpdateList = new List<Contact>();    
    List<Contact> failedContactUpdateList = new List<Contact>();
    
    //Start batchable
    global Iterable<Contact> start(Database.BatchableContext BC) {
        System.debug('BatchWelcomeEmail START');
        
        List<Contact> query = [SELECT Id, Name, SignUp__c, Register_confirmed__c, Email_verified__c, CreatedDate,
                               Email
                               FROM Contact 
                               WHERE SignUp__c = TRUE 
                               AND Email_verified__c = TRUE 
                               AND Register_confirmed__c = FALSE];
        return query;
    }
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        System.debug('BatchWelcomeEmail EXECUTE');
        
        List<Contact> contactList = new List<Contact>();
        for(Contact con : scope) {
            //Set Register confirmed as true, this Sends the welcome email via proces builder
            con.Register_confirmed__c = True;
            //Send Welcome email to the Customer From Process Builder to apex -- 26AUG2019.
    
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
            EmailTemplate template = [Select id from EmailTemplate where name=:'Register-confirmed' LIMIT 1];   
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { con.Email });
            mail.setTemplateId(template.Id);
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
            mail.setTargetObjectId(con.Id);
            mail.setSaveAsActivity(true);
            
            mailList.add(mail);   
            try{
            Messaging.sendEmail(mailList);     
            }catch(Exception cl){
                utility.sendException(cl, 'WELCOME MAIL FAILED TO SEND'+con.Id);
            }             
            contactList.add(con);
        }
        
        try {
            if(contactList.size() > 0){
                List<Database.SaveResult> srUpdate = Database.update(contactList);
                Integer conIndex = 0;  
                for(Database.SaveResult sr: srUpdate) {
                    Contact con = contactList[conIndex];
                    if(sr.isSuccess()) {
                        successContactUpdateList.add(con);  
                    } else {
                        failedContactUpdateList.add(con);
                    }
                    
                    conIndex++;
                }
            }
        } catch(Exception We) {
            // Send exception email to Admin
            Utility.sendException(We, 'FAILED TO Update Contacts with ');               
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchWelcomeEmail FINISH');
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 22MAY19 - For better control
        mail.setSubject('BatchWelcomeEmail ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         
        
        String body = 'BatchWelcomeEmail Status on Updating Contact records<br/>';
        //Processing Contact List
        body += '<br/> ' + (successContactUpdateList.size() + failedContactUpdateList.size()) + ' number of Contact records Update processing were completed.'; 
        if(successContactUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success Contact Updation details <br/>';
            
            for(Contact successList: successContactUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No Contact updation happened.';
        }
        if(failedContactUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Failed Contact updation details <br/>';
            
            for(Contact failedList: failedContactUpdateList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed Contact Updation processing happened.';
        }
        
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);      
        
        //LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if(failedContactUpdateList.size() > 0) {
                Messaging.sendEmail(mails);
            }                
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchWelcomeEmail';
        ab.Track_message__c = body;
        database.insertImmediate(ab);     
        }
    }  
}