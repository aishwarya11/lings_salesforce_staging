@isTest
public class RVGControllerTest {
    
    testmethod public static void TestRVGController() {
        Contact con = new Contact(firstname='firstname1', lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con.email='test1@test.comtest999';
        con.Token__c ='samplevalue';
        con.Payment_Info__c = '"brand":{"PaymentMethod":"VISA","Name":"';
        con.Birthdate = System.today()-30;
        insert con;
        
        Object__c obj = new Object__c();
        obj.Product__c = System.label.RVGProductId;
        obj.Customer__c = con.Id;
        obj.Insured_Value__c = 100;
        insert obj;
        
        Event__c eve = new Event__c();
        eve.Name = 'sampleEvent';
        eve.Start_Date__c = System.today()+2;
        eve.End_Date__c = System.today()+2;
        eve.Contact__c = con.Id;
        insert eve;
        
        Event_Object__c eo = new Event_Object__c();
        eo.Object__c = obj.Id;
        eo.Event__c = eve.Id;
        insert eo;
        
        
        List<String> strList = new List<String>();
        List<Event__c> eveList = new List<Event__c>();
        strList.add('dummyValue');
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        RVGController opr = new RVGController(sc);
        opr.eveList = eveList;
        opr.date1 = 'SampleDate';
        opr.conFirstName = 'SampleFirstName';
        opr.conLastName = 'SampleLastName';
        opr.conHouseNumber = 'sampleHouseNumber';
        opr.conCity = 'sampleCity';
        opr.conPostalCode = '1000';
        opr.conStreet = 'sampleStreet';
        opr.insuranceStartDate = ''+System.today();
        opr.insuranceEndDate = ''+System.today();
        opr.numberOfDaysInsured = ''+1;
        ApexPages.StandardController sc1 = new ApexPages.StandardController(con);
        RVGController opr1 = new RVGController(sc1);    
        
    }
    
    testmethod public static void TestRVGController1() {
        Contact con = new Contact(firstname='firstname1', lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con.email='test1@test.comtest999';
        con.Token__c ='samplevalue';
        con.Payment_Info__c = '"brand":{"PaymentMethod":"VISA"sddfsdssdsdl,"Name":"';
        con.Birthdate = System.today()-30;
        insert con;
        
        Object__c obj = new Object__c();
        obj.Product__c = System.label.RVGProductId;
        obj.Customer__c = con.Id;
        obj.Insured_Value__c = 100;
        obj.Insurance_Start_Date__c = System.today()+2;
        obj.Insurance_End_Date__c = System.today()+4;
        
        insert obj;
        
        Event__c eve = new Event__c();
        eve.Name = 'sampleEvent';
        eve.Start_Date__c = System.today()+2;
        eve.End_Date__c = System.today()+4;
        eve.Contact__c = con.Id;
        insert eve;
        
        Event_Object__c eo = new Event_Object__c();
        eo.Object__c = obj.Id;
        eo.Event__c = eve.Id;
        insert eo;
        
        
        List<String> strList = new List<String>();
        List<Event__c> eveList = new List<Event__c>();
        strList.add('dummyValue');
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        RVGController opr = new RVGController(sc);
        opr.eveList = eveList;
        opr.date1 = 'SampleDate';
        opr.conFirstName = 'SampleFirstName';
        opr.conLastName = 'SampleLastName';
        opr.conHouseNumber = 'sampleHouseNumber';
        opr.conCity = 'sampleCity';
        opr.conPostalCode = '1000';
        opr.conStreet = 'sampleStreet';
        opr.insuranceStartDate = ''+System.today();
        opr.insuranceEndDate = ''+System.today();
        opr.numberOfDaysInsured = ''+1;
        ApexPages.StandardController sc1 = new ApexPages.StandardController(con);
        RVGController opr1 = new RVGController(sc1);    
        
    }
    
}