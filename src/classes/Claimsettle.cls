public class ClaimSettle {
    //Null's the Claim field in Lings_Object__c
    @AuraEnabled 
    public static void updateObject(Object__c obj,String oId){
        String ClaimId = '';
        //03APR2019- To update the Claim status field.
        List <Claim__c> updateClaimList = New List<Claim__c>();
        for(Object__c ObjList : [Select id, Claim__c, Customer__c, Customer__r.Email from Object__c
                                 Where id = : oId LIMIT 1]){
                                     Claim__c clm = new Claim__c(id= ObjList.Claim__c);
                                     clm.Status__c = 'Closed';
                                     clm.Payment_Date__c = System.today(); //30JAN2019--Added payment date --LM-1073
                                     String Email = ObjList.Customer__r.Email;
                                     String contactId = ObjList.Customer__c;
                                     ClaimId = clm.Id;
                                     // Claimsettle.sendEmail(Email, contactId, ClaimId);
                                     updateClaimList.add(clm);
                                 }          
        try{
            //LM-770-- Handling the missing argument exception --06NOV2019
            if (ClaimId != NULL) {
                update updateClaimList;   
            }                
        }catch(Exception uc){
            utility.sendException(uc, 'UPDATE CLAIM FAILED'+uc.getStackTraceString());
        }
        
        
        Object__c sc = obj;
        sc.Id = oId;
        try{
            update sc;    
        }catch(Exception uo){
            utility.sendException(uo,'UPDATE OBJECT FAILED');
        }
        
        
    }
    //04JUL 2019 -- Send email to the customer after the approval of Claim Lings 2.0
    //To have a seperate button for approve claim with email.
    @AuraEnabled 
    public Static void sendEmail(Object__c obj,String oId){
        
        // Get Contact Id.
        // LM-704 --After querying the values we are updating the Object--01OCT2019
        String getId = 'SELECT id, Name, Customer__c, Claim__c, Claim__r.Damage_amount__c, Is_Deleted__c, Product__r.Searchable__c, Customer__r.Email FROM Object__c WHERE ID = \'' + oId + '\'';
        Object__c ObjValue = Database.query(getId);
        
        String contactId = ObjValue.Customer__c;
        String Email = objValue.Customer__r.Email;      
        String ClaimId = objValue.Claim__c;
        
        ClaimSettle.updateObject(obj, oId);      
        
        // Send Email
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();                 
        EmailTemplate template = [Select id from EmailTemplate where name=:'Approving a claim' LIMIT 1];   
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        mail.setToAddresses(new String[] { Email });
        mail.setTemplateId(template.Id);
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id); 
        mail.setwhatid(objValue.Claim__c);
        mail.setTargetObjectId(contactId);
        mail.setSaveAsActivity(true);
        
        mailList.add(mail);   
        try{
            //LM-770-- Handling the missing argument exception --06NOV2019
            if (ClaimId != NULL) {                
                Messaging.sendEmail(mailList); 
            }
        }catch(Exception cl){
            utility.sendException(cl, 'APPROVE CLAIM FAILED TO SEND EMAIL TO THE CUSTOMER');
        }                                     
    }
}