@isTest
public class UIClaimControllerTest {
    
    public static String CHF_STRING = 'CHF';    
    
        public class ClaimWithObject{
        public String damageReason;
        public Date damageDate;
        public String damageDetails;
        public String iban;
        public List<String> attachments = new List<String>();
        public String fileContent;
        public List<LingsObject> insuredProducts = new List<LingsObject>();
    }
    
    public with sharing class LingsObject{

        public String uuid;
        public String name = 'Test Object';
        public Decimal price = 5.20;
        @testvisible String currencyCode = CHF_STRING;
        public String thumbnail = 'www.dummyImage.com';
        public boolean published = True;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        public List<EventInfo> events;
        public List<SackInfo> sacks;
        public boolean insured = True;
        public boolean paid = True; 
        public boolean claimed = False;
        public String category = 'smartPhone';
        public boolean electricBike = False;
        public String shopName = 'Test Shop'; 
        public Decimal paidAmount = 25.00;
      //  public String state;
        public String serialNumber = 'LINGSTEST';
        public String comment = 'This is used for the code Coverage';
        public Date purchasedDate = System.today();
        public Date warrantyDate = System.today().addDays(50);
        public String shopUrl = 'www.thisistestshop.com';
        public String warrantyCard = 'TestCard';
        public String fileContent1 = 'Test source 1';
        public String receipt = 'Test receipt';
        public String fileContent2 = 'Test source 2';        
        public String manufacturer = 'Test company';
        public Boolean click = True;
       
    }
    
    public class OnDemandInfo {
        public Decimal daily = 0.15;
        public Decimal monthly = 2.00;
        public boolean enabled = True;
    }
    
    public class FlatRateInfo {
        public Decimal daily = 0.15;
        public Decimal monthly = 2.00;
        public Decimal savings = 0.25;
        public Date periodFrom = System.today();
        public Date periodTo = System.today().addDays(2);
        public boolean enabled = True;
        public boolean renewable = True;        
        Integer duration = Utility.numOfDays(null);
        String currencyCode = CHF_STRING;
    }     
    
    public with sharing Class EventInfo{
        public String uuid;
        public String name = 'Test Event';
        public Date periodFrom = System.today();
        public Date periodTo = System.today().addDays(2);      
    }    
    
    public with sharing Class SackInfo{
        public String uuid;
        public String name = 'Test Bag';
        public String state = 'Insured';        
        public boolean toggle = True;          
        public boolean hasClaim = False;      
        public integer objectsCount = 2;
        public decimal premiumTotal = 2.56;        
    }    
    
    testmethod public static void TestUserPolicy() {
        
        String userNameSuffix = 'lings.demo';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];        
        
        User u = new User(Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
        UserName= 'reva@testorg.com',CommunityNickname='Lings32184');
        insert u;
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        Contact con = new Contact(lastname='lastname1',OwnerId=u.id,Password__c='123pwd4598' );
        con.email='test@test.com';
        con.Street__c = 'Test Location';
        con.Place__c = 'Chennai';
        con.Post_Code__c = '1000';
        con.Phone = '8056489524';
        insert con;     

        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
            
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='OnDemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        
        Claim__c newClaim = new Claim__c();
        newClaim.Damage_Type__c = 'theft-loss';
        newClaim.Damage_Date__c = System.today();
        newClaim.Description__c  = 'Sample description';
        newClaim.Object__c = obj.id;
        newClaim.IBAN__c = 'Sample IBAN';
        newClaim.Contact__c = con.Id;
        newClaim.First_Name__c = u.FirstName;
        newClaim.Last_Name__c = u.LastName;
        newClaim.User_Name__c = u.Username;
        newClaim.Email__c = u.Email;
        newClaim.User_Id__c = u.Id;
        newClaim.CurrencyIsoCode = 'CHF';
        newClaim.Reporting_Date__c = System.today();
        newClaim.Insurance_Amount__c = 13.00;
        insert newClaim;
        
        UIClaimController UICC = new UIClaimController();
        UICC.setContext(con.id);
        UICC.getRecords();
            
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true);
            
         
        List<LingsObject> objectList = new List<LingsObject>();
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        objectclass.click = True;
        objectList.add(objectclass);
       String myobjJSON = JSON.serialize(objectclass,true);  
            
        ClaimWithObject instance = new ClaimWithObject();
        instance.damageReason = newClaim.Damage_Type__c;
        instance.damageDate =newClaim.Damage_Date__c; 
        instance.damageDetails =  'Sample description';
        instance.iban = newClaim.IBAN__c;
        //instance.attachments.add('fileelementfhajshfksjdaufrfawssfdsjncanssjadfjlkashfuiiewafnsdjfnsadjfueiwhfdkjfnlasdfjshfsjkadjgvuhvuft ykftvgyk fvtfgtujtfgvuftgufg');
        instance.insuredProducts = objectList;
        
        String myJSON = JSON.serialize(instance,true);
         
        UICC.getRecords();
        UIClaimController UIC  = new UIClaimController(con.id);
        UIC.updateRecords('inputJSON');
        UIC.deleteRecords('inputJSON');
        UIC.readRecords('inputJSON');
        UIC.createRecords(myJSON);
        }

    }
        testmethod public static void TestUserPolicy1() {
        
        String userNameSuffix = 'lings.demo';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];        
        
        User u = new User(Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
        UserName= 'reva@testorg213.com',CommunityNickname='Lings32184213');
        insert u;
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        Contact con = new Contact(lastname='lastname1',OwnerId=u.id,Password__c='123pwd4598' );
        con.email='test@test.com';
        con.Street__c = 'Test Location';
        con.Place__c = 'Chennai';
        con.Post_Code__c = '1000';
        con.Phone = '8056489524';
        insert con;     

        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
            
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='OnDemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
        
        Claim__c newClaim = new Claim__c();
        newClaim.Damage_Type__c = 'theft-loss';
        newClaim.Damage_Date__c = System.today();
        newClaim.Description__c  = 'Sample description';
        newClaim.Object__c = obj.id;
        newClaim.IBAN__c = 'Sample IBAN';
        newClaim.Contact__c = con.Id;
        newClaim.First_Name__c = u.FirstName;
        newClaim.Last_Name__c = u.LastName;
        newClaim.User_Name__c = u.Username;
        newClaim.Email__c = u.Email;
        newClaim.User_Id__c = u.Id;
        newClaim.CurrencyIsoCode = 'CHF';
        newClaim.Reporting_Date__c = System.today();
        newClaim.Insurance_Amount__c = 13.00;
        insert newClaim;
        
        UIClaimController UICC = new UIClaimController();
        UICC.setContext(con.id);
        UICC.getRecords();
            
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true);
            
         
        List<LingsObject> objectList = new List<LingsObject>();
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        objectclass.click = True;
        objectList.add(objectclass);
       String myobjJSON = JSON.serialize(objectclass,true);  
            
        ClaimWithObject instance = new ClaimWithObject();
        instance.damageReason = newClaim.Damage_Type__c;
        instance.damageDate =newClaim.Damage_Date__c; 
        instance.damageDetails =  'Sample description';
        instance.iban = newClaim.IBAN__c;
        instance.attachments.add('0690Q000000XdO3QAKfileelementfhajshfksjdaufrfawssfdsjncanssjadfjlkashfuiiewafnsdjfnsadjfueiwhfdkjfnlasdfjshfsjkadjgvuhvuft ykftvgyk fvtfgtujtfgvuftgufg');
        instance.insuredProducts = objectList;
        
        String myJSON = JSON.serialize(instance,true);
         
        UICC.getRecords();
        UIClaimController UIC  = new UIClaimController(con.id);
        UIC.updateRecords('inputJSON');
        UIC.deleteRecords('inputJSON');
        UIC.readRecords('inputJSON');
        UIC.createRecords(myJSON);
        }

    }
    
            testmethod public static void TestUserPolicy2() {
        
        String userNameSuffix = 'lings.demo';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];        
        
        User u = new User(Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
        UserName= 'reva@testorg213.com',CommunityNickname='Lings32184213');
        insert u;
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        Contact con = new Contact(lastname='lastname1',OwnerId=u.id,Password__c='123pwd4598' );
        con.email='test@test.com';
        con.Street__c = 'Test Location';
        con.Place__c = 'Chennai';
        con.Post_Code__c = '1000';
        con.Phone = '8056489524';
        insert con;     

        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
            
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='OnDemand';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;
            
        Policy__c pol = new Policy__c();
        pol.Object__c = obj.Id;
        pol.Contact__c = con.Id;
        pol.Active__c = true;
        pol.Insurance_Type__c = Constants.ONDEMAND_STRING;
        insert pol;
        
        Claim__c newClaim = new Claim__c();
        newClaim.Insurance__c = pol.Id;
        newClaim.Damage_Type__c = 'theft-loss';
        newClaim.Damage_Date__c = System.today();
        newClaim.Description__c  = 'Sample description';
        newClaim.Object__c = obj.id;
        newClaim.IBAN__c = 'Sample IBAN';
        newClaim.Contact__c = con.Id;
        newClaim.First_Name__c = u.FirstName;
        newClaim.Last_Name__c = u.LastName;
        newClaim.User_Name__c = u.Username;
        newClaim.Email__c = u.Email;
        newClaim.User_Id__c = u.Id;
        newClaim.CurrencyIsoCode = 'CHF';
        newClaim.Reporting_Date__c = System.today();
        newClaim.Insurance_Amount__c = 13.00;
        insert newClaim;
        
        UIClaimController UICC = new UIClaimController();
        UICC.setContext(con.id);
        UICC.getRecords();
            
        OnDemandInfo onclass = new OnDemandInfo();
        String myJSON1 = JSON.serialize(onclass,true);
        
        FlatRateInfo frclass = new FlatRateInfo();
        String myJSON2 = JSON.serialize(frclass,true);
            
         
        List<LingsObject> objectList = new List<LingsObject>();
        LingsObject objectclass = new LingsObject();
        objectclass.uuid = obj.Id;
        objectclass.insuranceRate = onclass;
        objectclass.flatRate = frclass;
        objectclass.click = True;
        objectList.add(objectclass);
       String myobjJSON = JSON.serialize(objectclass,true);  
            
        ClaimWithObject instance = new ClaimWithObject();
        instance.damageReason = newClaim.Damage_Type__c;
        instance.damageDate =newClaim.Damage_Date__c; 
        instance.damageDetails =  'Sample description';
        instance.iban = newClaim.IBAN__c;
        instance.attachments.add('0690Q000000XdO3QAKfileelementfhajshfksjdaufrfawssfdsjncanssjadfjlkashfuiiewafnsdjfnsadjfueiwhfdkjfnlasdfjshfsjkadjgvuhvuft ykftvgyk fvtfgtujtfgvuftgufg');
        instance.insuredProducts = objectList;
        
        String myJSON = JSON.serialize(instance,true);
         
        UICC.getRecords();
        UIClaimController UIC  = new UIClaimController(con.id);
        UIC.updateRecords('inputJSON');
        UIC.deleteRecords('inputJSON');
        UIC.readRecords('inputJSON');
        UIC.createRecords(myJSON);
        }

    }
}