public without sharing class UIProfileController implements DataHandler {
    private static Id PORTAL_ACCOUNT_ID = System.Label.AccountId;
    private static String TXN_ERR_MSG = '';
    @testvisible private String contactId = '';
    
    //constructor with one parameter to set the contact id 
    public UIProfileController(String contact) {
        this.contactId = contact;
    }
    
    public UIProfileController() {
        
    } 
    
    //setting contact id method 
    public void setContext(String cId) {
        this.contactId = cId;
    }
    
    //getting the records
    public String getRecords() {
        return '{"status":"failure","message":"List operation is not supported"}';
    }
    
    //read the records of the user by passing the json input 
    public String readRecords(String inputJSON) {
        //deserializing the input User data
        User usr = (User) JSON.deserialize(inputJSON, User.class);
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        try {
            UIResultWrapper.Profile usrProfile = new UIResultWrapper.Profile();
            
            usrProfile.uuid = UserInfo.getUserId();
            usrProfile.firstName = UserInfo.getFirstName();
            usrProfile.lastName = UserInfo.getLastName();
            usrProfile.email = UserInfo.getUserEmail();
            usrProfile.City = usr.City;
            usrProfile.Street = usr.Street;
            usrProfile.Phone = usr.Phone;
            usrProfile.PostalCode = usr.PostalCode;
            usrProfile.dob = usr.DOB__c;
            usrProfile.house = usr.House_Number__c;
            //     usrProfile.password = usr.Password__c;
            usrProfile.leadSource = usr.How_do_you_know_about_us__c;
            
            // Logic to reset FirstLogin Flag in User Object - 15FEB19
            if(!usr.First_Login__c) {
                usr.First_Login__c = true;                
                update usr;
            }
            
            if(usr.Payment_Info__c != null) {
                usrProfile.paymentMeans = (UIPaymentWrapper.SPPaymentInfo) JSON.deserialize(usr.Payment_Info__c, UIPaymentWrapper.SPPaymentInfo.class);
            }
            
            // MR - 01AUG19 - Yet to Complete
            // Read the contact object for voucherBalance info and its related policies for policyDues
            String queryString = 'SELECT id, Name, Balance_Pending__c, VIU_Balance__c, Freeze__c,'+
                '(SELECT Id, Name, Start_date__c, End_date__c From Events__r where End_date__c >= TODAY),'+
                '(SELECT id, Name, Insurance_state__c, Bag_IsDelete__c From Rucksacks__r WHERE Bag_IsDelete__c = FALSE),'+
                '(Select Id, Name, Insurance_Type__c, '+
                'Charges_Incurred_this_month__c FROM LingsContracts__r WHERE '+
                '(Invoice__c = NULL OR Invoice__r.Status__c != \'Success\' OR Invoice_Start_Date__c = THIS_MONTH))'+
                ' FROM Contact WHERE ID = \'' + this.contactId + '\'';
            
            Decimal polDues = 0.0;
            //1.Get the Voucher Balance, Freeze From Contact. 
            Contact con = Database.query(queryString);
            usrProfile.voucherBalance = con.VIU_Balance__c;
            usrProfile.freeze = con.Freeze__c;
            //2.Get the Policy Due.
            for(Policy__c pol: con.LingsContracts__r) {
                polDues += Utility.getRoundedPremium(pol.Charges_Incurred_this_month__c);
            }      
            usrProfile.policyDues = polDues;
            //3.Get the Event Count.
            if(con.Events__r.size() == 0){
                usrProfile.hasEvents = False; 
            }else{
                usrProfile.hasEvents = True;                
                for(Event__c eve: con.Events__r){
                    UIResultWrapper.EventInfo eveWrapper = new UIResultWrapper.EventInfo();
                    eveWrapper.uuid = eve.Id;
                    eveWrapper.name = eve.Name;
                    eveWrapper.periodFrom = eve.Start_Date__c;
                    eveWrapper.periodTo = eve.End_Date__c;
                    usrProfile.events.add(eveWrapper);
                }
            }
            //4. Get the Bag Count.
            if(con.Rucksacks__r.size() == 0){
                usrProfile.hasBags = False;
            }else{
                usrProfile.hasBags = True;                
                for(Rucksack__c bag: con.Rucksacks__r){
                    UIResultWrapper.SackInfo sackWrapper = new UIResultWrapper.SackInfo();
                    sackWrapper.uuid = bag.Id;
                    sackWrapper.name = bag.Name;
                    sackWrapper.state = bag.Insurance_state__c;
                    //sackWrapper.toggle = (sackWrapper.state.equalsIgnoreCase('Insured') ? true : false);
                    usrProfile.sacks.add(sackWrapper);
                }                
            }
            
            jsonGen.writeObjectField('profile', usrProfile);
            jsonGen.writeStringField('status', 'success');
            jsonGen.writeStringField('message', 'User Record read Successfully - ' + usrProfile);
            
        } catch(Exception gpe) {
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', gpe.getStackTraceString());            
        }
        
        jsonGen.writeEndObject();
        return jsonGen.getAsString();
    }
    
    //creation of records by passing the json as a parameter
    public String createRecords(String inputJSON) {
        return '{"status":"failure","message":"Create operation is not supported"}';
    }
    
    //updating records of the user who have already exists by passing the json as a parameter
    public String updateRecords(String inputJSON) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        UIResultWrapper.Profile usrProfile = (UIResultWrapper.Profile)JSON.deserialize(inputJSON, UIResultWrapper.Profile.class);
        
        User u = new User(id=usrProfile.uuid);
        u.LastName = usrProfile.lastName;
        u.FirstName = usrProfile.firstName;
        u.City = usrProfile.city;
        u.Street = usrProfile.street;
        u.Phone = usrProfile.phone;
        u.House_Number__c = usrProfile.house;
        u.PostalCode = usrProfile.postalCode;
        u.DOB__c = usrProfile.dob;
        
        // lastName is a required field on user, but if it isn't specified, we'll default it to the username
        try {
            update u;
            jsonGen.writeStringField('status', 'success');
            jsonGen.writeObjectField('profile', usrProfile);
            jsonGen.writeStringField('message', 'User Updated Successfully - ' + u.Id);
        } catch(Exception e) { //LM-772 -- Added different text to compare the error message --07NOV2019
            if(e.getMessage().containsIgnoreCase('This Postal Code is not covered for Insurance')){
                jsonGen.writeStringField('status', 'failure');                
                jsonGen.writeStringField('message', Constants.ERROR_POSTAL_CODE.get('de'));
            }else{
                jsonGen.writeStringField('status', 'failure');            
                jsonGen.writeStringField('message', e.getMessage());
            }
        }
        
        jsonGen.writeEndObject();
        return jsonGen.getAsString();
    }
    
    //sends the policy attachment as a mail to the user by calling the UserPolicy class sendPdf method
    public String setEmailPolicy(String langCode) {
        //LM-532 --Added new Parameter language code--20JULY2019
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        try {
            UserPolicy up = new UserPolicy(this.contactId);
            up.sendPdf();
            
            Contact customer = new Contact(id=this.contactId); 
            customer.Email_Policy_Datetime__c = System.now();
            
            update customer;
            jsonGen.writeStringField('status', 'success');
        } catch (Exception e) {
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message with ContactId', e.getStackTraceString());
        }
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString(); 
    }
    
    //sends the OKK policy attachment as a mail to the user by calling the UserPolicy class sendOKKPolicyPdf method
    /*public String setOKKEmailPolicy(String langCode, String OKKObjId) {
        //LM-532 --Added new Parameter language code--20JULY2019
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        try {
            System.debug('Inside the UIProfile Controller send Email method');
            UserPolicy up = new UserPolicy(this.contactId);
            up.sendOKKPolicyPdf(OKKObjId);
            
            Contact customer = new Contact(id=this.contactId); 
            customer.Email_Policy_Datetime__c = System.now();
            
            update customer;
            System.debug('After Updating the Contact');
            jsonGen.writeStringField('status', 'success');
        } catch (Exception e) {
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', e.getMessage());
        }
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString(); 
    }*/
    
    //deleting records of the user who have already exists, by passing the json as a parameter
    public String deleteRecords(String inputJSON) {
        return '{"status":"failure","message":"Delete operation is not supported"}';
    }
    
    // Change Password
    public string changePassword(String langCode, String oldPassword, String newPassword){  
        //LM-532 --Added new Parameter language code--20JULY2019
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        jsonGen.writeStringField('logged_in_user', UserInfo.getUserName());
        Boolean errFlag = false;
        
        try{
            PageReference pgRef = Site.changePassword(newPassword, newPassword, oldpassword);
            if(pgref == NULL){
                //LM-726 -- TO display error msg for the incorrect password --09OCT2019
                for (ApexPages.Message m : ApexPages.getMessages()) {
                    jsonGen.writeStringField('status', 'failure');                    
                    String getMsg = ''+m;
                    getMsg = getMsg.replaceAll('ApexPages.Message', '');
                    jsonGen.writeStringField('getMsg',''+getMsg);
                    if(getMsg.containsIgnoreCase('Your Password cannot equal or contain your user name')){ 
                        errFlag = TRUE;
                        jsonGen.writeStringField('message',Constants.ERROR_PASSWORD.get('de'));
                    }                    
                }            
            }            

            if(pgRef != null && newPassword != null) {
                try {                                        
                    // Update Contact with the newPassword
                    Contact updContact = new Contact(Id=contactId);
                    updContact.Password__c = newPassword;
                    //27JUNE2019 -- Token will be only generated at the Success attempt and when the Token is Null.
                    updContact.Token__c = Utility.generateToken(32);
                    updContact.Token_created__c = String.valueOf(System.now());
                    update updContact;                          
                } catch (Exception ue) {
                    Utility.sendException(ue, 'Contact PASSWORD CHANGE FAILED in changePassword');
                }                
                // On success
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeStringField('message', 'Password Changed Successfully - ' + UserInfo.getUserName());
            } else if (errFlag == False) {
                // On failure
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', Constants.ERROR_OLD_PWD.get('de'));  
            }
        } catch(Exception e) {
            jsonGen.writeStringField('status', 'failure');            
            Utility.sendException(e, 'PASSWORD CHANGE FAILED in changePassword'+e.getStackTraceString());
        }
        
        jsonGen.writeEndObject();
        return jsonGen.getAsString();
    }
    
    //LM-366--Get Alias ID from SaferPay TxnAuthorization
    public String updateTxnAlias() {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        // Read the Current Contact details based on the ID
        Contact contObj = [Select Id, Payment_Alias_Id__c, Token__c from Contact WHERE ID = :contactId LIMIT 1][0];
        jsonGen.writeStringField('contactId', contactId); 
        
        //Transaction Authorization.
        String reqBodyAuthorize = '{"RequestHeader": { "SpecVersion": "' + System.Label.SaferPayVersion + '", "CustomerId": "' + 
            Constants.SP_CUSTOMER_ID + '", "RequestId": "' + 
            contactId + '", "RetryIndicator": 0 }, "Token": "' + contObj.Token__c + '", "RegisterAlias": { "IdGenerator": "RANDOM" } }';
        
        jsonGen.writeStringField('inpRequestBody', reqBodyAuthorize); 
        HttpRequest spReq2 = new HttpRequest();
        spReq2.setBody(reqBodyAuthorize);
        spReq2.setEndpoint(Constants.SP_Tnx_Authorize_EP);
        spReq2.setMethod('POST');
        spReq2.setHeader('Authorization', Constants.SP_BASIC_AUTH);
        spReq2.setHeader('Content-Type', 'application/json');
        
        HttpResponse resp2 = new Http().send(spReq2);
        String respString = resp2.getBody().replaceAll('"Transaction"', '"txn"');
        UIPaymentWrapper.SPTxnPaymentResponse apiResp = (UIPaymentWrapper.SPTxnPaymentResponse) JSON.deserialize(respString, UIPaymentWrapper.SPTxnPaymentResponse.class);            
        String msgString = '';
        // MR - 22MAY19 - Changes for 3DS verification
        if(resp2.getStatusCode() == 200) {
            if(apiResp.ErrorMessage != null) {
                jsonGen.writeStringField('status', 'failure'); 
                msgString = apiResp.ErrorMessage;
                jsonGen.writeStringField('messageReason', 'Card Add Error: ' + msgString);
            } else if((apiResp.liability != null && apiResp.liability.threeDs != null && apiResp.liability.threeDs.authenticated) || 
                      ((apiResp.liability == null || apiResp.liability.threeDs == null) && apiResp.txn.status == Constants.AUTHORIZED_STRING)) {
                          //Transaction Cancel.
                          if(UIProfileController.SPTxnCancel(apiResp.txn.Id, contObj.Id)) {
                              jsonGen.writeStringField('status', 'success');
                              msgString = 'Txn Completed';
                          } else {
                              jsonGen.writeStringField('status', 'success');
                              msgString = 'Txn Cancellation Failed';
                              jsonGen.writeStringField('messageReason', TXN_ERR_MSG);
                          }
                      } else {
                          jsonGen.writeStringField('status', 'failure');
                          msgString = 'Txn Authorization Failed';
                          jsonGen.writeStringField('messageReason', 'Card Add Error: ' + msgString);
                      }
        } else {
            msgString = apiResp.ErrorMessage;
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('messageReason', 'Card Add Error: ' + msgString);
        }
        jsonGen.writeStringField('message', msgString);
        jsonGen.writeEndObject();
        
        // LM-485--Update the Alias Info in Contact --//21June2019 --To capture the card insert error messages.
        Utility.setSPTxnAlias(respString, msgString);
        //Utility.setSPTxnAlias(respString);
        return jsonGen.getAsString();
    }
    
    //LM-366--Charge a customer with 5CHF--03MAY2019
    public static String SPTxnInitialize(String sourceURL, String contId){
        String returnURL = '';
        
        // 1) Transaction Initialize.--Changed from 500 t0 1 cent 12MAY2019
        String reqBodyInitialize = '{"RequestHeader": { "SpecVersion": "' + System.Label.SaferPayVersion + '", "CustomerId": "' + 
            Constants.SP_CUSTOMER_ID + '", "RequestId": "' + 
            contId + '", "RetryIndicator": 0 }, "TerminalId": "' + Constants.SP_TERMINAL_ID + '",'+ 
            '"Payment": { "Amount": { "Value": "1", "CurrencyCode": "CHF"} },'+
            '"Payer": { "LanguageCode": "de" },'+
            '"ReturnUrls": { "Success": "' + sourceURL + '/' + contId + '", ' + 
            '"Fail": "' + sourceURL + '/carderror", ' +
            '"Abort": "' + sourceURL + '"},'+
            '"Styling": { "CssUrl": "https://s3.amazonaws.com/saferpaystyle/saferpay.css", "THEME": "NONE" } }';           
        
        HttpRequest spReq1 = new HttpRequest();
        spReq1.setBody(reqBodyInitialize);
        spReq1.setEndpoint(Constants.SP_Tnx_Initialize_EP);
        spReq1.setMethod('POST');
        spReq1.setHeader('Authorization', Constants.SP_BASIC_AUTH);
        spReq1.setHeader('Content-Type', 'application/json');
        
        HttpResponse resp1 = new Http().send(spReq1);
        if(resp1.getStatusCode() == 200) {
            UIPaymentWrapper.SPTxnInitResponse apiResp = (UIPaymentWrapper.SPTxnInitResponse) JSON.deserialize(resp1.getBody(), UIPaymentWrapper.SPTxnInitResponse.class);            
            returnURL = apiResp.redirect.redirectURL;
            
            // Call a future method to update the Contact with Safer Pay Token
            Utility.setSPToken(apiResp.ResponseHeader.RequestId, apiResp.token);      
        }
        
        return returnURL;
    }        
    
    //LM-366--Cancel Transaction--02MAY2019
    public static boolean SPTxnCancel(String TnxId, String conId){        
        if(TnxId != ''){
            String reqBody = '{"RequestHeader": { "SpecVersion": "' + System.Label.SaferPayVersion + '", "CustomerId": "' + 
                System.label.Customer_ID+'", "RequestId": "' + conId + '", "RetryIndicator": 0 }, ' + 
                '"TransactionReference": { "TransactionId": "' + TnxId + '"} }';
            
            HttpRequest spReq = new HttpRequest();
            spReq.setBody(reqBody);
            spReq.setEndpoint(Constants.SP_Tnx_Cancel_EP);
            spReq.setMethod('POST');
            spReq.setHeader('Authorization', Constants.SP_BASIC_AUTH);
            spReq.setHeader('Content-Type', 'application/json');
            
            HttpResponse resp = new Http().send(spReq);
            UIPaymentWrapper.SPTransactionCancel apiResp = (UIPaymentWrapper.SPTransactionCancel) JSON.deserialize(resp.getBody(), UIPaymentWrapper.SPTransactionCancel.class);            
            
            if(resp.getStatusCode() == 200) {            
                return true;
            } else {
                TXN_ERR_MSG = 'Error Cancelling the Txn' + resp.getBody();
            }
        } else {
            TXN_ERR_MSG = 'TxnId in SPTxnCancel is Blank';
        }
        
        return false;
    }    
}