global class InsuranceDisableScheduler implements Schedulable {
    public InsuranceDisableScheduler(){}

    //method to trigger the Insurance disable email notification to all user.
    global void execute(SchedulableContext sc) {
        // Instantiate InsuranceDisable with parameters
        BatchInsuranceDisable batchDisable = new BatchInsuranceDisable();
        database.executeBatch(batchDisable, 5);
    }
}