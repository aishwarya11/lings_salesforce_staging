@isTest
public class BatchPaymentProcessTest {
    testmethod public static void testBatchPaymentProcessTest() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=10.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 15.000;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 5.000;
        insert VCC;

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 1000.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 20;
        inv1.Voucher_Deductions__c = 10.0;
        inv1.Email__c = con.Email;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);
        
        /*List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Start_Date__c = System.now();
        pol.Insurance_Type__c = 'OnDemand';
        pol.End_Date__c = System.today()+1;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Contact__c = con.id;
        insert pol;
        polList.add(pol);
        
        Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;*/
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
        testmethod public static void testBatchPaymentProcessTest1() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=1000.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        //con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 15.000;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 500.000;
        insert VCC;

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 500.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 0;
        inv1.Voucher_Deductions__c = 500.0;
        inv1.Email__c = con.Email;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);
        
        /*List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Start_Date__c = System.now();
        pol.Insurance_Type__c = 'OnDemand';
        pol.End_Date__c = System.today()+1;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Contact__c = con.id;
        insert pol;
        polList.add(pol);
        
        Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;*/
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
        testmethod public static void testBatchPaymentProcessTest2() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=1000.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        //con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 15.000;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 500.000;
        insert VCC;

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 500.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 0;
        inv1.Voucher_Deductions__c = 500.0;
        inv1.Email__c = con.Email;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);
        
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;            
            
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Start_Date__c = System.now();
        pol.Insurance_Type__c = 'OnDemand';
        pol.End_Date__c = System.today()+1;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Object__c = obj1.Id;
        pol.Contact__c = con.id;
        insert pol;
        polList.add(pol);
        
        Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
        testmethod public static void testBatchPaymentProcessTest3() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=1000.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        //con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 15.000;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 500.000;
        insert VCC;

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 500.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 0;
        inv1.Voucher_Deductions__c = 500.0;
        inv1.Email__c = con.Email;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);
            
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;            
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today().adddays(-1);
        pol.Start_Date__c = System.now().adddays(-1);
        pol.Insurance_Type__c = 'FlatRate';
        pol.End_Date__c = System.today()+31;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        insert pol;
        polList.add(pol);
        
        Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
            testmethod public static void testBatchPaymentProcessTest4() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=1000.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c= null;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 1512.000;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 500.000;
        insert VCC;

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 500.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 0;
        inv1.Voucher_Deductions__c = 500.0;
        inv1.Email__c = con.Email;
        inv1.OKKPolicyAmount__c = 100;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);
        
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1; 
                
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today().adddays(-1);
        pol.Start_Date__c = System.now().adddays(-1);
        pol.Insurance_Type__c = 'FlatRate';
        pol.End_Date__c = System.today()+31;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        insert pol;
        //polList.add(pol);
        
        /*Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;*/
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
       testmethod public static void testBatchPaymentProcessTest5() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 1512.000;
        insert con;
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
            testmethod public static void testBatchPaymentProcessTest6() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=1000.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 15.000;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 500.000;
        insert VCC;

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 2000.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 0;
        inv1.Voucher_Deductions__c = 500.0;
        inv1.Email__c = con.Email;
        inv1.OKKPolicyAmount__c = 100;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);
            
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;            
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Start_Date__c = System.now().adddays(-1);
        pol.Insurance_Type__c = 'FlatRate';
        pol.End_Date__c = System.today()+31;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        insert pol;
        polList.add(pol);
        
        Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
        testmethod public static void testBatchPaymentProcessTest7() {
        //try{
        List<Invoice__c> invList = new List<Invoice__c>();
                    
        /*Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=1000.000;
        vc.From__c=System.today()-1;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+4;
        vc.Usage_Limit__c = 10;
        insert vc;*/
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        //con.Payment_Alias_ID__c= System.label.TestAlaisId;
        con.Email='revathy@rangerinc.cloud';
        con.Balance_Pending__c = 15.000;
        insert con;
        
        /*Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        VCC.Balance__c = 500.000;
        insert VCC;*/
        
        
            
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        insert obj1;     

        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 2000.000;
        inv1.Contact__c = con.Id;
        inv1.Status__c = 'New';
        inv1.Name = 'Invoice name';
        inv1.Date__c = System.today();
        inv1.Transaction_Id__c = 'TestId';
        inv1.Transaction_Amount__c = 0;
        inv1.Voucher_Deductions__c = 500.0;
        inv1.Email__c = con.Email;
        inv1.OKKPolicyAmount__c = 100;
        //inv1.RVGPolicyAmount__c = 100;
        //inv1.Card_Payment__c = 'a091w000001hKwJAAU';
        insert inv1;   
        
        invList.add(inv1);       
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Start_Date__c = System.now().adddays(-1);
        pol.Insurance_Type__c = 'FlatRate';
        pol.End_Date__c = System.today()+31;
        pol.Invoice__c = inv1.id;
        pol.Active__c = true;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        insert pol;
        polList.add(pol);
        
        Payment__c pay = new Payment__C();
        pay.Contact__c = con.id;
        pay.Invoice__c = inv1.id;
        pay.Paid__c = 110;
        pay.Date__c = System.today();
        insert pay;
                     
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        // Instantiate PolicyExpires apex batch class
        BatchPaymentProcess BCE = new BatchPaymentProcess(); // Add you Class Name
        //DataBase.executeBatch(BCE, 1); 
        ID batchID=Database.executeBatch(BCE);
        //System.abortJob(batchID);
        Test.stopTest();
        //}
        //catch(Exception e){}

    }
}