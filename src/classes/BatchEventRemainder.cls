//Sends mail to the user, before the Event get starts and before the Event get ends
// Should run on the Afternoon/Evening
global class BatchEventRemainder implements Database.Batchable<Event__c>, Database.Stateful {
    public Integer totalMailProcessedCount = 0;
    public Integer failedMailProcessedCount = 0;
    public Integer successBeforeStartsMailCount = 0;
    public Integer successBeforeEndsMailCount = 0;
    //public List<Messaging.SendEmailResult> successEmailList = new List<Messaging.SendEmailResult>();
    //public List<Messaging.SendEmailResult> failureEmailList = new List<Messaging.SendEmailResult>();
    public List<String> successEmailList = new List<String>();
    public List<String> failureEmailList = new List<String>();
    
    //Start batchable
    global Iterable<Event__c> start(Database.BatchableContext BC) {
        System.debug('BatchEventRemainder START');

        List<Event__c> query = [SELECT Id, Name, Start_date__c, End_date__c, Contact__r.Email, Contact__c 
                                  FROM Event__c 
                                 WHERE Contact__c != NULL AND (Start_date__c = TOMORROW OR End_date__c = TODAY)];
        return query;
    }
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Event__c> scope) {
        System.debug('BatchEventRemainder EXECUTE');

        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();

        // Get the Email Template
        EmailTemplate emailTempStart = [Select id from EmailTemplate where name='Event-notification-beforeStart' limit 1][0];
        EmailTemplate emailTempEnd = [Select id from EmailTemplate where name='Event-notification-beforeEnd' LIMIT 1][0];

        for(Event__c eve : scope) {
            Id ContactId = eve.Contact__c;
            
            //Send email before the start date
            if(eve.Start_date__c == System.today() + 1) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setUseSignature(false);
                mail.setToAddresses(new String[] { eve.Contact__r.Email });
                mail.setTemplateId(emailTempStart.Id);
                mail.setWhatId(eve.Id);
                OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
                mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
                mail.setTargetObjectId(contactId);
                mail.setSaveAsActivity(true);
                
                mailList.add(mail);
                successBeforeStartsMailCount++;
            }
            
            //Send email before the end date
            if(eve.End_date__c == System.today()) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setUseSignature(false);
                mail.setToAddresses(new String[] { eve.Contact__r.Email });
                mail.setTemplateId(emailTempEnd.Id);
                mail.setWhatId(eve.Id);
                OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress];
                mail.setOrgWideEmailAddressId(owea.get(0).Id);                 
                mail.setTargetObjectId(contactId);
                mail.setSaveAsActivity(true);
                mailList.add(mail);
                successBeforeEndsMailCount++;
            }
        }  

        try {
            totalMailProcessedCount += mailList.size();
            // Send the emails
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(mailList); 
            }
        } catch(Exception er) {
            failedMailProcessedCount++;
            // Send exception email to Admin
            Utility.sendException(er, 'EVENT REMAINDER MAIL NOT SEND');
        }    
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchEventRemainder FINISH');

    	List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 23MAY19 - Code Optimization
        mail.setSubject('BatchEventRemainder ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         

        String body = 'BatchEventRemainder Status on Sending email to the user<br/>';
        
        // Processing Email list 
        body += '<br/> ' + totalMailProcessedCount + ' number of Email processing were completed.'; 
        
        // Email Processing for Events with BeforeStartDate
        if(successBeforeStartsMailCount > 0) {
            body += '<br/> successful Emails were processed for the before EventStartDate = '+successBeforeStartsMailCount+'<br/>';
        } else {
            body += '<br/> No Email processing were happened for the Event records with BeforeStartDate criteria.';
        }
        
        // Processing the Events with BeforeEndDate
        if(successBeforeEndsMailCount > 0) {
            body += '<br/> successful Emails were processed for the before EventEndDate = '+ successBeforeEndsMailCount + ' <br/>';

        } else {
            body += '<br/> No Email processing were happened for Event records with BeforeEndDate criteria.';
        }
        
        // Failed Email Processing
        if(failedMailProcessedCount > 0) {
            body += '<br/> Failed Email processing count = '+ failedMailProcessedCount + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing were happened.';
        }
        //03JUN19
        if(successEmailList.size() > 0){
            body += '<br/> ***NEW*** successEmailList count = '+ successEmailList.size() + '. The successEmailList as follows: '+successEmailList+ ' <br/>';
        }
        if(failureEmailList.size() > 0){
           body += '<br/> ***NEW*** failureEmailList count = '+ failureEmailList.size() + '. The failureEmailList as follows: '+failureEmailList+ ' <br/>';
        }
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);        
        
		//LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedMailProcessedCount > 0 || failureEmailList.size() > 0) {
                Messaging.sendEmail(mails);
            }        
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchEventRemainder';
        ab.Track_message__c = body;
        database.insertImmediate(ab);         
        }
    }
}