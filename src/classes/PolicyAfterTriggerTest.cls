@isTest
public class PolicyAfterTriggerTest {
    @isTest static void TestPolicyAfterTriggerTest() { 
    
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598',Balance_Pending__c=0.0);
        insert con; 
        
        Premium__c pre = new Premium__c();
        pre.Name='laptop';
        pre.On_Demand__c=12;
        pre.FlatRate__c=32;
        insert pre;  
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        PO.Insured_Value__c = 200.00;
        PO.Price__c= PO.Insured_Value__c.setScale(2);
        PO.Price_Customer__c = 100;
        PO.Old_Price__c=PO.Price__c;        
        PO.Searchable__c = TRUE;
        PO.OnDemand_Premium__c = 2.3;
        PO.Flatrate_Premium__c = 5.6;
        PO.IsDeleted__c = False;
        PO.Status__c = 'Closed';
        PO.Manufacturer__c = 'Test Product';
        PO.Price_Customer__c = 56;
        PO.Product_Type__c = 'Laptop';
        insert PO;
        
        Event__c eve = new Event__c();
        eve.Name='sample event';
        eve.Start_Date__c=System.today();
        eve.End_Date__c=System.today();
        eve.Contact__c=con.Id;
        insert eve;        
		
        Object__c obj = new Object__c();
        obj.Customer__c = con.Id;
        obj.Product__c = PO.Id;
        obj.Insured_Value__c=100;
        obj.Included_Date__c=System.today();
        obj.FlatRate_Savings__c=50;
        obj.Coverage_Type__c='FlatRate';
        obj.Premium_FlatRate__c=10.00;
        obj.Premium_OnDemand__c=6.00;
        obj.Flatrate_Renewal_Flag__c=true;
        obj.Insurance_Start_Date__c=System.today();
        obj.Insurance_End_Date__c=System.today()+1;
        obj.Is_Deleted__c = FALSE;
        obj.Approved__c = true;   
        obj.Acquired_Year__c = '2019';
        obj.Actual_amount_paid__c = 200.00;
        obj.Description__c = 'This is the test Object';
        obj.Designation__c = 'test';
        obj.Is_Insured__c = TRUE;
        obj.Own_Comment__c = 'This is a comment section';
        obj.Sort_Order__c = 1;
        obj.Shop_Name__c = 'Test Object';
        insert obj;    
        
        Event_Object__c eo = new Event_Object__c();
        eo.Event__c = eve.Id;
        eo.Object__c = obj.Id;
        insert eo;
        
        Rucksack__c sack = new Rucksack__c();
        sack.Contact__c = con.id;
        sack.Name = 'Bag Test';
        sack.Bag_IsDelete__c = False;
        sack.Insurance_Flag__c = True;
        insert sack;
        
        Rucksack_Object__c sackObject = new Rucksack_Object__c();
        sackObject.Contact__c = Con.Id;
        sackObject.Is_Object_Insured__c = True;
        sackObject.Lings_Object__c = obj.Id;
        sackObject.Rucksack__c = sack.Id;
        insert sackObject;
        
        Policy__c pol = new Policy__c();
        pol.Contact__c=con.Id;
        pol.Object__c = obj.Id;
        pol.Premium__c = 10.00;   
        pol.Start_Date__c = System.today();
        pol.End_Date__c= System.today() + 1;
        pol.Invoice_Start_Date__c = System.today();
        pol.Active__c=true;
        pol.Insurance_Type__c='FlatRate';
        pol.Premium__c=100;
        insert pol;             
    }

}