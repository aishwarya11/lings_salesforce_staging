public with sharing class PolicyUtility {

    // Function to create Policy
    public static Policy__c newOnDemandPolicy(Object__c obj, String src, Boolean insertFlag) {
        Integer chargedDays;
        Integer diffDays;
        
        Policy__c policy = new Policy__c();
        policy.Insurance_Type__c = CONSTANTS.ONDEMAND_STRING;
        policy.Policy_Source__c = src;
        policy.Active__c = true;
        policy.Status__c = 'Insured';
        policy.Invoice_Start_Date__c = System.today();
        policy.Start_Date__c = system.now();
        policy.Contact__c = obj.Customer__c;
        policy.Object__c = obj.id;
        policy.Insured_Value__c = obj.Insured_Value__c;
        policy.Premium__c = obj.Premium_OnDemand__c;

        // Policy Charges calculation - Used for Invoicing ONLY
        DateTime ed = policy.Invoice_Start_Date__c.addMonths(1).toStartofMonth().addDays(-1);
        diffDays =  policy.Invoice_Start_Date__c.daysBetween(ed.Date());
        chargedDays = diffDays + 1;
        //LM-695--Rounded the policy charges--25SEPT2019
        policy.Policy_Charges__c = Utility.getRoundedPremium(obj.Premium_OnDemand__c * chargedDays);

        if(insertFlag) {
            try {           
                insert policy;
            } catch(Exception cpol) {            
                Utility.sendException(cpol, 'FAILED IN INSERTING ONDEMAND POLICY - newOnDemandPolicy FOR OBJECT ' + policy.Object__c);
            }
        }

        return policy;
    } 

    // Function to activate existing policy__c record
    public static Policy__c activatePolicy(Policy__c policy, String src, String insType, boolean updateFlag) {
        policy.Active__c = true;
        policy.Status__c = Constants.POL_STATUS_INSURED;
        policy.Policy_Source__c = src;
        policy.Insurance_Type__c = insType;

       //Object ID and Coverage Type is NULL Exception Mail 03AUG2019.
        String query = 'SELECT id, Name, Active__c, End_date__c, Object__c, Invoice_Start_date__c, Insurance_Type__c FROM Policy__c WHERE Id =\'' +policy.Id+'\'';
        Policy__c pol = Database.query(query);       
        policy.Invoice_Start_Date__c = pol.Invoice_Start_Date__c;
        policy.Object__c = pol.Object__c;
        
        if(insType.equalsIgnoreCase(Constants.ONDEMAND_STRING)) {
            policy.End_Date__c = null;
            //LM-695--Rounded the policy charges--25SEPT2019
            policy.Policy_Charges__c = null;
        }

        if(updateFlag) {
            try {        
                update policy;         
            } catch(Exception cp) {
                Utility.sendException(cp, 'FAILED IN Activate POLICY from closePolicy for ' + policy.Id);
            }
        }

        return policy;
    }

    // Function to close existing policy using Id
    public static Policy__c activateOnDemandPolicy(Id policyId, String src, boolean updateFlag) {        
        return activatePolicy(new Policy__c(Id = policyId), src, 'OnDemand', updateFlag);
    }

    // Function to close existing policy using Id
    public static Policy__c activateFlatRatePolicy(Id policyId, String src, boolean updateFlag) {        
        return activatePolicy(new Policy__c(Id = policyId), src, 'Flatrate', updateFlag);
    }

    // Function to close existing policy__c record
    public static Policy__c closePolicy(Policy__c policy, boolean updateFlag) {
        Integer chargedDays;

        policy.Active__c = false;
        policy.Status__c = Constants.POL_STATUS_EXPIRED;

        if(policy.End_Date__c == NULL) {
            policy.End_Date__c = System.now();   

            chargedDays =  policy.Start_Date__c.date().daysBetween(policy.End_Date__c.Date()) + 1;
            //LM-695--Rounded the policy charges--25SEPT2019
            policy.Policy_Charges__c = Utility.getRoundedPremium(policy.Premium__c * chargedDays);
        } else if(policy.Policy_Source__c == 'Event' && policy.End_Date__c.Date() > System.today()) {
            //LM-1293 --Updating the End date for the Event Policy -- 08APR2020
            policy.End_Date__c = System.now();
            policy.Comments__c = 'Closed the Event policy with Sooner date since the customer closed it via bag';
            chargedDays =  policy.Start_Date__c.date().daysBetween(policy.End_Date__c.Date()) + 1;
            //LM-695--Rounded the policy charges--25SEPT2019
            policy.Policy_Charges__c = Utility.getRoundedPremium(policy.Premium__c * chargedDays);            
        }

        if(updateFlag) {
            try {        
                update policy;         
            } catch(Exception cp) {
                Utility.sendException(cp, 'FAILED IN CLOSING POLICY from closePolicy for ' + policy.Id);
            }
        }

        return policy;
    }

    // Function to close existing policy using Id
    public static Policy__c closePolicy(String policyId, boolean updateFlag) {        
        // To Calculate Policy Charges.
        String query = 'SELECT id, Name, Start_date__c, Active__c, Object__c, End_date__c, Premium__c, Policy_Charges__c, Policy_Source__c, Comments__c FROM Policy__c WHERE Id = \''+policyId+'\'';
        List<Policy__c> policies = Database.query(query);

        try{
            if(policies.size() > 0) { 
                return closePolicy(policies[0], updateFlag);
            }             
        } catch (Exception pc) {
            utility.sendException(pc, 'Policy close has failed');
        }


        return null;
    }
}