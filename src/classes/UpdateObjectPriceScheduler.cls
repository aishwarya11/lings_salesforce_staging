Global class UpdateObjectPriceScheduler implements Schedulable{

    global void execute(SchedulableContext sc) {    
        BatchUpdateObjectPrice updatePrice = new BatchUpdateObjectPrice();
        database.executeBatch(updatePrice, 20);
    }
}