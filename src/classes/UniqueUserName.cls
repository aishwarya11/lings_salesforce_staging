public class UniqueUserName {
    
    public UniqueUserName(){}
  
    //creating the Unique UserName by passing their email address 
    public String getUniqueName(String UserEmail) {
        Integer maxSize = 68;
        String Concat;
        UserEmail = UserEmail.replace('.', '_');
        UserEmail = UserEmail.replace('@', '_');
        if(UserEmail.length() < maxSize ){
            Concat = UserEmail.substring(0, UserEmail.length()) + '@lings.demo';
        }else{
            Concat = UserEmail.substring(0, maxSize) + '@lings.demo';
        }
        return Concat;
    }
    
    public String getSingleStr(List<String> values){
        List<String> valueCopy = new List<String>(values);
        if(valueCopy.isEmpty()){return null;}
        String result;
        String separator = ', ';
		result = String.join(valueCopy, separator);
        return result;
    }
}