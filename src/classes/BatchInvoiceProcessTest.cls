@isTest
public class BatchInvoiceProcessTest {
    
    testmethod public static void testBatchInvoiceProcessTest() {
        //  try{		
		/*
        String queryString = 'SELECT Id,FROM Contact';*/
        
                
        
        Product2 PO= new Product2();
        PO.Name='sample product';
        //PO.Price__c=36;
        PO.Insured_Value__c = 50;
        PO.Flatrate_Premium__c = 10;
        PO.OnDemand_Premium__c = 5;
        PO.Product_Type__c = 'SmartPhone';
        //PO.Price_Customer__c = 36;
        //PO.DisplayUrl = 'https://dummyurl.com';
        PO.Searchable__c = true;       
        insert PO;
        
        List<Voucher_Contact__c> vcList =  new List<Voucher_Contact__c>();
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=100;
        vc.From__c=System.today()-10;
        vc.To__c=System.today();
        vc.Fixed__c=System.today()+10;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Name123';
        con.Password__c='123pwd45932133218';
        //con.VIU_Balance__c=100;
        //con.Voucher_In_Use__c = VC.Id;
        con.Payment_Alias_ID__c='603fbabeaf4629e95c96461df9563112';
        con.Email = 'revathypandian97@gmail.com';
        con.Balance_Pending__c = 5.00;
        insert con;
        
        Voucher_Contact__c VCO = new Voucher_Contact__c();
        VCO.Voucher__c =vc.id;
        VCO.Contact__c = con.Id;
        insert VCO;
        
        Object__c obj1 = new Object__c();
        //obj1.Policy__c=pol.Id;
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Product__c = PO.id;
        obj1.Premium_OnDemand__c = 5;
        obj1.Coverage_Type__c = 'OnDemand';
        obj1.Insurance_Start_Date__c = System.today();
        obj1.Insurance_End_Date__c=System.today()+1;
        obj1.Insured_Value__c = 100;
        insert obj1;
        vcList.add(VCO);
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Start_Date__c = System.today()-2;
        pol.End_Date__c = System.today()+2;
        pol.Invoice_Start_Date__c = System.today();
        pol.Insurance_Type__c = 'OnDemand';
        pol.Premium__c = 2.05;
        pol.Start_Date__c =System.today();
        pol.End_Date__c = System.today()+1;
        pol.Object__c = obj1.Id;
        //pol.Invoice__c = inv1.id;
        pol.Contact__c = con.id;
        insert pol;
        polList.add(pol);
      
        /*Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 0;
        inv1.Contact__c = con.Id;
        inv1.Email__c = con.Email;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Failed';
        insert inv1;  */
        
        
        Test.startTest();
        
        // Instantiate PolicyExpires apex batch class
        BatchInvoiceProcess BCE = new BatchInvoiceProcess(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Date todayDate = System.today();
        BatchInvoiceProcess BCE1 = new BatchInvoiceProcess(todayDate.month(), todayDate.year()); // Add you Class Name
        DataBase.executeBatch(BCE1);
        
        BatchInvoiceProcess BCE2 = new BatchInvoiceProcess(con.id, todayDate.month(), todayDate.year()); // Add you Class Name
        DataBase.executeBatch(BCE2);
        
        Test.stopTest();
        //}
        //catch(Exception e){}
    }
    
}