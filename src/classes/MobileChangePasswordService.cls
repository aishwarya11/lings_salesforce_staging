@RestResource(urlMapping='/change/*')
global with sharing class MobileChangePasswordService {
    @HttpPost
    global static String password(String oldPassword, String newPassword){
        return UIHomeController.resetPassword(oldPassword, newPassword);
    }
}