public class PaymentInfo {
    public Id invoiceId {get;set;}
  
    //Values we required to add in the Template 08MAR2019.
    public class InvoiceDetails {       
        public String email {get;set;}       
        public decimal Amount {get;set;}        
        public String Status {get;set;}               
    }     
    
    public List<InvoiceDetails> getInvoiceList() {        
        List<InvoiceDetails> payList = new List<InvoiceDetails>();        
        List<Invoice__c> invList = [SELECT Id, Contact__r.email, Card_Payment__r.Status__c, Amount__c from Invoice__c  WHERE Id =: invoiceId AND Card_Payment__c !=''];        
       
        for(Invoice__c inv: invList) {            
            InvoiceDetails detail = new InvoiceDetails();            
          	detail.email = inv.Contact__r.email;
            detail.Amount = inv.Amount__c;
            detail.Status = inv.Card_Payment__r.Status__c;
            
            payList.add(detail);
            
        }
        
        return payList;        
    }
}