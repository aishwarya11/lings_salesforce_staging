global class VoucherExpiresScheduler implements Schedulable {
    //method to trigger Reminder to all user before the Event gets start
    global void execute(SchedulableContext sc) {
        BatchVoucherExpires Voucher = new BatchVoucherExpires();
        database.executeBatch(Voucher, 50);
    }
}