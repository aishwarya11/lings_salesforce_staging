@isTest
public class InvoiceControllerTest {
    
    testmethod public static void testInvoiceControllerTest() {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.demo';
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        String contactId=p.Id;
        String param1=contactId;
        Exception ee;
        
        User u = new User(Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', ProfileId = p.Id,
                            TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                            UserName=uniqueUserName);
        insert u;

        System.runAs ( new User(Id = UserInfo.getUserId()) ) {    
            Contact con = new Contact(lastname='lastname1',Ownerid=u.Id,Password__c='123pwd4598' );
            con.Email = 'revathy@rangerinc.cloud';
            con.Balance_Pending__c = 2.5;
            con.Payment_Alias_ID__c = 'conaliasid';
            insert con;
            
            InvoiceController IV = new InvoiceController(con.id);  
            IV.processPolicies(IV.getPolicies(con.id, System.today()));
            
            InvoiceController.SummaryWrapper SW = new InvoiceController.SummaryWrapper();
            SW.leftSideText='Deductible Coupon/Credit';
            SW.rightSideText='Right';
            
            InvoiceController.InsuranceWrapper IW = new InvoiceController.InsuranceWrapper();
            IW.name='AAA';
            IW.sdate='30/12/2018';
            IW.edate='19-12-2018';
            IW.days=13;
            IW.cost=100.00;
            IW.Premium = 2.5;
            IW.InsuranceType='flatrate';
            
            InvoiceController.InsuranceWrapper IW1 = new InvoiceController.InsuranceWrapper();
            IW1.name='AAA';
            IW1.sdate='30/12/2018';
            IW1.edate= null;
            IW1.days=13;
            IW1.cost=100.00;
            IW1.Premium = 2.5;
            IW1.InsuranceType='OnDemand';
            //IW1.InsuranceType='On demand';            
            
            Product2 PO = new Product2();
            PO.Name='Sample Product';
            PO.Insured_Value__c=100;
            insert PO;
            
            Object__c obj = new Object__c();
            obj.Product__c = PO.Id;
            obj.Insured_Value__c=100;
            obj.Customer__c = con.Id;
            insert obj;        
            
            Policy__c pol = new Policy__c();
            pol.Insurance_Type__c='FlatRate';
            pol.Premium__c=100;
            pol.Start_Date__c = system.today();
            pol.Invoice_Start_Date__c = system.today();
            pol.End_Date__c = system.today()+1;
            pol.Contact__c = con.Id;
            pol.Object__c = obj.id;
            insert pol;
            
            Policy__c pol1= new Policy__c();
            pol1.Insurance_Type__c='FlatRate';
            pol1.Premium__c=100;
            //pol1.Start_Date__c = system.today();
            pol1.Invoice_Start_Date__c = system.today();
            pol1.End_Date__c = null;
            pol1.Contact__c = con.Id;
            pol1.Object__c = obj.Id;
            insert pol1;              
            
            Voucher__c vc = new Voucher__c();
            vc.Name='Voucher999';
            vc.Amount__c=100;
            vc.From__c=System.today()-5;
            vc.To__c=System.today();
            vc.Fixed__c=System.today();
            vc.Usage_Limit__c = 10;
            insert vc;    
                    
            Voucher_Contact__c VCC = new Voucher_Contact__c();
            VCC.Contact__c=con.id;
            VCC.Voucher__c=vc.id;
            insert VCC;                
            
            String invMonth1='FEB';
            Integer totalDays1=10;
            Decimal totalCost1=10.00;
            String insType1='A';
            //String attId = createAttachmentRecord(con.Id, 'a04f40000073J5NAAU');
            List<InvoiceController.InsuranceWrapper> insList1 = new List<InvoiceController.InsuranceWrapper>();
            List<InvoiceController.SummaryWrapper> sumList = new List<InvoiceController.SummaryWrapper>();
            Map<String,InvoiceController.InsuranceWrapper> inswrapmap = new Map<String,InvoiceController.InsuranceWrapper>();
            
            InvoiceController IG = new InvoiceController();
                        
            Date testDate = System.today();    
            testDate = testDate.addDays(-10);
            IV = new InvoiceController(con.id, testDate);  
            IV.processPolicies(IV.getPolicies(con.id, testDate));

            testDate = testDate.addDays(40);
            IV = new InvoiceController(con.id, testDate);  
            IV.processPolicies(IV.getPolicies(con.id, testDate));

            // Create attachment
            IG.invWrapper = new InvoiceController.InvoiceWrapper();
            IG.invWrapper.invName = 'TestAttachment';

            Invoice__c Inv = IG.createInvoice(con.Id, 1.25,1,1,2.5);
            insert Inv;

            String attId = IG.createAttachmentRecord(con.Id, Inv.Id);

            IG.generateInvoiceEmail(con.id, 'revathy@rangerinc.cloud', attId);

            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            InvoiceController test = new InvoiceController(sc);
            test.contactId = con.id;
            test.sendPdf();

            Utility.processPayment(con.id, 1.00, 'invName', con.Payment_Alias_ID__c);
        }
    }

    @IsTest
    static void createAttachmentExceptionTest(){
        
        Test.startTest();
        System.runAs( new User(Id = UserInfo.getUserId()) ) {
            InvoiceController invController = new InvoiceController();
            try {
                String attId = invController.createAttachmentRecord(null, null);
            } catch (Exception e) {
                // No need to handle the exception
            }
        }
        Test.stopTest();
    }

    // Testing for theprocessPayment
    testmethod public static void testprocessPayment() { 
        try{
            List<String> testInv = new List<String>();
            for(String InvId : System.Label.TestAlaisId.split(',')) {
                testInv.add(InvId);
            }
            Invoice__c testInvoice = [SELECT Id from Invoice__c WHERE id = :testInv limit 1][0];
            Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
            Utility.processPayment(testInvoice.Id, 1.20, 'sampleinvoice', testInvoice.Id);
        }
        catch(Exception e){}
    }
}