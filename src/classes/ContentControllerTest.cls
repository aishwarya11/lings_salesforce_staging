@isTest
public class ContentControllerTest {
     
    testmethod public static void TestUserPolicy() {
        
	ContentVersion cvlist = new Contentversion(); 
	cvlist.Title = 'CZDSTOU'; 
	cvlist.PathOnClient = 'test'; 
	cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
	List<ContentVersion> cvl = new List<ContentVersion>(); 
	cvl.add(cvlist); 
	insert cvl;        

        // Instantiate content controller with all parameters in the page
        ContentController CC = new ContentController();
        //Calling method
        CC.file = cvlist.VersionData;
        CC.upload();

    }
}