@isTest
public class PublishTest {
    
    testmethod public static void testPublishTest(){
        Premium__c pre = new Premium__c();
        pre.On_Demand__c = 4;
        pre.FlatRate__c = 8;
        pre.Name = 'smartphone';
        insert pre;

        Product2 PO= new Product2();
        PO.Name = 'sample name';
        PO.Product_Type__c = 'Smartphone';
        PO.Price__c = 120;
        PO.Insured_Value__c = 12;
        insert PO;

        Publish.updateFlag(PO, PO.Id);
        }

}