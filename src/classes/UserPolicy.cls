public without sharing class UserPolicy {
    
    public UserPolicy(){}
    
    public List<PolicyWrapper> polList {get;set;}
    public Map<String, PolicyWrapper> polWrapperMap { get; set; }
    public Contact cont {get;set;}
    @testvisible private Id contactId;
    public String email {get;set;}
    public List<Object__c> obj{ get; set;}
    public decimal PremiumMonth;
    public String PolTime{ get; set;}
    public String Poldate{ get; set;} 
    
    //defining a data structure for object__c object's fields 
    public class PolicyWrapper {
        public String name { get; set; }
        public String Category { get; set; }
        public decimal Value { get; set; }
        public String type { get; set; }
        public decimal Premium { get; set; }
        public decimal month { get; set; }
        public decimal Total{ get; set;}
    }   
    
    public class LastRowWrapper{
        public String polname { get; set; }
        public String polCategory { get; set; }
        public decimal polValue { get; set; }
        public String poltype { get; set; }
        public decimal polPremium { get; set; }
        public decimal polmonth { get; set; }
        public decimal polTotal{ get; set;}
    }
    
    //constructor with apex page controller parameter
    public UserPolicy(ApexPages.StandardController controller) {
        contactId = controller.getId();
        polList = processPolicies();
    }
    
    //constructor with one parameter
    public UserPolicy(String inputId) {
        contactId = inputId;        
    }
    
    public List<PolicyWrapper> processPolicies() {
        PolTime = System.now().format('dd.MM.yyyy / HH:mm:ss');
        Poldate = System.now().format('dd.MM.yyyy');
        String key = '';
        polWrapperMap = new Map<String, PolicyWrapper>();
        Decimal PremiumCal =0.0; 
        Decimal TotalPremium = 0.0;
        Decimal TotalPremiumMonth = 0.0;
        PolicyWrapper polWrap = null;
        
        
        for(Object__c obj : [select Product_Name__c,Product__r.Id,Product__r.Product_Type__c,Insured_Value__c,
                             Is_deleted__c, Insurance_End_date__c,Premium_OnDemand__c,Premium_FlatRate__c,Coverage_type__c,
                             Customer__r.email, Policy__c, Policy__r.Active__c from Object__c 
                             WHERE Customer__c = :contactId AND Is_deleted__c = false AND 
                             Product__r.Id <>: System.Label.OKKProductId
                             ORDER BY Product_Name__c ASC]){
                                 
                                 email = obj.Customer__r.email;
                                 // Premium calculation w.r.t coverage type
                                 if(obj.Coverage_type__c == constants.FLATRATE_STRING ){
                                     PremiumCal = obj.Premium_FlatRate__c.setScale(2);
                                     //LM-607--Policy Charges for the flatrate is done -- 20SEPT2019
                                     PremiumMonth = Utility.getRoundedPremium(obj.Premium_FlatRate__c * Utility.numOfDays(null));
                                 } else { //if(obj.Coverage_type__c == constants.ONDEMAND_STRING || obj.Coverage_type__c == 'NA') 
                                     PremiumCal = utility.getRoundedPremium(obj.Premium_OnDemand__c);
                                     //LM-607--Policy Charges for the flatrate is done -- 20SEPT2019
                                     PremiumMonth = utility.getRoundedPremium(PremiumCal*Utility.numOfDays(null));
                                 }
                                 
                                 // MR - 29MAR19 - Changed from Literal to Function
                                 //PremiumMonth = utility.getRoundedPremium(PremiumCal*Utility.numOfDays(null));
                                 //Roll-up Summary only for the coverage type
                                 //if(obj.Coverage_type__c == constants.ONDEMAND_STRING || obj.Coverage_type__c == constants.FLATRATE_STRING){
                                 //21MAY2019 --LM-422 Only when the Coverage type is On-demand and FR the Total Premium will be taken.
                                 
                                 //Changing the Coverage type same as the Lings.ch -01APR2019 -LM - 345
                                 if(obj.Coverage_Type__c == Constants.FLATRATE_STRING){
                                     obj.Coverage_Type__c = 'Flatrate';
                                     TotalPremium+=PremiumCal;
                                     TotalPremiumMonth+=PremiumMonth;        
                                     //29MAY2019 -- Changes made to include the Event Object also.
                                 }else if(obj.Coverage_Type__c == Constants.ONDEMAND_STRING && obj.Policy__c != null && obj.Policy__r.Active__c == true){
                                     obj.Coverage_Type__c = 'On-Demand';                                     
                                     TotalPremium+=PremiumCal;
                                     TotalPremiumMonth+=PremiumMonth;                                
                                 }else {
                                     obj.Coverage_Type__c = 'nein';
                                 }
                                 //Product Type in German - 28MAR2019 -LM-345
                                 
                                 if(obj.Product__r.Product_Type__c.equalsignorecase('smartphone')){
                                     obj.Product__r.Product_Type__c = 'Mobiltelefonie'; 
                                 }else if(obj.Product__r.Product_Type__c.equalsignorecase('camera incl. accessories')){
                                     obj.Product__r.Product_Type__c = 'Foto und Videografie';
                                 }else if(obj.Product__r.Product_Type__c.equalsignorecase('bike')){
                                     obj.Product__r.Product_Type__c = 'Velo';
                                 }                                     
                                 
                                 
                                 //assinging values to policywrapper class instance (i.e) polWrap from object__c object 
                                 polWrap = new PolicyWrapper();
                                 polWrap.name = obj.Product_Name__c;
                                 polWrap.Category = obj.Product__r.Product_Type__c;
                                 polWrap.Value = obj.Insured_Value__c.setScale(2);
                                 polWrap.type = obj.Coverage_type__c;
                                 polWrap.Premium = PremiumCal;
                                 polWrap.month = PremiumMonth;
                                 
                                 polWrapperMap.put(obj.Id, polWrap);
                             }
        
        
        //Last Row             
        polWrap = new PolicyWrapper();
        polWrap.name ='Aktuelle Gesamtprämie (inkl. 5% Stempelsteuer)';           
        polWrap.Category ='';
        polWrap.type ='';
        polWrap.Premium = TotalPremium;
        polWrap.month = TotalPremiumMonth;
        
        polWrapperMap.put('All', polWrap);
        return polWrapperMap.values();
    }
    
    //To generate as pdf
    public PageReference sendPdf() { 
        PageReference pdf = Page.Policy;
        System.debug('PDF -->'+pdf);
        //template
        EmailTemplate template = [Select id from EmailTemplate where name=:'Order-confirmation'];
        Attachment[] a = [Select ParentId From Attachment where parentId = :template.Id];
        delete a;
        // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('id', this.contactId);
        System.debug(this.contactId);
        System.debug(pdf.getParameters().put('id', this.contactId));
        // the contents of the attachment from the pdf
        Blob body;
        
        try {
            // returns the output of the page as a PDF
            body = pdf.getContentAsPdf();            
        } catch (Exception e) {
            body = Blob.valueOf('No Data Available!!');
        }
        //create attachment
        Attachment att = new Attachment(ParentId=this.contactId,
                                        Body=body,
                                        Name='Policy ' + System.now().format('dd.MM.yyyy')+ '.pdf');
        
        insert att;
        
        //sending an email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);
        mail.setToAddresses(new String[] { UserInfo.getUserEmail() });
        mail.setTemplateId(template.id);
        mail.setTargetObjectId(this.contactId);
        mail.setSaveAsActivity(true);
        
        //Set email file attachments
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(att.Name);
        efa.setBody(att.Body);
        
        mail.setFileAttachments(new List<Messaging.Emailfileattachment> { efa });   
        
        try{
            if(!Test.isRunningTest()){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }catch(Exception bounceadd){
            Utility.sendException(bounceAdd, 'Error Sending the Policy to the customer');
        }
        
        return null;
    } 
    
    //To generate as OKK pdf
    @future(callout=true)
    public static void sendOKKPolicyPdf(String OKKObjId, string custId, String operation) {     
        
        try{
            System.debug('Inside the method 1st line');
            
            // Reva - To attach the AVB document according to the Object (i.e Whether it is OKK or RVG)
            Object__c traInsObj = [Select id,name,Product__c,Included_Date__c,Is_Deleted__c,Insurance_Start_Date__c,createdDate
                                   from Object__c where id=:OKKObjId limit 1];
            
            Contact con = [Select id,firstname,lastname,name from contact where id=:custId];
            ContentVersion cv = new ContentVersion();
            System.debug('Inside the UserPolicy Class');
            String PDFName = '';
            
            // Reva - To set the PDF File Name depends on the Object 
            if(traInsObj.Product__c == System.label.OKKProductId){
                PDFName = 'ÖKK TOURIST SUBITO '+con.FirstName +' '+ con.LastName +' '+ System.now().format('dd.MM.yyyy')+ '.pdf';
            }else if(traInsObj.Product__c == System.label.RVGProductId){
                PDFName = 'RVG Fortuna LINGS '+con.FirstName +' '+ con.LastName +' '+ System.now().format('dd.MM.yyyy')+ '.pdf';
            }
            
            // the contents of the attachment from the pdf
            Blob body;
            //PageReference pdf = Page.OKKpdfpage;
            PageReference pdf = ApexPages.currentPage();
            EmailTemplate template = new EmailTemplate();
            //template
            if(operation == 'CreatePDF'){
                
                // Reva - To attach the AVB document according to the Object (i.e Whether it is OKK or RVG)
                if(traInsObj.Product__c == System.label.OKKProductId){
                    pdf = Page.OKKpdfpage;
                    template = [Select id,body from EmailTemplate where name='OKK Insurance Confirmation'];
                    cv = [Select id,contentdocumentid,versiondata from contentversion where id=:System.label.AVB_OKK_Document];
                }else if(traInsObj.Product__c == System.label.RVGProductId){
                    pdf = Page.RVGPDFPage;
                    template = [Select id,body from EmailTemplate where name='RVG Insurance Confirmation'];
                    cv = [Select id,contentdocumentid,versiondata from contentversion where id=:System.label.AVB_RVG_Document];
                }
                
                
                Attachment[] a = [Select ParentId From Attachment where parentId = :template.Id];
                System.debug('Already generated attachment Id = '+template.Id);
                if(a.size() > 0){
                    System.debug('List of attachment is available for this template');
                    delete a;
                }
                // add parent id to the parameters for standardcontroller
                pdf.getParameters().put('id', OKKObjId);
                
                try {
                    // returns the output of the page as a PDF
                    body = pdf.getContent();
                    System.debug('Body of the Email = '+body);
                } catch (VisualforceException e) {
                    body = Blob.valueOf('No Data Available!!');
                }
            }
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            OrgWideEmailAddress[] owea = new List<OrgWideEmailAddress>();
            
            // Set list of people who should get the email
            List<String> BCCTo = new List<String>();
            
            // Reva - To decide the sender Address
            if(traInsObj.Product__c == System.label.OKKProductId){
                owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.OKKSenderAdress];
                for(String eAddr: System.Label.OKKBCCEmailAddresses.split(',')) {
                    BCCTo.add(eAddr);  
                }
            }else if(traInsObj.Product__c == System.label.RVGProductId){
                owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.RVGSenderAdress];
                for(String eAddr: System.Label.RVGBCCEmailAddresses.split(',')) {
                    BCCTo.add(eAddr);  
                } 
            }
                
            //Reva -- LM-1230 - Need to send BCC on both case
            mail.setBccAddresses(BccTo);
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
            mail.setToAddresses(new String[] { UserInfo.getUserEmail() });
            
            mail.setTemplateId(template.id);
            mail.setTargetObjectId(custId);
            mail.setSaveAsActivity(true);
            //Set email file attachments
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(PDFName);
            Messaging.Emailfileattachment AVBFile = new Messaging.Emailfileattachment();
            
            // Reva - To attach the AVB document according to the Object (i.e Whether it is OKK or RVG)
            if(traInsObj.Product__c == System.label.OKKProductId){
                AVBFile.setFileName('AVB ÖKK TOURIST SUBITO.PDF');
            }else if(traInsObj.Product__c == System.label.RVGProductId){
                AVBFile.setFileName('AVB Fortuna LINGS.PDF');
            }
            
            if(operation == 'CancelPDF'){
                
                List<Event_Object__c> eveObjList = [select id,name,Object__c,Event__c from Event_Object__c where Object__c=:OKKObjId];
                List<Event__c> eveList = [select id,contact__c,name,start_date__c,end_date__c,createddate from Event__c 
                                          where id=:eveObjList[0].Event__c];
                
                Date insDate = traInsObj.Included_Date__c;
                String tempCreateDate = DateTime.newInstance(insDate.year(),insDate.month(),insDate.day()).format('dd.MM.YYYY');
                
                Date d = eveList[0].start_date__c;
                String dt = DateTime.newInstance(d.year(),d.month(),d.day()).format('dd.MM.YYYY');
                
                Date dEnd = eveList[0].end_date__c;
                String dtEnd = DateTime.newInstance(dEnd.year(),dEnd.month(),dEnd.day()).format('dd.MM.YYYY');
                String messageBody = '';
                
                // Reva - To use the Email Template according to the Object (i.e Whether it is OKK or RVG)
                if(traInsObj.Product__c == System.label.OKKProductId){
                    mail.setSubject('Ihre Stornierungsbestätigung ÖKK TOURIST SUBITO');
                    messageBody = '<html><body>Grüazi '+ con.FirstName+' '+ con.LastName +'<br/><br/>Hiermit bestätigen wir Ihnen die'
                        +' Stornierung der Reiseversicherung ÖKK TOURIST SUBITO, die Sie am '+tempCreateDate
                        +' abgeschlossen haben.<br/><br/>Bitte beachten Sie, dass die Versicherungspolice für den Zeitraum von '
                        +dt+' bis und mit '+dtEnd+' dadurch verfällt und Sie während Ihrer Reise im Ausland nicht mehr '
                        +'vor unerwarteten Ausgaben für Arzt- und Spitalkosten durch unsere Reiseversicherung geschützt sind.'
                        +'<br/><br/>Liebe Grüsse<br/>Ihre ÖKK<br/><br/><br/><b>ÖKK</b><br/>Kranken- und Unfallversicherungen AG<br/>'
                        +'Bahnhofstrasse 13, Postfach, 7302 Landquart<br/><br/>Gratis-Hotline 0800 838 000<br/><br/>'
                        +'<p style="line-height: 1.4;text-decoration: underline; color: #56A5EC;">privatkunden@oekk.ch</p> '
                        +'<p style="line-height: 1.4;text-decoration: underline; color: #56A5EC;">www.oekk.ch</p></body> </html> ';
                }
                else if(traInsObj.Product__c == System.label.RVGProductId){
                    mail.setSubject('Ihre Versicherungsstornierung');
                    messageBody = '<html><body>Grüezi '+ con.FirstName+' '+ con.LastName +'<br/><br/>Mit Bedauern nehmen wir zu Kenntnis, '
                        +'dass Sie ihre Reiserechtsschutzversicherung stornieren möchten.<br/><br/>'
                        +'Selbstverständlich entsprechen wir Ihrem Wunsch und bestätigen Ihnen die beantragte Auflösung Ihrer Police.'
                        +'<br/><br/>Die Versicherungsdeckung ist somit nie in Kraft getreten.'
                        +'<br/><br/>Freundliche Grüsse<br/>Fortuna Rechtsschutzversicherung<br/><br/><br/>'
                        +'<b>Fortuna Rechtsschutz-Versicherungsgesellschaft AG</b><br/>Soodmattenstrasse 2<br/>'
                        +'8134 Adliswil 1<br/>Schweiz<br/></body> </html> ';
                }
                
                mail.setHtmlBody(messageBody);
                //mail.setPlainTextBody(template.Body);
            }else{
                efa.setBody(body);
                AVBFile.setContentType('application/pdf');
                AVBFile.setBody(cv.VersionData);
                mail.setFileAttachments(new List<Messaging.Emailfileattachment> {efa,AVBFile}); 
            }  
            if(!Test.isRunningTest()){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                System.debug('Mail sent Successfully');
            }
            
            if(operation == 'CancelPDF'){
                try{
                    Event_Object__c eo = [Select id,name,Object__c,Event__c from Event_Object__c where Object__c=:OKKObjId limit 1];
                    Event__c eve = new Event__c(Id=eo.Event__c);
                    delete eve;
                }catch(Exception bounceadd){
                    Utility.sendException(bounceAdd, 'Error Sending the Policy to the customer');
                }
            }
        }catch(Exception bounceadd){
            Utility.sendException(bounceAdd, 'Error Sending the Policy to the customer');
        }
    }
}