public class UIObjectWrapper {
    public static String CHF_STRING = 'CHF';    
    
    public list<SackInfo> bags;
    public list<Category> categories;
    public list<LingsObject> assets;
    
    //Definiton of the OnDemandInfo customsetting
    public class OnDemandInfo {
        public Decimal daily;
        public Decimal monthly;
        @testvisible String currencyCode = CHF_STRING;
        public boolean enabled;
    }
    
    //Definiton of the FlatRateInfo customsetting
    public class FlatRateInfo {
        public Decimal daily;
        public Decimal monthly;
        public Decimal savings;
        public Date periodFrom;
        public Date periodTo;
        public boolean enabled;
        public boolean renewable;        
        Integer duration = Utility.numOfDays(null);
        String currencyCode = CHF_STRING;
    }    
    
    //Defining the LingsObject section (i.e) Products owned by the customer 
    public class LingsObject {
        public String uuid;
        public String name;
        public Decimal price;
        public Decimal OKKTotPreValue;
        public Decimal RVGTotPreValue;
        public String OKKDownloadURL;
        public Boolean IsOKKEventEnds;
        public Boolean IsRVGEventEnds;
        @testvisible String currencyCode = CHF_STRING;
        public String thumbnail;
        public boolean published;
        public OnDemandInfo insuranceRate;
        public FlatRateInfo flatRate;
        public List<EventInfo> events;
        public List<SackInfo> sacks;
        public boolean insured;
        public boolean paid; 
        public boolean claimed;
        public String category;
        public boolean electricBike;
        public String shopName; 
        public Decimal paidAmount;
        //  public String state;
        public String serialNumber;
        public String comment;
        public Date purchasedDate;
        public Date warrantyDate;
        public String shopUrl;
        public String warrantyCard;
        public String fileContent1;
        public String contentType1;
        public String receipt;
        public String fileContent2; 
        public String contentType2;
        public String brand;
        public String questionOnOff;
        public String removeQuestionOnOff; //third variable to remove the Object from Bag. --28AUG2019
        public Integer index;   // Sort Order
        public boolean click;	//Object select and Unselect --28AUG2019
        //Initialzing some of  the fields of MYObject section
        public LingsObject() {
            insured = false;
            claimed = false;
            published = true;
            insuranceRate = new OnDemandInfo();
            flatRate = new FlatRateInfo();
            events = new List<EventInfo>();
            sacks = new List<SackInfo>();
        }
    }
    
    //Calculating the FlatRateInfo by passing the Premium Value 
    public static FlatRateInfo getFlatRateWrapper(Decimal premium) {
        FlatRateInfo rate = new FlatRateInfo();
        rate.daily = premium;
        rate.monthly = Utility.getRoundedPremium(premium * Utility.numOfDays(null));    
        if(rate.monthly == 0.00) {
            rate.monthly = 0.05;
        }
        rate.enabled = false;
        rate.renewable = false;
        rate.periodFrom = System.today();
        // 22APR19 - LM398 - MR - Fix for FlatRate EndDate
        rate.periodTo = System.today() + Utility.numOfDaysForEndDate(null);
        return rate;
    }
    
    //Calculating the OnDemandInfo by passing the Premium Value 
    public static OnDemandInfo getOnDemandWrapper(Decimal premium) {
        OnDemandInfo rate = new OnDemandInfo();
        rate.daily = premium;
        rate.monthly = premium * Utility.numOfDays(null);
        rate.enabled = false;
        return rate;
    }
    
    //Definition of BalanceInfo (i.e) ExpirationDate of the Amount already paid for the Insurance
    public class BalanceInfo {
        public Decimal amount;
        public Date expirationDate;
        @testvisible String currencyCode = CHF_STRING;
        
        // 06JUN19 - Initialization for Object Insurance Handling
        public BalanceInfo() {
            amount = 0.0;
            expirationDate = System.today();
        }
    }
    
    public class Category {
        public String category;
        public Integer sortOrder;
        public List<LingsObject> insuredProducts;        
    }
    
    //Defining the EventInfo fields 
    public class EventInfo {
        public String uuid;
        public String name;
        public Date periodFrom;
        public Date periodTo;
    }
    
    //definition of EventInfo fields with the corresponding object name
    public class EventWithObject {
        public String uuid;
        public String name;
        public Date periodFrom;
        public Date periodTo;
        public List<LingsObject> insuredProducts;
        // new element to create OKKObject -- 14Jan2020
        public Boolean createOKKFlag;
        public Boolean createRVGFlag;
        public Decimal OKKPerDayPre = 0;
        public Decimal RVGPerDayPre = 0;
        public Decimal OKKTotalPre = 0;
        public Decimal RVGTotalPre = 0;
        public Boolean IsOKKObjPresent;
        public Boolean IsRVGObjPresent;
        public Boolean isParentOfOKK;
        public Boolean isParentOfRVG;
    }
    
    //Defining the RucksackInfo fields --26JULY2019
    public class SackInfo {
        public String uuid;
        public String name;
        public String state;            // Insured / Partly Insured / Not Insured
        public boolean toggle;          // true / false
        public boolean hasClaim;       // true / false
        public integer objectsCount;
        public decimal premiumTotal;
 	//Added for the Premium calculation of bag.--28AUG2019
        public decimal nonInsuredPremium;
        public decimal insuredPremium; 
        public String category; //LM-643-- To reduce teh bag Overlap
        public List<Id> objectIds = new List<Id>();
        public integer index;   // MR - 03SEP19 - LM-643 Fix
    }
    
    //definition of SackInfo fields with the corresponding object name --26JULY2019
    public class SackWithObject {
        public String uuid;
        public String name;
        public String state;            // Insured / Partly Insured / Not Insured
        public boolean toggle;          // true / false
        public boolean hasClaim;        // true / false
        public boolean customerOn;      // true / false [This could be in the Object level]
        public integer objectsCount;
        public decimal premiumTotal;
        public string removeoff;
        public string removeUuid;        
        public List<String> objectRelatedWithOtherbag;
        public List<LingsObject> insuredProducts;
    }         
    
    // MR - 01AUG19 - Function to return the MyObject Wrapper for the given object
    public static LingsObject convertToWrapper(Object__c obj) {
        LingsObject myObj = new LingsObject();
        myObj.uuid = obj.id;
        myObj.index = Integer.valueOf(obj.Sort_Order__c);

        if(obj.Product__r.Product_Type__c == 'Bike' || obj.Product__r.Product_Type__c == 'E-Bike'){
            myObj.name = obj.Product__r.Name+' '+obj.Designation__c;                    
            myObj.brand = obj.Product__r.Name;
			
        }else{
            myObj.name = obj.Product__r.Name; 
            myObj.brand = obj.Product__r.Manufacturer__c; 
        }                      
        myObj.price = obj.Insured_Value__c.setScale(2);
        myObj.thumbnail = obj.Product__r.DisplayURL;                
        myObj.category = obj.Product__r.Product_Type__c;
        // For the Object to be Insured it must be Approved
        myObj.published = obj.Approved__c;
        myObj.paid = false; 
        // To get the Additional Information from Object
        myObj.warrantyDate = obj.Warranty_Date__c;
        myObj.shopName = obj.Shop_Name__c;
        myObj.paidAmount = obj.Actual_amount_paid__c;
        //beObj.Product_State__c = myObj.state;
        myObj.serialNumber = obj.Serial_Number__c;
        myObj.comment = obj.Own_Comment__c;
        myObj.purchasedDate = obj.Purchased_date__c;  
        myObj.shopUrl = obj.Shop_url__c;
        if(myObj.category == 'Travel Insurance'){
            myObj.OKKTotPreValue = obj.OKKTotalPremium__c; 
            myObj.RVGTotPreValue = obj.RVGTotalPremium__c; 
            myObj.OKKDownloadURL = obj.DisplayURL__c;
            myObj.IsOKKEventEnds = obj.IsOKKEventEnds__c;
            myObj.IsRVGEventEnds = obj.IsRVGEventEnds__c;
        }
        
        for(Attachment att: obj.Attachments) {
            if(att != NULL) {
                if (att.Name.containsIgnoreCase('Warranty')){
                    String attName = att.Name;
                    attName = attName.replaceAll('WarrantyCard', '');
                    myObj.warrantyCard = attName;                     
                } if (att.Name.containsIgnoreCase('Receipt')){
                    String attName2 = att.Name;
                    attName2 = attName2.replaceAll('Receipt', '');
                    myObj.receipt = attName2;                       
                }                
            }
        }
        /* LM-724-- This needs to be removed to avoid too many SOQL --08OCT2019
        List<Attachment> att = [SELECT Id, ParentId, Name, CreatedDate FROM Attachment WHERE Name LIKE '%Warranty%' and ParentId = :obj.Id ORDER BY CreatedDate desc limit 1];
        if(att != NULL){
            for(Attachment attach : att){
                String attName = attach.Name;
                attName = attName.replaceAll('WarrantyCard', '');
                myObj.warrantyCard = attName;   
            }                
        }  
        
        List<Attachment> att2 = [SELECT Id, ParentId, Name, CreatedDate FROM Attachment WHERE Name LIKE '%Receipt%' and ParentId = :obj.Id ORDER BY CreatedDate desc limit 1];
        if(att != NULL){
            for(Attachment attach2 : att2){
                String attName2 = attach2.Name;
                attName2 = attName2.replaceAll('Receipt', '');
                myObj.receipt = attName2;   
            }                
        }       
		*/  
        
        // Insured Flag
        if(!obj.Coverage_Type__c.equalsIgnoreCase('na') && (obj.Insurance_End_Date__c == null || obj.Insurance_End_Date__c >= System.today())) {
            myObj.insured = true;
            myObj.paid = true; 
        } else {
            myObj.insured = false;                  
        }
        
        if(!myObj.insured && obj.Policy__c != null && obj.Policy__r.End_Date__c != null) {
            Date polEndDate = obj.Policy__r.End_Date__c.Date();
            Date gmtToday = System.today(); 
            if((obj.Policy__r.Insurance_Type__c.equalsIgnoreCase('ondemand') && polEndDate == gmtToday) ||
               (obj.Policy__r.Insurance_Type__c.equalsIgnoreCase('flatrate') && polEndDate <= gmtToday)) {
                   myObj.paid = true;
               }
        }
        
        myObj.electricBike = obj.E_bike_electric_support__c;
        
        // Claim Flag
        if(obj.Claim__c != null) {
            myObj.claimed = true;
        } else {
            myObj.claimed = false;
        }
        
        // This enabled for the On Demand
        myObj.insuranceRate = UIObjectWrapper.getOnDemandWrapper(obj.Premium_OnDemand__c);
        if(myObj.insured && obj.Policy__r.Insurance_Type__c == 'OnDemand') {
            if(obj.Policy__r.Active__c) {
                myObj.insuranceRate.enabled = true;
            } else {
                myObj.insured = false;
            }
        }
        
        // This enabled for the FlatRate
        myObj.flatRate = UIObjectWrapper.getFlatRateWrapper(obj.Premium_flatRate__c);
        if(myObj.insured && obj.Policy__r.Insurance_Type__c == 'FlatRate') {
            myObj.flatRate.enabled = true;
            myObj.flatRate.renewable = obj.Flatrate_Renewal_Flag__c;
            myObj.flatRate.periodFrom = obj.Insurance_Start_Date__c;
            myObj.flatRate.periodTo = obj.Policy__r.End_Date__c.date();
        }
        
        try {
            myObj.flatRate.savings = Integer.valueOf(obj.Flatrate_Savings__c*100);                
        } catch(Exception se) {
            myObj.flatRate.savings = 0;
        }
        
        if(!myObj.flatRate.enabled) {
            //Event details.
            for(Event_Object__c eo: obj.Object_Events__r) {
                EventInfo eventWrap = new EventInfo();
                eventWrap.uuid = eo.Event__c;
                eventWrap.name = eo.Event__r.Name;
                eventWrap.periodFrom = eo.Event__r.Start_Date__c;
                eventWrap.periodTo = eo.Event__r.End_Date__c;
                myObj.events.add(eventWrap);
            }
            //Bag details.
            for(Rucksack_Object__c ro: obj.Object_Rucksacks__r) {
                SackInfo sackWrap = new SackInfo();
                sackWrap.uuid = ro.Rucksack__c;
                sackWrap.name = ro.Rucksack__r.Name;
                sackWrap.state = ro.Rucksack__r.Insurance_State__c;
                myObj.sacks.add(sackWrap);
            }            
        }
        
        return myObj;
    }
}