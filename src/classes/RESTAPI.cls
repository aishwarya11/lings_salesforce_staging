@RestResource(urlMapping='/Migrate/*')
global class RESTAPI {
	@HttpPost
    global static ResponseWrapper insertChildRecords() {
        ResponseWrapper response = new ResponseWrapper();
        
        RestRequest req = RestContext.request;
        String dataType = req.params.get('type');
        
        try {
            if(dataType.equalsIgnoreCase('Voucher__c')) {
                List<Voucher__c> voucherList = (List<Voucher__c>)JSON.deserialize(req.requestBody.toString(), List<Voucher__c>.class);
		        upsert voucherList external_id__c;
            } else if(dataType.equalsIgnoreCase('Contact')) {
                List<Contact> contactList = (List<Contact>)JSON.deserialize(req.requestBody.toString(), List<Contact>.class);
		        upsert contactList external_id__c;
            } else if(dataType.equalsIgnoreCase('Voucher_Contact__c')) {
                List<Voucher_Contact__c> vcList = (List<Voucher_Contact__c>)JSON.deserialize(req.requestBody.toString(), List<Voucher_Contact__c>.class);
		        upsert vcList external_id__c;
            } else if(dataType.equalsIgnoreCase('Product2')) {
                List<Product2> prodList = (List<Product2>)JSON.deserialize(req.requestBody.toString(), List<Product2>.class);
		        upsert prodList external_id__c;
            } else if(dataType.equalsIgnoreCase('Object__c')) {
                List<Object__c> objList = (List<Object__c>)JSON.deserialize(req.requestBody.toString(), List<Object__c>.class);
		        upsert objList external_id__c;
            } else if(dataType.equalsIgnoreCase('Event__c')) {
                List<Event__c> eventList = (List<Event__c>)JSON.deserialize(req.requestBody.toString(), List<Event__c>.class);
		        upsert eventList external_id__c;
            } else if(dataType.equalsIgnoreCase('Event_Object__c')) {
                List<Event_Object__c> eoList = (List<Event_Object__c>)JSON.deserialize(req.requestBody.toString(), List<Event_Object__c>.class);
		        upsert eoList external_id__c;
            } else if(dataType.equalsIgnoreCase('User')) {
                List<User> userList = (List<User>)JSON.deserialize(req.requestBody.toString(), List<User>.class);
		        try{
                    Database.insert(userList, false);
                }catch(DMLException uerr){
                    response.isSuccess = FALSE;
                    response.errors = new List<Error>();
                    response.errors.add(new Error(uerr.getDmlMessage(0),String.valueOf(uerr.getDmlIndex(0)),String.valueOf(uerr.getDmlType(0))));
                }
            } else if(dataType.equalsIgnoreCase('Policy__c')) {
                List<Policy__c> policyList = (List<Policy__c>)JSON.deserialize(req.requestBody.toString(), List<Policy__c>.class);
		        upsert policyList external_id__c;
            } else if(dataType.equalsIgnoreCase('Claim__c')) {
                List<Claim__c> claimList = (List<Claim__c>)JSON.deserialize(req.requestBody.toString(), List<Claim__c>.class);
		        upsert claimList external_id__c;
            } else if(dataType.equalsIgnoreCase('Invoice__c')) {
                List<Invoice__c> invList = (List<Invoice__c>)JSON.deserialize(req.requestBody.toString(), List<Invoice__c>.class);
		        upsert invList external_id__c;
            } else {
            	response.errors = new List<Error>();
            	response.errors.add(new Error('dataType was not processed', '100', 'InvalidInput'));
            }

            response.isSuccess = true;
        } catch(DMLException de){
            response.isSuccess = FALSE;
            response.errors = new List<Error>();
            response.errors.add(new Error(de.getDmlMessage(0),String.valueOf(de.getDmlIndex(0)),String.valueOf(de.getDmlType(0))));
        }
        
        return response;
    }
    
    global class ResponseWrapper{
        public Boolean isSuccess;
        public Map<String,String> successMap;
        public List<Error> errors;
    }
    
    global class Error{
        public String errorMessage;
        public String errorCode;
        public String errorType;
        
        public Error(String errorMessage, String errorCode, String errorType){
            this.errorMessage = errorMessage;
            this.errorCode = errorCode;
            this.errorType = errorType;
        }
    }
}