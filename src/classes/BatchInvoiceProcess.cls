//Generate monthly invoice to the user
// Runs at the end of the Month
global class BatchInvoiceProcess implements Database.Batchable<Contact>, Database.Stateful {
    Integer totalInvoiceMailCount = 0;
    List<Voucher_Contact__c> successVCUpdateList = new List<Voucher_Contact__c>();
    List<Voucher_Contact__c> failedVCUpdateList = new List<Voucher_Contact__c>();

    // MR - 06JUN19 - Added date as Parameter
    Id customerId = null;
    Date invEndDate = null;
    public BatchInvoiceProcess() {
        invEndDate = system.today();
    }
    
    public BatchInvoiceProcess(Integer invMonth, Integer invYear) {
        invEndDate = Date.newInstance(invYear, invMonth+1, 1);
    }

    public BatchInvoiceProcess(String custId, Integer invMonth, Integer invYear) {
        customerId = custId;
        invEndDate = Date.newInstance(invYear, invMonth+1, 1);
    }

    // Start Batchable for Every Contact, Policies and Pending Invoices
    global Iterable<Contact> start(Database.BatchableContext BC) {
        System.debug('BatchInvoiceProcess START');
        String queryString = 'SELECT Id, (Select id, name, Insurance_Type__c, Premium__c,Object__r.OKKTotalPremium__c,' +
            				 ' Object__r.OKKEventEndDate__c, Object__r.OKKEventStartDate__c, Object__r.IsOKKEventEnds__c,'+
            				 ' Object__r.RVGTotalPremium__c, Object__r.RVGEventEndDate__c, Object__r.RVGEventStartDate__c, '+
            				 ' Object__r.IsRVGEventEnds__c, Object__r.RVGPolicyNumber__c, '+
                             ' Object__r.Product_Name__c, Object__r.Insured_Value__c, Object__r.Product__c,' +
                             ' Start_Date__c, Invoice_Start_Date__c, End_Date__c, Number_of_days_Insured__c, Dates__c,' +
                             ' Contact__r.Voucher_In_Use__c, Contact__r.VIU_End_date__c, Contact__r.VIU_Balance__c, Contact__r.Balance_Pending__c, ' +
                             ' Invoice__c, Invoice__r.Status__c, Contact__r.email ' + 
                             ' FROM LingsContracts__r ' + 
                             ' WHERE Object__c != NULL ' + 
                             '      AND Status__c <> \'SplitSource\'' +
                             '      AND Invoice_Start_Date__c < :invEndDate ' + 
                             '      AND (Invoice__c = NULL OR Invoice__r.Status__c != \'Success\')' +
                             ' ORDER BY Start_Date__c ASC) FROM Contact';
        String whereClause = (customerId == null ? '' : ' WHERE Id = :customerId');

        List<Contact> query = Database.query(queryString + whereClause);
        return query;
    }
    
    //Execute Batch
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        System.debug('BatchInvoiceProcess EXECUTE');
        // Step 01: Generate the Invoice
        InvoiceController invGen = null;
        
        for(Contact con : scope) {
            if(con.LingsContracts__r.size() > 0) {
                invGen = new InvoiceController(con.Id, invEndDate);
                invGen.invWrapper = invGen.processPolicies(con.LingsContracts__r);
                
                // Step 02: Send the Invoice
                if(!Test.isRunningTest()) {
                    invGen.sendPdf();
                }
            }
        }
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchInvoiceProcess FINISH and also BatchPaymentProcess ran for Payment batch process');
    //Payment batch process runs after Invoice - Commented it for Simpler processing
       // BatchPaymentProcess be = new BatchPaymentProcess();
       // Database.executeBatch(be,1);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String>{'riyaz.mohamed@outlook.com'});
        mail.setSubject('BatchInvoiceProcess ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);         

        String body = 'BatchInvoiceProcess Status on Updating VoucherContact and Sending an Invoice Email to the User <br/>';
        body += '<br/> ' + (successVCUpdateList.size() + failedVCUpdateList.size()) + ' number of VoucherContact update processing were completed.';
        
        // Processing Policy Update List
        if(successVCUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Success VoucherContact Update details <br/>';
            
            for(Voucher_Contact__c successList: successVCUpdateList) {
                body += '<br/> ' + successList;
            }
        } else {
            body += '<br/> No VoucherContact updation happened.';
        }

        if(failedVCUpdateList.size() > 0) {
            body += '<br/> Refer the below list of Failed VoucherContact Update details <br/>';
            
            for(Voucher_Contact__c failedList: failedVCUpdateList) {
                body += '<br/> ' + failedList;
            } 
        } else {
            body += '<br/> No Failed VoucherContact Update processing happened.';
        }
        
        // Processing Email List
        body += '<br/> ' + totalInvoiceMailCount + ' number of Email processing were completed for sending the Invoice mail to the User.';

        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);       
        
		//LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            //LM-699--Populate big object --26SEPT2019
            Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
            ab.Audit_Name__c = 'InvoiceProcessing@' + System.now();
            ab.Contact__c = customerId;
            ab.Track_message__c = body;
            database.insertImmediate(ab);     

            if(failedVCUpdateList.size() > 0) {
                Messaging.sendEmail(mails);
            }
        }
    }
}