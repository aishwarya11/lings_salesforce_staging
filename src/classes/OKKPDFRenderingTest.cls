@isTest
public class OKKPDFRenderingTest {
    
    testmethod public static void TestOKKPDFRendering() {
        OKKPDFRendering opr = new OKKPDFRendering();     
        Contact con = new Contact(lastname='lastname1',Password__c='123pwd4598', Balance_Pending__c = 0.0);
        con.email='test1@test.comtest999';
        con.Token__c ='samplevalue';
        insert con;
        
        Object__c obj = new Object__c();
        obj.Product__c = System.label.OKKProductId;
        obj.Customer__c = con.Id;
        obj.Insured_Value__c = 100;
        insert obj;
        opr.addAttachmentsToContract(obj.Id, con.Id);
        opr.addAttachmentsToContract(con.Id, con.Id);
    }
    
}