global class LightningSelfRegisterController {
    public static String userNameSuffix = System.Label.UserNameSuffix;
    public LightningSelfRegisterController() {
        
    }
    
    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
    @TestVisible 
    private static boolean siteAsContainerEnabled(String communityUrl) {
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');
        return authConfig.isCommunityUsingSiteAsContainer();
    }
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
        if(!Test.isRunningTest()) {
            Site.validatePassword(u, password, confirmPassword);
        }
        return;
    }
    
    @AuraEnabled
    public static String selfRegister(String firstname ,String lastname, String email, String password, 
                                      String confirmPassword, String accountId, String regConfirmUrl, 
                                      String extraFields, String startUrl, Boolean includePassword, 
                                      String Birthday, String Street, String HouseNumber, String Postcode, 
                                      String Place, String phone, String radioGrp, String additionaltext) {
                                          Savepoint sp = null;
                                          try {
                                              sp = Database.setSavepoint();
                                              if (!Test.isRunningTest()) { 
                                                  if(firstname == NULL || lastname == NULL || email == null || password == NULL || 
                                                     confirmPassword == NULL || Birthday == NULL || Street == NULL || HouseNumber == NULL || 
                                                     Postcode == NULL || Place == NULL || phone == NULL || radioGrp == NULL){
                                                         return 'Bitte fehlende Angaben ausfüllen.'; 
                                                     }
                                              }
                                              if (lastname == null || String.isEmpty(lastname)) {
                                                  return 'Bitte fehlende Angaben ausfüllen.';
                                              }
                                              
                                              if (email == null || String.isEmpty(email)) {
                                                  return 'Bitte fehlende Angaben ausfüllen.';
                                              }
                                              
                                              User u = new User();
                                              //25OCT2019--During registration added the usernamesuffix with Username.
                                              u.Username = email+userNameSuffix;
                                              u.put('Email',email);
                                              
                                              u.FirstName = firstname;
                                              u.LastName = lastname;
                                              u.Street = Street;
                                              u.DOB__c = Date.valueOf(Birthday);
                                              u.House_Number__c = HouseNumber;
                                              u.PostalCode = Postcode;
                                              u.City = Place;
                                              u.Phone = phone;
                                              //25OCT2019--Default first login value
                                              u.First_Login__c = True;
                                              u.How_do_you_know_about_us__c = radioGrp;
                                              //LM-741 -- Passing value to the backend --31OCT2019
                                              
                                              if(radioGrp == 'other') {
                                                  u.Others__c = additionaltext;
                                              } else if (radioGrp == 'recommendation') {
                                                  u.Recommendation__c = additionaltext;
                                              } 
                                              
                                              String networkId = Network.getNetworkId();
                                              
                                              // If using site to host the community the user should not hit s1 after logging in from mobile.
                                              if(networkId != null && siteAsContainerEnabled(Network.getLoginUrl(networkId))) {
                                                  u.put('UserPreferencesHideS1BrowserUI',true);
                                              }
                                              
                                              String nickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
                                              nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                                              u.put('CommunityNickname', nickname);
                                              
                                              if (extraFields != null) {
                                                  List<Object> extraFieldsList = (List<Object>) JSON.deserializeUntyped(extraFields);        
                                                  for (Object thisFieldObject : extraFieldsList) {
                                                      Map<String,Object> thisField = (Map<String,Object>) thisFieldObject;
                                                      Schema.SObjectField sof = Schema.SObjectType.User.fields.getMap().get((String) thisField.get('fieldPath'));
                                                      u.put(sof, thisField.get('value'));
                                                  }
                                              }
                                              
                                              if (includePassword) {    
                                                  if (!isValidPassword(password, confirmPassword)) {
                                                      return Label.site.passwords_dont_match;
                                                  }
                                                  validatePassword(u, password, confirmPassword);
                                              }
                                              else {
                                                  password = null;
                                              }
                                              
                                              // lastName is a required field on user, but if it isn't specified, we'll default it to the username
                                              String userId = Site.createPortalUser(u, accountId, password);
                                              
                                              // create a fake userId for test.
                                              if (Test.isRunningTest()) {
                                                  userId = 'fakeUserId';           
                                              }
                                              if (userId != null) { 
                                                  
                                                  if (password != null && password.length() > 1) {
                                                      //Added at 04NOV2019
                                                      if(!Test.isRunningTest()) {
                                                       	Utility.registerContact(userId, password);   
                                                      }                                                      
                                                      //25OCT2019--Assigned register confirm url to redirect to the register confirmed page
                                                      ApexPages.PageReference confirmRef = new PageReference(regConfirmUrl);                                       
                                                      if(!Test.isRunningTest()) {
                                                          aura.redirect(confirmRef);                    
                                                      }                    
                                                  }
                                              }
                                              return null;
                                          }
                                          catch (Exception ex) {
                                              Utility.sendException(ex, 'Failed to register in mobile'+ex.getStackTraceString());
                                              Database.rollback(sp);
                                              return ex.getMessage();            
                                          }
                                      }
    
    @AuraEnabled
    public static List<Map<String,Object>> getExtraFields(String extraFieldsFieldSet) { 
        List<Map<String,Object>> extraFields = new List<Map<String,Object>>();
        Schema.FieldSet fieldSet = Schema.SObjectType.User.fieldSets.getMap().get(extraFieldsFieldSet);
        if(!Test.isRunningTest()) {
            if (fieldSet != null) {
                for (Schema.FieldSetMember f : fieldSet.getFields()) {
                    Map<String, Object> fieldDetail = new Map<String, Object>();
                    fieldDetail.put('dbRequired', f.getDBRequired());
                    fieldDetail.put('fieldPath', f.getFieldPath());
                    fieldDetail.put('label', f.getLabel());
                    fieldDetail.put('required', f.getRequired());
                    fieldDetail.put('type', f.getType());
                    fieldDetail.put('value', '');   // client will populate
                    extraFields.add(fieldDetail);
                }}}
        return extraFields;
    }
    
    @AuraEnabled
    global static String setExperienceId(String expId) {
        // Return null if there is no error, else it will return the error message 
        try {
            if (expId != null) {
                Site.setExperienceId(expId);   
            }
            return null; 
        } catch (Exception ex) {
            return ex.getMessage();            
        }        
    }  
}