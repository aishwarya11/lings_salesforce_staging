@isTest
public class BatchEventRemainderTest {
    public static testmethod void BatchPopulateTest(){
         // Required test data for PolicyExpires Batch Job
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName= 'john99956486@acme.com',CommunityNickname='Lings456654');
        insert u;
        
        Contact con = new Contact();
        con.lastname ='Name123';
        con.Password__c='123pwd4598';
        con.Ownerid=u.Id;
        con.Email='revathypandian97@gmail.com';
        insert con;
        
        Attachment objAtt = new Attachment();
     	objAtt.Name = 'Test';
     	objAtt.body = Blob.valueof('string');
    	objAtt.ParentId = con.Id;
     	insert objAtt;
        
        Event__c EV = new Event__c();
        EV.Name='AAA';
        EV.Start_Date__c=System.today()+1;
        EV.Contact__c=con.id;
        EV.End_Date__c=System.today();
        insert EV;
        
        Test.startTest();
        
		// Instantiate beforeEventRemainder apex batch class
            BatchEventRemainder BCE = new BatchEventRemainder(); // Add you Class Name
            DataBase.executeBatch(BCE);
           
        Test.stopTest();
    }
    
}