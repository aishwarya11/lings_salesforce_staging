@isTest
public class UniqueUserNameTest {
    testmethod public static void testUniqueUserName() {
        //declaring required test data 
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        UserName='standarduserusername@testorg.com',CommunityNickname='Lings78');
        insert u;
        
        User u1 = new User(Alias = 'standt', UserName='stsfewffffff1232egandarduser@testorg.com',
        EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', 
        Email='userusernamkdkdkdkdfsdfsdkfjsdkfjdskfjsdkfjiefaeiwiiefdfeiowriewewe@testorg.com',CommunityNickname='Lingsss');
        insert u1;
        
        List<String> valueCopy = new List<String>();
        
        //calling methods from UniqueUserName class 
        valueCopy.add(u.Email);
        valueCopy.add(u1.Email);
        
        //Instantiate the UniqueUserName class 
        UniqueUserName UUN = new UniqueUserName();
        UUN.getUniqueName(u.Email);
        UUN.getUniqueName(u1.Email);
        UUN.getSingleStr(valueCopy);
        
    }

}