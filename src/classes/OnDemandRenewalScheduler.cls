global class OnDemandRenewalScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchOnDemandRenewal batch = new BatchOnDemandRenewal();
        database.executeBatch(batch, 10);
    }
}