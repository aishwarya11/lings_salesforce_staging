public class ConvertsObjectToJSON
{
    //Return the JSON string 
    public static string getJsonFromSObject()
    {
        String jsonData = '';
        try{
            
            String sObjectFields = '';
            
            
            Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
            //system.debug('==>m is==>'+m);
            Schema.SObjectType s = m.get('Object__c') ;
            //system.debug('==>Sobject Type is ==>'+s);
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            //system.debug('==>DescribeSObjectResult==>'+r);
            Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
            //system.debug('==>fields==>'+fields);
            
            //Create JSON
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeFieldName('attributes');
            gen.writeStartObject();
            gen.writeStringField('type', string.valueof(s)); 
            gen.writeEndObject();
            gen.writeFieldName('fields');
            gen.writeStartObject();            
            for(String f : fields.keyset())
            {
                Schema.DescribeFieldResult describeResult = fields.get(f).getDescribe();  
                gen.writeStringField(describeResult.getName(), string.valueof(describeResult.getSoapType()));
                //system.debug('==>describeResult==>'+describeResult.getName()+'Object Type =>'+describeResult.getSoapType());
            }                        
            
            gen.writeEndObject();
            gen.writeEndObject();
            gen.writeEndArray();
            //Getting the JSON String Data
            jsonData = gen.getAsString();
            
        }
        catch(Exception ex){
            
        }
        return jsonData;
    }
}