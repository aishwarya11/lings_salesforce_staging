@RestResource(urlMapping='/send/*')
global without sharing class MobileUserPolicyService {
    @HttpPost
    global static String policy(String langCode){
        return UIHomeController.sendPolicyEmail(langCode);
    }
}