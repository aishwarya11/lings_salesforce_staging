@RestResource(urlMapping='/*')
global class sendTestNotification {
    
    @HttpGet
    global static String SendFCMNotification(){
 
        /* string message ='test msg';
        string title ='Hello test 123';
        
           Messaging.PushNotification msg = new Messaging.PushNotification();
            Map<String, Object> androidPayload = new Map<String, Object>();
            
            androidPayload.put('message', message); 
            androidPayload.put('Title', title);             
            msg.setPayload(androidPayload);  
            msg.setTtl(100);
            
            String userId = '0053O000000t7UU';   
            
            // Adding recipient users to list
            Set<String> users = new Set<String>();
            
            users.add(userId); 
            
            msg.send('Lings_Mobile_App', users);  
         return ''; */
        //Your logic here for now I am using hardcoded values
        
        string deviceToken = 'fyTx5jIq4nM:APA91bE92r49wM72RdEKvA2RZF1P9bPKwKzsPKbxFM6cpVQwBduX0nz06aLKslpqWqalDG8VEuuHaQrLUMNg-kXF-CG6qWvDODARfAB_BgH94dnY5T_zm49bDE5XvpUsKuCigI0pPJV7'; //your device token
        string message ='test msg';
        string title ='Hello test 123';
        
        Http http = new Http(); 
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fcm.googleapis.com/fcm/send');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Key=AIzaSyD-icPWuXH5L58gSsNXTISJs9HvGROlZME'); //legacy server key
        //request.setHeader('Content-Length', '0');
        
        // Set the body as a JSON object
        request.setBody('{"to":"'+deviceToken+'", "notification":{"body":"'+message+'", "title":"'+title+'"} }');
        HttpResponse response = http.send(request);
        
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus()+ ' '+response.getBody());
            return string.valueOf(response.getStatus());
        } else {
            System.debug(response.getBody());
            
            Messaging.PushNotification msg = new Messaging.PushNotification();
            Map<String, Object> androidPayload = new Map<String, Object>();
            
            androidPayload.put('message', message); 
            androidPayload.put('Title', title);             
            msg.setPayload(androidPayload);  
            msg.setTtl(100);
            
            String userId = '0051t000002eMEn';   
            
            // Adding recipient users to list
            Set<String> users = new Set<String>();
            
            users.add(userId); 
            
            msg.send('Lings_Mobile_App', users);             
            return string.valueOf(response.getStatus());
        } 
    }
}