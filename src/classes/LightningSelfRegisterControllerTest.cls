@IsTest(SeeAllData = true)
public with sharing class LightningSelfRegisterControllerTest {
    LightningSelfRegisterController LSRC = new LightningSelfRegisterController();
    
    
    /* Verifies that IsValidPassword method with various password combinations. */
    @IsTest
    static void testIsValidPassword() {
        System.assert(LightningSelfRegisterController.isValidPassword('password?@12334', 'password?@12334') == true);
        System.assert(LightningSelfRegisterController.isValidPassword('password?@12334', 'dummyPassword') == false);
        System.assert(LightningSelfRegisterController.isValidPassword('password?@12334', null) == false);
        System.assert(LightningSelfRegisterController.isValidPassword(null, 'fakePwd') == false);
    }
    
    @IsTest
    static void testSiteAsContainerEnabled() {
        System.assertNotEquals(null, LightningSelfRegisterController.siteAsContainerEnabled('https://portaleu1-developer-edition.eu11.force.com'));
    }
    
    /* Verifies the selfRegistration method flow with various invalid inputs */
    @IsTest
    static void testSelfRegistration() {
        Map < String, String > paramsMap = initializeParams();
        System.assertNotEquals(null, paramsMap);
        
        System.assertEquals('Bitte fehlende Angaben ausfüllen.', LightningSelfRegisterController.selfRegister(paramsMap.get('firstName'), '', paramsMap.get('email'), paramsMap.get('password'), paramsMap.get('confirmPasswordCorrect'), null, paramsMap.get('regConfirmUrl'), null, paramsMap.get('startUrl'), true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
        System.assertEquals('Bitte fehlende Angaben ausfüllen.', LightningSelfRegisterController.selfRegister(paramsMap.get('firstName'), paramsMap.get('lastName'), '', paramsMap.get('password'), paramsMap.get('confirmPasswordCorrect'), null, paramsMap.get('regConfirmUrl'), null, paramsMap.get('startUrl'), true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
        System.assertEquals('Bitte fehlende Angaben ausfüllen.', LightningSelfRegisterController.selfRegister(null, paramsMap.get('lastName'), '', null, paramsMap.get('confirmPasswordCorrect'), null, paramsMap.get('regConfirmUrl'), null, paramsMap.get('startUrl'), true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
        System.assertEquals(Label.site.passwords_dont_match, LightningSelfRegisterController.selfRegister(paramsMap.get('firstName'), paramsMap.get('lastName'), paramsMap.get('email'), paramsMap.get('password'), paramsMap.get('confirmPasswordWrong'), paramsMap.get('accountId'), paramsMap.get('regConfirmUrl'), null, paramsMap.get('startUrl'),  true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
        System.assertNotEquals(null, LightningSelfRegisterController.selfRegister(paramsMap.get('firstname'), paramsMap.get('lastname'), '', paramsMap.get('password'), paramsMap.get('confirmPassword'), paramsMap.get('accountId'), paramsMap.get('regConfirmUrl'), paramsMap.get('extraFields'), paramsMap.get('startUrl'), true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
    }
    
    
    /* Verifies the selfRegistration flow for valid inputs */
    @IsTest
    static void testSelfRegisterWithProperCredentials() {
        Map < String, String > paramsMap = initializeParams();
        System.assertEquals(null, LightningSelfRegisterController.selfRegister(paramsMap.get('firstName'), paramsMap.get('lastName'), paramsMap.get('email'), paramsMap.get('password'), paramsMap.get('confirmPasswordCorrect'), null, paramsMap.get('regConfirmUrl'), null, paramsMap.get('startUrl'), true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
    }
    
    /* Verifies SelfRegistration flow with an accounId that is created within the test */
    @IsTest
    static void testSelfRegisterWithCreatedAccount() {
        Account acc = new Account(name = 'test acc');
        insert acc;
        List < Account > accounts = [SELECT Id FROM Account LIMIT 1];
        System.assert(!accounts.isEmpty(), 'There must be at least one account in this environment!');
        String accountId = accounts[0].Id;
        Map < String, String > paramsMap = initializeParams();
        System.assertEquals(null, LightningSelfRegisterController.selfRegister(paramsMap.get('firstName'), paramsMap.get('lastName'), paramsMap.get('email'), paramsMap.get('password'), paramsMap.get('confirmPasswordCorrect'), accountId, paramsMap.get('regConfirmUrl'), null, paramsMap.get('startUrl'), false, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
    }
    
    
    
    @IsTest
    static void testGetNonEmptyExtraFields() {
        System.assertEquals(new List < Map < String, Object >> (), LightningSelfRegisterController.getExtraFields('extraFieldsFieldSet'));
    }
    
    /* Verifies validation of extraFields within the Self Registration flow */
    @IsTest
    static void testGetExtraFieldsInSelfRegistration() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='dbds@mcode.llp',CommunityNickname='Lings456');
        insert u;
        List < Map < String, Object >> fieldlist = new List < Map < String, Object >> ();
        Map < String, String > paramsMap = initializeParams();
        Map < String, Object > fieldMap = new Map < String, Object > ();
        fieldMap.put('description', 'new field');
        fieldMap.put('fieldPath', 'dummyPath');
        fieldMap.put('DBRequired','sample value');
        fieldMap.put('Label','sample label');
        fieldMap.put('Required','sample value');
        fieldMap.put('Type','sample type');
        fieldlist.add(fieldMap);
        String extraFields = JSON.serialize(fieldlist);
        System.assertNotEquals(null, LightningSelfRegisterController.selfRegister(paramsMap.get('firstName'), paramsMap.get('lastName'), paramsMap.get('email'), paramsMap.get('password'), paramsMap.get('confirmPasswordCorrect'), null, paramsMap.get('regConfirmUrl'), extraFields, paramsMap.get('startUrl'), true, paramsMap.get('Birthday'), paramsMap.get('Street'), paramsMap.get('HouseNumber'), paramsMap.get('Postcode'), paramsMap.get('Place'), paramsMap.get('phone'), paramsMap.get('radioGrp'), paramsMap.get('additionaltext')));
        LightningSelfRegisterController.setExperienceId(u.id);
    }
    
    @IsTest
    static void LightningSelfRegisterControllerInstantiation() {
        LightningSelfRegisterController controller = new LightningSelfRegisterController();
        System.assertNotEquals(controller, null);
    }
    
    /* Helper method to initialize the parameters required for SelfRegistration. */
    private static Map < String, String > initializeParams() {
        Map < String, String > paramsMap = new Map < String, String > ();
        String firstName = 'test';
        String lastName = 'User';
        String email = 'testUser@salesforce.com';
        String password = 'testuser123';
        String confirmPasswordCorrect = 'testuser123';
        String confirmPasswordWrong = 'wrongpassword';
        String accountId = 'testuser123';
        String regConfirmUrl = 'http://registration-confirm.com';
        String startUrl = 'http://my.company.salesforce.com';
        String Birthday = String.valueof(Date.newInstance(2016, 12, 9));
        String Street = 'Main Street';
        String HouseNumber = '111';
        String Postcode = '1000';
        String Place = 'Chennai';
        String phone = '7854125689';
        String radioGrp = 'User';
        String additionaltext = 'User';
        String extraFields = 'testValue2019';
        paramsMap.put('firstName', firstName);
        paramsMap.put('lastName', lastName);
        paramsMap.put('email', email);
        paramsMap.put('password', password);
        paramsMap.put('extraFields', extraFields);
        paramsMap.put('confirmPasswordCorrect', confirmPasswordCorrect);
        paramsMap.put('confirmPasswordWrong', confirmPasswordWrong);
        paramsMap.put('accountId', accountId);
        paramsMap.put('regConfirmUrl', regConfirmUrl);
        paramsMap.put('startUrl', startUrl);
        paramsMap.put('Birthday', Birthday); 
        paramsMap.put('Street', Street); 
        paramsMap.put('HouseNumber', HouseNumber); 
        paramsMap.put('Postcode', Postcode); 
        paramsMap.put('Place', Place); 
        paramsMap.put('phone', phone); 
        paramsMap.put('radioGrp', radioGrp); 
        paramsMap.put('additionaltext', additionaltext);     
        return paramsMap;
    }
}