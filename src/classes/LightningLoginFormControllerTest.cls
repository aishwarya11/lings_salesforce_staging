@IsTest(SeeAllData = true)
public with sharing class LightningLoginFormControllerTest {

 @IsTest
 static void testLoginWithInvalidCredentials() {
  System.assertEquals('Fehlerhafte Zugangsdaten.', LightningLoginFormController.login('testUser', 'fakepwd', null));
 }
 @IsTest
 static void setExperienceId() {
          Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
     User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
     EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
     LocaleSidKey='en_US',ProfileId = p.Id,
     TimeZoneSidKey='America/Los_Angeles', 
     UserName='dbds@mcode.llp',CommunityNickname='Lings7898');
        insert u;
     LightningLoginFormController.setExperienceId( u.Id);
  //System.assertEquals('passing null value', LightningLoginFormController.setExperienceId( null));
 }
 @IsTest
 static void LightningLoginFormControllerInstantiation() {
  LightningLoginFormController controller = new LightningLoginFormController();
  System.assertNotEquals(controller, null);
 }
 @IsTest
 static void getForgotPasswordUrl() {
  //System.assertEquals(null, LightningLoginFormController.getForgotPasswordUrl());
 }
 @IsTest
 static void testIsUsernamePasswordEnabled() {
  System.assertEquals(true, LightningLoginFormController.getIsUsernamePasswordEnabled());
 }

 @IsTest
 static void testIsSelfRegistrationEnabled() {
  System.assertEquals(false, LightningLoginFormController.getIsSelfRegistrationEnabled());
 }

 @IsTest
 static void testGetSelfRegistrationURL() {
  System.assertEquals(null, LightningLoginFormController.getSelfRegistrationUrl());
 }

 @IsTest
 static void testgetForgotPasswordUrl() {
  System.assertEquals(null, LightningLoginFormController.getForgotPasswordUrl());
 }
    
 @IsTest
 static void testAuthConfig() {
  Auth.AuthConfiguration authConfig = LightningLoginFormController.getAuthConfig();
  System.assertNotEquals(null, authConfig);
 }
}