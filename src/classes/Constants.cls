public class Constants {
    //declaring constant values 
    public static String ONDEMAND_STRING = 'OnDemand';
    public static String FLATRATE_STRING = 'FlatRate';
    
    public static String POL_STATUS_INSURED = 'Insured';
    public static String POL_STATUS_EXPIRED = 'Expired';    

    public static String AUTHORIZED_STRING = 'AUTHORIZED';    
    
    public static String RECRUIT_FRIEND = 'a0F0Q000001RbKwUAK';		//LM-647--Voucher ID for Friend Recruit Friend--26AUG2019
    
    public static String SP_TERMINAL_ID = System.Label.Terminal_ID;
    public static String SP_CUSTOMER_ID = System.Label.Customer_ID;
    
    public static String SP_ALIAS_INSERT_EP = System.Label.SaferPay+'/api/Payment/v1/Alias/Insert';
    public static String SP_ALIAS_ASSERT_EP = System.Label.SaferPay+'/api/Payment/v1/Alias/AssertInsert';
    public static String SP_TXN_EP = System.Label.SaferPay+'/api/Payment/v1/Transaction/AuthorizeDirect';
    public static String SP_BASIC_AUTH = 'Basic ' + System.Label.SaferPayAuth;
    public static String SP_Tnx_Initialize_EP = System.Label.SaferPay+'/api/Payment/v1/Transaction/Initialize';
    public static String SP_Tnx_Authorize_EP = System.Label.SaferPay+'/api/Payment/v1/Transaction/Authorize';
    public static String SP_Tnx_Cancel_EP = System.Label.SaferPay+'/api/Payment/v1/Transaction/Cancel';
    public static String SP_Tnx_Capture_EP = System.Label.SaferPay+'/api/Payment/v1/Transaction/Capture';
    
    public static Map<String, String> ERROR_VOUCHER_EXPIRED = new Map<String, String> {
        'en' => 'We are sorry but the voucher has expired',
        'de' => 'Bitte entschuldige, dieser Gutscheincode ist bereits abgelaufen und kann nicht wieder verwendet werden. ',
        'fr' => ''
    };
        
    public static Map<String, String> ERROR_VOUCHER_LIMIT = new Map<String, String> {
        'en' => 'Sorry, but the limit has already been reached',
        'de' => 'Bitte entschuldige, das Limit dieses Gutscheincodes ist erreicht. Daher kann dieser Code nicht wieder verwendet werden.',
        'fr' => ''
    };
        
    public static Map<String, String> ERROR_VOUCHER_REUSE = new Map<String, String> {
        'en' => 'We are sorry but you have used the code before',
        'de' => 'Bitte entschuldige, du kannst einen Gutscheincode jeweils nur einmal einlösen.',
        'fr' => ''
    };
        
    public static Map<String, String> ERROR_VOUCHER_PAYOUT = new Map<String, String> {
        'en' => 'Before a new code can be redeemed, must the old voucher credits are used up',
        'de' => 'Bitte entschuldige, du kannst nur jeweils einen Gutschein gleichzeitig benutzen und musst für den 2. Gutschein dein 1. Guthaben zuerst aufbrauchen.',
        'fr' => ''
    };
        
    public static Map<String, String> ERROR_VOUCHER_INVALID = new Map<String, String> {
        'en' => 'We are sorry but this code is invalid',
        'de' => 'Bitte entschuldige, du hast einen ungültigen Gutscheincode verwendet.',
        'fr' => ''
    };
        
    public static Map<String, String> ERROR_VOUCHER_INUSE = new Map<String, String> {
        'en' => 'voucher can not be used due to existing voucher balance',
        'de' => 'Gutschein kann wegen vorhandenem Gutscheinguthaben nicht verwendet werden',
        'fr' => ''
    };    
        //Password Change Error message
        
    public static Map<String, String> ERROR_OLD_PWD = new Map<String, String> {
        'en' => 'The specified password is invalid.',
        'de' => 'Das angegebene Passwort ist ungültig',
        'fr' => ''
    };
        //Existing User Error message
      public static Map<String, String> ERROR_EXISTING_USER = new Map<String, String> { //LM-417 - Error message changed req by Henrik 
        'en' => 'Your credentials are not valid. If you have forgotten your password, please reset it.',
        'de' => 'Deine Anmeldeinformationen sind ungültig. Wenn du dein Passwort vergessen hast, bitte setze es zurück.',
        'fr' => ''
    };  
        //In Correct Credentials Error.
      public static Map<String, String> ERROR_INCORRECT_DATA = new Map<String, String> {
        'en' => 'Incorrect access data.',
        'de' => 'Falsche Zugangsdaten',
        'fr' => ''
    };  
        //Postal Code Error
      public static Map<String, String> ERROR_POSTAL_CODE = new Map<String, String> {
        'en' => 'You have to have a Swiss residence',
        'de' => 'Du musst einen schweizer Wohnsitz haben',
        'fr' => ''
    };        
        //First Login
        public static Map<String, String> ERROR_FIRST_LOGIN = new Map<String, String> {
        'en' => 'So that LINGS is even faster and safer for you, there was a bigger update. You have received an e-mail to reset your password. After that, everything will run for you as before.See you in your mailbox. ;-)',
        'de' => 'Damit LINGS für dich noch schneller und sicherer ist, gab es ein grösseres Update. Du hast eine E-Mail bekommen um dein Passwort neu zu setzen. Danach läuft für dich alles wieder wie vorher.Bis gleich in deinem Postfach. ;-)',
        'fr' => ''
    };
        //Error for Password User.
        public static Map<String, String> ERROR_Locked_User = new Map<String, String> {
        'en' => 'You entered your password too many times incorrectly. Therefore, we have blocked your profile for security reasons. Please try again in 15 minutes.',
        'de' => 'Du hast dein Passwort zu oft falsch eingegeben. Daher haben wir aus Sicherheitsgründen dein Profil gesperrt. Bitte versuche es in 15 Minuten noch einmal.',
        'fr' => ''
    };
        //Error for Invalid Token.
        public static Map<String, String> ERROR_Invalid_Token = new Map<String, String> {
        'en' => 'Unfortunately you used an old link. Please use the link "forgot password" again and use the new link in the mail.',
        'de' => 'Leider hast du einen alten Link benutzt. Bitte benutze den Link "Passwort vergessen" nochmals und verwende den neuen Link im Mail.',
        'fr' => ''
    };   
        //Error for Invalid Token.
        public static Map<String, String> ERROR_Login_Attempts = new Map<String, String> {
        'en' => 'Number of attempts:',
        'de' => 'Anzahl der Versuche:',
        'fr' => ''
    };
        //Error for Invalid Token.
        public static Map<String, String> ERROR_Attempts_Remaining = new Map<String, String> {
        'en' => ' Attempt remaining :',
        'de' => ' Versuch verbleibend:',
        'fr' => ''
    };   
        //LM-709-- Incorrect UserName error --04OCT2019
        public static Map<String, String> ERROR_USERNAME = new Map<String, String> { //Updated the errmsg -- 09OCT2019
        'en' => 'Please Enter the Correct UsernameThis e-mail address is not registered with us. Please use the e-mail address you use for LINGS.',
        'de' => 'Diese E-Mail Adresse ist bei uns nicht registriert. Bitte benutze die E-Mail Adresse, welche du für LINGS verwendest.',
        'fr' => ''
    };   
        //LM-726 --Incorrect Password --09OCT2019
        public static Map<String, String> ERROR_PASSWORD = new Map<String, String> { //Updated the errmsg -- 09OCT2019
        'en' => 'It is not possible to use the email address as password',
        'de' => 'Es ist nicht möglich die Email Adresse als Passwort zu benutzen',
        'fr' => ''
    };      
        //LM-794 -- Blocking Hacked User -- 26NOV2019
        public static Map<String, String> ERROR_BLOCK_USER = new Map<String, String> {
            'en' => 'You\'ve reached the limit of the maximum profiles you can create per day.',
            'de' => 'Du hast das Limit der maximal erstellbaren Profile pro Tag erreicht.',
            'fr' => ''
        };   
}