@isTest
public class BatchInsuranceDisableTest {
    testmethod public static void testUniqueUserName() {
        
        // Required test data for PolicyExpires Batch Job
        
        BatchInsuranceDisable BID = new BatchInsuranceDisable();
        
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=100;
        vc.From__c=System.today()-5;
        vc.To__c=System.today()+1;
        vc.Fixed__c=System.today()+1;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.Email = 'testuser@gmail.com';
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c = System.label.TestAlaisId; 
        con.Balance_Pending__c = null;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        insert VCC;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 100;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'Failed';
        insert inv1;  
        
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_Start_Date__c = System.today();
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        obj1.Coverage_Type__c = Constants.FLATRATE_STRING;
        insert obj1;        
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Insurance_Type__c = Constants.FLATRATE_STRING;
        pol.Start_Date__c = System.now();
        pol.End_Date__c = System.now();
        pol.Premium__c = 0.15;
        pol.Active__c = true;
        pol.Insured_Value__c = 100;
        pol.Invoice__c = inv1.Id;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;        
        insert pol;
        polList.add(pol);  
        
        
        Test.startTest();
        // Instantiate beforeEventRemainder apex batch class
        BatchInsuranceDisable BCE = new BatchInsuranceDisable(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        
        
    }
    
    testmethod public static void testUniqueUserName1() {
        
        // Required test data for PolicyExpires Batch Job
        
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c= 10;
        vc.From__c=System.today().addDays(-1);
        vc.To__c=System.today();
        vc.Fixed__c= System.today().addDays(+3);
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.Email = 'testuser@gmail.com';
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
       // con.Payment_Alias_ID__c = System.label.TestAlaisId; 
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        insert VCC;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 100;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'failed';
        insert inv1; 
        
        Object__c obj1 = new Object__c();
        obj1.FlatRate_Savings__c=0.5;
        obj1.Flatrate_Renewal_Flag__c=True;
        obj1.Customer__c=con.Id;
        obj1.Insurance_Start_Date__c = System.today();
        obj1.Insurance_End_Date__c=System.today();
        obj1.Insured_Value__c = 100;
        obj1.Coverage_Type__c = Constants.FLATRATE_STRING;
        insert obj1;        
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Insurance_Type__c = Constants.FLATRATE_STRING;
        pol.Start_Date__c = System.now();
        pol.End_Date__c = System.now();
        pol.Premium__c = 45.25;
        pol.Active__c = true;
        pol.Insured_Value__c = 10;
        pol.Invoice__c = inv1.Id;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;        
        insert pol;
        polList.add(pol);        
        
        obj1.Policy__c = pol.Id;
        update obj1;        
        
        Test.startTest();
        // Instantiate beforeEventRemainder apex batch class
        BatchInsuranceDisable BCE = new BatchInsuranceDisable(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        
        
    }
    
    testmethod public static void testUniqueUserName2() {
        
        // Required test data for PolicyExpires Batch Job
        
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c= 10;
        vc.From__c=System.today();
        vc.To__c=System.today();
        vc.Fixed__c = System.today()+1;
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.Email = 'testuser@gmail.com';
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c = null;
        con.Balance_Pending__c = 5;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        insert VCC;
        
        Invoice__c inv1 = new Invoice__c();
        inv1.Amount__c = 100;
        inv1.Contact__c = con.Id;
        inv1.Voucher_Deductions__c =100;
        inv1.Status__c = 'failed';
        insert inv1; 
        
        List<Object__c> objList = new List<Object__c>();
        Object__c obj1 = new Object__c();
       // obj1.Policy__c=pol.Id;
        obj1.Customer__c=con.Id;
        obj1.Insurance_Start_Date__c = System.today();
        obj1.Insurance_End_Date__c=System.today();
        obj1.Coverage_Type__c = Constants.ONDEMAND_STRING;
        obj1.Insured_Value__c = 100;
        insert obj1;
        objList.add(obj1);
        
        List<Policy__c> polList = new List<Policy__c>();
        Policy__c pol = new Policy__c();
        pol.Invoice_Start_Date__c = System.today();
        pol.Insurance_Type__c = Constants.ONDEMAND_STRING;
        pol.Start_Date__c = System.now();
        pol.End_Date__c = System.now();
        pol.Premium__c = 50;
        pol.Active__c = true;
        pol.Insured_Value__c = 100;
        pol.Invoice__c = inv1.Id;
        pol.Contact__c = con.id;
        pol.Object__c = obj1.Id;
        insert pol;    
        polList.add(pol);     
        
        
        Test.startTest();
        // Instantiate beforeEventRemainder apex batch class
        BatchInsuranceDisable BCE = new BatchInsuranceDisable(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        
        
    }    
    
    
}