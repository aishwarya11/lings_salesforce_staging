//LM-682--Automating the Payment Failure Mail Process --24SEPT2019
Global class BatchPaymentFailure implements Database.Batchable<Invoice__c>, Database.Stateful {
    public Integer totalMailProcessedCount = 0;
    public Integer failedMailProcessedCount = 0;
    public Integer successMailCount = 0;
    
    //Start batchable
    global Iterable<Invoice__c> start(Database.BatchableContext BC) {
        System.debug('BatchPaymentFailure START');
        List<Invoice__c> query = [SELECT ID, Name, Status__c, Date__c, Contact__c, 
                                  Contact__r.Email,Amount__c, Card_payment__r.Status__c From Invoice__c 
                                  where Date__c = THIS_MONTH AND Status__c != 'Success' and Card_payment__c <> null];        
        return query;
    }
    
    //Execute batchable
    global void execute(Database.BatchableContext BC, List<Invoice__c> scope) {
        System.debug('BatchPaymentFailure EXECUTE');
        Integer TnxFailed = 0;
        Integer CardExpired = 0;
        for(Invoice__c inv: scope){
            String Email = inv.Contact__r.Email;
            // Send Email
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();  
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setToAddresses(new String[] { Email });
            OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
            
            if(inv.Card_payment__r.Status__c == 'Transaction declined by acquirer'){
                TnxFailed = TnxFailed + 1;
                System.debug('TnxFailed Id -->'+ inv.Contact__c +'Mail -->'+ inv.Contact__r.Email);
                EmailTemplate template = [Select id from EmailTemplate where name=:'Payment Failure'];   
                
                mail.setTemplateId(template.Id);        
                mail.setwhatid(inv.Id);
                mail.setTargetObjectId(inv.Contact__c);
                mail.setSaveAsActivity(true);
                
                mailList.add(mail);   
                successMailCount++;
                
            }else if(inv.Card_payment__r.Status__c == 'Card expired'){
                CardExpired = CardExpired + 1;
                System.debug('CardExpired Id'+ inv.Contact__c +'Mail-->'+ inv.Contact__r.Email);
                EmailTemplate template = [Select id from EmailTemplate where name=:'Payment Failed when CC expired'];  
                
                mail.setTemplateId(template.Id);        
                mail.setwhatid(inv.Id);
                mail.setTargetObjectId(inv.Contact__c);
                mail.setSaveAsActivity(true);
                
                mailList.add(mail);   
                successMailCount++;
            }
            try {
                totalMailProcessedCount += mailList.size();
                // Send the emails
                if(!Test.isRunningTest()) {
                    Messaging.sendEmail(mailList); 
                }
            } catch(Exception er) {
                failedMailProcessedCount++;
                // Send exception email to Admin
                Utility.sendException(er, 'PAYMENT FAILURE MAIL NOT SEND');
            }
        } 
        System.debug('TnxFailed -->'+ TnxFailed);
        System.debug('CardExpired -->'+ CardExpired);
    }
    
    // Finish Batachable
    global void finish(Database.BatchableContext BC) {
        System.debug('BatchPaymentFailure FINISH');
    	List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(System.Label.ErrorEmails.split(','));   // MR - 23MAY19 - Code Optimization
        mail.setSubject('BatchPaymentFailure ran on ' + System.now());
        //17JUNE2019--Sender Name in the Batch status Mail. LM-496
        OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.SupportSenderAddress];
        mail.setOrgWideEmailAddressId(owea.get(0).Id);   

        String body = 'BatchPaymentFailure Status on Sending email to the user<br/>';
        
        // Processing Email list 
        body += '<br/> ' + totalMailProcessedCount + ' number of Email processing were completed.';  
        
        // Email Processing for BatchPaymentFailure.
        if(successMailCount > 0) {
            body += '<br/> successful Emails were processed for the BatchPaymentFailure = '+successMailCount+'<br/>';
        } else {
            body += '<br/> No Email processing were happened for the BatchPaymentFailure';
        }       
        // Failed Email Processing
        if(failedMailProcessedCount > 0) {
            body += '<br/> Failed Email processing count = '+ failedMailProcessedCount + ' <br/>';
        } else {
            body += '<br/> No Failed Email processing were happened.';
        }
        body += '<br/> <br/> Thanks';
        mail.setHtmlBody(body);
        mails.add(mail);       
        
		//LM-696--Send mail only when there is a failure --25SEPT2019
        if(!Test.isRunningTest()) {
            if (failedMailProcessedCount > 0) {
                Messaging.sendEmail(mails);
            }
        //LM-699--Populate big object --26SEPT2019
        Audit_Batch_Apex__b ab = new Audit_Batch_Apex__b();
        ab.Audit_Name__c = 'BatchPaymentFailure';
        ab.Track_message__c = body;
        database.insertImmediate(ab);
        }
    }
}