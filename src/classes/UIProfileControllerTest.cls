@isTest(seealldata=true)
public class UIProfileControllerTest {  
    public class Profiles {
        public String uuid;
        public String firstName = 'sample firstname';
        public String lastName = 'sample lastname';
        public Date dob = System.today();
        public String street = 'sample street';
        public String house = 'sample house';
        public String postalCode = '6000';
        public String city = 'chennai';
        public String phone = '7845525760';
        public String email = 'revathy@rangerinc.cloud';
        public String password = 'revathy@97';
        public String leadSource = 'others';
        public boolean terms = true;
        public SPPaymentInfo paymentMeans;
        public SPTxnPaymentResponse txnPaymentResponse;
        public SPTransactionCancel txnCancel;
    }
    public class SPTxnPaymentResponse{
        public SPResponseHeader responseHeader;
        public SPTransaction txn;
        public SPPaymentInfo paymentMeans;
        public SPPayer payer;
        public SPRegistrationResult registrationResult;
        public SPLiability liability;        
        public String ErrorMessage = 'This will show the Error message';
        public String Behavior = 'Good';
        public String ErrorName = 'To show error'; 
        //ErrorDetailList.add('errorvalue');
        
    } 
    
    public class SPTransactionCancel{
        public SPResponseHeader responseHeader;
        public String TransactionId = 'TestId';
        public String OrderId = 'TestOrder';
      //  public DateTime Date;
    }     
    
    public class SPResponseHeader{
        public String SpecVersion = '2.0';
        public String RequestId = System.Label.TestContact;        
    }
 
    public class SPPaymentInfo {
        public SPBrandInfo brand;
        public String displayText = 'sample text';
        public SPCardInfo card;
    }
    
    public class SPBrandInfo {
        public String PaymentMethod = 'sample payment method';
        public String Name = 'sample payment name';
    }
    public class SPCardInfo {
        public String MaskedNumber = '125454';
        public String ExpYear = '2019';
        public String ExpMonth = 'may';
        public String HolderName = 'RSK';
        public String CountryCode = 'sample code';
    }
    
    public class SPPayer {
        public String ipAddress = 'TestId';
        public String ipLocation = 'TestLocation';
    }    
    
    public class SPRegistrationResult {
        public boolean success = true;
        public SPAlias alias; 
        public SPError error;  
    }

    public class SPError {
        public String errorName = 'TestName';
        public String errorMessage = 'TestMessage';
    }    
    
    public class SPAlias {
        public String id = 'test';
        public Integer lifetime = 2000;
    }    
    
    public class SPTransaction {
        public String type = 'Test';
        public String status = 'Good';
        public String id ='test Id';
        public String txnDate = 'Today';
        public SPAmount amount;
        public String acquirerName ='test';
        public String acquirerReference ='test';
        public String sixTransactionReference ='test';
        public String approvalCode ='test';
    }
      
    public class SPLiability {
        public boolean liabilityShift = true;
        public String liableEntity = 'Test';
        public SP3D threeDs;
        public SPFraudFree fraudFree;
    }

    public class SP3D {
        public boolean authenticated = true;
        public boolean liabilityShift = true;
        public String xid = 'Test';
        public String verificationValue = 'TestValue';
    }

    public class SPFraudFree {
        public String id = 'TestId';
        public boolean liabilityShift = true;
        public decimal score = 2.5;
        public List<String> investigationPoints;
        public String errorMessage = 'ErrorTest';
    }    
    
    public class SPAmount {
        public String value = 'Test';
        public String currencyCode = 'CHF';
    }   
    
    testmethod public static void testUIProfileController() {
        //try{   
            Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
            //declaring required test data 
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                              UserName='abc@mcode.com',CommunityNickname='Lings48');
            insert u;

            SPResponseHeader SPRespInfoinstance = new SPResponseHeader();
            String SPRespInfoJSON = JSON.serialize(SPRespInfoinstance,true);    
            
            SPAlias SPAlaisInfoinstance = new SPAlias();
            String SPAlaisInfoJSON = JSON.serialize(SPAlaisInfoinstance,true); 
    
            SPCardInfo SPCardInfoinstance = new SPCardInfo();
            String SPCardInfoJSON = JSON.serialize(SPCardInfoinstance,true);
            
            SPBrandInfo SPBrandInfoinstance = new SPBrandInfo();
            String SPBrandInfoJSON = JSON.serialize(SPBrandInfoinstance,true);
            
            SPPaymentInfo SPPaymentInfoinstance = new SPPaymentInfo();
            SPPaymentInfoinstance.brand = SPBrandInfoinstance;
            SPPaymentInfoinstance.card = SPCardInfoinstance; 
            String SPPaymentInfoJSON = JSON.serialize(SPPaymentInfoinstance,true);
           
            SPPayer SPPayerinstance = new SPPayer();
            String SPPayerInfoJSON = JSON.serialize(SPPayerinstance,true);
            
            SPError SPErrorinstance = new SPError();
            String SPErrorInfoJSON = JSON.serialize(SPErrorinstance,true);            
           
            SPRegistrationResult SPRRinstance = new SPRegistrationResult();
            SPRRinstance.alias = SPAlaisInfoinstance;
            SPRRinstance.error = SPErrorinstance;
            String SPRRJSON = JSON.serialize(SPRRinstance,true); 
          
            SP3D SP3Dinstance = new SP3D();
            String SP3DInfoJSON = JSON.serialize(SP3Dinstance,true);
            
            SPFraudFree SPFraudFreeinstance = new SPFraudFree();
            String SPFFInfoJSON = JSON.serialize(SPFraudFreeinstance,true);
            
            SPAmount SPAmountinstance = new SPAmount();
            String SPAmountJSON = JSON.serialize(SPAmountinstance, true);
            
            SPLiability SPLiabilityinstance = new SPLiability();
            SPLiabilityinstance.threeDs = SP3Dinstance; 
            SPLiabilityinstance.fraudFree =SPFraudFreeinstance; 
            String SPLiabilityJSON = JSON.serialize(SPLiabilityinstance,true); 
            
            SPTransaction SPTransactioninstance = new SPTransaction();
            SPTransactioninstance.amount = SPAmountinstance;
            String SPTransactionJSON = JSON.serialize(SPTransactioninstance,true);
            
        	SPTransactionCancel SPTxnCancelinstance = new SPTransactionCancel();
        	SPTxnCancelinstance.responseHeader = SPRespInfoinstance;
        	String SPTxnCancelJSON = JSON.serialize(SPTxnCancelinstance,true);
                
            SPTxnPaymentResponse SPTxnPaymentInstance = new SPTxnPaymentResponse();
            SPTxnPaymentInstance.responseHeader = SPRespInfoinstance;
            SPTxnPaymentInstance.txn = SPTransactioninstance;
            SPTxnPaymentInstance.paymentMeans = SPPaymentInfoinstance;
            SPTxnPaymentInstance.registrationResult = SPRRinstance;
            SPTxnPaymentInstance.liability = SPLiabilityinstance;
            String SPTxnPaymentJSON = JSON.serialize(SPTxnPaymentInstance,true); 

            Profiles Profilesinstance2 = new Profiles();
            Profilesinstance2.txnCancel = SPTxnCancelinstance;
            String ProfilesJSON2 = JSON.serialize(Profilesinstance2,true);            
        
        	Profiles Profilesinstance1 = new Profiles();
            Profilesinstance1.txnPaymentResponse = SPTxnPaymentInstance;
            String ProfilesJSON1 = JSON.serialize(Profilesinstance1,true); 
            
            Profiles Profilesinstance = new Profiles();
            Profilesinstance.paymentMeans = SPPaymentInfoinstance;
            String ProfilesJSON = JSON.serialize(Profilesinstance,true);  
        
            Contact con = new Contact(lastname='lastname1',Ownerid=u.Id,Password__c='123pwd4598');
            con.Payment_Info__c = '{"displayText":"xxxx xxxx xxxx 2888","card":{"MaskedNumber":"XXXX XXXX XXXX 2888","HolderName":"Henrik Deecke","ExpYear":"1000","ExpMonth":"0","CountryCode":"CH"},"brand":{"PaymentMethod":"MASTERCARD","Name":"MasterCard"}}';
            insert con;
            
            //instantiate the UIProfileController apex class 
            UIProfileController UIProfile = new UIProfileController(con.Id);
            UIProfile.setContext(con.Id);
            UIProfile.getRecords();
            UIProfile.readRecords(ProfilesJSON);
            UIProfile.createRecords('ProfilesJSON');
            UIProfile.updateRecords(ProfilesJSON);
            UIProfile.setEmailPolicy('en');
            UIProfile.deleteRecords('ProfilesJSON');
            UIProfile.changePassword('en','oldPassword', 'newPassword');
            //UIProfile.getSCDURL('sourceURL');
            //UIProfile.updateAlias();
        //}
        //catch(Exception e){}
    }
    
    testmethod public static void testUIProfileController2() {
        //try{
            Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
            //declaring required test data 
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', City='Chennai',Street='kknagar',Phone='(908) 798-0325',DOB__c=System.today(),House_Number__c='9',
                              UserName='abc@mcode.com',CommunityNickname='Lings48');
            insert u;
            
            Contact con = new Contact(lastname='lastname1',Ownerid=u.Id,Password__c='123pwd4598');
            insert con;
            
            /*MyObject instance = new MyObject();
String myJSON = JSON.serialize(instance,true);*/
            SPCardInfo SPCardInfoinstance = new SPCardInfo();
            String SPCardInfoJSON = JSON.serialize(SPCardInfoinstance,true);
            
            SPBrandInfo SPBrandInfoinstance = new SPBrandInfo();
            String SPBrandInfoJSON = JSON.serialize(SPBrandInfoinstance,true);
            
            SPPaymentInfo SPPaymentInfoinstance = new SPPaymentInfo();
            SPPaymentInfoinstance.brand = SPBrandInfoinstance;
            SPPaymentInfoinstance.card = SPCardInfoinstance; 
            String SPPaymentInfoJSON = JSON.serialize(SPPaymentInfoinstance,true);
            
            Profiles Profilesinstance = new Profiles();
            Profilesinstance.uuid = u.id;
            Profilesinstance.paymentMeans = SPPaymentInfoinstance;
            String ProfilesJSON = JSON.serialize(Profilesinstance,true);
            
            //instantiate the UIProfileController apex class 
            UIProfileController UIProfile = new UIProfileController(con.Id);
            UIProfile.setContext(con.Id);
            UIProfile.getRecords();
            UIProfile.readRecords(ProfilesJSON);
            UIProfile.createRecords('ProfilesJSON');
            UIProfile.updateRecords(ProfilesJSON);
            UIProfile.setEmailPolicy('en');
            UserPolicy up = new UserPolicy(con.Id);
            up.sendPdf();
            con.Email_Policy_DateTime__c = System.now();
            UIProfile.deleteRecords('ProfilesJSON');
            UIProfile.changePassword('en','oldPassword', 'newPassword');
            //UIProfile.updateAlias();
        //}
        //catch(Exception e){}
    }

    // Testing for the updateTxnAlias
    testmethod public static void testupdateTxnAlias() { 
        List<String> testCon = new List<String>();
        for(String ConId : System.Label.TestContact.split(',')) {
            testCon.add(ConId);
        }
        Contact testContact = [SELECT Id from Contact limit 1][0];
        
        UIProfileController testUsr = new UIProfileController(testContact.Id);
        Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        try{
            testUsr.updateTxnAlias();
        }catch(Exception lc){
            System.debug('no content to map to object due to end of input');
        }
        
    }
    
    
    testmethod public static void testSPTxnInitialize() { 
        List<String> testCon = new List<String>();
        for(String ConId : System.Label.TestContact.split(',')) {
            testCon.add(ConId);
        }
        Contact testContact = [SELECT Id from Contact where id = :testCon limit 1][0];
        
        //UIProfileController testUsr = new UIProfileController(testContact.Id);
        Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        UIProfileController.SPTxnInitialize('Test',testContact.Id);
        
    }   

//SPTxnCancel
    testmethod public static void testSPTxnCancel() { 
        List<String> testCon = new List<String>();
        for(String ConId : System.Label.TestContact.split(',')) {
            testCon.add(ConId);
        }
        Contact testContact = [SELECT Id from Contact limit 1][0];
        
        //UIProfileController testUsr = new UIProfileController(testContact.Id);
        Test.setMock(HttpCalloutMock.class, new LingsCalloutMock());
        UIProfileController.SPTxnCancel('Test',testContact.Id);        
    }   
}