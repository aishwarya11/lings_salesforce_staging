public with sharing class ObjectUtility {
    @testvisible public static String param = null;
    
    public static String objBaseQueryString = 'Select Id,IsOKKEventEnds__c,OKKTotalPremium__c,OKKEventEndDate__c, '+
        'OKKEventStartDate__c,DisplayURL__c, Name, Insured_Value__c, Included_Date__c, Coverage_Type__c, Flatrate_Savings__c, '+
        'RVGEventStartDate__c,RVGTotalPremium__c, RVGEventEndDate__c,IsRVGEventEnds__c, '+
        'Warranty_Date__c, Shop_Name__c, Actual_amount_paid__c, Product_State__c, Serial_Number__c, Own_Comment__c, Purchased_date__c, ' +
        'Customer__r.VIU_Balance__c, Customer__r.VIU_Expiry_Date__c,Customer__r.Balance_Pending__c, Customer__r.Voucher_In_Use__c,' +
        'Product__c, Product__r.Name, Product__r.Insured_Value__c, Product__r.DisplayURL, Customer__r.Freeze__c, Shop_Url__c,' +
        'Product__r.Searchable__c, Claim__c, Product__r.Product_Type__c, Product__r.Manufacturer__c, Is_Insured__c, Customer__r.Sort_Category_Order__c,' +
        'Premium_OnDemand__c, Premium_FlatRate__c, Flatrate_Renewal_Flag__c, Policy__r.Status__c,' +
        'Policy__c, Policy__r.Active__c, Policy__r.End_Date__c, Policy__r.Insurance_Type__c, Policy__r.Policy_Source__c, ' +
        'Insurance_Start_Date__c, Insurance_End_Date__c, Is_Deleted__c, Approved__c, Designation__c, E_bike_electric_support__c, ' +
        'Sort_Order__c';
    
    public static String objDetailsQueryString = '(Select Id, Name, Insurance_Type__c, Policy_Charges__c, Policy_Source__c, Charges_Incurred_This_Month__c  ' +
        'FROM Policies__r WHERE (Invoice__c = NULL OR Invoice__r.Status__c != \'Success\' OR Invoice_Start_Date__c = THIS_MONTH)), ' + 
        '(Select Id, Name, Event__c, Event__r.Name, Event__r.Start_Date__c, Event__r.End_Date__c, Event__r.Comments__c ' +
        'FROM Object_Events__r WHERE Event__r.End_Date__c >= TODAY), ' + 
        '(Select Id, Name, Rucksack__c, Rucksack__r.Name, Rucksack__r.Bag_IsDelete__c, Rucksack__r.Insurance_State__c ' + 
        'FROM Object_Rucksacks__r WHERE Rucksack__r.Bag_IsDelete__c = FALSE),' + 
        '(Select Id, Name from Attachments ORDER BY CreatedDate DESC)';
    
    // Function to return the List of Objects without Events and Bags	
    public static List<Object__c> getObjects(Set<Id> objIds) {	//Added Lings Object Is deleted flag in the query--18SEPT2019
        String queryString = objBaseQueryString + ' FROM Object__c WHERE ID IN :objIds AND Is_Deleted__c = FALSE ORDER BY Product__r.Product_Type__c, Sort_Order__c';
        return Database.query(queryString);
    }
    
    // Function to return the List of Objects without Events and Bags
    public static List<Object__c> getCustomerObjects(Id custId) {	//Added Lings Object Is deleted flag in the query--18SEPT2019
        String queryString = objBaseQueryString 
            + ' FROM Object__c WHERE Customer__c = \'' + custId + '\' AND Is_Deleted__c = FALSE ORDER BY Product__r.Product_Type__c, Sort_Order__c';
        return Database.query(queryString);
    }
    
    // Function to return the List of Objects without Events and Bags
    public static List<Object__c> getObjectDetails(Set<Id> objIds) {	//Added Lings Object Is deleted flag in the query--18SEPT2019
        String queryString = objBaseQueryString + ', ' + objDetailsQueryString + ' FROM Object__c WHERE ID IN :objIds AND Is_Deleted__c = FALSE ORDER BY Product__r.Product_Type__c, Sort_Order__c';
        return Database.query(queryString);
    }
    
    // Function to return the List of Objects with Events and Bags
    public static List<Object__c> getCustomerObjectDetails(Id custId) {		//Added Lings Object Is deleted flag in the query--18SEPT2019
        String queryString = objBaseQueryString + ', ' + objDetailsQueryString 
            + ' FROM Object__c WHERE Customer__c = \'' + custId + '\' ORDER BY Product__r.Product_Type__c, Sort_Order__c'; //LM-852,Removed Isdeleted condition check from the quer as it was impacting the Voucher balance.
        return Database.query(queryString);
    }
    
    // Function to update the related rucksack objects
    public static void updateRucksackObjects(Map<Id, Boolean> objFlagMap) {
        List<Rucksack_Object__c> roList = new List<Rucksack_Object__c>();		//Added Lings Object Is deleted flag in the query--18SEPT2019
        for(Rucksack_Object__c ro: [Select Id, Lings_Object__c, Lings_Object__r.Is_deleted__c, Is_Object_Insured__c from Rucksack_Object__c where Lings_Object__r.Is_deleted__c = FALSE AND Lings_Object__c in :objFlagMap.keySet()]) {
            ro.Is_Object_Insured__c = objFlagMap.get(ro.Lings_Object__c);
            roList.add(ro);
        }
        
        try {
            update roList;
        } catch (Exception roE) {
            Utility.sendException(roE, 'Not Able to update Rucksack Objects');
        }
    }
    
    // LM-613--Function to sort the list--24AUG2019
    public static String reOrder(String custId, Integer fromIndex, Integer toIndex, String category) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        String tempKey;
        jsonGen.writeStringField('status', 'new');
        jsonGen.writeStringField('category', category);
        Integer changedToIndex = toIndex - 1;	//LM-684--Decremented by 1 from the UI To index-- 19SEPT2019
        if(category.equalsIgnoreCase('bags')) {
            List<Rucksack__c> objList = new List<Rucksack__c>();
            for(Rucksack__c obj: [Select Id, Sort_Order__c from Rucksack__c 
                                  where Contact__c = :custId AND Bag_IsDelete__c = FALSE 
                                  Order By Sort_Order__c]) {
                                      tempKey = ''+obj.Sort_Order__c+''+obj.Id;
                                      //step 1 :  To check the Greater between the From and To index for increment or decrementation of the Number.
                                      if(fromIndex > toIndex) {
                                          //Step 2 : Get the Number between from - to that is to be incremented.
                                          if(obj.Sort_Order__c < fromIndex && obj.Sort_Order__c >= toIndex){
                                              obj.Sort_Order__c = obj.Sort_Order__c + 1;
                                          }                
                                          //LM-684--Added the code inside the loop--19SEPT2019
                                        //step 5 : assign the original position the customer assigned.
                                        if(obj.Sort_Order__c == fromIndex){
                                            if(tempKey == ''+obj.Sort_Order__c+''+obj.Id){
                                                obj.Sort_Order__c = toIndex;	//LM-684--Decremented by 1 from the UI To index-- 19SEPT2019                                               
                                            }                                
                                        }                                           
                                      } else if(changedToIndex > fromIndex) { //Step 3 : To check the Greater between the From and To index.
                                          //step 4: Get the Number between from - to that is to be decremented.                
                                          if(obj.Sort_Order__c > fromIndex && obj.Sort_Order__c <= changedToIndex){
                                              obj.Sort_Order__c = obj.Sort_Order__c - 1;
                                          }      
                                          //LM-684--Added the code inside the loop--19SEPT2019
                                        //step 5 : assign the original position the customer assigned.
                                        if(obj.Sort_Order__c == fromIndex){
                                            if(tempKey == ''+obj.Sort_Order__c+''+obj.Id){
                                                obj.Sort_Order__c = changedToIndex;	//LM-684--Decremented by 1 from the UI To index-- 19SEPT2019                                               
                                            }                                
                                        }                                           
                                      }
                                      objList.add(obj);
                                  }
            
            try {
                update objList;
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeStringField('message', 'Object List Updated Successfully - ' + custId);               
            } catch(Exception so) {
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', so.getStackTraceString());              
                Utility.sendException(so, 'SORT ORDER FAILED for Rucksack!!');
            }
        } else {
            List<Object__c> objList = new List<Object__c>();
            for(Object__c obj: [Select Id, Name, Product_Name__c, Sort_Order__c, Product__r.Product_type__c from Object__c 
                                where Customer__c = :custId and Is_Deleted__c = false And Product__r.Product_type__c = :category
                                Order By Sort_Order__c]) {
                                    tempKey = ''+obj.Sort_Order__c+''+obj.Id;
                                    //step 1 :  To check the Greater between the From and To index for increment or decrementation of the Number.
                                    if(fromIndex > toIndex) {
                                        //Step 2 : Get the Number between from - to that is to be incremented.
                                        if(obj.Sort_Order__c < fromIndex && obj.Sort_Order__c >= toIndex){
                                            obj.Sort_Order__c = obj.Sort_Order__c + 1;
                                        }                
                                        //LM-684--Added the code inside the loop--19SEPT2019
                                        //step 5 : assign the original position the customer assigned.
                                        if(obj.Sort_Order__c == fromIndex){
                                            if(tempKey == ''+obj.Sort_Order__c+''+obj.Id){
                                                obj.Sort_Order__c = toIndex;	//LM-684--Decremented by 1 from the UI To index-- 19SEPT2019                                                
                                            }                                
                                        }                     
                                    } else if(changedToIndex > fromIndex) { //Step 3 : To check the Greater between the From and To index.
                                        //step 4: Get the Number between from - to that is to be decremented.                
                                        if(obj.Sort_Order__c > fromIndex && obj.Sort_Order__c <= changedToIndex){
                                            obj.Sort_Order__c = obj.Sort_Order__c - 1;
                                        } 
                                        //LM-684--Added the code inside the loop--19SEPT2019
                                        //step 5 : assign the original position the customer assigned.
                                        if(obj.Sort_Order__c == fromIndex){
                                            if(tempKey == ''+obj.Sort_Order__c+''+obj.Id){
                                                obj.Sort_Order__c = changedToIndex;	//LM-684--Decremented by 1 from the UI To index-- 19SEPT2019                                               
                                            }                                
                                        }                     
                                    }
                                    objList.add(obj);
                                }
            
            try {
                update objList;
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeStringField('message', 'Object List Updated Successfully - ' + custId);               
            } catch(Exception so) {
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', so.getStackTraceString());              
                Utility.sendException(so, 'SORT ORDER FAILED!!');
            }
        }
        
        jsonGen.writeEndObject();
        return jsonGen.getAsString();          
    }
}