public class UIClaimController implements DataHandler {
    @testvisible private String contactId = '';
    private static UIClaimController claimWrapper = new UIClaimController();
    public UIClaimController() {
        
    }
    
    public UIClaimController(String contact) {
        this.contactId = contact;
    }
    
    public void setContext(String cId) {
        this.contactId = cId;
    } 
    //getting the records
    public String getRecords() {
        return '{"status":"failure","message":"List operation is not supported"}';
    }
    //Read records by passing the json as a parameter
    public String readRecords(String inputJSON) {
        return '{"status":"failure","message":"Read operation is not supported"}';
    }
    
    //Create records in Claim__c object by getting values from UI when the user trying to claim insurance
    public String createRecords(String inputJSON) {
        UIResultWrapper.ClaimWithObject claimWrapper = (UIResultWrapper.ClaimWithObject)JSON.deserialize(inputJSON, UIResultWrapper.ClaimWithObject.class);
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject(); 
        
        try {     
            
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>(); 
            Set<Id> claimedObj = new Set<Id>();
            Map<String, List<UIObjectWrapper.LingsObject>> getObjectMap = new Map<String, List<UIObjectWrapper.LingsObject>>();
            List<Claim__c> claimsList = new List<Claim__c>();
            //LM-468--29MAY2019--To fill the Claim fields with the Contact details.
            String queryString = 'Select id, name, Place__c, Street__c, Phone, Post_code__c,Email from Contact where id = \'' +this.contactId + '\'';
            Contact con = database.query(queryString);
            String objId;
            // 01 - Creates Claim for Each of the Objects
            for(UIObjectWrapper.LingsObject obj: claimWrapper.insuredProducts) { 
                // Create claim for only the selected objects
                if(obj.click) {
                    Claim__c newClaim = new Claim__c();
                    newClaim.Damage_Type__c = claimWrapper.damageReason;
                    newClaim.Damage_Date__c = Date.valueOf(claimWrapper.Damagedate);
                    newClaim.Description__c = claimWrapper.damageDetails;
                    newClaim.Insurance_Amount__c = obj.price;
                    //newClaim.Object_Name__c = obj.name; 
                    newClaim.Object__c = obj.uuid;
                    newClaim.Insurance_Amount__c = obj.price;  // LM-579 - Inventory Claim with Wrong Price
                    if(obj.category == 'Bike' && obj.electricBike) {
                        newClaim.Category__c = 'E-Bike';
                    } else {
                        newClaim.Category__c = obj.category;   
                    } 
                    
                    newClaim.IBAN__c = claimWrapper.Iban;
                    // Contact Details
                    newClaim.Contact__c = con.Id; //this.contactId;
                    newClaim.First_Name__c = UserInfo.getFirstName();
                    newClaim.Last_Name__c = UserInfo.getLastName();
                    newClaim.User_Name__c = UserInfo.getUserName();
                    newClaim.Email__c = UserInfo.getUserEmail();
                    newClaim.User_Id__c = UserInfo.getUserId();
                    newClaim.CurrencyIsoCode = UserInfo.getDefaultCurrency();      
                    newClaim.Reporting_Date__c = System.today();        
                    //08APR2019 - Making Claim status New as default value.
                    newClaim.Status__c = 'New'; 
                    newClaim.Place__c = con.Place__c;
                    newClaim.Street__c = con.Street__c;
                    newClaim.Phone__c = con.Phone;
                    newClaim.Post_Code__c = con.Post_Code__c;
                    
                    getObjectMap.put(obj.uuid, claimWrapper.insuredProducts);	//To get the Object Name.--29AUG2019.
                    claimedObj.add(newClaim.Object__c); 
                    claimsList.add(newClaim);
                }
            }
            
            
            // 02 - Insert the Claims
            Map<Id, Id> objectClaimMap = new Map<Id, Id>();
            List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
            Integer claimIndex = 0;
            
            for(Database.SaveResult claimResult : Database.insert(claimsList)) {
                if(claimResult.isSuccess()) {
                    Claim__c claim = claimsList[claimIndex];
                    claim.Id = claimResult.getId();
                    objectClaimMap.put(claim.Object__c, claim.Id);
					//Get Object details.
					List<UIObjectWrapper.LingsObject> obj = getObjectMap.get(claim.Object__c);
                    //LM-464 24June2019--Send a email to the "customer" when a new claim is craeted.
                    //Have to change the Object Name formula field into text .(Note)                
                    // Send Email                                                
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setUseSignature(false);
                    mail.setToAddresses(new String[] { UserInfo.getUserEmail() });           
                    OrgWideEmailAddress[] owea = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.ClaimSenderAddress];
                    mail.setOrgWideEmailAddressId(owea.get(0).Id);   
                    mail.setSubject('Dein Schadenfall :(');
                    String messageBody = '<html><body>Ciao '+UserInfo.getFirstName()+'<br><br> Danke für die Meldung des Schadenfalls.<br>'+
                        '<br> Ich werde mich in den nächsten 48 Stunden bei dir melden, um den weiteren Prozess bezüglich deines Schadenfalles zu definieren.<br>'+
                        '<br>Beste Grüsse<br>Larissa von LINGS<br><br>'+
            			'<img src = "https://lingscrm--staging--c.documentforce.com/sfc/dist/version/download/?oid=00D0D0000000QTq&ids=0680D000000m8EbQAI&d=%2Fa%2F0D0000000Uvt%2FBvxHchkV9Y4DUe6VUp4Lj1k1QVvsOOVh6UmIyj4R._w&operationContext=DELIVERY&viewId=05H0D0000000IJ7UAM&dpt="/><br/>'+
           				'<br>'+'Generali Versicherungen'+'<br>'+'Soodmattenstrasse 4'+'<br>'+'8134 Adliswil'+'<br>'+'http://www.lings.ch<br>'+
            			'<br>'+'LINGS ist eine Marke der Generali Allgemeine Versicherungen AG, Avenue Perdtemps 23, 1260 Nyon'+'<br></body> </html>';
                    mail.setHtmlBody(messageBody);
                    mail.setSaveAsActivity(true);
                    mailList.add(mail);
                    
                        //Send mail to the "claims@lings.ch" --31AUG2019.
                    Messaging.SingleEmailMessage clmmail = new Messaging.SingleEmailMessage();
                    clmmail.setUseSignature(false);
                    clmmail.setToAddresses(new String[] { System.Label.ClaimSenderAddress });           
                    OrgWideEmailAddress[] owea1 = [select id, DisplayName, Address from OrgWideEmailAddress where Address = :System.Label.ClaimSenderAddress];
                    clmmail.setOrgWideEmailAddressId(owea1.get(0).Id);   
                    clmmail.setSubject('New Claim '+UserInfo.getFirstName()+' '+UserInfo.getLastName());
                    String messageBodyClm = '<html><body>Hi Team!!<br><br> New Claim from :&nbsp;&nbsp;'+UserInfo.getFirstName()+' '+UserInfo.getLastName()+
                        '<br> Damage Date :&nbsp;&nbsp; '+ Date.valueOf(claimWrapper.Damagedate)+'<br>The Claimed Object :&nbsp;&nbsp;'+obj.get(claimIndex).name+ 
                        '<br> Insurance Amount :&nbsp;&nbsp;CHF '+obj.get(claimIndex).price+ '<br>Damaged type :&nbsp;&nbsp;'+claimWrapper.damageReason+ 
                        '<br><br>Beste Grüsse<br>Larissa von LINGS<br><br>'+
            			'<img src = "https://lingscrm--staging--c.documentforce.com/sfc/dist/version/download/?oid=00D0D0000000QTq&ids=0680D000000m8EbQAI&d=%2Fa%2F0D0000000Uvt%2FBvxHchkV9Y4DUe6VUp4Lj1k1QVvsOOVh6UmIyj4R._w&operationContext=DELIVERY&viewId=05H0D0000000IJ7UAM&dpt="/><br/>'+
           				'<br>'+'Generali Versicherungen'+'<br>'+'Soodmattenstrasse 4'+'<br>'+'8134 Adliswil'+'<br>'+'http://www.lings.ch<br>'+
            			'<br>'+'LINGS ist eine Marke der Generali Allgemeine Versicherungen AG, Avenue Perdtemps 23, 1260 Nyon'+'<br></body> </html>';
                    clmmail.setHtmlBody(messageBodyClm);
                    mail.setTargetObjectId(this.contactId);
                    clmmail.setSaveAsActivity(true);
                    
                    mailList.add(clmmail);                      
                    
                    if(claimWrapper.attachments != NULL) {
                        //Loop all the attachment Id and map with parent Id
                        for(String cdIdwithName: claimWrapper.attachments) { 
                            // Attachment attach = new Attachment(Id = att);
                            // attach.ParentId = clm.Id;    
                            // updAttach.add(attach);
                            String cdId = cdIdwithName.subString(0, 18);
                            //Insert ContentDocumentLink
                            ContentDocumentLink cDocLink = new ContentDocumentLink();
                            cDocLink.ContentDocumentId = cdId;              //Add ContentDocumentId
                            cDocLink.LinkedEntityId = claim.Id;  //Add attachment parentId
                            cDocLink.ShareType = 'I';                       //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
                            //cDocLink.Visibility = 'AllUsers';          //AllUsers, InternalUsers, SharedUsers
                            cdlList.add(cDocLink);
                        }
                    }                       
                }
                
                claimIndex++;
            }
            
            // 03 - Update the Attachments to the Claim
            if(cdlList.size() > 0) {
                try {
                    insert cdlList; 
                } catch(Exception ua) {
                    Utility.sendException(ua, 'ERROR CREATING CONTENT DOCUMENT LINK IN THE CLAIM CONTROLLER');
                }                
            }                
            
            // 04 - Update the Objects and Policies with the Related Claims    
            List<Object__c> objList = new List<Object__c>();
            List<Policy__c> polList = new List<Policy__c>();
            Integer chargedDays = 0;
            Integer diffDays = 0;
            
            List<Object__c> objects = [Select Id, Policy__c, Policy__r.Active__c, Is_Insured__c, Claim__c, Coverage_Type__c, Flatrate_Renewal_Flag__c,
                                       Insurance_Start_Date__c, Insurance_End_Date__c, Policy__r.Invoice_Start_date__c, Policy__r.Policy_Charges__c, 
                                       Policy__r.Premium__c FROM Object__c WHERE Id IN :claimedObj];
            for(Object__c updObject : objects) {
                updObject.Claim__c = objectClaimMap.get(updObject.Id);
                //LM-612--made changes in such a way that only onDemand policy will be turned off -- 20AUG2019.
                if(updobject.Policy__r.Active__c && updObject.Coverage_Type__c == Constants.ONDEMAND_STRING) {
                    updObject.Is_Insured__c = FALSE;	//When we claim the Object we update the Insured Flag 14AUG2019.                   
                    updObject.Flatrate_Renewal_Flag__c = false;
                    updObject.Insurance_End_Date__c = System.today();                    
                }
                objList.add(updObject);
                // Close Related Policy
                Policy__c relatedPolicy = new Policy__c();
                //when the Object has no policy then update policy is not needed.
                if(updObject.Policy__c != null) {
                    relatedPolicy.Id = updObject.Policy__c;                    
                    //LM-612--Change the Policy Status to claimed when it is insured via On-Demand and unchange the active flag--20AUG2019.
                    if(Constants.ONDEMAND_STRING.equalsIgnoreCase(updObject.Coverage_Type__c)) {
                        relatedPolicy.End_Date__c = System.now();
                        relatedPolicy.Status__c = 'Claimed';
                        relatedPolicy.Active__c = false;
                    //Added Policy Charges on 26SEPT2019
                    diffDays =  updObject.Policy__r.Invoice_Start_Date__c.daysBetween(relatedPolicy.End_Date__c.date());	
                    chargedDays = diffDays + 1;                 
                    relatedPolicy.Policy_Charges__c = Utility.getRoundedPremium(updObject.Policy__r.Premium__c * chargedDays);                          
                    }    
                    polList.add(relatedPolicy);                                     
                }
                
                try {
                    if(objList.size() > 0) {
                        update objList; 
                    }                    
                } catch(Exception oe) {
                    utility.sendException(oe, 'UPDATE OBJECT FAILED IN CLAIM CREATION!!'+oe.getStackTraceString());
                }   
                
                try {
                    if(polList.size() > 0) {
                        update polList;
                    }                   
                } catch(Exception oe) {
                    utility.sendException(oe, 'UPDATE POLICY FAILED IN CLAIM CREATION!!'+oe.getStackTraceString());
                }                                                                                                                  
            }           
            
            // 05 - Send the Claim Emails to Lings Team
            try {
                Messaging.sendEmail(mailList);
            } catch (Exception me) {
                Utility.sendException(me, 'Claim Mail failed'+me.getStackTraceString());
            }            
            // Sent from the Trigger
            
            jsonGen.writeStringField('status', 'success');
            jsonGen.writeObjectField('message', 'created claim successfully');
        } catch(Exception ee) {
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', ee.getStackTraceString());
            Utility.sendException(ee,'CLAIM CREATION FAILED!!'+ee.getStackTraceString());            
        }
        
        return jsonGen.getAsString();                  
    }
    
    //updation of records by passing the json as a parameter
    public String updateRecords(String inputJSON) {
        return '{"status":"failure","message":"Update operation is not supported"}';
    }
    
    //deletion of records by passing the json as a parameter
    public String deleteRecords(String inputJSON) {
        return '{"status":"failure","message":"Delete operation is not supported"}';
    }
}