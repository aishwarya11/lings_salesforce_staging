@RestResource(urlMapping='/Check/*')
global with sharing class MobileCardCheckService {
    @HttpPost
    global static String card(String langCode, String sourceURL){
        return UIHomeController.checkCardAdd(langCode, sourceURL);
    }
}