@isTest
public class BatchCouponExpireTest
{
    public static testmethod void BatchPopulateTest(){
         try{
        
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=100;
        vc.From__c=System.today()-2;
        vc.To__c=System.today();
        vc.Fixed__c=System.today();
        vc.Usage_Limit__c = 10;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        //con.Voucher_In_Use__c =VCC.id;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        insert VCC;        
        
        Test.startTest();
        
        BatchCouponExpire BCE = new BatchCouponExpire(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        }catch(Exception e){}
        
    }
    public static testmethod void BatchPopulateTest2(){
        //try{
        
        Voucher__c vc = new Voucher__c();
        vc.Name='Voucher999';
        vc.Amount__c=120;
        vc.From__c=System.today().addDays(-4);
        vc.To__c=System.today() + 1;
        vc.Fixed__c= System.today() + 1;
        vc.Usage_Limit__c = 15;
        insert vc;
        
        Contact con = new Contact();
        con.lastname ='Sample Name123';
        con.Password__c='123pwd4598';
        con.Payment_Alias_ID__c = System.label.TestAlaisId;
        //con.Voucher_In_Use__c =VCC.id;
        insert con;
        
        Voucher_Contact__c VCC = new Voucher_Contact__c();
        VCC.Contact__c=con.id;
        VCC.Voucher__c=vc.id;
        insert VCC;
        
        EmailTemplate eTemplate = null;
        
        Test.startTest();
        
        BatchCouponExpire BCE = new BatchCouponExpire(); // Add you Class Name
        DataBase.executeBatch(BCE);
        
        Test.stopTest();
        //}catch(Exception e){}
        
    }
    
}