@isTest
public class UIRegisterExtTest { 
    
    public class ProfileWrapper {
        public String firstName;
        public String lastName;
        public Date	dob;
        public String street;
        public String houseNumber;
        public String zipCode;
        public String city;
        public String phoneNumber;
        public String email;
        public String leadSource;
        public boolean terms;
        public String password;
    }
    
    
    testmethod public static void testUIRegisterExtTest() {
        
                String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        String userNameSuffix = 'lings.demo';
        //  try{
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
        insert r;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User u = new User(Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', FirstName='User1',LastName='Testing', Alias='Usstin', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserRoleId = r.Id ,
                          UserName=uniqueUserName,CommunityNickname='Lings32184');
        u.City = 'chennai';
        u.Street = 'Ashok Nagar';
        u.Phone = '988456897';
        u.DOB__c = System.today();
        u.House_Number__c = '100';
        u.First_Login__c = false;        
        insert u;
        
        System.runAs (new User(Id = u.Id)) {
            
            Contact con = new Contact(firstname='firstname1',lastname='lastname1',Password__c='123pwd4598');
            con.email='test@test.com';
            con.Token__c ='samplevalue';
            con.Email_Verified__c = true;
            insert con;                       
            
            UIHomeController UIHC = new UIHomeController();
            UIRegisterExt UIHL = new UIRegisterExt(UIHC);
            
            ProfileWrapper PW = new ProfileWrapper();
            PW.firstName = con.FirstName;
            PW.lastName = con.LastName;
            PW.dob = u.DOB__c;
            PW.street = u.Street;
            PW.houseNumber = u.House_Number__c;
            PW.zipCode = u.PostalCode;
            PW.city = u.City;
            PW.phoneNumber = u.Phone;
            PW.email = u.Email;
            PW.leadSource = 'FaceBook';
            PW.terms = TRUE;
            PW.password = 'revathy@97';
            
            ProfileWrapper onclass = new ProfileWrapper();
            String inpParam = JSON.serialize(onclass,true);
            
            UIHL.registerUser(inpParam);
        }
    }

}