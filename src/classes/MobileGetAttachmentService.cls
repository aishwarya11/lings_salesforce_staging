@RestResource(urlMapping='/get/*')
global with sharing class MobileGetAttachmentService {
    @HttpPost
    global static String attachment(String objId, String name){
        return UIHomeController.getAttachmentString(objId, name);
    }
}