@RestResource(urlMapping='/getCardUrl/*')
global with sharing class MobileCardServices {
    @HttpPost
    global static String card(String langCode, String sourceURL) {        
        return UIHomeController.getCardURL(langCode, sourceURL);
    }
}