global class EventPolicyScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchEventPolicy batchEvent = new BatchEventPolicy(system.today());
        database.executeBatch(batchEvent, 5);
    }
}