//26JULY2019--Bag feature LINGS 2.0
public class UIRucksackController implements DataHandler {
    @testvisible private String contactId = '';
    
    public UIRucksackController() {
        
    }
    
    public UIRucksackController(String contact) {
        this.contactId = contact;
    }    
    
    public void setContext(String cId) {
        this.contactId = cId;
    }

    //getRecords
    public String getRecords() {
        List<UIObjectWrapper.SackWithObject> respList = new List<UIObjectWrapper.SackWithObject>();
        String queryString = 'SELECT id, Name, Insurance_State__c, Contact__c, ' +
            'Date__c, (SELECT id, Name, Lings_Object__c,Lings_Object__r.Product_Name__c, Lings_Object__r.Premium_OnDemand__c, ' +
            'Lings_Object__r.Premium_FlatRate__c, Lings_Object__r.Coverage_Type__c, ' +
            'Lings_Object__r.Policy__c, Lings_Object__r.Policy__r.Active__c, Lings_Object__r.Policy__r.Insurance_Type__c, ' + 
            'Lings_Object__r.Product__r.DisplayURL FROM Rucksack_Objects__r) ' +
            'FROM Rucksack__c WHERE Contact__c  = \'' + this.contactId + '\' AND Bag_IsDelete__c = FALSE order by Date__c';
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        for(Rucksack__c sack: Database.query(queryString)) {
            UIObjectWrapper.SackWithObject rsWrap = new UIObjectWrapper.SackWithObject();
            rsWrap.uuid = sack.Id;
            rsWrap.Name = sack.Name;
            
            // MR - 27AUG19 - Fix for LM626
            // rsWrap.hasClaim = (sack.Claim__c == null ? false : true);
            rsWrap.insuredProducts = new List<UIObjectWrapper.LingsObject>();
            for(Rucksack_Object__c rso: sack.Rucksack_Objects__r) {
                UIObjectWrapper.LingsObject objWrap = new UIObjectWrapper.LingsObject();
                objWrap.uuid = rso.Lings_Object__c;
                objWrap.name = rso.Lings_Object__r.Product_Name__c;
                objWrap.thumbnail = rso.Lings_Object__r.Product__r.DisplayURL;
                objWrap.insuranceRate.daily = rso.Lings_Object__r.Premium_OnDemand__c;
                objWrap.flatRate.daily = rso.Lings_Object__r.Premium_FlatRate__c;
                String typ = rso.Lings_Object__r.Coverage_Type__c;
                if(typ.equalsIgnoreCase('ondemand')) {
                    objWrap.insuranceRate.enabled = true;
                } else if(typ.equalsIgnoreCase('flatrate')) {
                    objWrap.flatRate.enabled = true;
                } 
                rsWrap.insuredProducts.add(objWrap);
            }
            
            rsWrap.state = sack.Insurance_State__c;
            if(rsWrap.state.equalsIgnoreCase('Insured')) {
                rsWrap.toggle = true;
            } else {
                rsWrap.toggle = false;
            }
            respList.add(rsWrap);
        }
        
        jsonGen.writeObjectField('rucksack', respList);
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString();    
    }        
    
    //readRecords
    public String readRecords(String inputJSON) {
        UIObjectWrapper.SackWithObject response = new UIObjectWrapper.SackWithObject();
        String queryString = 'Select Id, Name, Insurance_State__c, Insurance_Flag__c, Contact__c, ' + 
            ' (SELECT Id, Lings_Object__c, Lings_Object__r.Product_Name__c, Lings_Object__r.Premium_OnDemand__c,Lings_Object__r.Insurance_Start_Date__c, ' +
            'Lings_Object__r.Premium_FlatRate__c, Lings_Object__r.Coverage_Type__c, Lings_Object__r.Insurance_End_Date__c,' +
            'Lings_Object__r.Policy__c, Lings_Object__r.Policy__r.Active__c, Lings_Object__r.Policy__r.Status__c, Lings_Object__r.Policy__r.Insurance_Type__c, ' + 
            'Lings_Object__r.Product__r.DisplayURL from Rucksack_Objects__r WHERE Lings_Object__r.Is_Deleted__c = FALSE)' +
            'FROM Rucksack__c ' +
            'WHERE Id = \'' + inputJSON + '\' and Bag_IsDelete__c = FALSE';
        // "insuranceRate":{"daily":0.4,"monthly":12,"currencyCode":"CHF"}
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();

        for(Rucksack__c sack: Database.query(queryString)) {
            response.uuid = sack.Id;
            response.Name = sack.Name;
            response.state = sack.Insurance_State__c;
            if(response.state.equalsIgnoreCase('Insured')) {
                response.toggle = true;
            } else {
                response.toggle = false;
            }
            
            // Get all objects to read its related Events, Bags etc
            Set<Id> bagObjectIds = new Set<Id>();
            for(Rucksack_Object__c rso: sack.Rucksack_Objects__r) {
                bagObjectIds.add(rso.Lings_Object__c);
            }
            
            response.insuredProducts = new List<UIObjectWrapper.LingsObject>();
            // Read all the objects from the bag
            for(Object__c obj: ObjectUtility.getObjectDetails(bagObjectIds)) {            		
                UIObjectWrapper.LingsObject objWrap = UIObjectWrapper.convertToWrapper(obj);
                response.insuredProducts.add(objWrap);
            }

            jsonGen.writeObjectField('rucksack', response);            
        }
        
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString();        
    }
    
    //createRecords
    public String createRecords(String inputJSON) {        
        List<UIObjectWrapper.SackWithObject> sackWrapperList = (List<UIObjectWrapper.SackWithObject>)JSON.deserialize(inputJSON, List<UIObjectWrapper.SackWithObject>.class);
        
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();  
        Integer insuredProductCount = 0;
        set<id> objId = new set<id>();
        
        for(UIObjectWrapper.SackWithObject sackWrapper : sackWrapperList) {
            Rucksack__c newSack = new Rucksack__c();
            newSack.Name = sackWrapper.name;
            newSack.Contact__c = this.ContactId;
            newSack.Insurance_Flag__c = false;

            try {
                List<Rucksack_Object__c> sackObjList = new List<Rucksack_Object__c>();
                Boolean hasInsuredObjects = false;
                Boolean allObjectsInsured = true;
                for(UIObjectWrapper.LingsObject obj: sackWrapper.insuredProducts) {
                    objId.add(obj.uuid);
                    if(obj.insured) { 
                        hasInsuredObjects = true; 
                    } else { 
                        allObjectsInsured = false; 
                    }
                }
				
                //Update the Sort Order when the new bag is created.	04SEPT2019
                Integer bagSize = 0;
                List<Rucksack__c> sackList = [SELECT id, name, Bag_IsDelete__c, Contact__c, Sort_Order__c FROM Rucksack__c 
                                              where Contact__c = :this.ContactId AND Bag_IsDelete__c = FALSE];
                bagSize = sackList.size();
                newSack.Sort_Order__c = bagSize + 1;
                
                if(hasInsuredObjects) { 
                    if(allObjectsInsured) {
                        sackWrapper.state = 'Insured';
                        sackWrapper.toggle = true;
                    } else {
                        sackWrapper.state = 'Partly Insured';
                        sackWrapper.toggle = false;
                    }
                } else {
                    sackWrapper.state = 'Not Insured';
                    sackWrapper.toggle = false;
                }

                insert newSack;
                
                //To get the Only the Insured Product Lings Object.
                for(Object__c lo : [Select id, Name, Policy__c, Coverage_type__c, Is_Insured__c From Object__c Where id = :objId]){
                    //Creation of RO Is_Object_Insured__c is unchecked for all Objects.
                    Rucksack_Object__c sackObj = new Rucksack_Object__c();
                    sackObj.Rucksack__c = newSack.Id; 
                    sackObj.Lings_Object__c = lo.Id;
                    sackObj.Contact__c = this.ContactId;   
                    sackObj.Is_Object_Insured__c = false;                                   
                    if(lo.Is_Insured__c) {
                        insuredProductCount = insuredProductCount + 1; 
                        sackObj.Is_Object_Insured__c = true;
                    }
                    sackObjList.add(sackObj);  
                }

                jsonGen.writeObjectField('insuredProductCount', insuredProductCount);
                jsonGen.writeObjectField('listCount', sackObjList.size());
                
                if(sackObjList.size() > 0) {
                    try {
                        insert sackObjList;
                    } catch(Exception e) {
                        Utility.sendException(e, 'RUCKSACK CREATION FAILED'+e.getStackTraceString());
                    }
                }
                
                sackWrapper.uuid = newSack.Id;
                
                jsonGen.writeStringField('status', 'success');
                jsonGen.writeObjectField('rucksackList', new List<UIObjectWrapper.SackWithObject>{ sackWrapper });
            } catch(Exception ee) {
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', ee.getMessage() + ee.getStackTraceString());            
            }
        }
        

        return jsonGen.getAsString(); 
    }
    
    //updateRecords
    public String updateRecords(String inputJSON) {
        List<UIObjectWrapper.SackWithObject> sackWrapperList = (List<UIObjectWrapper.SackWithObject>)JSON.deserialize(inputJSON, List<UIObjectWrapper.SackWithObject>.class);
        List<UIObjectWrapper.SackWithObject> updateWrapperList = new List<UIObjectWrapper.SackWithObject>();

        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        for(UIObjectWrapper.SackWithObject sackWrapper: sackWrapperList) {
            if(sackWrapper.insuredProducts == null) {
                sackWrapper.insuredProducts = new List<UIObjectWrapper.LingsObject>();
            }
            
            // Flags for processing
            Boolean itemsAddFlag = false;
            Boolean itemsRemoveFlag = false;
            Boolean insuranceONFlag = false;
            Boolean insuranceOFFFlag = false;
            Boolean createNewPolicy = false;
            Boolean updatePolicy = false;
            List<Policy__c> policyList = new List<Policy__c>();
            List<Object__c> objList = new List<Object__c>();
            List<Id> objectId = new List<Id>();   //LM-1293 --This list is used to update the Event end date --08APR2020
            
            // Initialize the Rucksack for update based on the operation        
            Rucksack__c sackObject = new Rucksack__c(Id=sackWrapper.uuid);
            sackObject.name = sackWrapper.name;
            
            // Read all the objects related to the Rucksack for processing
            Map<String, Rucksack_Object__c> beObjectsMap = new Map<String, Rucksack_Object__c>();
            Set<Id> beObjectIds = new Set<Id>();
            for(Rucksack_Object__c robj: [SELECT Id, Lings_Object__c, Lings_Object__r.Policy__c, Lings_Object__r.Claim__c,
                                          Lings_Object__r.Customer__c, Lings_Object__r.Insured_Value__c, Lings_Object__r.Is_Deleted__c,
                                          Lings_Object__r.Premium_OnDemand__c, Lings_Object__r.Policy__r.Status__c, 
                                          Lings_Object__r.Policy__r.Active__c, Lings_Object__r.Policy__r.Insurance_Type__c, 
                                          Lings_Object__r.Policy__r.Invoice_Start_date__c, Rucksack__r.Insurance_Flag__c,
                                          Rucksack__r.Bag_IsDelete__c FROM Rucksack_Object__c WHERE Rucksack__c = :sackWrapper.uuid 
                                          AND Rucksack__r.Bag_IsDelete__c = FALSE AND Lings_Object__r.Is_Deleted__c = FALSE]) {	//09SEPT2019--Ignoring Deleted LO.
                                              
                beObjectIds.add(rObj.Lings_Object__c);
                beObjectsMap.put(rObj.Lings_Object__c, rObj);
                if(sackObject.Insurance_Flag__c == null) {
                    sackObject.Insurance_Flag__c = rObj.Rucksack__r.Insurance_Flag__c;
                }
            }
            
            if(sackWrapper.insuredProducts.size() == 0) {
                // Insurance ON/OFF
                sackObject.Insurance_Flag__c = sackWrapper.toggle;
                if(sackWrapper.toggle) {
                    sackWrapper.state = 'Insured';
                    // Turn ON the insurnace for all the items
                    for(Rucksack_Object__c rso: beObjectsMap.values()) {
                        if(rso.Lings_Object__r.Claim__c == NULL){ //Ignore the Object which is claimed--14AUG2019
                            if(rso.Lings_Object__r.Policy__c == NULL) {
                                Object__c obj = new Object__c(Id = rso.Lings_Object__c);
                                obj.Id = rso.Lings_Object__c;
                                obj.Customer__c = rso.Lings_Object__r.Customer__c;
                                obj.Insured_Value__c = rso.Lings_Object__r.Insured_Value__c;
                                obj.Premium_OnDemand__c = rso.Lings_Object__r.Premium_OnDemand__c;
                                //Update the Object with the new Policy when we turn On the bag.
                                createNewPolicy = true;
                                updatePolicy = False;
                                // Create new policy
                                policyList.add(PolicyUtility.newOnDemandPolicy(obj, 'Rucksack' , false));
                            } else if(!rso.Lings_Object__r.Policy__r.Active__c && rso.Lings_Object__r.Policy__r.Insurance_Type__c == 'OnDemand') {
                                createNewPolicy = False;
                                updatePolicy = True;
                                policyList.add(PolicyUtility.activateOnDemandPolicy(rso.Lings_Object__r.Policy__c, 'Rucksack', false));
                            }                            
                        }else{
                            sackWrapper.state = 'Partly Insured';	//after the Bag On only the Claimed Objects will be in OFF State.
                        }
                    }
                } else {
                    sackWrapper.state = 'Not Insured';
                    // Turn OFF the insurnace for all the items
                    for(Rucksack_Object__c rso: beObjectsMap.values()) {
                        if(rso.Lings_Object__r.Policy__c != NULL) {
                            policyList.add(PolicyUtility.closePolicy(rso.Lings_Object__r.Policy__c, false));
                        }
                    }
                }
                                                    
                try {
                    // Create/Update the policies
                    upsert policyList;

                    // Update the Objects IsInsuredFlag
                    for(Policy__c pol: policyList) {
                        Object__c obj = new Object__c(Id = pol.Object__c);
                        if(pol.Active__c) { 
                            obj.Policy__c = pol.Id; 
                            obj.Is_Insured__c = true;
                        } else {
                            //obj.Policy__c = null;
                            obj.Is_Insured__c = false;
                            //LM-1293 --When the Event Policy is closed with sooner date update the Object --08APR2020
                            if(pol.Policy_Source__c == 'Event' && pol.End_Date__c.date() == System.today()) {
                                obj.Insurance_End_Date__c = System.today();
                                objectId.add(obj.Id);
                            }
                        }
                        if(createNewPolicy){
                            obj.Insurance_Start_Date__c = System.today(); //Since Policy Startdate is system.now() 
                            obj.Coverage_Type__c = CONSTANTS.ONDEMAND_STRING;                            
                        }else if(updatePolicy){
                            obj.Insurance_End_Date__c = NULL; //for activateOnDemandPolicy                   
                        }
                        objList.add(obj);
                    }

                    if(objList.size() > 0) {
                        try {
                            update objList;
                        } catch (Exception oe) {
                            Utility.sendException(oe, 'FAILED IN UPDATING Objects Insurance Flag from updateRecords');
                        }
                    }
                } catch (Exception policyException) {
                    Utility.sendException(policyException, 'FAILED IN UPDATING Policies/Objects from updateRecords');
                }

                //Getting all the Object Info
                Map<id, Object__c> objectsMaps = new Map<id, Object__c>();
                for(Object__c obj : ObjectUtility.getObjectDetails(beObjectIds)) {
                    objectsMaps.put(obj.Id, obj);
                    sackWrapper.insuredProducts.add(UIObjectWrapper.convertToWrapper(obj));
                }
                
                //LM-1293 -- Updating the Event End date as well when the customer close it via bag --08APR2020
                if(objectId.size() > 0) {
                    List<Event__c> eveList = new List<Event__c> ();
                    Integer count = 0;
                    for(Id o: objectId) {
                 		Object__c eobj = objectsMaps.get(o);
                        if(eobj.Object_Events__r.size() > 0) {
                            //Event_Object__c eve = eobj.Object_Events__r[0].Event__c;
                            //for(Event_Object__c eve : eobj.Object_Events__r) {
                                count = count + 1;
                                if(count == 1) {
                                    Event__c e = new Event__c(Id = eobj.Object_Events__r[0].Event__c);
                                    e.End_date__c = System.today();
                                    e.Comments__c = 'Updated Event End date as event is closed via bag';
                                    eveList.add(e);
                                }
                            //}
                        }
                    }
                    try {
                        //Updating the Event List
                        if(eveList.size() > 0) {
                            update eveList;
                        }   
                    } catch (Exception eo) {
                        Utility.sendException(eo, 'Error in Updating the Event record VIA Bag '+eo.getStackTraceString());
                    }                   
                }                
            } else {
                // Add/Remove Items
                Set<Id> uiObjects = new Set<Id>();
                Map<String, Boolean> unselectMap = new Map<String, Boolean>();
                List<Rucksack_Object__c> insertList = new List<Rucksack_Object__c>();
                List<Rucksack_Object__c> deleteList = new List<Rucksack_Object__c>();
                Set<Id> quesOnSet = new Set<Id>();
                Set<Id> quesOffSet = new Set<Id>();

                for(UIObjectWrapper.LingsObject obj: sackWrapper.insuredProducts) {
                    uiObjects.add(obj.uuid);
                    unselectMap.put(obj.uuid, obj.click);
                    if(obj.questionOnOff != null) {                     
                        if(obj.questionOnOff.equalsIgnoreCase('on')) {
                            quesOnSet.add(obj.uuid);
                        } else if(obj.questionOnOff.equalsIgnoreCase('off')) {
                            quesOffSet.add(obj.uuid);
                        }
                    }else if(obj.removeQuestionOnOff != null) {		//Added new variable to remove the Object from the Bag.                    
                        if(obj.removeQuestionOnOff.equalsIgnoreCase('on')) {
                            quesOnSet.add(obj.uuid);
                        } else if(obj.removeQuestionOnOff.equalsIgnoreCase('off')) {
                            quesOffSet.add(obj.uuid);
                        }                        
                    }
                }

                jsonGen.writeObjectField('quesOnSet', quesOnSet);
                jsonGen.writeObjectField('quesOffSet', quesOffSet);
                
                //Getting all the Object Info
                Map<id, Object__c> objectsMap = new Map<id, Object__c>();
                Set<Id> allObjIds = new Set<Id>();
                allObjIds.addAll(uiObjects); 
                allObjIds.addall(beObjectIds);
                for(Object__c obj : ObjectUtility.getObjectDetails(allObjIds)) {
                    objectsMap.put(obj.Id, obj);
                }
                
                // Add all the newly selected Objects
                for(String uiObjUUID: uiObjects) {
                    if(!beObjectsMap.containsKey(uiObjUUID)) {
                        Rucksack_Object__c ro = new Rucksack_Object__c();
                        ro.Rucksack__c = sackObject.Id;
                        ro.Lings_Object__c = uiObjUUID;
                        ro.Contact__c = this.contactId;
                        
                        Object__c obj = objectsMap.get(uiObjUUID);
                        ro.Is_Object_Insured__c = obj.Is_Insured__c; //11AUG2019--Assigning the flag based on the Object during the Creation of RO.
                        if(sackWrapper.state.equalsIgnoreCase('Insured')) {   // sackObject.Insurance_Flag__c                 
                            // Bag - ON							   
                            if(obj.Policy__c == null) {
                                // OnDemand - OFF
                                if(obj.Object_Events__r.size() == 0) {	//24AUG2019--Subquery to check with size()
                                    // Events - OFF
                                    if(quesOnSet.contains(uiObjUUID)) {
                                        // Create new policy
                                        policyList.add(PolicyUtility.newOnDemandPolicy(obj, 'Rucksack' , false));
                                    }
                                } else {
                                    // Events - ON
                                    // Create new policy
                                    Policy__c pol = PolicyUtility.newOnDemandPolicy(obj, 'Rucksack' , false);
                                    policyList.add(pol);
                                }
                            } else if(!obj.Policy__r.Active__c) {
                                // OnDemand - OFF
                                if(obj.Object_Events__r.size() == 0) {	//24AUG2019--Subquery to check with size()
                                    // Events - OFF
                                    if(quesOnSet.contains(uiObjUUID)) {
                                        policyList.add(PolicyUtility.newOnDemandPolicy(obj, 'Rucksack' , false));
                                    }
                                } else {
                                    // Events - ON     
                                    policyList.add(PolicyUtility.activateOnDemandPolicy(obj.Policy__c, 'Rucksack', false));
                                }
                            } else if(obj.Policy__r.Policy_Source__c == 'Object') {
                                // OnDemand - ON
                                // Events ON/OFF - Do Nothing
                            } else if(obj.Policy__r.Policy_Source__c == 'Event') {
                                // OnDemand - ON, Event - ON
                                // Do Nothing
                            } 
                        } else {
                            // Bag - OFF and adding the New Object which is Insured, then the state is Partly insured. 
                            // OnDemand ON/OFF or Events ON/OFF - Do Nothing						
                        }
                        
                        insertList.add(ro);                        
                    }
                }
                
                if(insertList.size() > 0) {
                    try {
                        insert insertList;
                    } catch(Exception e) {
                        // Not able to create Event Objects
                        Utility.sendException(e, 'ERROR IN CREATING RUCKSACK OBJECTS'); 
                    }
                }

                Integer insuredProductCount = 0;
                Integer listCount = 0;

                // Remove all the unselected Objects
                for(String beObjUUID: beObjectsMap.keySet()) {                  
                    if(!uiObjects.contains(beObjUUID) || unselectMap.get(beObjUUID) == FALSE) {	//When Click is false we remove the Object from the Bag
                        Rucksack_Object__c ro = new Rucksack_Object__c(Id=beObjectsMap.get(beObjUUID).Id);
                        Object__c obj = objectsMap.get(beObjUUID); 
                        if(sackWrapper.state.equalsIgnoreCase('Insured')) {  // sackObject.Insurance_Flag__c                  
                            // Bag - ON                                                       
                            if(obj.Policy__c != null) {	//Policy will not be Null Since the bag is ON --28AUG2019(Remove Questioning).                               
                                // OnDemand - OFF
                                if(obj.Object_Events__r.size() == 0) {	//24AUG2019--Subquery to check with size().                                    
                                    // Events - OFF // Why this needs to be questioned?
                                    if(quesOffSet.contains(beObjUUID)) {
                                        // Close the existing OnDemand policy
                                        Policy__c pol = PolicyUtility.closePolicy(obj.Policy__c, false);
                                        policyList.add(pol);
                                    }
                                } else {
                                    // Events - ON
                                    // Do Nothing
                                }
                            } else if(obj.Policy__r.Policy_Source__c == 'Object') {
                                // OnDemand - ON
                                if(quesOffSet.contains(beObjUUID)) {
                                    // Close the existing OnDemand policy
                                    Policy__c pol = PolicyUtility.closePolicy(obj.Policy__c, false);
                                    policyList.add(pol);
                                }
                            } else if(obj.Policy__r.Policy_Source__c == 'Event') {
                                // OnDemand - ON, Event - ON
                                // Do Nothing
                            }
                        } else if(sackWrapper.state.equalsIgnoreCase('Partly Insured')) {
                            
                            // Bag - OFF
                            // OnDemand ON/OFF or Events ON/OFF - Do Nothing
                        }
                        
                        deleteList.add(ro);
                    }
                }
                
                if(deleteList.size() > 0) {
                    try {
                        delete deleteList;
                    } catch(Exception e) {
                        // Not able to delete Event Objects
                        Utility.sendException(e, 'ERROR removing rucksack objects'); 
                    }
                }

                try {
                    // Create/Update the policies
                    upsert policyList;

                    // Update the Objects IsInsuredFlag
                    for(Policy__c pol: policyList) {
                        Object__c obj = new Object__c(Id = pol.Object__c);
                        if(pol.Active__c) { 
                            obj.Policy__c = pol.Id; 
                            obj.Is_Insured__c = true;
                            obj.Insurance_Start_Date__c = pol.Invoice_Start_date__c;
                            obj.Coverage_Type__c = pol.Insurance_Type__c;
                        } else {
                            //obj.Policy__c = null;
                            obj.Is_Insured__c = false;
                        }
                        objList.add(obj);
                    }

                    if(objList.size() > 0) {
                        try {
                            update objList;
                        } catch (Exception oe) {
                            Utility.sendException(oe, 'FAILED IN UPDATING Objects Insurance Flag from updateRecords');
                        }
                    }
                } catch (Exception policyException) {
                    Utility.sendException(policyException, 'FAILED IN UPDATING Policies from updateRecords');
                }
            }
            
            try {
                update sackObject;      
                updateWrapperList.add(sackWrapper);
                jsonGen.writeStringField('status', 'success');
            } catch(Exception ee) {
                jsonGen.writeStringField('status', 'failure');
                jsonGen.writeStringField('message', ee.getStackTraceString()); 
                utility.sendException(ee, 'SACK UPDATE FAILED'+ee.getStackTraceString());
            }		
        }
        jsonGen.writeObjectField('rucksackList', updateWrapperList);
        return jsonGen.getAsString(); 
    }
    
    //deleteRecords
    public String deleteRecords(String inputJSON) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        
        try {
            //Re-Order the Bag after deletion -- 04SEPT2019.
            List<Rucksack__c> sackList = [SELECT id, name, Bag_IsDelete__c, Contact__c, Sort_Order__c FROM Rucksack__c 
                                              where Contact__c = :this.ContactId AND Bag_IsDelete__c = FALSE Order By Sort_Order__c];
            Decimal getOrder;
            List<Rucksack__c> bagList = new List<Rucksack__c>();
            for(Rucksack__c bag : sackList){
                
                if(bag.Id == inputJSON){  
                    //Soft delete for the bag.
                    bag.Bag_IsDelete__c = True;
                    getOrder = bag.Sort_Order__c;
                    bag.Sort_Order__c = 0;  
                }     
                //Decrement the sack order from the deleted sack.
                if(bag.Sort_Order__c > getOrder){
                  bag.Sort_Order__c = bag.Sort_Order__c - 1; 
                }
                bagList.add(bag);
            }
            update bagList;
            jsonGen.writeStringField('status', 'success');
        } catch (Exception e) {
            jsonGen.writeStringField('status', 'failure');
            jsonGen.writeStringField('message', e.getMessage());
        }
        
        jsonGen.writeObjectField('user', UserInfo.getUserName());
        jsonGen.writeEndObject();
        
        return jsonGen.getAsString(); 
    }
}