public without sharing class RVGController {
    @testvisible public Id lingsObjectId = null;
    public object__c lingsObject { get; set; }
    public List<Event_Object__c> eveObjList { get; set; }
    public List<Event__c> eveList { get; set; }
    public String date1 { get; set; }
    public String conFirstName { get; set; }
    public String conLastName { get; set; }
    public String conStreet { get; set; }
    public String conPostalCode { get; set; }
    public String conHouseNumber { get; set; }
    public String conCity { get; set; }
    public String insuranceStartDate { get; set; }   
    public String insuranceEndDate { get; set; }   
    public String numberOfDaysInsured { get; set; }
    public Integer noOfDays { get; set; }
    public Decimal RVGTotalPre { get; set; }
    public String RVGPolNum { get; set; }
    
    public RVGController(ApexPages.StandardController controller){
        try{
            
            String dob = '';
            

            lingsObjectId = controller.getId(); 
            //lingsObjectId = 'a081w00000280d8AAA';
            System.debug('Inside the Controller class and the controller id is = '+lingsObjectId);
            
            lingsObject = [select id,OKKTotalPremium__c,OKKEventEndDate__c,RVGPolicyNumber__c,OKKEventStartDate__c,IsRVGEventEnds__c,
                              RVGEventEndDate__c,RVGEventStartDate__c,RVGTotalPremium__c,DisplayURL__c,Customer__r.name,
                              Customer__r.firstname,Customer__r.lastname,customer__r.Birthdate,customer__r.Street__c,
                              customer__r.House_Number__c,customer__r.Post_Code__c,customer__r.Payment_Info__c,
                              customer__r.Place__c,Insurance_Start_Date__c,Insurance_End_Date__c,
                           	  Policy__c from object__c where Id =:lingsObjectId limit 1];
            System.debug('Object record detail = '+lingsObject);
            
            eveObjList = [select id,name,Object__c,Event__c from Event_Object__c where Object__c=:lingsObjectId];
            eveList = [select id,name,start_date__c,end_date__c,createddate from Event__c where id=:eveObjList[0].Event__c];
            
            
            Date d = System.today();
            
            //Reva -- To validate whether Birthdate is null or not to avoid null error
            if(String.valueOf(lingsObject.customer__r.Birthdate) != null && String.valueOf(lingsObject.customer__r.Birthdate) != '' ){             
                d = lingsObject.customer__r.Birthdate;
                System.debug('Event StartDate = '+d);
                dob = DateTime.newInstance(d.year(),d.month(),d.day()).format('dd.MM.YYYY');
                date1 = dob;
            }   
            conFirstName = lingsObject.customer__r.firstname;
            System.debug('ContactFirstName =  '+conFirstName);
            conLastName = lingsObject.customer__r.lastname;
            conStreet = lingsObject.customer__r.Street__c;
            conPostalCode = lingsObject.customer__r.Post_Code__c;
            conHouseNumber = lingsObject.Customer__r.House_Number__c;
            conCity = lingsObject.Customer__r.Place__c;
            RVGPolNum = lingsObject.RVGPolicyNumber__c;
            
            
            
            string insurancestart = string.valueof(lingsObject.Insurance_Start_Date__c);
             //Reva - To also correct the format of InsuranceStartDate field when it is not null
            if(insurancestart  != '' && insurancestart != null){
                Date temp = lingsObject.Insurance_Start_Date__c;
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
                insurancestart = string.valueof(dt);
            }
            else if(insurancestart == null || insurancestart == ''){
                Date temp = eveList[0].start_date__c;
                System.debug('Event StartDate = '+d);
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
            	insurancestart = string.valueof(dt);
            }
            insuranceStartDate = insurancestart;
            
            string insuranceend = string.valueof(lingsObject.Insurance_End_Date__c);
             //Reva - To also correct the format of InsuranceEndDate field when it is not null
            if(insuranceend  != '' && insuranceend != null){
                Date temp = lingsObject.Insurance_End_Date__c;
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
                insuranceend = string.valueof(dt);
            }
            else if(insuranceend == null || insuranceend == ''){
                Date temp = eveList[0].end_date__c;
                System.debug('Event StartDate = '+d);
                String dt = DateTime.newInstance(temp.year(),temp.month(),temp.day()).format('dd.MM.YYYY');
            	insuranceend = string.valueof(dt);
            }
            insuranceEndDate = insuranceend; 
            
            noOfDays = eveList[0].start_date__c.daysBetween(eveList[0].end_date__c)+1;
            if(noOfDays > 1){
            	numberOfDaysInsured = '('+noOfDays+' Tage)';
            }else if(noOfDays == 1){
                numberOfDaysInsured = '('+noOfDays+' Tag)';
            }
            
            RVGTotalPre = lingsObject.RVGTotalPremium__c;
            RVGTotalPre = RVGTotalPre.setScale(2);
            
        }
        catch(Exception error){
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {'revathy@rangerinc.cloud'});
            mail.setSubject('RVGPdf Error'); 
            String body ='Cause=>'+ error.getCause() +', ' +'getMessage=>'+ error.getMessage()+', '+'getStackTraceString=>'+error.getStackTraceString()+', '+'getTypeName=>'+error.getTypeName();     
            mail.setHtmlBody(body);  
            mails.add(mail);
            Messaging.sendEmail(mails);
        }
    }
    
}