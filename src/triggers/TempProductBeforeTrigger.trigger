trigger TempProductBeforeTrigger on Product2 (before update){
    /* Temporary Product Update Trigger */ 
    
    if(Trigger.isUpdate) { 
        for (Product2 prod: Trigger.new) {
            // Insured Value and Old Price are same for new records
            prod.Insured_Value__c = prod.Price__c;
            prod.Old_Price__c = prod.Price__c;
            date CreateDate = date.valueof(prod.CreatedDate);
            
            if(prod.Price__c > 0 && String.isNotBlank(prod.Product_Type__c)) {
                Premium__c premium = Premium__c.getInstance(prod.Product_Type__c.toLowerCase());
                prod.OnDemand_Premium__c = (prod.Insured_Value__c * (premium.On_Demand__c / 100)) / 365;
                prod.OnDemand_Premium__c = Utility.getRoundedPremium(prod.OnDemand_Premium__c);         // LM399 - MR - 22APR19 - Removed setScale
                
                prod.Flatrate_Premium__c = ((prod.Insured_Value__c * (premium.FlatRate__c / 100)) / 365).setScale(3); 

                //if(prod.DisplayUrl == null) { prod.DisplayUrl = premium.ImageUrl__c; }
            }
        }
    }
}