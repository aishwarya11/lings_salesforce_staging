trigger DB2AfterTrigger on DB_Fotichaeschtli__c (after insert, after update) {
    //Writing trigger to insert new records or update old records from DB2 to PD2
    //List<DB_Fotichaeschtli__c> DB1List = [Select Id, Name, EAN__c, Link__c, Price__c, Title__c, ID__c, Manufacturer_Parts_Number__c, Brand__c, Description__c from DB_Fotichaeschtli__c];
    List<Product2> PD2List = New List<Product2>(); 
    // code to update PD2 once new data inserted
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isInsert) { 
        for(DB_Fotichaeschtli__c DB2 : trigger.new){
            PD2List.add(Utility.convertToProductDB2(DB2, new Product2()));
        }
    }
    
    // code to update PD2 once old data updated  
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isUpdate) {
        // Get Existing Product info
        Map<String, Product2> eProducts = new Map<String, Product2>();
        // Error occurs in the below query while changing values for a record in DB2
        for(Product2 prod: Database.query('Select id, ProductCode, EAN_Code__c, Price__c, Old_Price__c, DisplayUrl, Searchable__c, Product_type__c, Insured_Value__c, Name, Manufacturer_Number__c, Manufacturer__c, Description from product2')) {
            eProducts.put(prod.ProductCode, prod);
        }
        
        for(DB_Fotichaeschtli__c DB2 : trigger.new) {
            DB_Fotichaeschtli__c oldValue = Trigger.oldMap.get(db2.id);
            if(oldValue.Description__c != DB2.Description__c ||
               oldValue.Brand__c != DB2.Brand__c ||
               oldValue.Price__c != DB2.Price__c ||
               oldValue.EAN__c != DB2.EAN__c ||
               oldValue.Image_URL__c != DB2.Image_URL__c ||
               oldValue.Product_Type__c != DB2.Product_Type__c ||
               oldValue.Manufacturer_Parts_Number__c != DB2.Manufacturer_Parts_Number__c ||
               oldValue.Title__c != DB2.Title__c) {
                   Product2 PDupdated = Utility.convertToProductDB2(DB2, eProducts.get(DB2.ID__c));
                   PDupdated.Old_Price__c = oldValue.Price__c;
                   PD2List.add(PDupdated);
               }
        }
    } 
    upsert PD2List;
}