trigger ContactBeforeTrigger on Contact (before update) {
    for(Contact con : trigger.new) {
        //LM-1326--Not making Category sort as null when there is still object present --06APR2020
        if (con.Sort_Category_Order__c == null || con.Sort_Category_Order__c == '') {
            //To check originally all objects are removed or not
            List<Object__c> obj = [SELECT ID, Name, Is_deleted__c FROM Object__c WHERE Customer__c = :con.Id AND Is_deleted__c = FALSE];
            if (obj.size() > 0) {
                Contact oldValue = Trigger.oldMap.get(con.id);
                con.Sort_Category_Order__c = oldValue.Sort_Category_Order__c;				                
            }
        }
    }
}