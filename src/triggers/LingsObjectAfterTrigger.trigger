trigger LingsObjectAfterTrigger on Object__c (after update,after insert) {
    // MR - 12AUG19 - Trigger added for updating Rucksacks on Object Is Insured Flag updates
    if(Trigger.isUpdate){
        if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c) {
            Map<Id, Boolean> objInsuranceFlagMap = new Map<Id,Boolean>();
            for(Object__c obj: Trigger.New) {
                if(obj.Is_Insured__c != Trigger.oldMap.get(obj.Id).Is_Insured__c) {
                    objInsuranceFlagMap.put(obj.Id, obj.Is_Insured__c);
                }
                // To send the Cancellation Email for OKK object
                // sendRVGPolicyPdf
                if(obj.Is_Deleted__c != Trigger.oldMap.get(obj.Id).Is_Deleted__c && obj.Is_Deleted__c == true && 
                   obj.Product__c != null && (obj.Product__c == System.label.OKKProductId ||
                                              obj.Product__c == System.label.RVGProductId)){//Reva - To send the mail and delete the OKK and RVG Event
                                                  System.debug('Inside the LingsObject Trigger send Email method');
                                                  UserPolicy.sendOKKPolicyPdf(obj.Id, obj.Customer__c,'CancelPDF');
                                              }
            }
            
            ObjectUtility.updateRucksackObjects(objInsuranceFlagMap);
        } 
    }
    
    // To send the Insurance Confirmation mail for OKK object 
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isInsert){
        for (Object__c obj : Trigger.new) {
            System.debug('Inside the LingsObject Trigger send Email method');
            if(obj.Product__c != null && (obj.Product__c == System.label.OKKProductId ||
                                          obj.Product__c == System.label.RVGProductId)){//Reva - To send the mail for the OKK and RVG Event
                                              UserPolicy.sendOKKPolicyPdf(obj.Id, obj.Customer__c,'CreatePDF');
                                          }
        }
    }
}