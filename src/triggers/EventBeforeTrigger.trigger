trigger EventBeforeTrigger on Event__c (before insert) {

    List<Event__c> eveList = new List<Event__c>();
    String externalId = '';
    Event__c newEve = new Event__c();
    for (Event__c eve: Trigger.new) {
        //String comment = 'Created Event with OKK'+eve.External_ID__c;
        if(eve.Comments__c != null && eve.Comments__c.startsWith('Created Event with OKK')) {
            newEve = new Event__c();
            newEve.Name = eve.Name;
            newEve.Start_Date__c = eve.Start_Date__c;
            newEve.End_Date__c = eve.End_Date__c;
            newEve.Contact__c = eve.Contact__c;
            newEve.Comments__c = eve.External_ID__c;
            externalId = eve.External_ID__c;
            newEve.IsOKKObjPresent__c = true;            
            eveList.add(newEve);
            utility.createEvent(newEve, externalId);       
        }
    }    
    
    //assign the inserted event id 
    if(eveList.size() > 0) {
     	String queryString = 'SELECT ID, Name, OKKChildEventId__c, Comments__c FROM Event__c WHERE Comments__c=  \'' + externalId + '\'';
        Event__c updEve = Database.query(queryString);
        for(Event__c eve2 : Trigger.new) {
    		//Event__c eve2 = new Event__c(Id = newEve.Id);
            //eve2.OKKChildEventId__c = newEve.Id;
            eve2.OKKChildEventId__c = updEve.Id;
        }
    }
        //eve.OKKChildEventId__c = updEve.Id; 
   }