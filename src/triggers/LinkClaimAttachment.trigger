trigger LinkClaimAttachment on Claim__c (after insert) {
    for(Claim__c clm: trigger.new) {
        string claimId = clm.Id;
        if(clm.Attachment_Id__c != NULL) {
            List<String> lstString = new List<String>();
            List<ContentDocumentLink> cdList= new List<ContentDocumentLink>();
            lstString = clm.Attachment_Id__c.split(',');
            for(String cDocLink: lstString) {
                //Insert ContentDocumentLink
                if(cDocLink != null) {
                    ContentDocumentLink cDoc = new ContentDocumentLink();
                    cDoc.ContentDocumentId = cDocLink;              //Add ContentDocumentId
                    cDoc.LinkedEntityId = claimId;  //Add attachment parentId
                    cDoc.ShareType = 'I';                       //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
                    //cDocLink.Visibility = 'AllUsers';          //AllUsers, InternalUsers, SharedUsers   
                    cdList.add(cDoc);                                
                } 
            }
            /*//Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = clm.Attachment_Id__c;              //Add ContentDocumentId
            cDocLink.LinkedEntityId = clm.Id;  //Add attachment parentId
            cDocLink.ShareType = 'I';                       //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            //cDocLink.Visibility = 'AllUsers';          //AllUsers, InternalUsers, SharedUsers*/
            if(cdList.size() > 0) {
                insert cdList;
            }        
        }
        
        //utility.claimObjectMap(clm.Id, clm.Object__c);
    }	    
}