trigger DB1AfterTrigger on DB_Brack__c (after insert, after update) {
    
    /* Writing trigger to insert new records or update old records from DB1 to PD2*/ 
    //List<DB_Brack__c> DB1List = [Select Id, Name, EAN_Code__c, WWW_Link__c, UVP__c, Designation__c, Item_Number_2__c, Manufacturer_Number__c, Manufacturer__c, Web_Text__c from DB_Brack__c];
    List<Product2> PD2List = New List<Product2>();
    // code to update PD2 once new data inserted
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isInsert) {
        for(DB_Brack__c DB1 : trigger.new){
            PD2List.add(Utility.convertToProductDB1(DB1, new Product2()));
        }
    }

    // code to update PD2 once old data updated      
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isUpdate) {
        // Optimized to reduce reading all product records
        Set<String> productCodes = new Set<String>();
        for(DB_Brack__c DB1 : trigger.new) {
            productCodes.add(DB1.Item_Number_2__c);
        }

        // Get Existing Product info
        Map<String, Product2> eProducts = new Map<String, Product2>();
        for(Product2 prod: Database.query('Select id, ProductCode, EAN_Code__c, Price__c, Old_Price__c, DisplayUrl, Searchable__c, Product_Type__c, Insured_Value__c, Name, Manufacturer_Number__c, Manufacturer__c, Description from product2 where productCode in :productCodes')) {
            eProducts.put(prod.ProductCode, prod);
        }
        
        for(DB_Brack__c DB1 : trigger.new) {
            // 04JUL19 - Commented below conditions for updating product properly
            /*DB_Brack__c oldValue = Trigger.oldMap.get(db1.id);
            if(oldValue.Web_Text__c != DB1.Web_Text__c ||
               oldValue.Manufacturer__c != DB1.Manufacturer__c ||
               oldValue.UVP__c != DB1.UVP__c ||
               oldValue.EAN_Code__c != DB1.EAN_Code__c ||
               oldValue.WWW_Link__c != DB1.WWW_Link__c ||
               oldValue.Category_2__c != DB1.Category_2__c ||
               oldValue.Manufacturer_Number__c != DB1.Manufacturer_Number__c ||
               oldValue.Designation__c != DB1.Designation__c) {*/
               	PD2List.add(Utility.convertToProductDB1(DB1, eProducts.get(DB1.Item_Number_2__c)));
			//}
        }
    }
    
    upsert PD2List;
}