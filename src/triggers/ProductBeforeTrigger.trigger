trigger ProductBeforeTrigger on Product2 (before insert, before update) {
    /* Writing trigger to update 'Insured Value' field and Premium fields*/ 
    
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isInsert) { 
        for (Product2 prod: Trigger.new) {
            // Insured Value and Old Price are same for new records
            if(prod.Price_Customer__c > 0) {
                prod.Price__c = prod.Price_Customer__c;
            }
            prod.Insured_Value__c = prod.Price__c;
            prod.Old_Price__c = prod.Price__c;
            date CreateDate = date.valueof(prod.CreatedDate);
            
            if((prod.Price__c > 0 || prod.Price_Customer__c > 0) && String.isNotBlank(prod.Product_Type__c)) {
                Premium__c premium = Premium__c.getInstance(prod.Product_Type__c.toLowerCase());
                //23APR2019 - Null check  
                if(premium.On_Demand__c != null && premium.FlatRate__c != null && prod.Insured_Value__c != null){
                    //prod.OnDemand_Premium__c = (prod.Insured_Value__c * (premium.On_Demand__c / 100)) / 365;
                    prod.OnDemand_Premium__c = (prod.Insured_Value__c * (premium.On_Demand__c / 100));
                    prod.OnDemand_Premium__c = prod.OnDemand_Premium__c / 365;
                    prod.OnDemand_Premium__c = Utility.getRoundedPremium(prod.OnDemand_Premium__c);             // LM399 - MR - 22APR19 - Removed setScale                
                    
                    prod.Flatrate_Premium__c = ((prod.Insured_Value__c * (premium.FlatRate__c / 100)) / 365);   // LM399 - MR - 22APR19 - Removed setScale
                }
                // 01JUL19 Get default Image
                if(prod.DisplayUrl != null && Prod.DisplayUrl.StartsWith('https://') && (prod.DisplayUrl.contains('.jpg') || prod.DisplayUrl.contains('.png') || prod.DisplayUrl.contains('.svg') || prod.DisplayUrl.contains('jpeg') || prod.DisplayUrl.contains('lingscrm--c.documentforce.com/sfc/dist/version/'))){
                }else {
                    prod.DisplayUrl = premium.ImageUrl__c;
                }
            }
        }
    }
    
    if(Trigger.isUpdate) {
        for(Product2 prod: trigger.new) {
            Product2 oldValue = Trigger.oldMap.get(prod.id);
            date CreateDate = date.valueof(prod.CreatedDate);
            //LM-538--Update Price One time--22JULY2019.
             //LM-685--Cannot able to edit/ Create a bike/ e-bike brand, ignored the insured value calculation since it is Bike--20SEPT2019
            if(prod.Status__c != 'Closed' && prod.Price_Customer__c == prod.Price__c && !oldValue.Product_Type__c.equalsIgnorecase('bike') && !oldValue.Product_Type__c.equalsIgnorecase('E-bike')){ 
                // && prod.Price_Customer__c == prod.Insured_Value__c --Removed this condition check 20AUG2019                
                prod.Insured_Value__c = Utility.getRoundedPremium(prod.Insured_Value__c).setScale(2);
                prod.Price__c = Utility.getRoundedPremium(prod.Insured_Value__c);
                prod.Old_Price__c = Utility.getRoundedPremium(prod.Insured_Value__c);	//23AUG2019--To change Old Price value also as requested by Henrik.
                
                // Premium Values was not getting updated after changing the Price. -- 28AUG2019--LM-431.
                Premium__c premium = Premium__c.getInstance(prod.Product_Type__c.toLowerCase()); 
                
                if(premium.On_Demand__c != null && premium.FlatRate__c != null) {
                    prod.OnDemand_Premium__c = (prod.Insured_Value__c * (premium.On_Demand__c / 100)) / 365; 
                    prod.OnDemand_Premium__c = Utility.getRoundedPremium(prod.OnDemand_Premium__c.setScale(2));                  // LM399 - MR - 22APR19 - Removed setScale
                    
                    prod.Flatrate_Premium__c = ((prod.Insured_Value__c * (premium.FlatRate__c / 100)) / 365).setScale(3);                    
                }   //LM-685--Cannot able to edit/ Create a bike/ e-bike brand, ignored the insured value calculation since it is Bike--20SEPT2019
            } else if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && !oldValue.Product_Type__c.equalsIgnorecase('bike') && !oldValue.Product_Type__c.equalsIgnorecase('E-bike')) {        
                Premium__c premium = Premium__c.getInstance(prod.Product_Type__c.toLowerCase());
                Double newInsuredValue = Utility.getRoundedPremium(((prod.Price__c + oldValue.Price__c + oldValue.Old_Price__c) / 3).setScale(2));
                
                if((prod.Price__c > 0 && String.isNotBlank(prod.Product_Type__c)) &&
                   (oldValue.Product_Type__c != prod.Product_Type__c || oldValue.Price__c != prod.Price__c)) {
                       // Case 1: If price or product type is updated                        
                       //23APR2019 - Null check  
                       if(premium.On_Demand__c != null && premium.FlatRate__c != null && prod.Insured_Value__c != null) {                   
                           // Calculate the Insured Value on Price Change 
                           if(prod.Price__c != oldValue.Price__c) {
                               prod.Insured_Value__c = newInsuredValue; 
                               prod.Old_Price__c = oldValue.Price__c;
                           }
                           
                           prod.OnDemand_Premium__c = (prod.Insured_Value__c * (premium.On_Demand__c / 100)) / 365; 
                           prod.OnDemand_Premium__c = Utility.getRoundedPremium(prod.OnDemand_Premium__c.setScale(2));                  // LM399 - MR - 22APR19 - Removed setScale
                           
                           prod.Flatrate_Premium__c = ((prod.Insured_Value__c * (premium.FlatRate__c / 100)) / 365).setScale(3);        // LM399 - MR - 22APR19 - Removed setScale
                       }
                       //07MAY2019 -- Price History Tracking with/without Price Change of the Product
                   } else if(trigger.new.size() == 1 && 
                             (prod.DisplayUrl != oldValue.DisplayUrl || 
                              prod.Status__c != oldValue.Status__c || 
                              prod.Comment__c != oldValue.Comment__c || 
                              prod.IsDeleted__c != oldValue.IsDeleted__c ||
                              prod.Searchable__c != oldValue.Searchable__c || 
                              prod.Price_Online__c != oldValue.Price_Online__c ||
                              prod.Price_Customer__c != oldValue.Price_Customer__c ||
                              prod.Manufacturer_Number__c != oldValue.Manufacturer_Number__c || 
                              prod.Weight__c != oldValue.Weight__c ||
                              prod.Description != oldValue.Description || 
                              prod.Name != oldValue.Name)) {
                                  // Case 2: If Product is updated for non-price changes and its a single record
                                  // No Insured Value Update is Needed for Price History Tracking
                              } else if(newInsuredValue > 0 && 
                                        prod.Insured_Value__c != newInsuredValue &&
                                        (prod.DB_Brack__c != null || prod.DB_Fotichaeschtli__c != null)) {
                                            // Case 3: If the insured value is changed - MR - 31JUL19 
                                            prod.Insured_Value__c = newInsuredValue;
                                            prod.Old_Price__c = oldValue.Price__c;
                                            
                                            if(premium.On_Demand__c != null && premium.FlatRate__c != null && prod.Insured_Value__c != null) {
                                                prod.OnDemand_Premium__c = (prod.Insured_Value__c * (premium.On_Demand__c / 100)) / 365; 
                                                prod.OnDemand_Premium__c = Utility.getRoundedPremium(prod.OnDemand_Premium__c.setScale(2)); 
                                                
                                                prod.Flatrate_Premium__c = ((prod.Insured_Value__c * (premium.FlatRate__c / 100)) / 365).setScale(3);        
                                            }
                                        }
            } 
        }
    }
}