trigger ObjectBeforeTrigger on Object__c (before update,before delete) {
    //10APR2019 Updates the Object Value when the Product Changes.   
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c && Trigger.isUpdate) {
        String category = '';
        Set<Id> proId = new Set<Id>();
        Object__c oldValue = new Object__c();
        Object__c newobj = new Object__c();
        Map<String, String> objPolMap = new Map<String, String>();
        for(Object__c obj : trigger.new){
            newobj = obj;
            oldValue = Trigger.oldMap.get(obj.Id);
            proId.add(obj.Product__c);
            
            if((obj.Coverage_Type__c != oldValue.Coverage_Type__c || (obj.Policy__c == null && obj.Coverage_Type__c != 'NA' )) 
               && obj.Comments__c.contains('local_')) {
                   objPolMap.put(obj.Id, 'local_' + newobj.Comments__c.substringAfter('local_'));
               }
        }
        // MR - 30NOV19 - Changes made to handle Policy update for the Objects from Mobile App
        if(objPolMap.size() > 0) {
            Map<String, String> polIdMap = new Map<String, String>();
            // Read the Policies
            for(Policy__c pol: [Select Id, External_Id__c from Policy__c where External_Id__c in :objPolMap.values()]) {
                polIdMap.put(pol.External_Id__c, pol.Id);    
            }
            
            if(polIdMap.size() > 0) {
                for(Object__c obj : trigger.new) {
                    obj.Policy__c = polIdMap.get(objPolMap.get(obj.Id));
                }
            }
        }
        
        if(oldValue.Product__c != newobj.Product__c) { //Moved this condition before query to avoid unwanted query of object--22AUG2019
            for(Product2 pro : [Select id, Insured_value__c, OnDemand_Premium__c, Source__c,
                                Flatrate_Premium__c,flatrate_savings__c,Status__c FROM Product2 WHERE ID IN : proId]){
                                    
                                    // 04JUN19 - Skipped Brand price updates to Object
                                    if(pro.Source__c != 'DB Bike') {
                                        newobj.Insured_value__c = pro.Insured_value__c;  
                                        newobj.Premium_OnDemand__c = pro.OnDemand_Premium__c;
                                        newobj.Premium_FlatRate__c = pro.Flatrate_Premium__c;
                                        newobj.FlatRate_Savings__c = pro.FlatRate_Savings__c;
                                    }
                                    
                                    //07MAY2019 - update the Product when the Lings Object is reconnected.
                                    if(!oldValue.Approved__c){
                                        utility.updateProduct(oldValue.Product__c); //09May2019 --outside the Loop. 
                                    }
                                    //}
                                }//LM-849--Draft bike creation, Premium value is changed on click of E-bike flag
        } else if (oldValue.E_bike_electric_support__c != newobj.E_bike_electric_support__c) {
            Decimal OndemandMonthly = 0.0;
            Decimal FlatrateMonthly = 0.0;
            
            if (newobj.E_bike_electric_support__c) {
                category = 'e-'+newobj.Category__c;
            } else {
                category = newobj.Category__c;
            }
            
            if(category != null) {
                Premium__c premium = Premium__c.getInstance(category.toLowerCase()); 
                
                if(premium.On_Demand__c != null && premium.FlatRate__c != null) {
                    newobj.Premium_OnDemand__c = (newobj.Insured_Value__c * (premium.On_Demand__c / 100)) / 365; 
                    newobj.Premium_OnDemand__c = Utility.getRoundedPremium(newobj.Premium_OnDemand__c.setScale(2));                  // LM399 - MR - 22APR19 - Removed setScale
                    
                    newobj.Premium_FlatRate__c = ((newobj.Insured_Value__c * (premium.FlatRate__c / 100)) / 365).setScale(3);  
                    //Assigning minimum value
                    if(newobj.Premium_OnDemand__c < 0.05) {
                        newobj.Premium_OnDemand__c = 0.05;
                    } else if (newobj.Premium_FlatRate__c < 0.05) {
                        newobj.Premium_FlatRate__c = 0.05;
                    }
                    
                    OndemandMonthly = newobj.Premium_OnDemand__c * Utility.numOfDays(null);
                    FlatrateMonthly = Utility.getRoundedPremium(newobj.Premium_FlatRate__c * Utility.numOfDays(null)); 
                    //12DEC2019--Added to avoid Math Exception.
                    if(OndemandMonthly <= 0.05) {
                        OndemandMonthly = 0.05;
                    } else if(FlatrateMonthly <= 0.05) { 
                        FlatrateMonthly = 0.05;
                    }
                    newobj.FlatRate_Savings__c = ((OndemandMonthly - FlatrateMonthly) / OndemandMonthly).setScale(2);
                }                     
            }
            
        } else if (oldValue.Insured_Value__c != newobj.Insured_Value__c && (newobj.Category__c == 'Bike' || newobj.Category__c == 'E-Bike')) { //LM-1254 --Correcting the Premium value based on the Insured value Changes --30MAR2020
            Decimal OndemandMonthly = 0.0;
            Decimal FlatrateMonthly = 0.0;
            
            Premium__c premium = Premium__c.getInstance(newobj.Category__c.toLowerCase());
            //23APR2019 - Null check  
            if(premium.On_Demand__c != null && premium.FlatRate__c != null && newobj.Insured_Value__c != null){
                newobj.Premium_OnDemand__c = (newobj.Insured_Value__c * (premium.On_Demand__c / 100));
                newobj.Premium_OnDemand__c = newobj.Premium_OnDemand__c / 365;
                newobj.Premium_OnDemand__c = Utility.getRoundedPremium(newobj.Premium_OnDemand__c.setScale(2));                
                
                newobj.Premium_FlatRate__c = ((newobj.Insured_Value__c * (premium.FlatRate__c / 100)) / 365).setScale(3);
                
                OndemandMonthly = newobj.Premium_OnDemand__c * Utility.numOfDays(null);
                FlatrateMonthly = Utility.getRoundedPremium(newobj.Premium_FlatRate__c * Utility.numOfDays(null)); 
                //12DEC2019--Added to avoid Math Exception.
                if(OndemandMonthly <= 0.05) {
                    OndemandMonthly = 0.05;
                } else if(FlatrateMonthly <= 0.05) { 
                    FlatrateMonthly = 0.05;
                }
                newobj.FlatRate_Savings__c = ((OndemandMonthly - FlatrateMonthly) / OndemandMonthly).setScale(2);
                
            }            
        }
    }
}