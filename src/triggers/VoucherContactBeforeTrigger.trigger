trigger VoucherContactBeforeTrigger on Voucher_Contact__c (before insert) {
    // Set for inserted Vouchers
    Set<Id> voucherIds = new Set<Id>();
    // Set for inserted Contact
    Set<Id> contactIds = new Set<Id>();
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c){
        // Get all Voucher Contact Combination
        for(Voucher_Contact__c vcInTrigger: Trigger.new) {
            voucherIds.add(vcInTrigger.Voucher__c);
            contactIds.add(vcInTrigger.Contact__c);
        }
        
        // VC Combined Id
        Set<String> VC_Combined_Ids = new Set<String>();
        for(Voucher_Contact__c vc: [SELECT Id, Voucher__c, Contact__c, VC_Id__c from Voucher_Contact__C    
                                    WHERE Voucher__c IN :voucherIds AND Contact__c IN :contactIds]) {
                                        VC_Combined_Ids.add(vc.Voucher__c + '' + vc.Contact__c);
                                    }
        // Check for existing Voucher Contact
        
        for(Voucher_Contact__c vcInTrigger: Trigger.new) {
            if(VC_Combined_Ids.contains(vcInTrigger.Voucher__c + '' + vcInTrigger.Contact__c) && vcInTrigger.Voucher__c != System.label.FriendRecruitFriendId) {
                vcInTrigger.addError('Code is Already used by you');
            }
        }
    }
}