trigger PolicyAfterTrigger on Policy__c (after insert) {
    // MR - 18AUG19 - LM-603 Fix - Remove the Object from Events and Rucksack
    if(!Migration_Settings__c.getInstance(UserInfo.getUserId()).Restrict_Triggers__c) {
        List<sObject> relatedList = new List<sObject>();
        Set<Id> objIds = new Set<Id>();
        for(Policy__c pol: Trigger.new) {
            if(pol.Insurance_Type__c.equalsIgnoreCase(Constants.FLATRATE_STRING)) {
                objIds.add(pol.Object__c);
            }
        }

        if(objIds.size() > 0) {
            for(Object__c obj: [Select Id, (Select Id from Object_Events__r), (Select Id from Object_Rucksacks__r) from Object__c where Id IN :objIds]) {
                for(Event_Object__c eObj: obj.Object_Events__r) {
                    Event_Object__c eObj_del = new Event_Object__c(Id = eObj.Id);
                    relatedList.add((SObject)eObj_del);
                }

                for(Rucksack_Object__c rsObj: obj.Object_Rucksacks__r) {
                    Rucksack_Object__c rsObj_del = new Rucksack_Object__c(Id = rsObj.Id);
                    relatedList.add((SObject)rsObj_del);
                }
            }
        }

        if(relatedList.size() > 0) {
            try {
                delete relatedList;
            } catch (Exception de) {
                Utility.sendException(de, 'Not able to delete all the related records');
            }
        }
    }
}