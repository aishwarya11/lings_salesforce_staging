({
    handleClick : function (cmp, event, helper) {
        var check = cmp.get("v.Con");        
        check.OwnerId = $A.get("$SObjectType.CurrentUser.Id");         
        // update more fields here
      
        var action = cmp.get("c.newUserRecruited");
        action.setParams({
            con: check,
            cId : cmp.get("v.recordId")
        }); 
		
      // set call back 
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert('update successful');
                $A.get('e.force:refreshView').fire();
            }
             else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
   }      
})