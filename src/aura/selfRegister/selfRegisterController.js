({
    initialize: function(component, event, helper) {
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap2}).fire();        
        component.set('v.extraFields', helper.getExtraFields(component, event, helper));
    },
    
    handleSelfRegister: function (component, event, helper) {
        var firstName = component.find("firstname");
        firstName.reportValidity();
        var lastName = component.find("lastname");
        lastName.reportValidity();
        var birthDay = component.find("Birthday");
        birthDay.reportValidity();
        var street = component.find("Street");
        street.reportValidity();
        var houseNumber = component.find("HouseNumber");
        houseNumber.reportValidity();
        var postCode = component.find("Postcode");
        postCode.reportValidity();
        var place = component.find("Place");
        place.reportValidity();
        var phone = component.find("phone");
		phone.reportValidity();        
        var email = component.find("email");
        email.reportValidity();
        var password = component.find("password");
        password.reportValidity();
        var confirmPassword = component.find("confirmPassword");
        confirmPassword.reportValidity();
        var radioGrp = component.find("radioGrp");
        radioGrp.reportValidity();
        var checkbox = component.find("checkbox");
        //checkbox.reportValidity();
        
           helper.handleSelfRegister(component, event, helper); 
    },
    
    setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if(startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
    
    setExpId: function (component, event, helper) {
        var expId = event.getParam('expid');
        if (expId) {
            component.set("v.expid", expId);
        }
        helper.setBrandingCookie(component, event, helper);
    },
    
    onKeyUp: function(component, event, helpler){
        //checks for "enter" key
        if (event.getParam('keyCode')===13) {
            helpler.handleSelfRegister(component, event, helpler);
        }
    },   
    
    navToLoginPage : function(cmp, event, helper) {
        let urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ "url" : '/s/login' });
        urlEvent.fire();

    } ,
    
   /*call dateUpdate function on onchange event on date field*/ 
    dateUpdate : function(component, event, helper) {
        
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
     // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
    // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
     var todayFormattedDate = yyyy+'-'+mm+'-'+dd;       
    },
    
    handleChange : function(component, event, helper) {
        var radiobutton = component.get("v.radiobutton");
        console.log('radiobutton',radiobutton);
    },    

    handleCustomValidation : function(component,event) {
        var inputValue = event.getSource().get("v.value");
		console.log(inputValue);
        if(inputValue <= 8) {
            return 'Das Passwort muss mindestens 8 und maximal 20 Zeichen lang sein mit mindestens einer Zahl. Folgende Zeichen sind erlaubt: a-z, A-Z, 0-9, !@#$%^&*()_+-=:<>/`~.?;:';
        } else if (inputValue == '' || inputValue == null) {
            return 'Das Passwort muss mindestens 8 und maximal 20 Zeichen lang sein mit mindestens einer Zahl. Folgende Zeichen sind erlaubt: a-z, A-Z, 0-9, !@#$%^&*()_+-=:<>/`~.?;:';
        }
    },
    
	 onCheck: function(cmp, evt) {         
		 var checkCmp = cmp.find("checkbox").get("v.value");
         console.log(checkCmp);
	 }    
})