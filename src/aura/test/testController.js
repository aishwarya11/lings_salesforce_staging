({
    handleValidation : function(cmp) {
        var allValid = cmp.find('validfield').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        if (allValid) {
            //Submit information on Server
        } else {
            alert('Please fill required and update all invalid form entries');
        }
    },
    handleCustomValidation : function(cmp,event) {
        var inputValue = event.getSource().get("v.value");
        var allValid = cmp.find('validfield').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        // is input valid text?
        if (inputValue.indexOf('@')>=0) {
            inputCmp.setCustomValidity("Please Don't include '@' in name");
        } else {
            inputCmp.setCustomValidity(""); // reset custom error message
        }
        inputCmp.reportValidity();
    }
})