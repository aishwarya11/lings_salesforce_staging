({
  checkapprove : function(component, event, helper) {
      
      var Object = component.get("v.Obj");
        Object.OwnerId = $A.get("$SObjectType.CurrentUser.Id");
      	//Object.Approved__c = "TRUE";
       // update more fields here
       
      var action = component.get("c.approveProduct");
          action.setParams({
            inpobj: Object,
            objId : component.get("v.recordId")
        });
      // set call back 
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
               // alert('update successful');
                $A.get('e.force:refreshView').fire();
            }
             else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
   }
})