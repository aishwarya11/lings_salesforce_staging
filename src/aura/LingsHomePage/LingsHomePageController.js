({
    navToLoginPage : function(cmp, event, helper) {
        let urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ "url" : '/s/login' });
        urlEvent.fire();

    }, 
    navToRegisterPage : function(cmp, event, helper) {
        let urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ "url" : '/s/login/SelfRegister' });
        urlEvent.fire();

    } 
})