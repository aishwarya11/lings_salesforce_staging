  var prevEleClicked = '';

  // Show an element
  var show = function (elem) {

    // Get the natural height of the element
    var getHeight = function () {
      elem.style.display = 'block'; // Make it visible
      var height = elem.scrollHeight + 'px'; // Get it's height
      elem.style.display = ''; //  Hide it again
      return height;
    };

    var height = getHeight(); // Get the natural height
    elem.classList.add('is-visible'); // Make the element visible
    elem.style.height = height; // Update the max-height

    // Once the transition is complete, remove the inline max-height so the content can scale responsively
    window.setTimeout(function () {
      elem.style.height = '';
    }, 350);

    if (prevEleClicked != elem) {
        hide(prevEleClicked);
    }
    prevEleClicked = elem;
  };

// Hide an element
  var hide = function (elem) {

    if (elem == '') {
      return;
    }

    // Give the element a height to change from
    elem.style.height = elem.scrollHeight + 'px';

    // Set the height back to 0
    window.setTimeout(function () {
      elem.style.height = '0';
    }, 1);

    // When the transition is complete, hide it
    window.setTimeout(function () {
      elem.classList.remove('is-visible');
    }, 350);

  };

// Toggle element visibility
  var toggle = function (elem, timing) {

    // If the element is visible, hide it
    if (elem.classList.contains('is-visible')) {
      hide(elem);
      return;
    }

    // Otherwise, show it
    show(elem);

  };

// Listen for click events
  document.addEventListener('click', function (event) {

    // Make sure clicked element is our toggle
    if (!event.target.classList.contains('accordion-title')) return;

    // Prevent default link behavior
    event.preventDefault();

    // Get the content
    var content = document.querySelector(event.target.hash);
    if (!content) return;

    // Toggle the content
    toggle(content);

  }, false);

   function navigateToWorkFlow() { //300519 - LM- 403 - in mobile screen clicked workflow from other page
        if (window.location.hash === '#/'){
             $('html, body').animate({
                 scrollTop: $("#homepage-slider").offset().top
             }, 1000);
        } else {
            window.open(window.location.protocol + '//' + window.location.host + '/#workflow', '_self');
            window.location.reload();
        }
   }

   function navigateToAssure() { //300519 - LM- 403 - in mobile screen clicked assure from other page
        if (window.location.hash === '#/'){
             $('html, body').animate({
                 scrollTop: $("#searching").offset().top
             }, 1000);
        } else {
            window.open(window.location.protocol + '//' + window.location.host + '/#assure', '_self');
            window.location.reload();
        }
   }

   function navigateToFaq() { //300519 - LM- 403 - in mobile screen clicked faq from other page
        if (window.location.hash === '#/'){
             $('html, body').animate({
                 scrollTop: $("#faq").offset().top
             }, 1000);
        } else {
            window.open(window.location.protocol + '//' + window.location.host + '/#faq', '_self');
            window.location.reload();
        }
   }

  function addSlick() {
    /**
     * Ambassadors Slider
     */
    const childElements = $('.images-wrap .images .influencer-block').length;
    if(childElements > 1) {
      $('.images-wrap .images').not('.slick-initialized').slick({
        slidesToShow: childElements === 2 ? 2 : 3,
        centerMode: false,
        autoplay: true,
        autoplaySpeed: 4000,
        variableWidth: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 2
            }
          },
          {
            breakpoint: 640,
            settings: {
              arrows: false,
              centerMode: true,
              slidesToShow: 1
            }
          }
        ]
      });
    }

    if (childElements === 2) {
      window.setTimeout(function() {
        const slickList = window.document.getElementsByClassName('slick-list draggable').item(0);
        slickList.setAttribute('style', 'width: 100%');

        const slickTrack = window.document.getElementsByClassName('slick-track').item(0);
        slickTrack.setAttribute('style', 'opacity: 1;width: 800px;transform: translate3d(0px, 0px, 0px);');

        const slickSlides = window.document.getElementsByClassName('slick-slide');
        for (let i = 0; i < slickSlides.length; i++) {
          slickSlides.item(i).setAttribute('style', 'width: 400px;');
        }
      }, 3000);
    }
  }

$(document).ready(function() {
    addSlick();
    /*$("#toFocus").click(function (){
        $("#searchEnterKey").focus();
    });*/
	$("#objectUpdateInfo").click(function() {
		//console.log('clicked');
	  $('html, body').animate({
		scrollTop: $("#goUp").offset().top
	  }, 1000);
	});
    var languageInput = localStorage.getItem('language');
    var hashLocation = '#/' + languageInput + '/foto';
    var hashLocation1 = '#/' + languageInput + '/velo';
    $(window).on('popstate', function() {
        if (location.hash === "#/" || location.hash === hashLocation || location.hash === hashLocation1) { //|| location.hash === "#/de/gutschein-einlosen" || location.hash === "#/de/meine-gegenstaende" || location.hash === "#/de/profil"
            location.reload(true);
        }
    });
    $("#daily").click(function (){
        $('html, body').animate({
            scrollTop: $("#searching").offset().top
        }, 1000);
    });
    $("#calculateDaily").click(function (){
        $('html, body').animate({
            scrollTop: $("#searching").offset().top
        }, 1000);
    });
    $("#mfnwsn-smooth-scroll").click(function (){
        $('html, body').animate({
            scrollTop: $("#community").offset().top
        }, 1000);
    });
    $("#04rbts-smooth-scroll").click(function (){
        $('html, body').animate({
            scrollTop: $("#searching").offset().top
        }, 1000);
    });
    $("#92wz8b-smooth-scroll").click(function (){
        $('html, body').animate({
            scrollTop: $("#searching").offset().top
        }, 1000);
    });
});
