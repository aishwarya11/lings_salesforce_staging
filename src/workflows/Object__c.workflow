<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Confirms_Insurance</fullName>
        <description>Confirms Insurance</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Order_confirmation</template>
    </alerts>
    <alerts>
        <fullName>Insurance_ended</fullName>
        <description>Insurance ended</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Insurance_disabled</template>
    </alerts>
</Workflow>
