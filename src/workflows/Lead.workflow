<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sent_email_to_the_participants</fullName>
        <description>Sent email to the participants</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>info@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Traillove_Email</template>
    </alerts>
    <rules>
        <fullName>Send Traillove Email</fullName>
        <actions>
            <name>Sent_email_to_the_participants</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>TRAILLOVE</value>
        </criteriaItems>
        <description>This rule is to sent mail for the traillove festival participants.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
