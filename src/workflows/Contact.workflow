<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Confirming_Registration_Process</fullName>
        <description>Confirming Registration Process</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Register_confirm</template>
    </alerts>
    <alerts>
        <fullName>Password_Resetting</fullName>
        <description>Password Resetting</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Password_reset</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email</fullName>
        <description>Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Register_confirmed</template>
    </alerts>
</Workflow>
