<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Balance_Update</fullName>
        <description>to update the voucher amount per user</description>
        <field>Balance__c</field>
        <formula>IF((Voucher__r.Id == $Label.FriendRecruitFriendId) || Voucher__r.Name == &apos;RecruitFriend&apos; ,0.0 , Voucher__r.Amount__c)</formula>
        <name>Balance Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Voucher_Contact_ID</fullName>
        <description>to get an unique as well as External ID</description>
        <field>VC_ID__c</field>
        <formula>Voucher__c &amp; &quot;:&quot; &amp; Contact__c</formula>
        <name>Voucher Contact ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Voucher Contact ID</fullName>
        <actions>
            <name>Balance_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>to get an unique ID to make it External ID.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
