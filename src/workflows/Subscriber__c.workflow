<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Newsletter_Subscription</fullName>
        <description>Newsletter Subscription</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>support@lings.ch</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lings_Email_Template/Newsletter_Registration</template>
    </alerts>
</Workflow>
